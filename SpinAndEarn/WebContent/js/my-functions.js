/**
 * 
 */
$.ajaxSetup({
	async : false
});
function getAgents() {
	var listItems = "<option selected='true' value='1'>Agents</option>')";

	$.getJSON('GetAgents', function(data) {
		$.each(data, function(index, agent) {
			listItems += "<option value='" + agent.agentId + "'>" + agent.name
					+ "</option>";
		})
	});
	return listItems;
}

function getpriority() {
	var listItems = "<option selected='true' value='1'>Select Priority</option>')";

	$.getJSON('GetPriority', function(data) {
		$.each(data, function(index, Priority) {
			listItems += "<option value='" + Priority.id + "'>" + Priority.priority
					+ "</option>";
		})
	});
	return listItems;
}

function getCompain() {
	var listItems = "<option selected='true' value='1'>Select Campaign</option>')";

	$.getJSON('GetCampain', function(data) {
		$.each(data, function(index, campain) {
			listItems += "<option value='" + campain.campainId + "'>" + campain.campainName
					+ "</option>";
		})
	});
	return listItems;
}

function getStage() {
	var listItems = "<option selected='true' value='1'>Select Stage</option>')";

	$.getJSON('GetStage', function(data) {
		$.each(data, function(index, stage) {
			listItems += "<option value='" + stage.stageid + "'>" + stage.stageName
					+ "</option>";
		})
	});
	return listItems;
}

function validate(evt) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  var regex = /[0-9]|\./;
	  if( !regex.test(key) ) {
	    theEvent.returnValue = false;
	    if(theEvent.preventDefault) theEvent.preventDefault();
	  }
	  
	}

(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);

function callJSONAjax(url,data,responceFunc){

	if(data!=""){
		data = "&"+data;
	}
	//alert(url+data);
	var l ="";
	try{
		var jqxhr =
		$.ajax({
			url: url,
			processData : false,
			type : "GET",
			data: data,
			beforeSend: function(HttpRequest) 
            {
            }
		})
		.done (function(data) {eval(responceFunc)(data);})
		.fail   (function()  {eval(responceFunc)("Error");});
	}
	catch(err){
		var jqxhr =
		parent.$.ajax({
			url: url,
			processData : false,
			type : "POST",
			data: data
		})
		.done (function(data) {eval(responceFunc)(data);  })
		.fail   (function()  {eval(responceFunc)("Error");});
	}
}


function sendSMS(templateId,mobile,url)
{
	$.ajax({
		url : url,
		type : 'POST',
		//dataType : "json",
		data :{templateId:templateId,mobileNo:mobile},
		success : function(data) {
			alert("Message Successfully Send.")
			
		},
		error : function(e) {
			console.log(e.message);
		}
	});
}

/* Sort Function */
function NASort(a, b) {    
     if (a.innerHTML == 'NA') {
         return 1;   
     }
     else if (b.innerHTML == 'NA') {
         return -1;   
     }       
     return (a.innerHTML > b.innerHTML) ? 1 : -1;
 };
