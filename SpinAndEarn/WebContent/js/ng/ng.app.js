/*
 * Unminified JS files are available after purchase
 */
var smartApp=angular.module("smartApp",["ngRoute","ngAnimate","ui.bootstrap","app.controllers","app.WordCounterDemo","app.ajaxDEMO","app.demoControllers","app.main","app.navigation","app.localize","app.activity","app.smartui"]);
smartApp.config(["$routeProvider","$provide",function(a,b){a.when("/",{redirectTo:"/finance/ManagerDashboard"}).when("/:page",{templateUrl:function(c){return"finance/"+c.page+".jsp"},controller:"PageViewController"}).when("/TicketDetail/:child*",{templateUrl:function(c){return"views/tables/TicketDetail.jsp?tid="+c.child},controller:"PageViewController"}).when("/:page/:child*",{templateUrl:function(c){return"views/"+c.page+"/"+c.child+".jsp"},controller:"PageViewController"}).otherwise({redirectTo:"/dashboard"});b.decorator("$log",["$delegate",function(d){function c(){c.info.apply(c,arguments)}angular.extend(c,d);return c}])}]);
//smartApp.run(["$rootScope","settings",function(a,b){b.currentLang=b.languages[0]}]);
smartApp.run(function ($rootScope, $location,$route, $timeout) {

    $rootScope.config = {};
    $rootScope.config.app_url = $location.url();
    $rootScope.config.app_path = $location.path();
    $rootScope.layout = {};
    $rootScope.layout.loading = false;

    $rootScope.$on('$routeChangeStart', function () {
       // console.log('$routeChangeStart');
       
       
        $("#content").html('<i class="fa fa-cog fa-spin fa-3x fa-fw margin-bottom" ng-hide="!layout.loading" ></i>');
     
          $rootScope.layout.loading = true;       
          
          if(grafint!=null){
        	  clearInterval(grafint);
          }
        
    });
    $rootScope.$on('$routeChangeSuccess', function () {
          $rootScope.layout.loading = false;        
    });
    $rootScope.$on('$routeChangeError', function () {

        //hide loading gif
       // alert('wtff');
        $rootScope.layout.loading = false;

    });
});