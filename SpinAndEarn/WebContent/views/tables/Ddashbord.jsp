<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

 <style>
 .morris-hover{position:absolute;z-index:1000}.morris-hover.morris-default-style{border-radius:10px;padding:6px;color:#666;background:rgba(255,255,255,0.8);border:solid 2px rgba(230,230,230,0.8);font-family:sans-serif;font-size:12px;text-align:center}.morris-hover.morris-default-style .morris-hover-row-label{font-weight:bold;margin:0.25em 0}
.morris-hover.morris-default-style .morris-hover-point{white-space:nowrap;margin:0.1em 0}
 
 
 
 </style>
 <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

 <!-- <link href="css/img/styles.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet" /> -->
</head>
<!-- <script src="js/chart.min.js"></script> -->
<!-- 	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script> -->
<script>
$.getScript('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',function(){
$.getScript('http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js',function(){

  
  Morris.Donut({
      element: 'line-example',
      data: [
             {label: "Inprocess", value: 55},
             {label: "Open", value: 55},
             {label: "Close", value: 55},
      		 {label: "Client Waiting", value: 45},
      		 {label: "No Reponce", value: 55}
      ],
     
      labelColor: '#14CFFD',
      colors: [
        '#12BEE8',
        '#14CFFD',   
        '#16E0FF',
        '#17F2FF'
      ],
    });
 
     Morris.Donut({
         element: 'donut-example',
         data: [
          {label: "Open", value: 45},
          {label: "Close", value: 55}
         ],
        
         labelColor: '#060',
         colors: [
           '#0BA462',
           '#39B580',
           '#67C69D',
           '#95D7BB'
         ],
       });
     
   
       Morris.Donut({
          element: 'donut-priority',
          data: [
           {label: "High", value: 12},
           {label: "Low", value: 30}
          ],
         
          labelColor: '#FD9494',
          colors: [
            '#FD9494',
            '#FDBDBE',
            '#67C69D',
            '#95D7BB'
          ],
        });
      
       
       
       
       Morris.Donut({
           element: 'bar-example1',
           data: [
            {label: "2010", value: 200},
            {label: "2011", value: 300},
            {label: "2012", value: 120},
            {label: "2013", value: 242},
            {label: "2014", value: 214},
            {label: "2015", value: 117}
           ],
          
           labelColor: '#FF7444',
           colors: [
             '#FF7444',
             '#FF7F4A',
             '#FF8950',
             '#FF8950',
             '#FF9F5D'
           ],
         });
      
       Morris.Bar({
      	 barSizeRatio:0.35,
           element: 'bar-example3',
           data: [
              {y: 'Category1', a: 100},
              {y: 'Category2', a: 75},
              {y: 'Category3', a: 50},
              {y: 'Category4', a: 75},
              {y: 'Category5', a: 50},
              {y: 'Category6', a: 75}
           ],
           xkey: 'y',
           ykeys: ['a'],
           labels: ['Lead'],
           barColors:['#4DA74D'],
           hideHover: 'auto'
        }); 
      
  	
         Morris.Bar({
        	 barSizeRatio:0.45,
          element: 'bar-example',
          data: [
             {y: 'Jan 2014', a: 100, b: 90},
             {y: 'Feb 2014', a: 75,  b: 65},
             {y: 'Mar 2014', a: 50,  b: 40},
             {y: 'Apr 2014', a: 75,  b: 65},
             {y: 'May 2014', a: 50,  b: 40},
             {y: 'Jun 2014', a: 75,  b: 65}
          ],
          xkey: 'y',
          ykeys: ['a', 'b'],
          labels: ['Success', 'Fail'],
          barColors: ['#0B62A4','#f75b68','#4DA74D','#646464'],
       }); 
         
         
         Morris.Bar({
        	 barSizeRatio:0.35,
             element: 'bar-example2',
             data: [
                {y: 'Category1', a: 100},
                {y: 'Category2', a: 75},
                {y: 'Category3', a: 50},
                {y: 'Category4', a: 75},
                {y: 'Category5', a: 50},
                {y: 'Category6', a: 75}
             ],
             xkey: 'y',
             ykeys: ['a'],
             labels: ['Lead'],
             barColors:['#4DA74D'],
             hideHover: 'auto'
          }); 
         
         
        
});
});

</script>
<body>

                            
              
               	<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Dashboard</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row" >
			<div class="col-xs-12 col-md-6 col-lg-3" style=""  >
				<div class="panel panel-blue panel-widget ">
					<div class="row no-padding" style=" border:2px solid; border-color:#30A5FF;    border-radius:5px;">
						<div class="col-sm-3 col-lg-5 widget-left">
							<!--  <i class="fa fa-th-list fa-4x" style=" padding-top:1px;padding-left:5px;"></i>  -->
							<div style=" padding-top:-90px;padding-left:5px;"><img alt="scsd" src="img/lead.png" ></div>
							<!-- <span class=" glyphicon glyphicon-th-list " aria-hidden="true"></span> -->
						</div>
						<div class="col-sm-9 col-lg-7 widget-right" style="border-radius: 0px;" >
							<div class="large">121</div>
							<div class="text-muted">Total Lead </div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-orange panel-widget">
					<div class="row no-padding" style=" border:2px solid; border-color:#FFB53E;    border-radius:5px;">
						<div class="col-sm-3 col-lg-5 widget-left">
						<div style=" padding-top:-90px;padding-left:5px;"><img alt="scsd" src= "img/unassign1.png"></div>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">52</div>
							<div class="text-muted">Unassign</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-teal panel-widget">
					<div class="row no-padding" style=" border:2px solid; border-color:#1EBFAE;    border-radius:5px;">
						<div class="col-sm-3 col-lg-5 widget-left">
							<div style=" padding-top:-2px;padding-left:5px;"><img alt="scsd" src="img/squares36.png" ></div>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">24</div>
							<div class="text-muted">Lead Category</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-red panel-widget">
					<div class="row no-padding" style=" border:2px solid; border-color:#F9243F;    border-radius:5px;">
						<div class="col-sm-3 col-lg-5 widget-left">
								<div style=" padding-top:-90px;padding-left:5px;"><img alt="scsd" src="img/bar24.png" ></div>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">25</div>
							<div class="text-muted">Lead Status</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->
                 <!-- /. ROW  -->
                <hr />                
               <div class="col-lg-12" >
				<div class="panel panel-default">
					<div class="panel-heading">Category Wise Lead Overview</div>
					<div class="panel-body">
						<!-- <div >
							<canvas class="main-chart" id="bar-example2" height="200" width="600"></canvas>
							<div id="bar-example2"> </div>
						</div> -->
						 <div class="container-fluid">
  
  <div class="row" >
    <div class="col-md-12">
        <div id="bar-example3"  ></div>
    </div>
   
  </div>
</div> 
					</div>
				</div>
				<br><br>
			</div>
			
			
			
		<!-- 	<hr />     -->   
		<div class="row" >         
               <div class="col-lg-12">
                   <div>                       
                    <div class="panel panel-primary text-center no-boder bg-color-green" style="float:left;width:20%;margin-left:50px;">
                        <div class="panel-body">
                          <img alt="scsd" src="img/my2.png" >
                            <div class="row">
   							 <h3 style="color: white;">20,000 </h3>
  </div>
                        </div>
                        <div class="panel-footer back-footer-green">
                         Ussign Telecaller
                            
                        </div>
                    </div>
                                             
                   <div class="panel panel-primary text-center no-boder bg-color-green bg-color-red" style="float:left;width:20%;margin-left:30px;">
                        <div class="panel-body">
                          <img alt="scsd" src="img/my.png" >
                            <div class="row">
   							 <h3 style="color: white;">20,000 </h3>
  </div>
                        </div>
                        <div class="panel-footer back-footer-red">
                         Unassign Telecaller
                            
                        </div>
                    </div>
                    
                    <div class="panel panel-primary text-center no-boder bg-color-green" style="float:left;width:20%;margin-left:30px;">
                        <div class="panel-body">
                          <img alt="scsd" src="img/usr.png" >
                            <div class="row">
   							 <h3 style="color: white;">20,000 </h3>
  </div>
                        </div>
                        <div class="panel-footer back-footer-green">
                        Total Staff
                            
                        </div>
                    </div>
                    
                    <div class="panel panel-primary text-center no-boder bg-color-green bg-color-red" style="float:left;width:20%;margin-left:30px;">
                        <div class="panel-body">
                          <img alt="scsd" src="img/tele.png" >
                            <div class="row">
   							 <h3 style="color: white;">20,000 </h3>
  </div>
                        </div>
                        <div class="panel-footer back-footer-red">
                        Total Telecaller
                            
                        </div>
                    </div>
                    
                    </div>
			
			</div>
              </div>
                 
                   
               
                 
                 <div class="row">
			
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Open-Close Lead</h4>
						<div id="donut-example"  > </div>
						<!-- <div class="easypiechart" id="easypiechart-red" data-percent="27" ><span class="percent">27%</span> -->
						</div>
					</div>
				</div>
				
				
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Priority</h4>
					<div id="donut-priority"  > </div>
						</div>
					</div>
				</div>
			
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Process State</h4>
						<div id="line-example"  > </div>
						</div>
					</div>
				</div>
			
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Customes</h4>
						<div id="bar-example1"> </div>
						<!-- <div class="easypiechart" id="easypiechart-red" data-percent="27" ><span class="percent">27%</span> -->
						</div>
					</div>
				</div>
		
		</div><!--/.row-->
	 <div class="col-lg-12" >
				<div class="panel panel-default">
					<div class="panel-heading">Category Wise Lead Overview</div>
					<div class="panel-body">
						<!-- <div >
							<canvas class="main-chart" id="bar-example2" height="200" width="600"></canvas>
							<div id="bar-example2"> </div>
						</div> -->
						 <div class="container-fluid">
  
  <div class="row" >
    <div class="col-md-12">
        <div id="bar-example"  ></div>
    </div>
   
  </div>
</div> 
					</div>
				</div>
				<br><br>
			</div>
              
</body>
</html>