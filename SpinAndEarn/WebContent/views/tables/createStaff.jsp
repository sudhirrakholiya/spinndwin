<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.sudhir.model.MediUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%--   <%@include file="/index.jsp"%>  --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<script src=js/jquery.js></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>
<script src=js/fnReloadAjax.js></script>

<style>
#validity{z-index:1151 !important;}
</style>
<script type="text/javascript">
var uid=null;
$(document).ready(function() {
	

   
//	alert('Ready...');
		$('body').on('hidden.bs.modal', '.modal', function() {
			$(this).removeData('bs.modal');
		});
	         dataTable = $('#staff-grid').DataTable( {
	            "processing": true,
	            "serverSide": true,
	            "ajax": "GetStaff",
	                "columnDefs": [ {
	                    "targets": -1,
	                    "data":null,
	                    "defaultContent": "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal2'><span class='glyphicon glyphicon-edit' aria-hidden='true' ></span>   Edit   </button>&nbsp;&nbsp;<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"
	                }],

	        } );
	         
	         $('#staff-grid tbody').on( 'click', '#edit-btn', function () {
	        	    var data = dataTable.row( $(this).parents('tr') ).data();
	        	 //   alert("'Edit is: "+ data[ 2 ] );
	        	    $('#eid').val(data[0]);
	        	    $('#euname').val(data[1]);
	        	    $('#ename').val(data[2]);
	        	    $('#ecity').val(data[3]);
	        	    $('#email').val(data[4]);
	        	    $('#emob').val(data[5]);
	        	} );
	         
	         
	         $('#staff-grid tbody').on( 'click', '#delete-btn', function () {
	        	    var data = dataTable.row( $(this).parents('tr') ).data();
	        	   // alert("'Edit is: "+ data[ 0 ] );
	        	   var id=data[0];
	        	    $.ajax({
	  	       	  	  url: "DeleteStaff",
	  	       	  	  type: "get",
	  	       	  	  data:{id:id},
	  	       	  	  success: function(html){
	  	       	  		alert(' Daat Successfully Deleted');	
	  	       	  		dataTable.ajax.reload();
	  	       	  	  }
	  	       	  	}); 	    
	         
	         } );
	       
	        } );  
	        
var uname=$("#myname").val();
$("#cname").val(uname);
	        function addStaff()
	        {
	        	var username=$('#uname').val();
	        	var email=$("#addeid").val();
	        	var password=$("#pass").val();
	        	var name=$("#fullname").val();
	        	var city=$("#cty").val();
	        	var mobile=$("#mob").val();
	        	
	        	
	        	 // alert($("#addeid").val())
	        	//alert(username+" "+email+" "+password+" "+name+" "+city+" "+mobile)
	        	   $.ajax({
	       	  	  url: "AddStaff",
	       	  	  type: "get",
	       	  	  data:{username:username,email:email,password:password,city:city,mobile:mobile,name:name},
	       	  	  success: function(html){
	       	  		alert(' Daat Successfully Added');	
	       	  		dataTable.ajax.reload();
	       	  	  }
	       	  	});   
	        }
	        
	        
function updateStaff()
{
	//alert('sfsf')
		 var id= $('#eid').val();
		 var username=  $('#euname').val();
		 var name=  $('#ename').val();
	     var city=   $('#ecity').val();
	     var email= $('#email').val();
	     var mobile=  $('#emob').val();
	   //  alert(id)
	 //   console.log(id+" "+username+" "+name+" "+city+" "+email+" "+mobile);
		 $.ajax({
      	  	  url: "UpdateStaff",
      	  	  type: "get",
      	  	  data:{id:id,username:username,email:email,city:city,mobile:mobile,name:name},
      	  	  success: function(html){
      	  		alert(' Daat Successfully Updated');	
      	  		dataTable.ajax.reload();
      	  	  }
      	  	}); 
}
</script>
</head>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser" data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Staff Manage</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger"   data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span> Add Staff</button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table id="staff-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>


						<tr role="row">
							<th>ID</th>
							<th>Username</th>
							<th>Name</th>
							<th>City</th>
							<th>Email</th>
							<th>Mobile No</th>
							<th>Register Date</th>
							<th>Action</th>
							
						</tr>
					</thead>


				</table>

			</div>

		</div>

	</div>


<!--Edit  -->
 
        
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:55%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Staff Detail</h4>
      </div>
      <div class="modal-body">
    
      
         <div class="row">
  <div class="col-xs-2 col-sm-2">Id</div>
  <div class="col-xs-2 col-sm-4"><input readonly="readonly" type="text" id="eid" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Username</div>
  <div class="col-xs-2 col-sm-4"><input type="text" id="euname" class="form-control"></div>
        </div>
        
        <hr>
        
         <div class="row">
  <div class="col-xs-2 col-sm-2">Name</div>
  <div class="col-xs-2 col-sm-4"><input type="text" id="ename" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">City</div>
  <div class="col-xs-2 col-sm-4" ><input  type="text" id="ecity" class="form-control"></div>
        </div>
       
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Email</div>
  <div class="col-xs-2 col-sm-4"><input type="text" id="email" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Mobile No.</div>
  <div class="col-xs-2 col-sm-4"><input type="text" id="emob" class="form-control"></div>
        </div>
        </div>
         <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="updateStaff();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
        </div></div></div>    




















<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:70%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New Staff</h4>
      </div>
      <div class="modal-body">
    
      
         <div class="row">
  <div class="col-xs-6 col-sm-3">Username</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="uname" class="form-control"></div>
  <div class="col-xs-6 col-sm-3">Email Id</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="addeid" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-3">Password</div>
  <div class="col-xs-6 col-sm-3"><input type="password" id="pass" class="form-control"></div>
  <div class="col-xs-6 col-sm-3">Confirm Password</div>
  <div class="col-xs-6 col-sm-3"><input type="password" id="cno" class="form-control"></div>
        </div>
        <hr>
         <%
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id=currentUser.getUserid();
		out.println("<input type='hidden' id='myname' value='"+currentUser.getUsername()+"'>");
        
        
        %>
         <div class="row">
  <div class="col-xs-6 col-sm-3">Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="fullname" class="form-control"></div>
  <div class="col-xs-6 col-sm-3">Company Name</div>
  <div class="col-xs-6 col-sm-3" ><input readonly="readonly" type="text" id="cname" class="form-control"></div>
        </div>
       
        <hr>
         <div class="row">
  <div class="col-xs-6 col-sm-3">City</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="cty" class="form-control"></div>
  <div class="col-xs-6 col-sm-3">Mobile No.</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="mob" class="form-control"></div>
        </div>
        </div>
         <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="addStaff();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save Data</button>
      </div>
        </div></div></div>        
        
        <!--Edit  -->
           
        
        
      



</html>
