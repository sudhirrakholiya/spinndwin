<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.sudhir.model.MediUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
#validity{z-index:1151 !important;}
</style>
  <script type="text/javascript"> 
        
var setcaval;
var leadId=null;
var lId=null
function addgetCat()
{
	$.ajax({
	  	  url: 'GetCategory',
	  	  type: 'GET',
	  	  data: 'compid='+uid,
	  	  success: function(data) {
	  		var msg=data;
	  		var html="<option>Select Category</option>";
	  		var i=0;
	  		for(i=0;i<msg.length;i++)
	  			{
	  			var val='"'+msg[i][0]+'"';
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#category').html("<select class='form-control' id='adcat'>"+html+"</select>");
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );
	
	leadId=null;
	}
function loadcatleadst()
{
	$.ajax({
	  	  url: 'GetCategory',
	  	  type: 'GET',
	  	  data: 'compid='+uid,
	  	  success: function(data) {
	  		var msg=data;
	  		var html="<option>Select Category</option>";
	  		var i=0;
	  		for(i=0;i<msg.length;i++)
	  			{
	  			var val='"'+msg[i][0]+'"';
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#category').html("<select class='form-control' id='cat'>"+html+"</select>");
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );
}

function setval(val)
{
	    var textToFind =val;
	    var dd = document.getElementById('cat');
	    for (var i = 0; i < dd.options.length; i++) {
	        if (dd.options[i].text === textToFind) {
	            dd.selectedIndex = i;
	            break;
	        }
	    }
	    var value = document.getElementById("cat").value;
	 
}

function setTallval(myvalue)
{
	 $.ajax({
		  url:'GetTellyCallerBycompanyId',
		  type: 'GET',
		  success: function(msg) {
			  var sthtml="<option value='0'>Unassign</option>";
			  for(i=0;i<msg.length;i++)
	  			{
	  			if(msg[i][0]==myvalue)
	  				sthtml+="<option selected value='"+msg[i][0]+"'>"+msg[i][2]+" "+msg[i][3]+"</option>";
	  				else
	  					sthtml+="<option value='"+msg[i][0]+"'>"+msg[i][2]+" "+msg[i][3]+"</option>";
	  			}
	  		$('#tcalleredit').html("<select class='form-control' id='tcIdEdit'>"+sthtml+"</select>");
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		});
}

var uid=null;
  $(document).ready(function() {
	  $('#schedule').datetimepicker({
		  format: 'YYYY-MM-DD HH:mm',
	  });
	  
	 uid=$('#uid').val();
	$.ajax({
  	  url: 'GetCategory',
  	  type: 'GET',
  	  data: 'compid='+uid,
  	  success: function(data) {
  		var msg=data;
  		var html="<option>Select Category</option>";
  		var i=0;
  		
  		for(i=0;i<msg.length;i++)
  			{
  			
  			var val='"'+msg[i][0]+'"';
  				html+="<option value="+val+">"+msg[i][1]+"</option>";
  			}
  		$('#catled').html("<select class='form-control' id='cat'>"+html+"</select>");
  		
  	
  		
  	  },
	 error: function(e) {
 		console.log(e.message);
 	  }
	} );
	
	$.ajax({
		  url:'GetTellyCallerBycompanyId',
		  type: 'GET',
		  success: function(msg) {
			  var sthtml="<option value='0'>Unassign</option>";
			  for(i=0;i<msg.length;i++)
	  			{
	  		sthtml+="<option value='"+msg[i][0]+"'>"+msg[i][2]+" "+msg[i][3]+"</option>";
	  			}
	  		$('#tcaller').html("<select class='form-control' id='tcId'>"+sthtml+"</select>");
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		});
	        } );  
  
  function setLeadProcessSate(proceesState)
  {
	 	$.ajax({
			  url: 'GetLeadState',
			  type: 'GET',
			  success: function(data) {  
				var msg=data;
		  		var html="<option>Lead State</option>";
		  		var i=0;
		  		for(i=0;i<data.length;i++)
		  			{
		  	 	if(data[i]==proceesState)
		  			 html+="<option selected='selected' value='"+data[i]+"'>"+data[i]+"</option>";	
		  		else
		  			html+="<option  value='"+data[i]+"'>"+data[i]+"</option>";
		  			}
		  		$('#lidst').html("<select class='form-control' id='lpstate'>"+html+"</select>"); 
			  },
			 error: function(e) {
				console.log(e.message);
			  }
			} );
  }

 var dataTable=null; 
var data1=null;
var dtable=null;
var dl=null;
function showfollowups()
{
	if(dtable!=null){
		 dl=leadId;
	    	dtable.ajax.reload();
	    	return;
	}
	 dl=leadId;
	 dtable= $('#hist-grid').DataTable( {
 	        "processing": true,
 	        "serverSide": true,
 	       "bPaginate": true,
 	        "ajax":{
 	            "url": "getFollowupsHistory",
 	           " type": "get",
 	           "data": function ( d ) {
 	        	   d.leadid=dl;
 	           }
 	        }, 
	"columnDefs": [ {
        "targets": -1,
        "data":null,
        "defaultContent": "<button id='play-btn' class='btn btn-success'>Audio File</button>"
    }]
	   } );
 	       $('#hist-grid tbody').on( 'click', '#play-btn', function () {
 			  var  data = dtable.row( $(this).parents('tr') ).data();
 		 	 var win = window.open('Sound/'+data[7]+''); 
 			} );
 	      dtable.on( 'order.dt search.dt', function () {
 	    	 dtable.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
 		    	     cell.innerHTML = i+1;
 		    	 } );
 		    	} ).draw();
}
var dataTable=null;
var data1=null;
var proceesState="";
var globalTag="";
function addTag()
{
	 $.ajax({
	  	  url: 'AddTag',
	  	  type: 'GET',
	  	  data:'tagName='+$("#eid").val(),
	  	  success: function() {
	  		 getAllTag()
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );
}

function getAllTag()
{
	 $.ajax({
	  	  url: 'getAllTag',
	  	  type: 'GET',
	  	  data:'',
	  	  success: function(data) {
	  		var tg="";
	  		for(var i=0;i<data.length;i++)
	  			{
	  			var s=data[i][1].split(" ").join("")
	  			tg+=' <div class="col-md-5"><label class="checkbox-inline" ><input  id="'+s+'" class="Checkbox" name="sport" type="checkbox" value="'+data[i][1]+'">'+data[i][1]+'</label></div>';
	  			}
	  		$('#ALLTag').html(tg);
			if(globalTag!=null)
	  		setTag(globalTag);
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );
		 
}
function addLeadTag()
{
	var tage="";
	 var favorite = [];
     $.each($("input[type='checkbox']:checked"), function(){            
         favorite.push($(this).val());
     });
     
     var Id=$("#tgcomid").val();
     $.ajax({
	  	  url: 'addTagToLead',
	  	  type: 'GET',
	  	data:{leadId:Id,tagList:favorite.join(", ")},
	  	  success: function(data) {
	  		$("#eid").val("");
	  		$("#TagModal").modal('hide');
	  		 dataTable.ajax.reload();
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );
}

function setTag(tt)
{
	var msg = tt.split(",");
  	for(var i=0;i<msg.length;i++)
		{
    	   $("#"+msg[i].split(" ").join("")+"").prop("checked", true);
		}
}
$(document).ready(function() {
	getAllTag();
	$('#schedule').attr('readonly', true);
	$('#vehicle').on('change', function(){ // on change of state
		   if(this.checked) // if changed state is "CHECKED"
		    {
			   $('#schedule').attr('readonly', true);
		    }
		   else
			   {
			   $('#schedule').attr('readonly', false);
			   }
		})
		
	var options = [];
	  $( '.dropdown-menu a' ).on( 'click', function( event ) {

	     var $target = $( event.currentTarget ),
	         val = $target.attr( 'data-value' ),
	         $inp = $target.find( 'input' ),
	         idx;

	     if ( ( idx = options.indexOf( val ) ) > -1 ) {
	        options.splice( idx, 1 );
	        setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
	     } else {
	        options.push( val );
	        setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
	     }

	     $( event.target ).blur();
	     return false;
	  });
	  
		$('body').on('hidden.bs.modal', '.modal', function() {
			$(this).removeData('bs.modal');
		});
	         dataTable = $('#cat-grid').DataTable( {
	            "processing": true,
	            "serverSide": false,
	            "bPaginate": true,
	            "ajax": "GetLead",
	 	          "order": [[ 0, 'desc' ]],
	            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	            	
	            	var tag='<button class="btn btn-sm btn-success" id="edit-btn" data-toggle="modal" data-target="#myModal" data-placement="bottom" title="Change Lead Data"><span class="glyphicon glyphicon-edit" aria-hidden="true" ></span></button>&nbsp;&nbsp;'
		            	 +'<button class="btn btn-sm btn-danger" id="delete-btn"><span class="glyphicon glyphicon-trash" aria-hidden="true" data-placement="bottom" title="Delete Lead"></span></button>&nbsp;&nbsp;'
  		            	 +' <button class="btn btn-sm btn-warning" id="view-btn" data-toggle="modal" data-target="#myModal3" ><span class="glyphicon glyphicon-list-alt" aria-hidden="true" data-placement="bottom" title="View Lead Data"></span></button>&nbsp;&nbsp;'
  		            	// +'<button class="btn btn-sm btn-info" id="tag-btn" data-toggle="modal" data-target="#TagModal" ><span class="glyphicon glyphicon-tags" aria-hidden="true" data-placement="bottom" title="Add Lead TAG"></span></button>&nbsp;&nbsp;'
  		            	// +'<button class="btn btn-sm btn-primary" id="comment-btn"  ><span class="glyphicon glyphicon-comment" aria-hidden="true" data-placement="bottom" title="'+aData[16]+'"></span></button><br>'
	            var tt=aData[17];
				if(tt!=null)
				{
  		            	var msg = tt.split(",");
  		            	for(var i=0;i<msg.length;i++)
		  				{
		  				if(msg.length!=0)
		  					{
		  					//tag=tag.concat("<span class='label label-danger'>"+msg[i]+"</span>&nbsp;");
		  					}
		  				}
				}
				console.log(aData[7])
				var dd=aData[7];
				if(dd=="Jun 30, 1980 5:36:00 PM")
					 $('td:eq(7)', nRow).html( "<b><font color='red'>Not Set</font></b>" );
				 
				 
				 $('td:eq(9)', nRow).html( tag );
	            }
	        } );
	         $('#cat-grid tbody').on( 'click', '#tag-btn', function () {
	        	 $("#eid").val("");
	        	    var data = dataTable.row( $(this).parents('tr') ).data();
					var tt="";
					if(data[17]!=null)
					{
						 tt=data[17].toString();
	        	    globalTag=data[17].toString();
					}
	        	    $("#tgcomid").val(data[0]); 
	        	    $.ajax({
	        		  	  url: 'getAllTag',
	        		  	  type: 'GET',
	        		  	  data:'',
	        		  	  success: function(data) {
	        		  		var tg="";
	        		  		for(var i=0;i<data.length;i++)
	        		  			{
	        		  			var s=data[i][1].split(" ").join("")
	        		  			tg+=' <div class="col-md-5"><label class="checkbox-inline" ><input  id="'+s+'" class="Checkbox" name="sport" type="checkbox" value="'+data[i][1]+'">'+data[i][1]+'</label></div>';
	        		  			}
	        		  		$('#ALLTag').html(tg);
	        		  		setTag(tt);
	        		  	  },
	        			 error: function(e) {
	        		 		console.log(e.message);
	        		 	  }
	        			} );
	        	} );
	        
	         $('#cat-grid tbody').on( 'click', '#delete-btn', function () {
	        	    var data = dataTable.row( $(this).parents('tr') ).data();
	        	    lId=data[0];
	        		  $("#mstpass").val('')
	        		  $("#myModal44").modal('show');
	        	} );
	         
	         $('#cat-grid tbody').on( 'click', '#comment-btn', function () {
	        	    var data = dataTable.row( $(this).parents('tr') ).data();
	        	    lId=data[0];
	        		  $("#comid").val(data[0])
	        		  $("textarea#comment").val(data[16]);
	        		  $("#CommentModal").modal('show');
	        	} );
	         
	         $('#cat-grid tbody').on( 'click', '#view-btn', function () {
	        	   var data = dataTable.row( $(this).parents('tr') ).data();
	        	    var name=data[1];
		        	var res = name.split(" ");
		        	var uname=$('#cmp').val();
		        	leadId=data[ 0 ];
		        	 $.ajax({
	        	  	  url: "getfollowupsCount",
	        	  	  type: "get",
	        	  	  data:"leadid="+data[ 0 ],
	        	  	  success: function(html){
	        	  	   $("#follow").text('FollowsUp '+html);	
	        	  	  }
	        	  	});
		        	
		        	   $('#lid').text(data[0]);
		        	   $('#lcomp').text(uname);
		        	   //$('#lfname').text(res[0]);
		        	  // $('#llname').text(res[1]);
		        	  $('#lfname').text(data[19]);
		        	   $('#llname').text(data[20]);
		        	   $('#lemail').text(data[2]==null?"N/A":data[2]);
		        	   $('#lcno').text(data[3]==null?"N/A":data[3]);
		        	   $('#lcat').text(data[4]);
		        	   $('#lst').text(data[5]);
		        	   $('#lregdate').text(new Date(data[6]));
		        	   $('#lshdate').text(data[7].toString()=="May 25, 1993 12:00:00 AM"?"N/A":data[7]);
		        	   $('#pstate').text(data[8]);
		        	   $('#pstate').text(data[8]);
		        	   $('#loc').text(data[14]+","+data[13]+","+data[12])
		        	   $('#cinfo').text(data[10]+"("+data[11]+")");
		        	   
		        	   var fullString=data[9];
		        	   var heading=fullString.split("\\n");
		        	   var head=heading[0];
		        	   var data=heading[1];
		        	   var splithead=head.split(",");
		        	   var splitdata=data.split(",");
		        	   var finalstr="";
		        	   var i=0;
		        	   for(i=0;i<splithead.length-1;i++)
		        		   {
		        		   finalstr +=splithead[i]+" : "+splitdata[i]+'<br/>';
		        		   }
		        	   $('#csv').html('<div>'+finalstr+'</div>');	        	    
	        	} );
	         
	         $('#cat-grid tbody').on( 'click', '#edit-btn', function () {
	        	    var data = dataTable.row( $(this).parents('tr') ).data();
	        	    proceesState= data[ 5 ];
	        	   var tell=data[8];
	        	   var name=data[1];
	        	   var res = name.split(" ");
	        	   // $('#fname').val(res[0]);
	        	   // $('#lname').val(res[1]);
	        	    $('#fname').val(data[19]);
	        	    $('#lname').val(data[20]);
	        	    $('#email').val(data[2]);
	        	    $('#cno').val(data[3]);
	        	    $('#comp').val(data[11]);
	        	    $('#web').val(data[12]);
	        	    $('#cont').val(data[13]);
	        	    $('#stat').val(data[14]);
	        	    $('#cty').val(data[15]);
	        	    
	        	    
	        	    
	        	   
	        	    if(data[7]!="Jun 30, 1980 5:36:00 PM")
	        	  {
	        	    	 var msec = Date.parse(data[7]);
	 	        	    var d = new Date(msec);
	 	        	    $("#schedule").data("DateTimePicker").date(new Date(msec));
	        	  }
	        	    setTallval(data[16]);
	        	    setLeadProcessSate(proceesState);
	        	   
	        	    var telecaller=data[15];
	        	    leadId=data[ 0 ];
	        	  
	        	    $.ajax({
	  	  url: 'GetLeadCategoryName',
	  	  type: 'GET',
	  	  data:'lid='+leadId,
	  	  success: function(data) {
	  		var msg=data[0];
	  		setcaval='';
	  		setcaval=msg;
	  		setval(setcaval);  
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );
	        	    $.ajax({
	        			  url:'GetTellyCallerBycompanyId',
	        			  type: 'GET',
	        			  success: function(msg) {
	        				  var sthtml="<option value='0'>Unassign</option>";
	        				  for(i=0;i<msg.length;i++)
	        		  			{
	        		  			if(msg[i][0]==telecaller)
	        		  				sthtml+="<option selected value='"+msg[i][0]+"'>"+msg[i][2]+" "+msg[i][3]+"</option>";
	        		  				else
	        		  					sthtml+="<option value='"+msg[i][0]+"'>"+msg[i][2]+" "+msg[i][3]+"</option>";
	        		  			}
	        		  		//$('#tcalleredit').html("<select class='form-control' id='tcIdEdit'>"+sthtml+"</select>");
	        			  },
	        			  error: function(e) {
	        				console.log(e.message);
	        			  }
	        			});
	        	    loadcatleadst();
	        	} );
	         
	         dataTable.on( 'order.dt search.dt', function () {
	        	 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	                cell.innerHTML = i+1;
	            } );
	        } ).draw();
	        
} );

function updateLeadComment()
{
	var Id=$("#comid").val();
	var comment = $('textarea#comment').val();
	$.ajax({
	  	  url: "updateLeadComment",
	  	  type: "get",
	  	data:{Id:Id,comment:comment},
	  	  success: function(html){
	  		 $('#CommentModal').modal('hide');
	  	 dataTable.ajax.reload();
	  	 $.smallBox({
				title : "Lead Followup Successfully Added.",
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 4000
				});
	  	  }
	  	});  
	 $("#myModal44").modal('hide');
}

function deleteLeadData()
{
	$.ajax({
	  	  url: "GetUserData",
	  	  type: "get",
	  	  data:"mstPass="+$("#mstpass").val(),
	  	  success: function(html){
	  	var data=html.toString();
	  	if(data=="Success")
	  		{
	  	 	$.ajax({
	  	  	  url: "DeleteLeadHistoryByLeadId",
	  	  	  type: "get",
	  	  	data:"lId="+lId,
	  	  	  success: function(html){
	  	  		 $('#myModal4').modal('hide');
	  	  		 $("#mstpass").val("")
	  	  	 dataTable.ajax.reload();
	  	  	 $.smallBox({
	  				title : "Lead Data are Successfully Deleted.",
	  				color : "#296191",
	  				iconSmall : "fa fa-thumbs-up bounce animated",
	  				timeout : 4000
	  				});
	  	  	  }
	  	  	});  
	  		}
	  	else
	  		{
	  	 $.smallBox({
				title : "Invalid Transaction Password.",
				color : "#8B0000",
				iconSmall : "fa fa-thumbs-o-down animated",
				timeout : 4000
				});
	  	  }
	  	 $("#myModal44").modal('hide');
	  	  }
	}); 
}
function saveLead()
{
	   var country="N/A";  
	   var state="N/A";  
	   var city="N/A"; 
	   var cname="N/A"; 
	   var cweb="N/A";
	   var company="N/A";
	   var web="N/A";
	    var uid=$('#uid').val();
	    var fname=$('#fname1').val();
	   var lname=$('#lname1').val();
	    var email=$('#eid').val();
	   var mob=$('#mobno').val();
	   var cat=$('#adcat').val();
	   var date=$('#validity').val(); 
	   var tcaler=$('#tcId').val();
	    country=$('#cont11').val();  
	    state=$('#stat11').val();  
	    city=$('#cty11').val(); 
	    company=$('#comp1').val(); 
	    web=$('#url').val(); 

	    if(web=="")
			 web="N/A";
			 if(company=="")
				company="N/A";
			 if(city=="")
				city="N/A";
			 if(state=="")
				state="N/A";
			 if(web=="")
				web="N/A";
			 if(country=="")
				 country="N/A";
	
		mob=mob.replace(/^\s+|\s+$/gm,'');
	  	  var comastr=uid+","+fname+","+lname+","+email+","+mob+","+cat+","+date+","+country+","+state+","+city+","+company+","+web+","+tcaler; 
	 if(mob=="")
		 {
		 document.getElementById('mobno').style.borderColor='red';
	return;
		 }
	 else if(cat=="Select Category")
		 {
		 document.getElementById('adcat').style.borderColor='red';
		 }
	 else
		 {
		 document.getElementById('mobno').style.borderColor='';
	   $.ajax({
	  	  url: "AddLead",
	  	  type: "get",
	  	  data:"comastr="+comastr,
	  	  success: function(html){
	  	   $('#fname1').val("");
	  	   $('#lname1').val("");
	  	   $('#eid').val("");
	  	   $('#mobno').val("");
	  	   $('#cont').val("");
	  	   $('#web').val("");
	  	   $('#cont11').val("");
	  	   $('#url').val("");
	  	   $('#stat11').val("");
	  	   $('#cty11').val("");
	  	   $('#comp1').val("");
	  		dataTable.ajax.reload();
	  		$.smallBox({
				title : "Lead Successfullt Added.",
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 4000
				});
	  		 $('#myModal1').modal('hide');
	  	  }
	  	}); 
		 }
}
function saveChangeLead()
{
	var lid=leadId;
    var fname=$('#fname').val();
   var lname=$('#lname').val();
    var email=$('#email').val();
   var mob=$('#cno').val();
   var cat=$('#cat').val();
   var status=$('#lpstate').val();
   var company= $('#comp').val();
   var website=$('#web').val();
   var cont=$('#cont').val();
   var state= $('#stat').val();
   var city=$('#cty').val();
   var tcaller=$("#tcIdEdit").val();
   var comastr=lid+","+fname+","+lname+","+email+","+mob+","+cat+","+status+","+company+","+website+","+cont+","+state+","+city+","+tcaller;
 
   if(mob=="")
	 {
	 document.getElementById('cno').style.borderColor='red';
return;
	 }
else if(cat=="Select Category")
	 {
	 document.getElementById('cat').style.borderColor='red';
	 }
else
	{
	 document.getElementById('cno').style.borderColor='';
	 document.getElementById('cat').style.borderColor='';

   $.ajax({
	  	  url: "UpdateLead",
	  	  type: "get",
	  	  data:"comastr="+comastr,
	  	  success: function(html){
	  		if($('input[name="vehicle"]').is(':checked'))
	  		{
	  			$('#myModal').modal('hide');
	  			$.smallBox({
						title : "Lead Successfullt Updated.",
						color : "#296191",
						iconSmall : "fa fa-thumbs-up bounce animated",
						timeout : 1000
						});
			  		dataTable.ajax.reload();
	  		}else
	  		{
	  		 $.ajax({
 			  	  url: "updateScheduledate",
 			  	  type: "get",
 			  	  data:{tid:lid,shDate:$("#schedule").val()},
 			  	  success: function(html){
 			  		$("#schedule").val("");  
 			  		$('#myModal').modal('hide');
 						$.smallBox({
 						title : "Lead Successfullt Updated.",
 						color : "#296191",
 						iconSmall : "fa fa-thumbs-up bounce animated",
 						timeout : 1000
 						});
 			  		dataTable.ajax.reload();
 			  	  }       
 			  	
 			  	});  
	  		}
	  	  }       
	  	
	  	});    
	}
}

</script>
</head>
<br/>

<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Lead Manage</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger" onclick="addgetCat()"  data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span> Add Lead</button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>


						<tr role="row">
							<th>ID</th>
							<th>Name</th>
							<th>Email</th>
							<th>Mobile No</th>
							<th>Category</th>
							<th>Process State</th>
							<th>Created Date</th>
							<th>Schedule Date</th>
							<th>Tallycaller</th>
							<th style="width:width: 12%;">Action</th>
							
						</tr>
					</thead>


				</table>

			</div>

		</div>

	</div>
	
	<!-- Trigger the modal with a button -->
<!-- Modal -->
<div id="CommentModal" class="modal fade" role="dialog" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Lead Comment</h4>
      </div>
      <div class="modal-body">
      

<div class="row">
  <input type="hidden" id="comid" class="form-control" >
  <div class="col-md-12">
  <textarea rows="8" cols="15" class="form-control" id="comment" >
N/A</textarea>
  
  </div>
</div>

      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" onclick="updateLeadComment()">Save Comment</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="TagModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tags</h4>
      </div>
      <div class="modal-body">
       <div class="row">
  <div class="col-md-4"><input type="text" id="eid" class="form-control" placeholder="Tag Name"></div>
  <div class="col-md-2"><button type="button" class="btn btn-danger" onclick="addTag()">Add Lead Tag</button></div>
  <div class="col-md-6" >
 
   <input type="hidden" id="tgcomid" class="form-control" >
  </div>
</div>
<br>
<!-- <div class="row">
  <div class="col-md-4">Lead Tag</div>
  <div class="col-md-8">
  
  
  </div>
</div> -->
<br>
<div class="row">
  <div class="col-md-12" id="ALLTag">
 <!--  <input type="text" id="dTAG" /> -->
  </div>
</div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-danger" onclick="addLeadTag()">Add Lead Tag</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="width: 115%;" >
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Lead Data</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         <div class="row">
  <div class="col-xs-2 col-sm-2">First Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="fname" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Last Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="lname" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Email</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="email" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Contact No</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="cno" class="form-control"></div>
        </div>
        
        
        
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Category</div>
  <div class="col-xs-6 col-sm-3" id="catled"></div>
  <div class="col-xs-2 col-sm-2">Lead State</div>
  <div class="col-xs-6 col-sm-3" id="lidst"></div>
        </div>
        
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Company</div>
  <div class="col-xs-6 col-sm-3" id="catled"><input type="text" id="comp" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Web Url</div>
  <div class="col-xs-6 col-sm-3" id="lidst"><input type="text" id="web" class="form-control"></div>
        </div>
        
        <hr>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Telecaller</div>
  <div class="col-xs-6 col-sm-3" id="tcalleredit"></div>

  <div class="clearfix visible-xs-block"></div> 

  <div class="col-xs-6 col-sm-2">Schedule</div>
  <div class="col-xs-6 col-sm-3"><input type="checkbox" checked name="vehicle" id="vehicle" value="Bike"><input class="form-control" value="" name="schedule" id="schedule" type="text" placeholder="Shedule Date"></div>
</div>

         <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Location</div>
  <div class="col-xs-6 col-sm-3" id=""><input type="text" id="cont" class="form-control"></div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="stat" class="form-control"></div>
  <div class="col-xs-6 col-sm-3" id=""><input type="text" id="cty" class="form-control"></div>
        </div>
        
        
         
        
        
        <%
        Authentication auth2 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser2 = (MediUser) auth2.getPrincipal();
		long id=currentUser2.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        %>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveChangeLead();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel" >
  <div class="modal-dialog" role="document" style="width:60%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Lead</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
       <div class="row"  >
  <div class="col-xs-4 col-sm-2">First Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="fname1" class="form-control" placeholder="First Name"></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-2">Last Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="lname1" class="form-control" placeholder="Last Name"></div>
</div>
<br>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Email Id</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="eid" class="form-control" placeholder="Email Address"></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-2">Contact No&nbsp;<b style="color:red;">*</b>&nbsp;</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="mobno" class="form-control" placeholder="Mobile/Landline No."></div>
</div>
<br>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Category&nbsp;<b style="color:red;">*</b>&nbsp;</div>
  <div class="col-xs-6 col-sm-3" id="category">
  

</div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-2">Shedule Date</div>
  <div class="col-xs-6 col-sm-3">
            <input class="form-control" value="01-01-2016" name="validity" id="validity" type="text" placeholder="Shedule Date">
											
											
  </div>
  
</div>

<br>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Company</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="comp1" class="form-control" placeholder="Company Name"></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-2">Web URL</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="url" class="form-control" placeholder="Website"></div>
</div>

<br>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Telecaller</div>
  <div class="col-xs-6 col-sm-3" id="tcaller"></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-4"><b>Assign Lead To Telecaller</b></div>
  <div class="col-xs-6 col-sm-1"><b></b></div>
</div>

<br>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Location</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="cont11" class="form-control" placeholder="Country"></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-4"><input type="text" id="stat11" class="form-control" placeholder="State"></div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="cty11" class="form-control" placeholder="City"></div>
</div>
      
      
       
        <%
        Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser1 = (MediUser) auth1.getPrincipal();
		long id1=currentUser1.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id1+"'>");
        %>
        
       <!--  </table> -->
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveLead();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Lead</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
  <div class="modal-dialog" role="document" style="width: 75%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Lead Full Detail</h4>
      </div>
      <div class="modal-body" >
      <form method="get">
       <table border="1" class="table table-bordered">
       <tr>
       <th class="">Id</th>
       <td><label id="lid"></label></td>
       <th class="">Company</th>
       <td><label id="lcomp"></label></td>
       </tr>
       
       <tr>
       <th class="">First Name</th>
       <td><label id="lfname"></label></td>
       <th class="">Last Name</th>
       <td><label id="llname"></label></td>
       </tr>
       
         <tr>
       <th class="">Email Id</th>
       <td><label id="lemail"></label></td>
       <th class="">Mobile No</th>
       <td><label id="lcno"></label></td>
       </tr>
       
        <tr>
       <th class="">Lead State</th>
       <td><label id="pstate"></label></td>
       <th class="">Process State</th>
       <td><label id="lst"></label></td>
       </tr>
       
       <tr>
       <th class="">Register Date</th>
       <td><label id="lregdate"></label></td>
       <th class="">Shedule Date</th>
       <td><label id="lshdate"></label></td>
       </tr>
       
       <tr>
       <th class="">Location</th>
       <td><label id="loc">Ahmedabad,Gujarat,India</label></td>
       <th class="">Company Info.</th>
       <td><label id="cinfo">Bonric Software System(www.bonric.co.in)</label></td>
       </tr>
       
        <tr>
       <th class="">Category</th>
      <td><label id="lcat"></label></td>
      <th class="">CSV Data</th>
       <td><label id="csv" style="overflow: scroll;width: 100%;">&nbsp;</label></td>
       </tr>

         <tr align="left">
       <td><button type="button" class="btn btn-primary" id="follow" data-toggle="modal" data-target="#myModal4" onclick="showfollowups()">Follow Up </button></td>
     <!--   <td><button type="button" class="btn btn-success">Attend Calls 20</button></td>
       <td><button type="button" class="btn btn-info">Assign Lead 5</button></td>
       <td><button type="button" class="btn btn-warning">Miss Calls 7</button></td> -->
       </tr> 
       
       </table>
        
        <%
        Authentication auth0 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser0 = (MediUser) auth2.getPrincipal();
		long id0=currentUser0.getUserid();
		String uname0=currentUser2.getUsername();
		out.println("<input type='hidden' id='uid' value='"+id0+"'>");
		out.println("<input type='hidden' id='cmp' value='"+uname0+"'>");
        
        
        %>
       
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
  <div class="modal-dialog" role="document" style="width: 70%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">FollowUps Detail</h4>
      </div>
      <div class="modal-body" >
		   <table id="hist-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>


						<tr role="row">
							<th>ID</th>
							<th>Calling Time</th>
							<th>Remark</th>
							<th>Shedule Time</th>
							<th>New Status</th>
							<th>Call Duration</th>
							<th>Call Status</th>
							<th>Audio FIle Name</th>
							<th>Action</th>
							
						</tr>
					</thead>


				</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal44" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are You Sure You want to Delete Category?</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
       <div class="row">
  <div class="col-xs-12 col-sm-12">
  <b>If You Delete Category It Will Delete Following Data.</b><br>
  <b class="text-danger">1. All Leads are Deleted Permanently.</b><br>
  <b class="text-danger">2. All Follow Ups of Leads are Deleted Permanently.</b><br>
  <b class="text-danger">3. Audio Files of Leads are also Deleted Permanently.</b><br><br>
  <b class="text-warning">If  <kbd>Yes</kbd>  Please Enter Transaction Password otherwise Click on  <kbd>No</kbd>  Button.</b>
  </div>
<!--   <div class="col-xs-6 col-sm-3"><input type="password" style="width:150%;" id="mstpass" class="form-control" ></div>
 -->        </div>
        
       <div class="row">
       <p><br></p>
  <div class="col-xs-6 col-sm-4">Transaction Password</div>
  <div class="col-xs-6 col-sm-3"><input type="password" style="width:150%;" id="mstpass" class="form-control" ></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-primary" onclick="deleteLeadData()">Yes</button>
      </div>
    </div>
  </div>
</div>
</html>
