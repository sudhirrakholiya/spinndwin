<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>


						<tr role="row">
							 <th width=5%>ID</th>
 							<th width=20%>Name</th>
							<th width=25%>Telecaller</th>
							<th width=25%>Message Template</th>
							<th width=25%>Email Template</th>
							
						</tr>
						
					</thead>
					<tbody id="tblbody"></tbody>

				</table>
</body>

<script src=js/jquery.js></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>
<script>
var dataTable=null;
$(document).ready(function() {
	
	 $.ajax({
		  url:'GetAssignCategory',
		  type: 'GET',
		  dataType: "json",
		  success: function(msg) {
		var msg=msg.data
		
		 var html="";
	  		for(i=0;i<msg.length;i++)
	  		{
	  			html+="<tr>";
	  			html+="<td>"+msg[i][0]+"</td>";
	  			html+="<td>"+msg[i][1]+"</td>";
	  			html+="<td id='tcombo"+i+"'></td>";
	  			html+="<td id='msgcombo"+i+"'></td>";
	  			html+="<td id='emailcombo"+i+"'></td>";

	  			html+="</tr>";
	  			generateTelecallerCombo(msg[i][0],msg[i][3],"tcombo"+i);
	  			generateMsgCombo(msg[i][0],msg[i][4],"msgcombo"+i);
	  			generateEmailCombo(msg[i][0],msg[i][1],"emailcombo"+i);
	  		       
	  			
	  		}
		$("#tblbody").html(html) 
		
		  },
		  error: function(e) { 
			//called when there is an error
			console.log(e.message);
		  }
		});
});

function generateTelecallerCombo(catId,tcId,id)
{
	id="#"+id;
		$.ajax({
	  	  	  url: 'getTallyCallerByCompanyId',
	  	  	  type: 'GET',
	  	  	  success: function(data) {
	  	  		var msg=data;
	  	  		var html='<select id="tcaller'+catId+'" class="form-control" onchange=AssignDefaultLead(\''+catId+'\',\''+id+'\')>'
	  	  	    html+='<option value="0">Unassign</option>';
	  	  		var i=0;
	  	  		
	  	  		for(i=0;i<msg.length;i++)
	  	  			{
	  	  		
	  	  			var val='"'+msg[i][0]+'"';	
	  	  			var tcid=msg[i][0];
	  	  			tcid.toString();
	  	  			var cat =tcId;
	  	  			cat.toString();
	  	  			if(cat==tcid)
	  	  				{
	  	  				html+="<option selected  value="+val+">"+msg[i][3]+" "+msg[i][4]+"</option>";
	  	  				}
	  	  				else
	  	  					{
	  	  				
	  	  				html+="<option  value="+val+">"+msg[i][3]+" "+msg[i][4]+"</option>";
	  	  					}

	  	  			}
	  	  		 html+="</select>";
	  	  		 $(""+id+"").html(html);
	  	  	  },
	  		 error: function(e) {
	  	 		//called when there is an error
	  	 		console.log(e.message);
	  	 	  }
	  		} ); 	
}

function AssignDefaultLead(val1,val2)
{
	 var id="tcaller"+val1;
		id1="#"+id;

var catId=val1;
var tcId=$(""+id1+"").val();
$.ajax({
	  url: 'DefaultLeadAssignToTelecaller',
	  type: 'GET',
	  data:{catId:catId,tcId:tcId},
	  success: function(data) {
		  alert("Leads are Successfully Assign to "+$(""+id1+" option:selected").text());
	  },
	 error: function(e) {
		//called when there is an error
		console.log(e.message);
	  }
	} );
}
function generateMsgCombo(catId,tempId,id)
{
	id="#"+id;
		$.ajax({
	  	  	  url: 'getMessageTemplateByCompanyId',
	  	  	  type: 'GET',
	  	  	  success: function(data) {
	  	  		  
	  	  		var msg=data;
	  	  		var html='<select onchange="AssignDefaultMsgTemp(\''+catId+'\',\''+catId+'\')" id="msg'+catId+'" class="form-control">'
	  	  	html+='<option value="0" >Unassign</option>';
	  	  		var i=0;
	  	  		var msgTepmId=tempId;
	  	  	msgTepmId.toString();
	  	  		for(i=0;i<msg.length;i++)
	  	  			{
	  	   	var val='"'+msg[i][0]+'"';	
	  	  	if(msgTepmId==msg[i][0])
				{
				html+="<option selected  value="+val+">"+msg[i][1]+"</option>";
				}
				else
					{
				
				html+="<option  value="+val+">"+msg[i][1]+"</option>";
					}
	  	  			}
	  	  		 html+="</select>";
	  	  		 $(""+id+"").html(html);
	  	  	  },
	  		 error: function(e) {
	  	 		//called when there is an error
	  	 		console.log(e.message);
	  	 	  }
	  		} ); 	
}

function AssignDefaultMsgTemp(catId1,tempId1)
{
	var id="msg"+catId1;
	id1="#"+id;
	var catId=catId1;
	var tempId=$(""+id1+"").val();
	$.ajax({
		  url: 'AssignDefaultMsgTemp',
		  type: 'GET',
		  data:{catId:catId,tempId:tempId},
		  success: function(data) {
			  alert("Message Template is Successfully Assign to "+$(""+id1+" option:selected").text());
		  },
		 error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		} );

}

function generateEmailCombo(catId,tempId,id)
{
	id="#"+id;
	$.ajax({
  	  	  url: 'getEmailTemplateByCompanyId',
  	  	  type: 'GET',
  	  	  success: function(data) {
  	  		  
  	  		var msg=data;
  	  		var html='<select onchange="AssignDefaultEmailTemp(\''+catId+'\',\''+catId+'\')" id="email'+catId+'" class="form-control">'
  	  	html+='<option value="0" >Unassign</option>';
  	  		var i=0;
  	  		var msgTepmId=tempId;
  	  	msgTepmId.toString();
  	  		for(i=0;i<msg.length;i++)
  	  			{
  	   	var val='"'+msg[i][0]+'"';	
  	  	if(msgTepmId==msg[i][0])
			{
			html+="<option selected  value="+val+">"+msg[i][1]+"</option>";
			}
			else
				{
			
			html+="<option  value="+val+">"+msg[i][1]+"</option>";
				}
  	  			}
  	  		 html+="</select>";
  	  		 $(""+id+"").html(html);
  	  	  },
  		 error: function(e) {
  	 		//called when there is an error
  	 		console.log(e.message);
  	 	  }
  		} ); 
}

function AssignDefaultEmailTemp(catId1,tempId1)
{
	var id="email"+catId1;
	id1="#"+id;
	var catId=catId1;
	var tempId=$(""+id1+"").val();
	$.ajax({
		  url: 'AssignDefaultEmailTemp',
		  type: 'GET',
		  data:{catId:catId,tempId:tempId},
		  success: function(data) {
			  alert("Email Template is Successfully Assign to "+$(""+id1+" option:selected").text());
		  },
		 error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		} );

}
</script>
</html>