<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="js/jquery.nicescroll.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.timeline {
  list-style: none;
  padding: 20px 0 20px;
  position: relative;
}
.timeline:before {
  top: 0;
  bottom: 0;
  position: absolute;
  content: " ";
  width: 3px;
  background-color: white;
  left: -0%;
  margin-left: -1.5px;
}
.timeline > li {
  margin-bottom: 20px;
  position: relative;
}
.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}
.timeline > li:after {
  clear: both;
}
.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}
.timeline > li:after {
  clear: both;
}
.timeline > li > .timeline-panel {
  width: 57%;
  float: left;
  left:40px;
  background-color:white;
  border: 1px solid #d4d4d4;
  border-radius: 2px;
  padding: 20px;
  position: relative;
  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
}
.timeline > li > .timeline-panel:before {
  position: absolute;
  top: 26px;
  right: -15px;
  display: inline-block;
  border-top: 15px solid transparent;
  border-left: 15px solid #ccc;
  border-right: 0 solid #ccc;
  border-bottom: 15px solid transparent;
  content: " ";
}
.timeline > li > .timeline-panel:after {
  position: absolute;
  top: 27px;
  right: -14px;
  display: inline-block;
  border-top: 14px solid transparent;
  border-left: 14px solid #fff;
  border-right: 0 solid #fff;
  border-bottom: 14px solid transparent;
  content: " ";
}
.timeline > li > .timeline-badge {
  color: #fff;
  width: 50px;
  height: 50px;
  line-height: 50px;
  font-size:2em;
  text-align: center;
  position: absolute;
  top: 16px;
  left: 50%;
  margin-left: -595px;
  background-color: #999999;
  z-index: 100;
  border-top-right-radius: 50%;
  border-top-left-radius: 50%;
  border-bottom-right-radius: 50%;
  border-bottom-left-radius: 50%;
 font-family: "Courier New";
   font-style: normal;
}
.timeline > li.timeline-inverted > .timeline-panel:before {
  border-left-width: 0;
  border-right-width: 15px;
  left: -15px;
  right: auto;
}
.timeline > li.timeline-inverted > .timeline-panel:after {
  border-left-width: 0;
  border-right-width: 14px;
  left: -14px;
  right: auto;
}
</style>
<%
/* String s=request.getParameter("tid");
out.print("<input type='hidden' name='tid'id='tid' value='"+s+"'>"); */

%>
</head>
<script>
$(document).ready(
		
		  function() { 
			 
		    $("html").niceScroll({cursorborder:"",boxzoom:true});
			$("#replaymsg").fadeOut();
			$.ajax({
			  	  url: 'getAgent',
			  	  type: 'GET',
			  	  success: function(data) {
			  		  var msg=data;
			  		 // alert(data)
			  		 var html="<option>Select Agent</option>"; 
			  		for(i=0;i<msg.length;i++)
			  			{
			  			var val='"'+msg[i][0]+'"';
			  			html+="<option value="+val+">"+msg[i][1]+"</option>";
			  			}
			  		$('#agent').html("<select class='form-control' id='selagent' style='width:190px;''>"+html+"</select>"); 
			  		getTicketDetail();
			  	  },
				 error: function(e) {
			 		//called when there is an error
			 		console.log(e.message);
			 	  }
				} );
		
			$.ajax({
			  	  url: 'getDepartment',
			  	  type: 'GET',
			  	  success: function(data) {
			  		  var msg=data;
			  		var html="<option>Select Group</option>"; 
			  		for(i=0;i<msg.length;i++)
			  			{
			  			var val='"'+msg[i][0]+'"';
			  			html+="<option value="+val+">"+msg[i][1]+"</option>";
			  			}
			  		$('#group').html("<select class='form-control' id='seldep' style='width:190px;'>"+html+"</select>");
			  	  },
				 error: function(e) {
			 		//called when there is an error
			 		console.log(e.message);
			 	  }
				} );
			
			
		  }

		);
		function SetPriority(val)
		{
			 var textToFind =val;
			  
			    var dd = document.getElementById('priority');
			    for (var i = 0; i < dd.options.length; i++) {
			        if (dd.options[i].text === textToFind) {
			            dd.selectedIndex = i;
			            break;
			        }
			    }
		}
		
		function SetStatus(val)
		{
			 var textToFind =val;
			  
			    var dd = document.getElementById('status');
			    for (var i = 0; i < dd.options.length; i++) {
			        if (dd.options[i].text === textToFind) {
			            dd.selectedIndex = i;
			            break;
			        }
			    }
		}
		function getTicketDetail()
		{
			//var tkid=$("#tid").val();
			var tkid=$("#tid").val();
			
			//alert('getTicketDetail is called')
			$.ajax({
			  	  url: 'GetTicketDetail',
			  	  data:'tid='+tkid,
			  	  type: 'GET',
			  	  
			  	  success: function(data) {
			  		  var data=eval(data)
			 		console.log("Ticket Id :"+data[0][1]);
			 		var dd = document.getElementById('selagent');
			 		if(dd!=null)
			 			{
			 		setAgent(data[0][7])
			 		setDepartment(data[0][8])
			 		SetPriority(data[0][3])
			 		SetStatus(data[0][4])
			 		getContactInfo(data[0][2])
			 		
			 		 	var start = moment(data[0][9]);
			  			var end=new Date().toJSON().slice(0,10);
			 		//	alert( Math.abs(start.diff(end, "days"))+2);
			 			$('#dt').text(Math.abs(start.diff(end, "days"))+2);
			 			$('#tm').text(data[0][9])
			 			}
			  	  },
				 error: function(e) {
			 		//called when there is an error
			 		console.log(e.message);
			 	  }
				} ); 
			
			
		}
		function showreplaydiv()
		{
			//alert('dgd')
			$("#replay").fadeOut();
			
			$("textarea#repmsg").val('Hi Sajan\n');
			//$('textarea#repmsg').focus();
			$("#replaymsg").fadeIn(2000);
			
			
		}
		function postReplay()
		{
			var html="";
			$("#replay").fadeIn(2000);
			$("#replaymsg").fadeOut();
			var uri=$('textarea#repmsg').val()
			var msg = encodeURI(uri);
			 $.ajax({
			  	  url: 'addTicketchildMST',
			  	  type: 'GET',
			  	  data:{tickid:1,msg:msg},
			  	  success: function(data) {
			  		 addMSGToTimeLine();
			  	  },
				 error: function(e) {
			 		//called when there is an error
			 		console.log(e.message);
			 	  }
				} ); 	
		}
		function addMSGToTimeLine()
		{
			
			var tkid=$("#tid").val();
			 $.ajax({
			  	  url: 'GetTicketChatHistory',
			  	  type: 'GET',
			  	  data:'tid='+tkid,
			  	  success: function(data) {
			  		  var data=eval(data)
			  		  var html="";
			  		  var lst=eval(data.length);
					  var start = moment([eval(data.length-1)][2]);
			  		  var end=new Date().toJSON().slice(0,10);
			 		  var day=Math.abs(start.diff(end, "days"));
			 		  console.log(decodeURI(data[eval(data.length-1)][1]))
			 		 if(day=='0')
						{
			 				day='Replied Today ('+data[i][2]+')'; 
						}
					else
						{
						day='Replied '+ (day+2) +' day ago ('+data[i][2]+')';
						}
						var name=data[eval(data.length-1)][3];
						var firstchar=name.charAt(0).toUpperCase();
						html+="<li class='timeline-inverted'>";
						html+="<div class='timeline-badge warning'><div style=' border-radius: 50%;width:50px;height:50px;padding: 8px;background:#40b3ff;border: 2px solid #40b3ff;color:white;text-align: center;font: 25px Courier New, Courier, monospace;'><b>"+firstchar+"</b></div></div>";
						html+="<div class='timeline-panel'><div class='timeline-heading' style='display: inline-block;'><h4 class='timeline-title' style='display: inline-block;'><font face='Times New Roman'>"+data[eval(data.length-1)][3]+"&nbsp;</font></h4>";
						html+="<div style='display: inline-block;'><span class='fa fa-reply' ><font size='+0'  color='gray' face='Times New Roman'>&nbsp;Replied "+day+"</font></span></div></div>";
						html+="<div class='timeline-body'><p>"+decodeURI(data[eval(data.length-1)][1])+"</p> </div>";
						html+="<div style='float:right;'><span class='fa fa-pencil' ><font size='2' face='Times New Roman' color='gray'>&nbsp;<b>"+data[eval(data.length-1)][2]+"</b></font></span></div></div></li>";
						
						$("#replay").before(html); 
			  	  },
				 error: function(e) {
			 		//called when there is an error
			 		console.log(e.message);
			 	  }
				} ); 
		}
		function setAgent(val)
		{
			    var textToFind=val
			    var dd = document.getElementById('selagent');
			  
			     for (var i = 0; i < dd.options.length; i++) {
			        if (dd.options[i].value == textToFind) {
			            dd.selectedIndex = i;
			            break;
			        }
			    }	  
		}
		
		function setDepartment(val)
		{
			    var textToFind=val
			    var dd = document.getElementById('seldep');
			    for (var i = 0; i < dd.options.length; i++) {
			        if (dd.options[i].value == textToFind) {
			            dd.selectedIndex = i;
			            break;
			        }
			    }	 
		}
		function getContactInfo(val)
		{
			var email=val;
			//alert(email)
			$.ajax({
			  	  url: 'getContactInfoByEmail',
			  	  type: 'GET',
			  	  data:'email='+email,
			  	  success: function(data) {
			  		//alert(data);
			  		var msg=data;
			  		var html="";
			  		var name=data[0][1];
					var firstchar=name.charAt(0).toUpperCase();
			  		      html +="<b><font size='4'>Requestor Info</font></b><br><br>";
			  			  html +=" 	<div style='width:25px;height:25px;padding:2px;background:#40b3ff;border-radius: 50%;border: 2px solid #40b3ff;color:white;text-align: center;font: 17px Courier New, Courier, monospace;float: width: 100px;display: inline-block;'><b>"+firstchar+"</b></div>";
			  			  html +="  <div style=' width: 100px;display: inline-block;'><font color='gray'>"+data[0][1]+"</font></div><br><br>";
			  			  html +=" 	<div class='fa fa-envelope-o fa-lg'></div>";
			  			  html +="	<div style=' width:50px;display: inline-block;'><font color='gray'>"+data[0][3]+"</font></div><br><br>";
			  			  html +="	<div class='glyphicon glyphicon-phone fa-lg' ></div>";
			  			  html +="	<div style=' width:50px;display: inline-block;'><font color='gray'>"+data[0][2]+"</font></div>";
			  			$('#cinfo').html(html)
			  			GetTicketChatHistory(data[0][1])
			  	  },
				 error: function(e) {
						//called when there is an error
			 		console.log(e.message);
			 	  }
				} );
		}
		
		function GetTicketChatHistory(val)
		{
			var tkid=$("#tid").val();
			var name=val;
			
			 $.ajax({
			  	  url: 'GetTicketChatHistory',
			  	  data:'tid='+tkid,
			  	  type: 'GET',
			  	  success: function(data) {
			  		 var data=eval(data)
			  		 console.log(data)
			 		var html="";
			  		var name=data[0][2];
					var firstchar=name.charAt(0).toUpperCase();
			  		 for(i=0;i<data.length;i++)
			  			{
			  			var start = moment(data[i][2]);
			  			var end=new Date().toJSON().slice(0,10);
			 		//	alert( Math.abs(start.diff(end, "days"))+2);
			 			var day=Math.abs(start.diff(end, "days"));
			 			if(day=='0')
						{
			 				day='Replied Today ('+data[i][2]+')'; 
						}
					else
						{
							day='Replied '+ (day+2) +' day ago ('+data[i][2]+')';
						}
			  			var name=data[i][3];
						var firstchar=name.charAt(0).toUpperCase();
						var newDay="";
						if(data[i][0]==1)
							{
							var start1 = moment(data[i][2]);
				  			var end1=new Date().toJSON().slice(0,10);
				 		//	alert( Math.abs(start.diff(end, "days"))+2);
				 			var day1=Math.abs(start1.diff(end1, "days"));
							if(day=='0')
							{
								newDay='reported Today ('+data[i][2]+')'; 
							}
						else
							{
							newDay='reported '+ (day1+2) +' day ago ('+data[i][2]+')';
							}
							html+="<li class='timeline-inverted'>";
							html+="<div class='timeline-badge warning'><div style=' border-radius: 50%;width:50px;height:50px;padding: 8px;background:#40b3ff;border: 2px solid #40b3ff;color:white;text-align: center;font: 25px Courier New, Courier, monospace;'><b>"+firstchar+"</b></div></div>";
							html+="<div class='timeline-panel'><div class='timeline-heading' style='display: inline-block;'><h4 class='timeline-title' style='display: inline-block;'><font face='Times New Roman'>"+data[i][3]+"</font></h4>";
							html+="<div style='display: inline-block;'><span><font size='+0'  color='gray' face='Times New Roman'>&nbsp;"+newDay+"</font></span></div></div>";
							html+="<div class='timeline-body'><p>"+decodeURI(data[i][1])+"</p> </div>";
							html+="<div style='float:right;'><span class='fa fa-pencil' ><font size='2' face='Times New Roman' color='gray'>&nbsp;<b>"+data[i][2]+"</b></font></span></div></div></li>";
							}
						else
							{
							
			  	html+="<li class='timeline-inverted'>";
				html+="<div class='timeline-badge warning'><div style=' border-radius: 50%;width:50px;height:50px;padding: 8px;background:#40b3ff;border: 2px solid #40b3ff;color:white;text-align: center;font: 25px Courier New, Courier, monospace;'><b>"+firstchar+"</b></div></div>";
				html+="<div class='timeline-panel'><div class='timeline-heading' style='display: inline-block;'><h4 class='timeline-title' style='display: inline-block;'><font face='Times New Roman'>"+data[i][3]+"</font></h4>&nbsp;";
				html+="<div style='display: inline-block;'><span class='fa fa-reply' ><font size='+0'  color='gray' face='Times New Roman'>&nbsp;"+day+"</font></span></div></div>";
				html+="<div class='timeline-body'><p>"+decodeURI(data[i][1])+"</p> </div>";
				html+="<div style='float:right;'><span class='fa fa-pencil' ><font size='2' face='Times New Roman' color='gray'>&nbsp;<b>"+data[i][2]+"</b></font></span></div></div></li>";
							
							}	
						
						} 
			  		var pending="Pending";
			  		html+="<li class='timeline-inverted' id='replay'>";
					html+="<div class='timeline-badge warning'><div style=' border-radius: 50%;width:50px;height:50px;padding: 8px;background:#40b3ff;border: 2px solid #40b3ff;color:white;text-align: center;font: 25px Courier New, Courier, monospace;'><b>R</b></div></div>";
					html+="<div class='timeline-panel'><div class='timeline-heading' style='display: inline-block;'><h4 class='timeline-title' style='display: inline-block;'><font face='Times New Roman'></font></h4>&nbsp;";
					html+="<div style='display: inline-block;'><span class='fa fa-reply' style='color:#40b3ff;'></span>&nbsp;<a type='button' onclick='showreplaydiv()'  role='button'>Replay</a></div></div>";
					html+="<div class='timeline-body'><p></p> </div></div></li>";
					
					 html+="<li class='timeline-inverted' style='display: none;'id='replaymsg'>";
					html+="<div class='timeline-badge warning'><div style=' border-radius: 50%;width:50px;height:50px;padding: 8px;background:#40b3ff;border: 2px solid #40b3ff;color:white;text-align: center;font: 25px Courier New, Courier, monospace;'><b>S</b></div></div>";
					html+="<div class='timeline-panel'><div class='timeline-heading' style='display: inline-block;'><h4 class='timeline-title' style='display: inline-block;'><font face='Times New Roman'></font></h4>&nbsp;";
					html+="<div style='display: inline-block;'><textarea id='repmsg' class='form-control' rows='8' cols='80'>Hi Sajan,</textarea></div>";
					html+="<div style='float:right;'>";
					html+="<div class='btn-group dropup open '>";
					html+="<button type='button' class='btn btn-info' onclick='postReplay()'>Send</button>";
					html+="<button type='button' class='btn btn-info dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>";
					html+="<span class='caret'></span><span class='sr-only'></span></button>";
					html+="<ul class='dropdown-menu dropdown-menu-right '>";
					html+="<li><a  href='#' onclick=postReplayWith('Pending')>Send and Set as <b>Pending</b></a></li>";
					html+="<li><a  href='#' onclick=postReplayWith('Resolved')>Send and Set as <b>Resolved</b></a></li>";
					html+="<li><a  href='#' onclick=postReplayWith('Closed')>Send and Set as <b>Closed</b></a></li>";
				//	html+="<li><a href='#'>Send and Set as <b>Waiting On Customer</b></a></li>";;
				//  html+="<li><a href='#'>Send and Set as <b>Waiting On Thirrd Party</b></a></li>";
					
					html+="</ul></div>";
	                html+="</div></div><div class='timeline-body'><p></p> </div></div></li>";
			  		$('#chat').html(html)
			  	  },
				 error: function(e) {
			 		//called when there is an error
			 		console.log(e.message);
			 	  }
				} );  
		}
		
		function postReplayWith(val)
		{
			var status=val;
		//	alert(status);
			var uri=$('textarea#repmsg').val()
			var msg = encodeURI(uri);
			$.ajax({
			  	  url: 'sendTicketWithStatus',
			  	  type: 'GET',
			  	  data:{tid:1,ststus:val,msg:msg},
			  	  success: function(data) {
			  		  if(status=='Pending')
		  					$("#status").val("2");
			  		  else if(status=='Resolved')
		  					$("#status").val("3");
			  		  else if(status=='Closed')
		  					$("#status").val("4");
			  		  else
			  			$("#status").val("1").change();
			    alert("Update Successfully...")
			  		 addMSGToTimeLine();
			  	  },
				 error: function(e) {
			 		//called when there is an error
			 		console.log(e.message);
			 	  }
				} );
		}
		
		function updateTicketProprities()
		{
			var satatus=$("#status :selected").text();
			var priority=$("#priority :selected").text();
			var departmentId=$("#seldep").val();
			var agentId=$("#selagent").val();
			
			$.ajax({
			  	  url: 'updateTicketProprities',
			  	  type: 'GET',
			  	  data:{satatus:satatus,priority:priority,tickId:1,departmentId:departmentId,agentId:agentId},
			  	  success: function(data) {
			  	
			  		 alert("Update Successfully...")
			  	
			  	  },
				 error: function(e) {
			 		//called when there is an error
			 		console.log(e.message);
			 	  }
				} );
			
		}
		
	
		
</script>
<body style="overflow:scroll;">
<!-- 
<button class="btn btn-primary btn-lg"  data-toggle="popover-x"  data-target="#myPopover1" id="popover" data-placement="right">Top</button>
 <div id="myPopover1" class="popover popover-default">
                    <div class="arrow"></div>
                
                    <div class="popover-content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                    </div>
                    <div class="popover-footer">
                        <button type="submit" class="btn btn-sm btn-primary">Submit</button><button type="reset" class="btn btn-sm btn-default">Reset</button>
                    </div>
                </div>  -->
<div class="col-md-8" style="max-height: 80%;overflow:hidden;" id="myDiv" >
  <div class="container">
 <!--  <div class="page-header">
    <h1 id="timeline">Timeline</h1>
  </div> -->
  <ul class="timeline" id="chat">
    
				
    
   
  </ul>
</div>
  
  
  
</div>
<br>
  <div class="col-md-4" style="float:right;">
 
    
  
    
    

    <ul class="list-group">
  <li class="list-group-item">
  	<b><font size="4" >Open</font></b>
  	<br>
  	<font color="gray">Due <label id='dt'></label> days ago</font> 
  	<br>
  	<font color="gray">on <label id='tm'></label></font>
  
  
  
  </li> 
   
  
  <li class="list-group-item " id="cinfo">
  
  	
  
  <!-- style="color:#40b3ff;text-align: center;font: 25px Courier New, Courier, monospace;float: width: 102px ;height:30%;display: inline-block;" -->
  
  </li>
  
   
  <li class="list-group-item ">
  
  	<b><font size="4">Ticket Properties</font>&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" onclick="updateTicketProprities()" class="btn btn-success btn-sm">Update</button></b>
  	<br><br>
  	<div class="col-md-3"><div style="display: inline-block;">Priority<span class="required_star" style="color:red; font-size: 20px;">*</span></div></div>
  	<div class="col-md-9">
  	<div style="display: inline-block;">
  
  <select class=form-control style="width:190px;  " id='priority'>
  <option value="7">Select Priority</option>
  <option value="2">Medium</option>
<option value="3">High</option>
<option value="4">Urgent</option>
<option value="3">Low</option></select>

  	</div></div>
  	<br><br>
  	<div class="col-md-3"><div style="display: inline-block;">Status<span class="required_star" style="color:red; font-size: 20px;">*</span></div></div>
  	<div class="col-md-9">
  	<div style=" display: inline-block;">
  
  <select class=form-control style="width:190px;  " id='status'>
    <option value="7">Select Status</option>
  <option value="1">Open</option>
<option value="2">Pending</option>
<option value="3">Resolved</option>
<option value="4">Closed</option>
<!-- <option value="5">Waiting on Customer</option>
<option value="6">Waiting on Third Party</option>--></select> 

  	</div></div>
  	<br><br>
  	<div class="col-md-3"><div style="display: inline-block;">Department</div></div>
  	<div class="col-md-9">
  	<div style="display: inline-block;" id="group">
  
  
  	</div></div>
  	<br><br>
  	<div class="col-md-3"><div >Agent</div></div>
  	<div class="col-md-9">
  	<div style=" display: inline-block; width:60%;" id="agent">
  
  
  	</div></div>
  <!-- style="color:#40b3ff;text-align: center;font: 25px Courier New, Courier, monospace;float: width: 102px ;height:30%;display: inline-block;" -->
  <br><br>  
  </li>
   </ul> 
    
</div>
</body>
<input type="hidden" id="tid" value=<%=request.getParameter("tid")%>>
</html>