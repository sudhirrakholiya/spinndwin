
<section id="widget-grid" class="">

	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-13"
		data-widget-editbutton="true">
<header role="heading">
					<span class="widget-icon"> <i class="fa fa-hand-o-up"></i> </span>
					<h2><strong>Contact</strong> <i>List</i></h2>		
							
					<div class="widget-toolbar" role="menu">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
					
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

		<div>
			<div class="widget-body no-padding" id="childtables">
				<table id="listContects"
					class="table table-striped table-bordered table-hover">
					<thead>
						<tr role="row">
							<th style="width: 27px;">ID</th>
							<th>Name</th>
							<th>Mobile Number</th>
							<th style="width: 15%;">Number</th>
							<th>Email</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th style="width: 5%;">Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">



var mobile='';
var	name='';
var	email='';
var gid=<%=request.getParameter("gid")%>

var mediatable=null;


	
	
	function reloadtable(){
		
	
		 mobile=$('#mobile').val();
			name	=$('#name').val();
			email	=$('#email').val();
			gid=gid;
		mediatable.ajax.reload();		
		
	}
	
	 $(document).ready(function(){
		 getdata2();
			});
	function getdata2(){
		
	      mediatable=	$('#listContects').DataTable({
	        "processing": false,
	        "serverSide": true,	         
	        "pageLength": 10,
	        "ordering": false,
	        "bdestroy": true,
	        "aoColumns": [{"bVisible": false},null,{"bVisible": false},null,null,{"bVisible": false},{"bVisible": false},{"bVisible": false},{"bVisible": false},null],
	        "ajax": {"url":"getContects",
	      "data": function ( d ) {
              d.mobile =mobile;
              d.name=name;
              d.email=email;
              d.gid=gid
          }
	        }	      
	      , "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	        	$('td:eq(0)',nRow).html(aData[3]);
	        	$('td:eq(1)',nRow).html(aData[4]);
	        	$('td:eq(2)',nRow).html(aData[1]);
	        	$('td:eq(3)',nRow).html(
						'<div class="btn-group btn-group-justified" style="width:50px;"><a href="#"  class="btn btn-danger btn-xs" onclick="delhttp('+ aData[0]+ ')">Delete</a></div>');
	        },
			"autoWidth" : true,	
			"rowCallback" : function(nRow) {
			
			},
			"drawCallback" : function(oSettings) {
		
			}
		});
	    
  var htm='<form class="form-inline" role="form">'+
     '<div class="icon-addon addon-md">'
        +'<input type="text" id="mobile" style="width:125px;"  placeholder="Mobile" class="form-control"><input type="text" id="name" style="width:150px;"  placeholder="Name" class="form-control"><input type="text" style="width:150px;"  id="email"  placeholder="Email" class="form-control">'
         +'<label for="email" class="glyphicon glyphicon-search" rel="tooltip" title="Mobile"></label>'
     +'<span class="input-group-btn"><button class="btn btn-success" onclick="reloadtable();" type="button">Search</button></span></div>';
     
    // $('#listContects').find('.dataTables_filter').html(htm);
 $("#childtables .dataTables_filter").html(htm);
 
	}

	
	function delhttp(id){
		var conf = confirm('Continue delete?');
		
		if(conf){
		$.get("deleteContactbyid?cid="+id,function(msg){
			reloadtable();
			if(msg==1){
				msg="Sucessfully Deleted!"
			}
			$.smallBox({
				title : msg,content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",color : "#296191",iconSmall : "fa fa-thumbs-up bounce animated",timeout : 4000
								});
			
			
		});
		}
		
	}
	function ellipsis(text, n) {
	    if(text.length>n)
	        return text.substring(0,n)+"...";
	    else
	        return text;
	}
</script>
