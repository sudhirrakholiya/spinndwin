<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.sudhir.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
/* Adjust feedback icon position */
#movieForm .form-control-feedback {
    right: 15px;
}
#movieForm .selectContainer .form-control-feedback {
    right: 25px;
}
</style>

<script type="text/javascript">
$(document).ready(function() {
var uid=$('#uid').val();
//alert(uid)
$.ajax({
	
	url : 'GetCategory',
	type: 'GET',
	data: 'compid='+uid,
	success:function(data)
	{
		var cat=data;
		var i=0;
		var html="<option value='0' >Select Category</option>";
		for(i=0;i<cat.length;i++)
			{
			html+="<option value='"+cat[i][0]+"'>"+cat[i][1]+"</option>";
			}
		$('#category').html("<select class='form-control' id='inputcategory'>"+html+"</select>");
  		console.log("<select class='form-control' id='inputcategory'>"+html+"</select>");
		
	},
	
	error: function(e) {
		//called when there is an error
		console.log(e.message);
	  }
	
	});
	
	
$.ajax({
	
	url : 'GetTellyCallerBycompanyId',
	type: 'GET',
	success:function(data)
	{
		var cat=data;
		var i=0;
		var html="<option value='0'>Select Tellycaller</option>";
		for(i=0;i<cat.length;i++)
			{
			html+="<option value='"+cat[i][0]+"'>"+cat[i][1]+"</option>";
			}
		$('#tellycaller').html("<select class='form-control' id='inputcaller'>"+html+"</select>");
  		//console.log("<select class='form-control' id='inputcategory'>"+html+"</select>");
		
	},
	
	error: function(e) {
		//called when there is an error
		console.log(e.message);
	  }
	
	});
});

			function addregex()
			{
				var from=$('#inputEmail').val();
				var subject=$('#inputsubject').val();
				var category=$('#inputcategory').val();
				var namepatten=$('#inputnamepatten').val();
				var emailpatten=$('#inputemailpatten').val();			
				var mobilepatten=$('#inputmobilepatten').val();
				var tallycallerId=$('#inputcaller').val();
				
				$.ajax({
					url : 'addRegeX',
					type: 'GET',
					data:{from:from,subject:subject,category:category,namepatten:namepatten,emailpatten:emailpatten,mobilepatten:mobilepatten,tallycallerId:tallycallerId},
					success:function(data)
					{
						alert('Regex Successfully Added')
					},
					error: function(e) {
						//called when there is an error
						console.log(e.message);
					  }
					
					});
			}
			
			
			
			function getregex()
			{
				$.ajax({
					url : 'runThread',
					type: 'GET',
					success:function(data)
					{
						alert('Regex Successfully Added')
					},
					error: function(e) {
						//called when there is an error
						console.log(e.message);
					  }
					
					});
			}
</script>
</head>
<body>
<div class="bs-example">
    <h1>New RegeX Patten</h1>
    <form class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-xs-3" for="inputEmail"><b>From</b></label>
            <div class="col-xs-4">
                <input type="email" class="form-control" id="inputEmail" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="inputPassword"><b>Subject</b></label>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="inputsubject" placeholder="Subject">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="confirmPassword"><b>Category</b></label>
            <div class="col-xs-4" id="category">
               
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-xs-3" for="confirmPassword"><b>Telecaller</b></label>
            <div class="col-xs-4" id="tellycaller">
               
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-3" for="confirmPassword"><b>Name Patten</b></label>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="inputnamepatten" placeholder="Name Patten">
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-3" for="confirmPassword"><b>Email Patten</b></label>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="inputemailpatten" placeholder="Email Patten">
            </div>
        </div>
        
         <div class="form-group">
            <label class="control-label col-xs-3" for="confirmPassword"><b>Mobile  Patten</b></label>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="inputmobilepatten" placeholder="Mobile  Patten">
            </div>
        </div>
     
        
     
        
        <br>
        <div class="form-group">
            <div class="col-xs-offset-3 col-xs-9">
                <button class="btn btn-primary" onclick="addregex()">Add RegeX</button>
                 <!-- <button class="btn btn-primary" onclick="getregex()">Get RegeX</button> -->
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
        </div>
          <%
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id=currentUser.getUserid();
		String uname=currentUser.getUsername();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
		out.println("<input type='hidden' id='cmp' value='"+uname+"'>");
        
        
        %>
    </form>
</div>
</body>
</html>