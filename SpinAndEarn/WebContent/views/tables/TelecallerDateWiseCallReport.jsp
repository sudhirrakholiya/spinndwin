<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!--  <script src="http://cdn.anychart.com/js/7.10.1/anychart-bundle.min.js"></script>
 <script src="js/bootstrap-datetimepicker.js"></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script> 
        <link rel="stylesheet" href="http://cdn.anychart.com/css/latest/anychart-ui.min.css" />-->

</head>
<body>
 <div id="datewise">
            <h3>All Time Report</h3> 
            <div class="row">
  <div class="col-xs-4 col-sm-2">
     <select class="form-control"  id="category" ></select>
  
  </div>
  <div class="col-xs-4 col-sm-2">
        <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
  
  </div>
  <div class="col-xs-4 col-sm-2">
 <button type="button" class="btn btn-danger" onclick="reloadData()">Search</button>
  
  </div>
  
  </div>
  <br>
              <table id="example" class="table table-striped table-bordered" style="font-weight:normal;" cellspacing="0" width="100%">
        <thead>
            <tr>
               
                <th>Name</th>
                 <th>Total Call</th> 
                <th>Success Call</th>
                <th>Fail Calls</th>   
                <th>Total Call Duration</th>     
                            
            </tr>
            </thead>   
    </table> 
               </div>  


</body>

<!-- <script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>  -->


        
<script type="text/javascript">
var successRate=null;
var failRate=null;
var successI=null;
var FailI=null;
var successArray="[";
var failArray="[";
var telecallerArray="[";

var successArray1="";
var failArray1="";
var telecallerArray1="";

var getDate="NA";
var dataTable=null;
var tcId="3";
$(document).ready(function() {
	
	
});
$(function () {
    $('#datetimepicker1').datetimepicker({
   	  defaultDate: new Date(), 
   	 
     format: 'YYYY-MM-DD'
        /* disabledDates: [
            moment("2016-12-30"),
            new Date(2013, 11 - 1, 21),
            "01/01/2016 00:53"
        ] */
    });
});
$( document ).ready(function() {

$.ajax({
  	  url: 'getTallyCallerByCompanyId',
  	  type: 'GET',
  	  success: function(data) {
  		
  		var msg=data;
  		var html="<option value='0'>Select Tellycaller</option>";
  		var i=0;
  		
  		for(i=0;i<msg.length;i++)
  			{
  		
  			var val='"'+msg[i][0]+'"';
  				html+="<option value="+val+">"+msg[i][3]+" "+msg[i][4]+"</option>";
  			}
  		$('#category').html(html);
  		tcId=$('#category').val();
  		getDate=$('#datetimepicker1').data('date');
  		dataTable=  $('#example').DataTable( {
	        "ajax": {
	            "url": "GetTelecallerDateWiseDetails",
	            "data": function ( d ) {
		             d.getDate=getDate;
		             d.tcId=tcId;
	           },    
	        },   
	        "rowCallback": function( row, data, index ) {
	        	 var result="";
				  var totalSeconds=data[4];
				  var hours   = Math.floor(totalSeconds / 3600);
				  var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
				  var seconds = totalSeconds - (hours * 3600) - (minutes * 60);

				  // round seconds
				  seconds = Math.round(seconds * 100) / 100

				   result += (hours < 10 ? "0" + hours : hours);
				      result += ":" + (minutes < 10 ? "0" + minutes : minutes);
				      result += ":" + (seconds  < 10 ? "0" + seconds : seconds);
		         $('td:eq(4)', row).html( result);

	          },
	        "language": {
	            "emptyTable": "Select Telecaller to Show Data"
	          } ,
	          "sPaginationType" : "full_numbers",
			  "oLanguage" : {
			  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
			  },
			  //"sDom" : '<"tbl-tools-searchbox"fl<"clear">>,<"tbl_tools"CT<"clear">>,<"table_content"t>,<"widget-bottom"p<"clear">>',
			   "sDom": 'T<"clear">lfrtip',
			  "oTableTools" : {
			  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			  }
	            
    });
/*  dataTable.on( 'order.dt search.dt', function () {
 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
     cell.innerHTML = i+1;
 } );
} ).draw();  */
  		
  	  },
	 error: function(e) {
 		//called when there is an error
 		console.log(e.message);
 	  }
	} );
	
});

function reloadData()
{
	getDate=$('#datetimepicker1').data('date');
	tcId=$('#category').val()
	dataTable.ajax.reload();
	
}






 function getStatistic(type,id)
{
	 $.ajax({
		  url: 'getStatistic',
		  type: 'GET',
		  data:"type="+type+"&&id="+id,
		  success: function(Mydata) {
			  var html="";
			  html+="<table class='table table-bordered'>";
			  html+="<thead>";
			  html+="<tr>";
				html+="<th>Lead Process State</th>";
				html+="<th>Count</th>";
				html+="</tr>";	
			  html+="</thead>";
			  html+="<tbody>";
			  for(i=0;i<Mydata.length;i++)
				  {
			  html+="<tr>";
			  html+="<td>"+Mydata[i][0]+"</td>";
			  html+="<td>"+Mydata[i][1]+"</td>";
			  html+="</tr>";
				  }  
			  html+="</tbody>";
			  html+="</table>";
			  $("#successStatisticHeader").html(type +" Call Statistic")
			  $("#successStatisticBody").html(html);
		  },
		  error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		});	
} 



</script>
</html>