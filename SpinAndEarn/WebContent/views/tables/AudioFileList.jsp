<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
    <%@page import="org.springframework.security.core.Authentication"%>
    <%@page import="com.sudhir.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        %>
<table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Sr No.</th>
                <th>Telecaller Name</th>
                <th>Audio File</th>
                 <th>Audio File</th>
            </tr>
        </thead>
    </table>
</body>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        "ajax": {
            "url": "getAudioFileAPI",
            "dataSrc": ""
        },
        "columns": [
        	{ "data": "srNo" },
        	{ "data": "Telecaller_Name" },
            { "data": "Audio_File_Name" },
            { "data": "Audio_File_Name" }
        ],
        "rowCallback": function( row, data, index ) {
	         $('td:eq(3)', row).html( '<a type="button" target="_blank" href="http://localhost:8191/spring-hib/Sound/'+$("#uid").val()+'/'+data["Audio_File_Name"]+'" class="btn btn-labeled btn-warning btn-sm"><span class="btn-label"><i class="glyphicon glyphicon-save"></i></span>Download</a>' );
         },
    } );
} );

</script>
</html>