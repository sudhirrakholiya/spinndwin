<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%
	String name = currentUser.getUsername();
%>
<c:set var="name" scope="session" value="<%=currentUser.getUsername()%>" />







<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<div class="row">
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
				<a href="./user/createTransTamplate.jsp" data-toggle="modal"
					data-target="#remoteModal" class="btn btn-primary"> <i
					class="fa fa-circle-arrow-up fa-lg"></i> Create Transactional Template
				</a>

				<!-- MODAL PLACE HOLDER -->
				<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
					aria-labelledby="remoteModalLabel" aria-hidden="true"
					style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content"></div>
					</div>
				</div>

			</div>

		</div>
	</div>

</div>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">



			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
				data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<!-- <h2>Export to PDF / Excel</h2> -->

				</header>

				<div>

					<div class="jarviswidget-editbox">

					</div>

					<div class="widget-body no-padding">

						<table id="datatable_tabletools"
							class="table table-striped table-bordered table-hover"
							width="100%">
							<thead>
								<tr>

									<th data-class="expand">Sr no</th>
									<th data-class="expand">Title</th>
									<th data-class="expand">Template</th>
									<th data-class="expand">UserName</th>
									<th data-class="expand">Status</th>
									<!-- <th >Action</th> -->

								</tr>
							</thead>
							<tbody>




							</tbody>
						</table>

					</div>

				</div>

			</div>

		</article>

	</div>


</section>

<script type="text/javascript">

	var mediatable;
	pageSetUp();
	function getdata() {

		mediatable = $('#datatable_tabletools')
				.dataTable(
						{
							"processing" : false,
							"serverSide" : false,
							"bServerSide" : false,
							/* "aoColumns" : [ null, null, null, null, null, null,
									{
										"bVisible" : false
									} ], */
							// "bdestroy": true,
							//  "bDestroy": true,
							"ajax" : "getTamplates?istrans=true",
							"fnCreatedRow" : function(nRow, aData, iDataIndex) {

								$('td:eq(0)', nRow).html(aData[0]);
								$('td:eq(1)', nRow).html(aData[1]);
								$('td:eq(2)', nRow).html(aData[2]);
								$('td:eq(3)', nRow).html(aData[3]);

								//$('td:eq(7)', nRow).html('<div class="btn-group btn-group-justified" style="width:150px;"><a href="./views/media/TinyHitList.jsp?mid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-success btn-xs">Detail</a><a href="./views/quickMessage.jsp?url=http://'+$(location).attr("hostname")+'/'+aData[5]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</a></div>');

								if (aData[4] == '0') {
									$('td:eq(4)', nRow)
											.html(
													'<input type="button" class="btn btn-xs bg-color-blueLight txt-color-white" value="PENDDING">');

								} else if (aData[4] == '1') {
									$('td:eq(4)', nRow)
											.html(
													'<input type="button" class="btn btn-xs bg-color-green txt-color-white" value="APPROVED">');

								}

								else {
									$('td:eq(4)', nRow)
											.html(
													'<input type="button" class="btn btn-xs bg-color-redLight txt-color-white" value="REJECTED">');

								}

							/* 	$('td:eq(5)', nRow)
										.html(
												'<a href="#" id="tid" class="btn btn-primary btn-xs"  onclick="action('
														+ aData[0]
														+ ' )"> Action </a>') */


							},
							"autoWidth" : true,
							"rowCallback" : function(nRow) {
							},
							"drawCallback" : function(oSettings) {
							}
						});
		
	
	}
	function action(e) {

		$
				.SmartMessageBox(
						{
							title : "Action",
							content : "This is a confirmation box for Perform action.",
							buttons : '[Close][Approve][Rejected][Edit][Delete][Pendding]'
						},
						function(ButtonPressed) {
							if (ButtonPressed === "Many") {

								//alert("many");
								$
										.smallBox({
											title : "Callback function",
											content : "<i class='fa fa-clock-o'></i> <i>You pressed Yes...</i>",

											color : "#659265",
											iconSmall : "fa fa-check fa-2x fadeInRight animated",
											timeout : 4000,

										});
							}
							if (ButtonPressed === "Approve") {
								var tid = $('#tid').val();
								var option = "1";
								$
										.ajax({
											url : "approveTamplate",
											type : "GET",
											//contentType: "application/json; charset=utf-8", 
											data : {
												'tid' : e,
												'option' : option
											},
											beforeSend : function(response) {
												$('#sendit').attr('disabled',
														true);
											},
											success : function(response) {
												$
														.smallBox({
															title : response
																	+ ButtonPressed,
															content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>",
															color : "#296191",
															iconSmall : "fa fa-thumbs-up bounce animated",
															timeout : 4000
														});
												reloadtable();
											},
											error : function(xhr, status, error) {
												alert(xhr.responseText);
												$('#sendit').attr('disabled',
														false);
											}
										});
								return false;
							}
							if (ButtonPressed == "Rejected") {
								var tid = $('#tid').val();
								var option = "2";
								$
										.ajax({
											url : "approveTamplate",
											type : "GET",
											//contentType: "application/json; charset=utf-8", 
											data : {
												'tid' : e,
												'option' : option
											},
											beforeSend : function(response) {
												$('#sendit').attr('disabled',
														true);
											},
											success : function(response) {
												$
														.smallBox({
															title : response
																	+ ButtonPressed,
															content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>",
															color : "#296191",
															iconSmall : "fa fa-thumbs-up bounce animated",
															timeout : 4000
														});
												reloadtable();
											},
											error : function(xhr, status, error) {
												alert(xhr.responseText);
												$('#sendit').attr('disabled',
														false);
											}
										});
								return false;
							}
							if (ButtonPressed == "Pendding") {
								var tid = $('#tid').val();
								var option = "0";
								$
										.ajax({
											url : "approveTamplate",
											type : "GET",
											//contentType: "application/json; charset=utf-8", 
											data : {
												'tid' : e,
												'option' : option
											},
											beforeSend : function(response) {
												$('#sendit').attr('disabled',
														true);
											},
											success : function(response) {
												$
														.smallBox({
															title : response
																	+ ButtonPressed,
															content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>",
															color : "#296191",
															iconSmall : "fa fa-thumbs-up bounce animated",
															timeout : 4000
														});
												reloadtable();
											},
											error : function(xhr, status, error) {
												alert(xhr.responseText);
												$('#sendit').attr('disabled',
														false);
											}
										});
								return false;
							}

							if (ButtonPressed == "Delete") {

								var tid = $('#tid').val();
								var option = "2";
								$
										.ajax({
											url : "deleteTamplate",
											type : "POST",
											data : {
												'tid' : e,
												'option' : option
											},
											beforeSend : function(response) {
												$('#sendit').attr('disabled',
														true);
											},
											
											success : function(response) {
												$
														.smallBox({
															title : response
																	+ ButtonPressed+"d.",
															content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>",
															color : "#296191",
															iconSmall : "fa fa-thumbs-up bounce animated",
															timeout : 4000
														});

												reloadtable();

											},
											error : function(xhr, status, error) {
												alert(xhr.responseText);
												$('#sendit').attr('disabled',
														false);
											}
										});
								return false;
							}

							if (ButtonPressed == "Edit") {
								$("#remoteModal")
										.modal(
												{
													remote : "./user/updateTamplate.jsp?tid="
															+ e,
													show : 'true'
												});

							}

						});
		//e.preventDefault();
	};

	function reloadtable() {

		mediatable.fnReloadAjax();
	}
	pageSetUp();

	// pagefunction	
	var pagefunction = function() {
		//console.log("cleared");

		/* BASIC ;*/
		
	getdata();
		$('body').on('hidden.bs.modal', '.modal', function() {
			$(this).removeData('bs.modal');
		});
	
		$('#remoteModal').on('hidden.bs.modal', function() {
			reloadtable();
		});


		$("div.toolbar")
				.html(
						'<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

		// Apply the filter
		$("#datatable_fixed_column thead th input[type=text]").on(
				'keyup change',
				function() {

					otable.column($(this).parent().index() + ':visible')
							.search(this.value).draw();

				});

	};


	loadScript(
			"js/plugin/datatables/jquery.dataTables.min.js",
			function() {
				loadScript(
						"js/plugin/datatables/dataTables.colVis.min.js",
						function() {
							loadScript(
									"js/plugin/datatables/dataTables.tableTools.min.js",
									function() {
										loadScript(
												"js/plugin/datatables/dataTables.bootstrap.min.js",
												function(){loadScript(
														"js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js",
														function (){
															loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js",
																	
																	function() {
																		loadScript(
																				"js/fnReloadAjax.js",pagefunction)
																	});
											});
									});
									});
						});
			});

	/* function noAnswer() {

		$.smallBox({
			title : "Sure, as you wish sir...",
			content : "",
			color : "#A65858",
			iconSmall : "fa fa-times",
			timeout : 5000
		});

	};

	function closedthis() {
		$.smallBox({
			title : "Great! You just closed that last alert!",
			content : "This message will be gone in 5 seconds!",
			color : "#739E73",
			iconSmall : "fa fa-cloud",
			timeout : 5000
		});
	}; */
	
	function delhttp(id) {
		var conf = confirm('Are you sure you want to delete this?');

		if (conf) {
			$.get("deleteTamplate?tid=" + id, function(msg) {

				reloadtable();
			});
		}

	}
</script>
