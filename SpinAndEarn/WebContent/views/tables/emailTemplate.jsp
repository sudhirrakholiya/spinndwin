
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<a href="./user/createEmailTemplate.jsp" data-toggle="modal"
			data-target="#remoteModal" class="btn btn-primary"> <i
			class="fa fa-circle-arrow-up fa-lg"></i> Create Email Template
		</a>

		<!-- MODAL PLACE HOLDER -->
		<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
			aria-labelledby="remoteModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content"></div>
			</div>
		</div>

	</div>

</div>


<%@include file="/includes/SessionCheck.jsp"%>




<!-- MODAL PLACE HOLDER -->
<!-- <div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 1800px;"></div>
	</div>
</div>
 -->




<!-- widget grid -->
<section id="widget-grid" class="">


	<!-- end widget -->

	<!-- Widget ID (each widget will need unique ID)-->

	<!-- end widget -->



	<!-- Widget ID (each widget will need unique ID)-->
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
		data-widget-editbutton="false">
		<header>
			&nbsp;&nbsp;&nbsp;<b> Email Template Setting</b>

		</header>



		<!-- widget div-->
		<div>

			<!-- widget edit box -->
			<div class="jarviswidget-editbox">
				<!-- This area used as dropdown edit box -->

			</div>
			<!-- end widget edit box -->

			<!-- widget content -->
			<div class="widget-body no-padding">

				<table id="datatable_tabletools"
					class="table table-striped table-bordered table-hover">
					<thead>


						<tr role="row">
							<th data-hide="phone" class="sorting_asc" tabindex="0"
								aria-controls="datatable_tabletools" rowspan="1" colspan="1"
								aria-sort="ascending"
								aria-label="ID: activate to sort column ascending"
								style="width: 27px;">ID</th>
							<th data-class="expand" class="sorting" tabindex="0"
								aria-controls="datatable_tabletools" rowspan="1" colspan="1"
								aria-label="Name: activate to sort column ascending">Message</th>
							<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="datatable_tabletools" rowspan="1" colspan="1">Template</th>
							<!-- 	<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="datatable_tabletools" rowspan="1" colspan="1">Template</th> -->
							<!-- <th data-hide="phone" class="sorting" tabindex="0"
								aria-controls="datatable_tabletools" rowspan="1" colspan="1">PAssword</th>
							<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="datatable_tabletools" rowspan="1" colspan="1">Display
								Name</th>
							<th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="datatable_tabletools" rowspan="1" colspan="1">
								From</th> -->
							<!-- <th data-hide="phone,tablet" class="sorting" tabindex="0"
								aria-controls="datatable_tabletools" rowspan="1" colspan="1">Host
								Name</th> -->

							<th>Action</th>

						</tr>
					</thead>

				</table>

			</div>
			<!-- end widget content -->

		</div>
		<!-- end widget div -->

	</div>
	<!-- end widget -->

	</article>
	<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
	var responsiveHelper_dt_basic = undefined;
	var responsiveHelper_datatable_fixed_column = undefined;
	var responsiveHelper_datatable_col_reorder = undefined;
	var responsiveHelper_datatable_tabletools = undefined;

	var mediatable = null;

	pageSetUp();

	function reloadtable() {

		mediatable.fnReloadAjax();
	}


	var pagefunction = function() {

		$('body').on('hidden.bs.modal', '.modal', function() {
			$(this).removeData('bs.modal');
		});
		getdata();
		$('#remoteModal').on('hidden.bs.modal', function() {
			reloadtable();
		});

		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$("div.toolbar")
				.html(
						'<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

	};

	// load related plugins

	loadScript(
			"js/plugin/datatables/jquery.dataTables.min.js",
			function() {
				loadScript(
						"js/plugin/datatables/dataTables.colVis.min.js",
						function() {
							loadScript(
									"js/plugin/datatables/dataTables.tableTools.min.js",
									function() {
										loadScript(
												"js/plugin/datatables/dataTables.bootstrap.min.js",
												function() {

													loadScript(
															"js/plugin/datatable-responsive/datatables.responsive.min.js",

															function() {
																loadScript(
																		"js/fnReloadAjax.js",
																		pagefunction)
															});
												});
									});
						});
			});

	function getdata() {
		if (mediatable)
			mediatable.fnDestroy();
		mediatable = $('#datatable_tabletools')
				.dataTable(
						{
							"processing" : false,
							"serverSide" : false,

							//"bdestroy": true,

							//  "bDestroy": true,
							"ajax" : "getEmailTamplate",
							"fnCreatedRow" : function(nRow, aData, iDataIndex) {

								$('td:eq(0)', nRow).html(aData[0]);
								$('td:eq(1)', nRow).html(aData[1]);
								$('td:eq(2)', nRow).html(aData[2]);
							/* 	$('td:eq(3)', nRow).html(aData[3]); */
							
								/* 								$('td:eq(5)', nRow).html(aData[5]); */
								/* $('td:eq(6)', nRow).html(aData[6]); */

								$('td:eq(3)', nRow)
										.html(
												'<div class="btn-group btn-group-justified" style="width:150px;"><a href="./user/updateEmailTemplate.jsp?id='
														+ aData[0]
														+ '" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Edit</a><a  class="btn btn-danger btn-xs" onclick="delhttp('
														+ aData[0]
														+ ')");">Delete</a></div>');

							},
							"autoWidth" : true,
							"rowCallback" : function(nRow) {

							},
							"drawCallback" : function(oSettings) {

							}
						});

	}

	function delhttp(id) {
		var conf = confirm('Are you sure you want to delete this?');
		if (conf) {
			$.get("deleteEmailTemplate?id=" + id, function(msg) {
				noAnswer();

				reloadtable();

			});
		}

	}
	function noAnswer() {

		$
				.smallBox({

					content : "<i class='fa fa-clock-o'></i> <i>Email template Delete Succefully.</i>",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
				});

	};

	var pagefunction = function() {

		$('body').on('hidden.bs.modal', '.modal', function() {
			$(this).removeData('bs.modal');
		});
		getdata();
		$('#remoteModal').on('hidden.bs.modal', function() {
			reloadtable();
		});
	};

	function reloadtable() {

		mediatable.fnReloadAjax();
	}
</script>
