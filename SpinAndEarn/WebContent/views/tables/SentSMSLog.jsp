<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>

td.details-control {
    background: url('css/img/details.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('css/img/details_close.png') no-repeat center center;
}


</style>

<script type="text/javascript">
var startDt=''; 
var endDt='';
var  table = null;
$('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        
       
        },
        function (start, end) {
          $('#daterange-btn').text(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );
    
   
$(document).ready(function() {
	
      table = $('#example').DataTable( {
    	  "ajax": {"url":"getTodaySentSMSLog",	      
  	        "data": function ( d ) {
                d.startDate =startDt;
                d.endDate=endDt;
              
            }
    	  },
         "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":null,
                "defaultContent": ''
            },
             { "data": "id" },
            { "data": "message" },
            { "data": "mobileNo" },
            { "data": "MSGStatus" },
            { "data": "sendDate" }
        ],
        
        "order": [[1, 'asc']] ,"rowCallback" : function(nRow) {
        
			 var $cell=$('td:eq(3)', nRow);
			 var $cell5=$('td:eq(5)', nRow);
			// alert($cell5.text());
			 if($cell5.text()=="SUCCESS")
			 $cell5.html("<span class='label label label-info'>Success</span>");
			 else
				 $cell5.html("<span class=' label label-danger'>&nbsp;&nbsp;&nbsp;&nbsp;Fail&nbsp;&nbsp;&nbsp;&nbsp;</span>");
			 
             $cell.text(ellipsis($cell.text(),50));
             
             
             
		}
       
    } ); 
    
  
      table.on( 'order.dt search.dt', function () {
    	  table.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
          } );
         } ).draw(); 
     
     $('#example tbody').on('click', 'td.details-control', function () {
         var tr = $(this).closest('tr');
    
         var row = table.row( tr );
  
         if ( row.child.isShown() ) {
             // This row is already open - close it
             row.child.hide();
             tr.removeClass('shown');
         }
         else {
             // Open this row
             row.child( format(row.data()) ).show();
             tr.addClass('shown');
         }
     } );
     
     function format ( d ) {
    	    // `d` is the original data object for the row
    	    var t=''; 
    	    console.log(d.MSGStatus)
    	    if(d.MSGStatus.localeCompare("SUCCESS"))
    	    	{
    	    //	alert('Blue')
    	    	t='<td style="color:blue;">'+d.sendDate+'</td>';
    	    	}
    	    	else if(d.MSGStatus.localeCompare("FAIL"))
    	    		{
    	    		alert('red')
    	    		t='<td style="color:red;">'+d.sendDate+'</td>';
    	    		}
    	    		
    	    return '<table cellpadding="5" cellspacing="0" class="table table-bordered" border="1" style="padding-left:10px;font-size : 85%; font-family : "Myriad Web",Verdana,Helvetica,Arial,sans-serif; background : #efe none; color : #630;">'+
    	        
    	            '<td><b>Message</b></td>'+
    	            '<td style="color: blue;">'+d.mobileNo+'</td>'+
    	        '</tr>'+
    	        '<tr>'+
    	            '<td><b>Mobile No</b></td>'+
    	            '<td >'+d.message+'</td>'+
    	        '</tr>'+
    	        '<tr>'+
	            '<td style="width:12%;"><b>Send Date</b></td>'+
	             '<td>'+d.MSGStatus+'</td>' +
	        '</tr>'+
	        '<tr>'+
            '<td><b>Message Status</b></td>'+
            t+
        '</tr>'+
    	    '</table>';
    	}
} );

function ellipsis(text, n) {
    if(text.length>n)
        return text.substring(0,n)+"...";
    else
        return text;
}

$('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {
    console.log(picker.startDate.format('YYYY-MM-DD'));
    console.log(picker.endDate.format('YYYY-MM-DD'));
    getLog(picker.startDate.format('YYYY-MM-DD'), picker.endDate.format('YYYY-MM-DD'))
  });
  

function getLog(startDt1,endDt1)
{
	
	$("#sdate").val(startDt1);
	$("#edate").val(endDt1);
	
	  startDt=startDt1;
	   endDt=endDt1;
	   table.ajax.reload();
	           
	        
}
</script>
</head>
<body >

			
				
					<h2>Sent SMS Log</h2>
		
	
<div class="">
               <!-- <p>&nbsp;</p> --><!-- &nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
                <button type="button" class="btn btn-default pull-right" id="daterange-btn" >
                    <i class="fa fa-calendar"></i> chose Date Range For Log
                    <i class="fa fa-caret-down"></i>
                  </button>
                </div>
                
             <!--   <input type="text" id="dt"> -->
                <p>&nbsp;</p> 
                  <p>&nbsp;</p>  
                <table id="example" class="table table-striped table-bordered display" cellspacing="0" width="100%"  style="background-color: white; ">
                
        <thead>
            <tr>
             <th width="1%"></th>
              <th width="5%">Sr No.</th>
                <th width="5%">Mobile Number</th>
                <th width="20%">Message</th>
               <th width="9%">Send Date</th> 
                <th width="5%">Message Status</th> 
            </tr>
        </thead>
        </table>
</body>
</html>