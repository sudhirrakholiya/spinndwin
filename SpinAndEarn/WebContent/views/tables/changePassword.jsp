<%@page import="com.sudhir.service.UserService"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.Import"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@include file="/includes/SessionCheck.jsp"%>


		
				
<section id="widget-grid" class="ng-scope">
	<!-- START ROW -->

		<!-- NEW COL START -->
		

			<script type="text/javascript">
				$(document).ready(function() {

					$('#datepicker').datepicker();
					
					 $('body').on('hidden.bs.modal', '.modal', function () {
						  $(this).removeData('bs.modal');
						});
				});

				function formSub(event) {

					//alert("asda");
					var username = $('#username').val();
					var password = $('#password').val();
					var oldPassword= $('#oldPassword').val();
					

					if($("#oldPassword").val()==""){
						alert("Enter Old Password");
						return;
					}
					if($("#passwordConfirm").val()!=password || password ==""){
						alert("password does not match");
						return;
					}
					$.ajax({
						url : "changePassword",
						type : "GET",
						data : {
							'username' : username,
							'oldPassword':oldPassword,
							'password' : password
						
						},

						success : function(response) {
							$.smallBox({
								title : response,
								/* content : "<i class='fa fa-clock-o'></i>", */
								color : "#296191",
								iconSmall : "fa fa-thumbs-up bounce animated",
								timeout : 4000
							});
							
							$('#password').val('');
							$('#passwordConfirm').val('');
							 $('#remoteModal').modal('hide');
							 
							//window.location = "login.jsp";

						},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>
			
			
				
			<div class="jarviswidget jarviswidget-sortable" id="wid-id-1"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false" role="widget" style="">




				<!-- widget div-->
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					<div class="well no-padding">

						<form name="register" id="register" class="smart-form client-form">
							<header>Reset Password </header>
							
							<input type="hidden"  id="username" name="username">
							<fieldset>
							
						<!-- 	<div class="row">
								
								
								<section class=" col col-4">
										<label class="input"> <i
											class="icon-append fa fa-envelope"></i> <input type="password"
											id="oldPassword" name="oldPassword" placeholder="Old Password"  value="">
											<b class="tooltip tooltip-bottom-right">Enter Old Password</b>
										</label>
									</section>
								</div>
							 -->
								<div class="row">
								
									<section class=" col col-4">
										<label class="input"> <i
											class="icon-append fa fa-envelope"></i> <input type="password"
											id="password" name="password" placeholder="Password"  value="">
											<b class="tooltip tooltip-bottom-right">Enter Password</b>
										</label>
									</section>
									</div>
								<div class="row">	
									<section class=" col col-4">
										<label class="input"> <i
											class="icon-append fa fa-envelope"></i> <input type="password"
											id="passwordConfirm" name="passwordConfirm" placeholder="ReEnter Password"  value="">
											<b class="tooltip tooltip-bottom-right">ReEnter Your Password</b>
										</label>
									</section>
									</div>
									<div class="row">
									<section class="col col-4">

									<footer>
										<button type="button" onclick="formSub()"
											class="btn btn-primary">Reset</button>
										<!-- <button type="button" class="btn btn-default"
											data-dismiss="modal">Close</button> -->
									</footer>

								</section>
								</div>
								
								
							</fieldset>
						
						
							</form>
</section>


			<script type="text/javascript">
			
			var selectedsect=$("#temp").val();
			$('#s1 option[value='+selectedsect+']').attr('selected','selected');
			
			</script>


