<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<section id="widget-grid" class="ng-scope">

		

						
			
				
			<div class="jarviswidget jarviswidget-sortable" id="wid-id-1"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false" role="widget" style="">




				<!-- widget div-->
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					<div class="well no-padding">

						<form name="register" id="register" class="smart-form client-form">
							<header>Manage Transaction Password </header>
							
							<input type="hidden"  id="username" name="username">
							<fieldset id="data">
						
								
							</fieldset>
						
						
							</form>
</section>


</body>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script>
$(document).ready(function() {
	readyPass();
} );
function readyPass()
{

	$.ajax({
	 	  url: 'GenerateMasterPassword',
	 	  type: 'GET',
	 	  data: {mstPass:'N/A',OldmstPass:'N/A',status:'GEN_PASS'},
	 	  success: function(data) {
	 		var Mydata=data.toString();
	 		var html="";
	 		if(Mydata=="Success")
	 				{
	 			html+="<div class='row'><section class=' col col-4'>";
	 			html+="<label class='input'> <i class='icon-append fa fa-key'></i> ";
	 			html+="<input type='password' id='oldpassword' name='password' placeholder='Old Password'  value=''>";
	 			html+="<b class='tooltip tooltip-bottom-right'>Old Password</b>";
	 			html+="</label></section>";
	 			html+="</div>";
	 												
	 			html+="<div class='row'><section class=' col col-4'>";
	 			html+="<label class='input'> <i class='icon-append fa fa-key'></i>";
	 			html+="<input type='password' id='nwepassword'  placeholder='New Password'  value=''>";
	 			html+="<b class='tooltip tooltip-bottom-right'>New Password</b>";
	 			html+="</label>";
	 			html+="</section></div>";
	 			
	 			html+="<div class='row'><section class=' col col-4'>";
	 			html+="<label class='input'> <i class='icon-append fa fa-key'></i>";
	 			html+="<input type='password' id='repassword'  placeholder='ReType New Password'  value=''>";
	 			html+="<b class='tooltip tooltip-bottom-right'> New Password</b>";
	 			html+="</label>";
	 			html+="</section></div>";

	 			
	 											   
	 											   
	 			html+="<div class='row'>";
	 				html+="<section class='col col-4'><footer>";
	 					html+="<button type='button' onclick='chabgePassword()' class='btn btn-primary'>Reset</button>";
	 						html+="</footer></section></div>";
	 				}
	 		
	 		else
	 			{
	 			
	 				
	 				
	 			html+="<div class='row'><section class=' col col-4'>";
	 			html+="<label class='input'> <i class='icon-append fa fa-key'></i>";
	 			html+="<input type='password' id='Cnewpassword' name='Cnewpassword' placeholder='New Password'  value=''>";
	 			html+="<b class='tooltip tooltip-bottom-right'>New Password</b>";
	 			html+="</label>";
	 			html+="</section></div>";
	 			
	 			html+="<div class='row'><section class=' col col-4'>";
	 			html+="<label class='input'> <i class='icon-append fa fa-key'></i>";
	 			html+="<input type='password' id='Coldpassword' name='Coldpassword' placeholder='ReType New Password'  value=''>";
	 			html+="<b class='tooltip tooltip-bottom-right'> New Password</b>";
	 			html+="</label>";
	 			html+="</section></div>";

	 			
	 											   
	 											   
	 			html+="<div class='row'>";
	 				html+="<section class='col col-4'><footer>";
	 					html+="<button type='button' onclick='generateNewPassword()' class='btn btn-primary'>Set Password</button>";
	 						html+="</footer></section></div>";
	 			}
	 		$("#data").html(html);
	 	  },
		 error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		} );
}
function chabgePassword()
{
	var newPass=$('#nwepassword').val();
	var renewPass=$('#repassword').val();
	newPass.toString();
	renewPass.toString();
	if(newPass===renewPass)
		{
	$.ajax({
		url : "GenerateMasterPassword",
		type : "GET",
		data : {
			'status':'RE_GEN_MST_PASS',
			'OldmstPass':$("#oldpassword").val(),
			'mstPass' : $("#nwepassword").val()
		
		},

		success : function(response) {
			var data=response.toString();
			 if(data=="Transaction Password Successfully Changed.")
				{
			$.smallBox({
				title : response,
				/* content : "<i class='fa fa-clock-o'></i>", */
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 4000
			});
				}
			 else
				{
				$.smallBox({
					title : response,
					/*  content : "<i class='fa fa-clock-o'></i>",  */
					color : "#8B0000",
					iconSmall : "fa fa-thumbs-o-down bounce animated",
					timeout : 4000
				});
				
				} 
			
			$('#oldpassword').val('');
			$('#nwepassword').val('');
			$('#repassword').val('');
			 
			

		 },
		error : function(xhr, status, error) {
		} 
	});
		}
	else
		{
		$.smallBox({
			title : "Old Password And New Password Dosen't Match.",
			/*  content : "<i class='fa fa-clock-o'></i>",  */
			color : "#8B0000",
			iconSmall : "fa fa-thumbs-o-down bounce animated",
			timeout : 4000
		});
		$('#oldpassword').val('');
		$('#nwepassword').val('');
		$('#repassword').val('');
		}
	
	
	
}

function generateNewPassword()
{
	var newPass=$('#Cnewpassword').val();
	var renewPass=$('#Coldpassword').val();
	newPass.toString();
	renewPass.toString();
	if(newPass===renewPass)
		{
	$.ajax({
		url : "GenerateMasterPassword",
		type : "GET",
		data : {
			'status':'GEN_MST_PASS',
			'OldmstPass':'N/A',
			'mstPass' : $("#Cnewpassword").val()
		
		},

		success : function(response) {
			var data=response.toString();
			 if(data=="Transaction Password Successfully Generated.")
				{
			$.smallBox({
				title : response,
				/* content : "<i class='fa fa-clock-o'></i>", */
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 4000
			});
				}
			 else
				{
				$.smallBox({
					title : response,
					/*  content : "<i class='fa fa-clock-o'></i>",  */
					color : "#8B0000",
					iconSmall : "fa fa-thumbs-o-down bounce animated",
					timeout : 4000
				});
				
				} 
			
			 $('#Cnewpassword').val('');
			 $('#Coldpassword').val('');
			 
			 readyPass();
			

		 },
		error : function(xhr, status, error) {
		} 
	});
		}
	else
		{
		$.smallBox({
			title : "Old Password And New Password Dosen't Match.",
			/*  content : "<i class='fa fa-clock-o'></i>",  */
			color : "#8B0000",
			iconSmall : "fa fa-thumbs-o-down bounce animated",
			timeout : 4000
		});
		$('#Cnewpassword').val('');
		$('#Coldpassword').val('');
		 
	
	
		}	
}

</script>
</html>