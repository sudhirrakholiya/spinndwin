<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.sudhir.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!-- <link rel=stylesheet type=text/css media=screen href="css/dataTables.jqueryui.min.css"/> -->

</head>
<style>
table { table-layout:fixed; } td{ overflow:hidden; text-overflow: ellipsis; }
</style>
<!-- <script src="js/daterangepicker/daterangepicker.js"></script> -->
<body>
<div class="row">
  <div class="col-xs-4 col-sm-2">
     <select class="form-control"  id="category" ></select>
  
  </div>
  
  <div class="col-xs-4 col-sm-2">
<select id="cat" class="form-control">
 
</select>  
  </div>
  
  
  <div class="col-xs-4 col-sm-2">
<select id="callType" class="form-control">
 <option value="OutBound">OutBound</option>
  <option value="InBound">InBound</option>
 
</select>  
  </div>
  
  <div class="col-xs-4 col-sm-2">
        <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
  
  </div>
  <div class="col-xs-4 col-sm-2">
 <button type="button" class="btn btn-danger" onclick="reloadData()">Search</button>
  
  </div>
  
  </div>

 <%
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        
        
        %>

 <p><br></p>
<!--  <div id="Mytbl">
 -->  <div id="">
 <table id="example" class="table table-striped table-bordered" style="font-weight:normal;" cellspacing="0" width="100%">
        <thead>
            <tr>
             <th>Sr No.</th> 
              <th>Name</th> 
                <th>Mobile No.</th> 
                 <th>Email</th> 
                   <th>Remark</th> 
                    <th>State</th>
                    <th>Category</th> 
                <th>Calling Time</th>
                <th>Durations(S)</th>   
                <th>Telecaller</th> 
                 <th>Audio</th>             
            </tr>
            </thead>   
    </table> 
 
 
 </div>
</body>

<script>
var getDate="NA";
var dataTable=null;
var tcId="3";
var callType="";
var cat="0";
$(function () {
    $('#datetimepicker1').datetimepicker({
   	  defaultDate: new Date(), 
     format: 'YYYY-MM-DD'
    });
});
$( document ).ready(function() {
	getCat();
	
	
$.ajax({
  	  url: 'getTallyCallerByCompanyId',
  	  type: 'GET',
  	  success: function(data) {
  		var date=new Date();
  		var msg=data;
  		var html="<option value='All'>All Tellycaller</option>";
  		var i=0;
  		
  		for(i=0;i<msg.length;i++)
  			{
  		
  			var val='"'+msg[i][0]+'"';
  				html+="<option value="+val+">"+msg[i][3]+" "+msg[i][4]+"</option>";
  			}
  		$('#category').html(html);
  		tcId=$('#category').val();
  		getDate=$('#datetimepicker1').data('date');
  		if(cat.toString=="")
  			cat="0";


  		dataTable=  $('#example').DataTable( {
  	        "ajax": {
  	        	"url":"GetInOutBoundSuccessCallLog",	      
  	 	        "data": function ( d ) {
  	 	         d.getDate=getDate;
	             d.tcId=tcId;
	             d.callType=$("#callType").val();
	             d.cat=$('#cat').val();
  	           },
  	           
  	        },  
  	        "createdRow": function( row, data, dataIndex ) {
  			   $('td:eq(10)', row).html( '<a type="button" target="_blank" href="http://crmdemo.bonrix.in/Sound/'+$('#uid').val()+"/"+data[10]+'" class="btn  btn-warning"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>' );
  		  	          },
  	          
  	 	       "lengthMenu": [[10, 25, 50,100,250,500,1000,2000,5000, -1], [10, 25, 50,100,250,500,1000,2000,5000, "All"]],
  	         "sPaginationType" : "full_numbers",
  			  "oLanguage" : {
  			  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
  			  },
  			  "sDom": 'T<"clear">lfrtip',
  			  "oTableTools" : {
  			  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
  			  "aButtons": [
  				  {
  	 				  "sExtends": "pdf",
  	 				  "sTitle" : "OutBound Success Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
  	 				  },
  	 				  {
  	 					  "sExtends": "csv",
  	 					  "sTitle" : "OutBound Success Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
  	 					},
  	 					{
  	 						  "sExtends": "xls",
  	 						  "sTitle" : "OutBound Success Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
  	 					}
  				  ],
  			  } 
  	    } );

  		 dataTable.on( 'order.dt search.dt', function () {
  			 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
  			     cell.innerHTML = i+1;
  			 } );
  			} ).draw(); 
  		/*dataTable=  $('#example').DataTable( {
  	  		
	        "ajax": {
	            "url": "GetInOutBoundSuccessCallLog",
	            "data": function ( d ) {
		             d.getDate=getDate;
		             d.tcId=tcId;
		             d.callType=$("#callType").val();
		             d.cat=$('#cat').val();
	           },    
	        },  
	 	       "lengthMenu": [[10, 25, 50,100,250,500,1000,2000,5000, -1], [10, 25, 50,100,250,500,1000,2000,5000, "All"]],

	        "rowCallback": function( row, data, index ) {
		         $('td:eq(10)', row).html( '<a type="button" target="_blank" href="http://crmdemo.bonrix.in/Sound/'+$('#uid').val()+"/"+data[10]+'" class="btn btn-labeled btn-warning"><span class="btn-label"><i class="glyphicon glyphicon-save"></i></span></a>' );

	          },
	        "language": {
	            "emptyTable": "No Data Found."
	          } ,
	          "sPaginationType" : "full_numbers",
			  "oLanguage" : {
			  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
			  },
			  //"sDom" : '<"tbl-tools-searchbox"fl<"clear">>,<"tbl_tools"CT<"clear">>,<"table_content"t>,<"widget-bottom"p<"clear">>',
			   "sDom": 'T<"clear">lfrtip',
			  "oTableTools" : {
			  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			  },
			  "aButtons": [
				  {
	 				  "sExtends": "pdf",
	 				  "sTitle" : "All Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
	 				  },
	 				  {
	 					  "sExtends": "csv",
	 					  "sTitle" : "All Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
	 					},
	 					{
	 						  "sExtends": "xls",
	 						  "sTitle" : "All Call Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
	 					}
				  ],
	         
    });*/

  		
 $('#example').on( 'error.dt', function ( e, settings, techNote, message ) {
		console.log( 'An error has been reported by DataTables: ', message );
		} ) .DataTable();
  	  },
	 error: function(e) {
 		//called when there is an error
 		console.log(e.message);
 	  }
	} );
});

function getCat()
{
	$.ajax({
		  url: 'GetCategoryById',
		  type: 'GET',
		  success: function(data) {
			
			var msg=data;
			var html="<option value='All'>All Category</option>";
			var i=0;
			
			for(i=0;i<msg.length;i++)
				{
			
				var val='"'+msg[i][0]+'"';
					html+="<option value="+val+">"+msg[i][1]+"</option>";
				}
			$('#cat').html(html);
			
		  },
		 error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		} );	
	
}
	function reloadData()
	{
		getDate=$('#datetimepicker1').data('date');
		tcId=$('#category').val()
		callType=$("#callType").val();
		cat=$('#cat').val();
		dataTable.ajax.reload();
		
	}
</script>
</html>