
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/includes/SessionCheck.jsp"%>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
<sql:query var="msg" dataSource="SMS">
                 	select * from groupname where uid=<%=currentUser.getUserid()%>

</sql:query>



<script type="text/javascript" src="js/jquery.form.min.js">
	
</script>


<script type="text/javascript">


	function formSub3(event) {

		var styles = $('#styles').val();
		var name = $('#name').val();
		var number = $('#number').val();
		var email = $('#email').val();
		var gid = $('#gid2').val();
		$.ajax({

			url : "saveContectSingle",
			dataType : "json",
			type : "POST",

			data : {

				'styles' : styles,
				'name' : name,
				'number' : number,
				'gid' : gid,
				'email' : email

			},
			success : function(response) {
				$('#name').val('');
				$('#number').val('');
				$('#email').val('');
				
				if(response==1){
					response="Sucessfully Inserted";	
				}else
					{
					response="Number Already Exist in this Group";
					}
				$.smallBox({
					title : response,content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",color : "#296191",iconSmall : "fa fa-thumbs-up bounce animated",timeout : 4000
									});
				
			},
			error : function(xhr, status, error) {
				$('#name').val('');
				$('#number').val('');
				$('#email').val('');
			}
		});
		return false;
	}
		function formSub4(event) {
		//	alert("formSub4");
			var styles = $('#styles').val();
			var txt = $('#txt').val();
			
			var gid = $('#gid2').val();
			$.ajax({
					
				url : "saveContectCopyPaste",
				dataType : "json",
				type : "POST",

				data : {

					'styles' : styles,
					'txt' : txt,
					'gid' : gid					

				},
				success : function(response) {
					$('#txt').val('');
					$.smallBox({
						title : "success:"+response.success+"\n"+"Duplicated:"+response.duplicate+"\n"+"Invalid:"+response.invalid,content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",color : "#296191",iconSmall : "fa fa-thumbs-up bounce animated",timeout : 4000
										});
					
				},
				error : function(xhr, status, error) {
					$('#txt').val('');
					
				}
			});
			return false;
		}
</script>


<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-sm-12 col-md-12 col-lg-8">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0"
				data-widget-editbutton="false" data-widget-deletebutton="false">
			
				<header>
					<span class="widget-icon"> <i class="fa fa-check"></i>
					</span>
					<h2>Import Contact</h2>

				</header>

				<div>

					<div class="jarviswidget-editbox">

					</div>
					<div class="widget-body">


						<div class="row">
						
						          
							
								<div id="bootstrap-wizard-1" class="col-sm-12">
								<form id="wizard-1" name="wizard-1" novalidate="novalidate"	 method="post" >
									<div class="form-bootstrapWizard">
										<ul class="bootstrapWizard form-wizard">
											<li class="active" data-target="#step1"><a href="#tab1"
												data-toggle="tab"> <span class="step">1</span> <span
													class="title">Select GroupName</span>
											</a></li>
											<li data-target="#step2"><a href="#tab2"
												data-toggle="tab"> <span class="step">2</span> <span
													class="title">Select Option</span>
											</a></li>
											<li data-target="#step3"><a href="#tab3"
												data-toggle="tab"> <span class="step">3</span> <span
													class="title">Fill Data</span>
											</a></li>
											<!-- <li data-target="#step4"><a href="#tab4"
												data-toggle="tab"> <span class="step">4</span> <span
													class="title">Save Form</span>
											</a></li> -->
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="tab-content">
										<div class="tab-pane active" id="tab1">
											<br>
											<h3>
												<strong>Step 1 </strong> - Select group Or Create	.
											</h3>



											<div class="row">
												<fieldset>
													<section>
														<label class="label">Select Small</label> <label
															class="select"> 
															
															<select class="input-sm" id="gid2"
															name="gid2" onchange="changegid()" >
																<option value="">Select Group</option>
																<c:forEach var="all" items="${msg.rows}">
																	<option value=${all.gid}>${all.groupname}</option>

																</c:forEach>

														</select>
														</label> <a href="./group/createGroup.jsp" data-toggle="modal"
															data-target="#remoteModal" class="btn btn-primary"> <i
															class="fa fa-circle-arrow-up fa-lg"></i> Create Group
														</a>

														<div class="modal fade" id="remoteModal" tabindex="-1"
															role="dialog" aria-labelledby="remoteModalLabel"
															aria-hidden="true" style="display: none;">
															<div class="modal-dialog">
																<div class="modal-content"></div>
															</div>
														</div>
													</section>

												</fieldset>


											</div>

										</div>
										<div class="tab-pane" id="tab2">
											<br>
											<h3>
												<strong>Step 2</strong> - Select Option
											</h3>

											<div class="row">
												<fieldset class="demo-switcher-1">
													<legend></legend>

													<div class="form-group">
														<label class="col-md-2 control-label">Select
															Option </label>
														<div class="col-md-10">
															<div class="radio">
																<label> <input type="radio"
																	class="radiobox styles" checked="checked" id="r1"
																	value="r1" name="styles"> <span>Import
																		From File</span>
																</label>
															</div>
															<div class="radio">
																<label> <input type="radio"
																	class="radiobox styles" id="r2" value="r2"
																	name="styles"> <span>Input Manually (One
																		By One)</span>
																</label>
															</div>
															<div class="radio">
																<label> <input type="radio"
																	class="radiobox styles" id="r3" value="r3"
																	name="styles"> <span>Copy & Paste</span>
																</label>
															</div>
												
														</div>
													</div>

												</fieldset>


											</div>
											<div class="widget-body no-padding"></div>
										</div>
										
										</form>
										<div class="tab-pane" id="tab3">
											<br>
											<h3>
												<strong>Step 3</strong> - Enter Data
											</h3>
											<div id="d1">

												<div class="widget-body">

													<fieldset>
                             <form  id="mydropzone2" class="dropzone"
                                         enctype="multipart/form-data" action="saveContect"   method="POST">
                                   
                                <input type="hidden" name="gid" id="gid" value="1">
                                         </form>
													</fieldset>
													
												</div>
											</div>
											<div id="d2">



												<section>
													<label class="label">Select Small</label> <label
														class="input"> <input type="text" name="name"
														id="name" placeholder="Name">
													</label>
												</section>

												<section>
													<label class="label">Select Small</label> <label
														class="input"> <input type="text" name="number"
														id="number" placeholder="Phone Number">
													</label>
												</section>
												<section>
													<label class="label">Select Small</label> <label
														class="input"> <input type="text" name="email"
														id="email" placeholder="Email">
													</label>
												</section>


												<section>
													<label class="label"></label> <label class="input"  style="position:  relative;">
														<input type="submit" onclick="formSub3()" value="Save"
														class="btn btn-primary">
													</label>
												</section>




											</div>
											
												<div id="d3">

												<div class="widget-body">

													<fieldset>

														<section>
									<label class="label">Textarea</label>
									<label class="textarea state-error">
										<textarea id="txt" name="txt" rows="5" cols="75"></textarea>
									</label>
									<label class="textarea state-error note note-error"  style="color: red ;font-size: 14px;">
									eg. MobileNumber[mandatory],Name[Optional],Email[Optional]
									</label>
								</section>
													</fieldset>
													<fieldset>
														<input type="submit" onclick="formSub4()" value="Save"
														class="btn btn-primary">
													</fieldset>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="tab4">
											<br>
											<h3>
												<strong>Step 4</strong> - Save Form
											</h3>
											<br>

											<br> <br>
										</div>

										<div class="form-actions">
											<div class="row">
												<div class="col-sm-12">
													<ul class="pager wizard no-margin">
														<!--<li class="previous first disabled">
														<a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
														</li>-->
														<li class="previous disabled"><a
															href="javascript:void(0);" class="btn btn-lg btn-default">
																Previous </a></li>
														<!--<li class="next last">
														<a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
														</li>-->


														<li class="next"><a href="javascript:void(0);"
															class="btn btn-lg txt-color-darken"> Next </a></li>
													</ul>
												</div>
											</div>
										</div>

									</div>
								</div>
							
						</div>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
	

	</div>


</section>

<script type="text/javascript">
	 
	 function changegid(){
		 
		 $("#gid").val($("#gid2").val());
	 }
	pageSetUp();

	var pagefunction = function() {


		 
		Dropzone.autoDiscover = false;
		$("#mydropzone2").dropzone({
			maxFilesize: 25000,
			acceptedFiles:'.xls,.xlsx,.csv',
			dictResponseError: 'Error uploading file!',
			 init: function() {
				    this.on("success", function(file, responseText) {
		
				    	var scc="Success:"+responseText.success+"\n"+" duplicate:"+responseText.duplicate+"\n"+"Invalid:"+responseText.invalid;
				    	  
				      file.previewTemplate.appendChild(document.createTextNode(scc));
				    }),
				    this.on("addedfile", function(file) {
				     // alert(file[0]);
				     $("#loader").html("<b><img height=30 width=30 src='img/ajax-loader.gif'> Please Wait While We cooking your File</b>");
				      });
				    
				  }
			
			
			
		});
		
		
		loadScript("js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js",

		runBootstrapWizard);

		//Bootstrap Wizard Validations

		function runBootstrapWizard() {

			var $validator = $("#wizard-1")
					.validate(
							{
								rules : {
		 txt : {required : true,txt : "Your email address must be in the format of name@domain.com"},fname : {required : true},lname : {required : true},country : {required : true},city : {required : true},postal : {required : true,minlength : 4},wphone : {
										required : true,
										minlength : 10
									},hphone : {
										required : true,
										minlength : 10
									}},
								messages : {
									fname : "Please specify your First name",
									lname : "Please specify your Last name",
									email : {
										required : "We need your email address to contact you",
										email : "Your email address must be in the format of name@domain.com"
									}
								},
								highlight : function(element) {
									$(element).closest('.form-group')
											.removeClass('has-success')
											.addClass('has-error');
								},
								unhighlight : function(element) {
									$(element).closest('.form-group')
											.removeClass('has-error').addClass(
													'has-success');
								},
								errorElement : 'span',
								errorClass : 'help-block',
								errorPlacement : function(error, element) {
									if (element.parent('.input-group').length) {
										error.insertAfter(element.parent());
									} else {
										error.insertAfter(element);
									}
								}
							});

			$('#bootstrap-wizard-1').bootstrapWizard(
					   {
						'tabClass' : 'form-wizard',
						'onNext' : function(tab, navigation, index) {
							var $valid = $("#wizard-1").valid();
							if (!$valid) {
								$validator.focusInvalid();
								return false;
							} else {
								$('#bootstrap-wizard-1').find('.form-wizard')
										.children('li').eq(index - 1).addClass(
												'complete');
								$('#bootstrap-wizard-1').find('.form-wizard')
										.children('li').eq(index - 1).find(
												'.step').html(
												'<i class="fa fa-check"></i>');
							}
						}
					});
		};
	};

	
	function checkDisabled(evt) {
		var val = $("input[name=styles]:checked").val();
		$('#d1').hide();
		$('#d2').hide();
		$('#d3').hide();
		if (val == "r1") {
			//alert("r1")
			$('#d1').show();
		} else if (val == "r2") {
			//alert("r2")
			$('#d2').show();
		} else if (val == "r3") {
			//alert("r2")
			$('#d3').show();
		}
	}
	$('input[name=styles]:radio').change(checkDisabled);

	checkDisabled();

	loadScript("js/plugin/dropzone/dropzone.min.js", pagefunction);
	
</script>
