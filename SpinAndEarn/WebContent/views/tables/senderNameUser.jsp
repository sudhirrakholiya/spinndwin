<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%
	String name = currentUser.getUsername();
     Long sender=currentUser.getDefaltsenderid();
     
%>

<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
</div>

<input type="hidden" value="<%=currentUser.getDefaltsenderid()%>" id="dsender"/>
<section id="widget-grid" class="">

	<div class="row">

		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-senderuser"
				data-widget-editbutton="false">
				
				
				<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>SenderName</strong> </h2>		
							
					<div class="widget-toolbar" role="menu">
						<a href="./user/createSenderName.jsp" data-toggle="modal"
					data-target="#remoteModal" class="btn btn-primary">  <i
			class="fa fa-circle-arrow-up fa-lg"></i>Create Sender Name
		</a>
					</div>
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

				<div>

					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>

					<div class="widget-body no-padding">

						<table id="senderusertable_tabletools"
							class="table table-striped table-bordered table-hover"
							width="100%">
							<thead>
								<tr>
									<th data-class="expand">No</th>
									<th data-class="expand">User Name</th>
									<th data-class="expand">Sender Name</th>
									<th data-class="expand">Assign Date</th>
									<th data-class="expand">Approved Date</th>
									<th data-class="expand">Status</th>
									<th data-class="expand">IsDefault</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>

					</div>

				</div>

			</div>

		</article>

	</div>

</section>

<script type="text/javascript">
	var senderusertable;

	pageSetUp();

	function getdata() {

		senderusertable = $('#senderusertable_tabletools')
				.DataTable(
						{
							"processing" : false,
							"serverSide" : false,
							"bServerSide" : false,
							//   "aoColumns": [null,null,null,null,null,null,{"bVisible": false}],
							// "bdestroy": true,

							//  "bDestroy": true,
							"ajax" : "getSenderName",
							"fnCreatedRow" : function(nRow, aData, iDataIndex) {

								$('td:eq(0)', nRow).html(aData[0]);
								$('td:eq(1)', nRow).html(aData[2]);
								$('td:eq(2)', nRow).html(aData[1]);
								$('td:eq(3)', nRow).html(aData[3]);
								$('td:eq(4)', nRow).html(aData[4]);
								if (aData[5] == '0') {
									$('td:eq(5)', nRow)
											.html(
													'<input type="button" class="btn btn-xs bg-color-blueLight txt-color-white" value="PENDDING">');

								} else if (aData[5] == '1') {
									$('td:eq(5)', nRow)
											.html(
													'<input type="button" class="btn btn-xs bg-color-green txt-color-white" value="APPROVED">');

								}

								else {
									$('td:eq(5)', nRow)
											.html(
													'<input type="button" class="btn btn-xs bg-color-redLight txt-color-white" value="REJECTED">');

								}
								var dstr='<a href="#" id="sid" class="btn btn-success btn-xs"  onclick="makedefault('+ aData[0]+ ' )"> Make Default </a>';
								if(aData[0]!=$.cookie('dsender')){								
								$('td:eq(6)', nRow).html(dstr);
								}else{
									
									$('td:eq(6)', nRow).html('<a href="#"  class="btn btn-primary btn-xs">Default</a>');			
								}
			
							},

							"autoWidth" : true,
							"rowCallback" : function(nRow) {

							},
							"drawCallback" : function(oSettings) {

							}
						});

	}

	var pagefunction = function() {

		$('body').on('hidden.bs.modal', '.modal', function() {
			$(this).removeData('bs.modal');
		});
		getdata();
		$('#remoteModal').on('hidden.bs.modal', function() {
			reloadtable();
		});
	};
	
	function makedefault(id){
	//	alert(id);
	$.get("user/defaultSender/"+id,function(msg){
		
		$("#dsender").val(id);
		$.cookie('dsender', id);
		

		$.smallBox({
			title : "Successfully Set As Default",
			content : "",
			color : "#739E73",
			iconSmall : "fa fa-times",
			timeout : 5000
		});
		reloadtable();
		
	});	
	}

	function reloadtable() {
		
		senderusertable.ajax.reload();		
	}

	loadScript(
			"js/plugin/datatables/jquery.dataTables.min.js",
									function() {
										loadScript(
												"js/plugin/datatables/dataTables.bootstrap.min.js",
															function() {
																loadScript(
																		"js/plugin/datatable-responsive/datatables.responsive.min.js",

																		function() {
																			loadScript(
																					"js/plugin/datatable-responsive/datatables.responsive.min.js",
																					pagefunction)
																		});
															});
			});
</script>
<script type="text/javascript">
	pageSetUp();
	function noAnswer() {

		$.smallBox({
			title : "Sure, as you wish sir...",
			content : "",
			color : "#A65858",
			iconSmall : "fa fa-times",
			timeout : 5000
		});

	};

	function closedthis() {
		$.smallBox({
			title : "Great! You just closed that last alert!",
			content : "This message will be gone in 5 seconds!",
			color : "#739E73",
			iconSmall : "fa fa-cloud",
			timeout : 5000
		});
	};
</script>
