<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script>
var dataTable=null;
$(document).ready(function() {
	
	          dataTable = $('#temp-grid').DataTable( {
	            "processing": true,
	            "serverSide": false,
	            "ajax": "GetTemplate",
	            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
		   
		        	    var btnStr= "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>"+
	                    "Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;"+
	                    "<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"+
	                    "&nbsp;&nbsp;&nbsp;&nbsp;"+
	                    "<button class='btn btn-sm btn-warning' id='deactive-btn'><span class='glyphicon glyphicon-eye-close' aria-hidden='true'></span>&nbsp; Active &nbsp;  </button>";
	            
	                    var btnStr1= "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>"+
	                    "Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;"+
	                    "<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"+
	                    "&nbsp;&nbsp;&nbsp;&nbsp;"+
	                    "<button class='btn btn-sm btn-warning' id='active-btn'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span> Deactive  </button>";
	                    
	                    if(aData[3]==true)
	                    $('td:eq(3)', nRow).html(btnStr1);
	                    else 
	                    	 $('td:eq(3)', nRow).html(btnStr);
	            	
		        },
	        } ); 
	           dataTable.on( 'order.dt search.dt', function () {
	          dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	              cell.innerHTML = i+1;
	          } );
	         } ).draw(); 

$('#temp-grid tbody').on( 'click', '#edit-btn', function () {
   var data = dataTable.row( $(this).parents('tr') ).data();
   $('#tempid').val(data[0]);
   $('#tempname').val(data[1]);
   $('#tempdesc').val(data[2]);
} );

$('#temp-grid tbody').on( 'click', '#delete-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
    $.ajax({
  	  url: "deleteTemplate",
  	  type: "get",
  	  data:"tempid="+data[ 0 ],
  	  success: function(html){
  	 dataTable.ajax.reload();
  	  }
  	});
} ); 

$('#temp-grid tbody').on( 'click', '#deactive-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
    $.ajax({
  	  url: "deactiveTemplate",
  	  type: "get",
  	  data:"tempid="+data[ 0 ],
  	  success: function(html){
  	
  	 dataTable.ajax.reload();
  	  }
  	});
} ); 

$('#temp-grid tbody').on( 'click', '#active-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
    $.ajax({
  	  url: "activeTemplate",
  	  type: "get",
  	  data:"tempid="+data[ 0 ],
  	  success: function(html){
  	 dataTable.ajax.reload();
  	  }
  	});
} );
} );

function updateTemplate()
{
	var tempid=$('#tempid').val();
	var tempname=$('#tempname').val();
	var tempdesc=$('#tempdesc').val();
	$.ajax({
	  	  url: "updateTemplate",
	  	  type: "get",
	  	  data:{tempid:tempid,tempname:tempname,tempdesc:tempdesc},
	  	  success: function(){
	  		dataTable.ajax.reload();
	  		$('#myModal').modal('hide');
	  		$('body').removeClass('modal-open');
	  		$('.modal-backdrop').remove();
	  		$.smallBox({
				title : "Template Successfully Updated.",
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 2000
				});
	  	  }
	  	});
}

function addTemplate()
{
	var tempname=$('#addtempname').val();
    var tempdesc=$('#addtempdesc').val();
	$.ajax({
	  	  url: "addTemplate",
	  	  type: "get",
	  	   data:{tempname:tempname,tempdesc:tempdesc}, 
	  	  success: function(){
	  		dataTable.ajax.reload();
	  		 $("#myModaladd").hide();
	  		$('body').removeClass('modal-open');
	  		$('#myModaladd').modal('hide');
	  		$('.modal-backdrop').remove();
	  		 $('#addtempname').val("");
	  		$('#addtempdesc').val(""); 
	  		$.smallBox({
				title : "Template Successfully Created.",
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 2000
				});
	  	  }
	  	});
}
</script>
</head>
<body>

<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Template Manage</h2>
			<div class="widget-toolbar" role="menu">
			<button class="btn btn-sm btn-danger"  data-toggle="modal" data-target="#myModaladd"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New Template</button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>
			<div class="widget-body no-padding">
				<table id="temp-grid" class="table table-striped table-bordered">
<thead>
<tr>
<th width="5%">Id</th>
<th width="12%">Template Name</th>
 <th>Message</th> 
 <th width="25%">Action</th>
</tr>
</thead>
</table>
			</div>
		</div>
	</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:40%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modify Template</h4>
      </div>
      <div class="modal-body">
      <form method="get">
         <input type="hidden" id="tempid" readonly="readonly" class="form-control">
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Template Name</b></div>
  <div class="col-xs-6 col-sm-6"><input type="text" id="tempname" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Template Descreption</b></div>
   <div class="col-xs-6 col-sm-6"><textarea rows="10" cols="40" id="tempdesc"></textarea></div>
        </div>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="updateTemplate()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
  <div class="modal-dialog" role="document" style="width:40%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New Template</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Template Name</b></div>
   <div class="col-xs-6 col-sm-6"> <input type="text" id="addtempname" class="form-control">
        </div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-4 col-sm-4"><b>Template Descriptions</b></div>
   <div class="col-xs-6 col-sm-6"> <textarea rows="10" cols="40" id="addtempdesc"></textarea>
        </div>
        </div>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="addTemplate()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span>Add Template</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>