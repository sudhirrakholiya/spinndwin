<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%
	String name = currentUser.getUsername();
%>
<c:set var="name" scope="session" value="<%=currentUser.getUsername()%>" />
<c:if test="${name=='admin'}">

	<sql:query var="msg" dataSource="SMS">
                 	select * from sendername
                           
                           </sql:query>


</c:if>


<c:if test="${name!='admin'}">

	<sql:query var="msg" dataSource="SMS">
                 	select * from sendername where uid=<%=currentUser.getUserid()%>

	</sql:query>



</c:if>


<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
					aria-labelledby="remoteModalLabel" aria-hidden="true"
					style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content"></div>
					</div>
				</div>


<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">



			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-senderadmin"
				data-widget-editbutton="false">
				
				
				
<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>Sender Name</strong> <i>  List</i></h2>		
							
					<div class="widget-toolbar" role="menu">
							<a href="./user/createSenderName.jsp" data-toggle="modal"
					data-target="#remoteModal" class="btn btn-primary"> <i
			class="fa fa-circle-arrow-up fa-lg"></i>Create Sender Name
		</a>
		
					</div>
					
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
				

				<div>

					<div class="jarviswidget-editbox">

					</div>
					<div class="widget-body no-padding">

						<table id="datatable_tabletools"
							class="table table-striped table-bordered table-hover"
							width="100%">
							<thead>
								<tr>
									<th data-class="expand">No</th>
									<th data-class="expand">User Name</th>
									<th data-class="expand">Sender Name</th>
									<th data-class="expand">Assign Date</th>
									<th data-class="expand">Approved Date</th>
									<th data-class="expand">Status</th>
									<th>Action</th>
					
								</tr>
							</thead>
							<tbody>

						

							</tbody>
						</table>

					</div>

				</div>

			</div>

		</article>

	</div>

</section>

<script type="text/javascript">
	var senderadmintable;
	

	pageSetUp();

		
	
	function getdata(){
		
		 /* if (senderadmintable)
			 senderadmintable.fnDestroy(); */
//		alert("getdata");
	      senderadmintable=	$('#datatable_tabletools').DataTable({
	        "processing": false,
	        "serverSide": false,
	        "bServerSide": false, 
	     //   "aoColumns": [null,null,null,null,null,null,{"bVisible": false}],
	       // "bdestroy": true,
	        
	      //  "bDestroy": true,
	        "ajax": "getSenderName", "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	        	
	        	$('td:eq(0)', nRow).html(aData[0]);
	        	$('td:eq(1)', nRow).html(aData[2]);
	        	$('td:eq(2)', nRow).html(aData[1]);
	        	$('td:eq(3)', nRow).html(aData[3]);
	        	$('td:eq(4)', nRow).html(aData[4]);
	            //$('td:eq(7)', nRow).html('<div class="btn-group btn-group-justified" style="width:150px;"><a href="./views/media/TinyHitList.jsp?mid='+aData[0]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-success btn-xs">Detail</a><a href="./views/quickMessage.jsp?url=http://'+$(location).attr("hostname")+'/'+aData[5]+'" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</a></div>');
	            		
	            		if(aData[5]=='0')
	            			{
	        			  $('td:eq(5)', nRow).html('<input type="button" class="btn btn-xs bg-color-blueLight txt-color-white" value="PENDDING">');
	        			  
	            		}
	            		else if(aData[5]=='1')
            			{
		        			  $('td:eq(5)', nRow).html('<input type="button" class="btn btn-xs bg-color-green txt-color-white" value="APPROVED">');
		        			  
		            		}
	            		
	            		else
            			{
		        			  $('td:eq(5)', nRow).html('<input type="button" class="btn btn-xs bg-color-redLight txt-color-white" value="REJECTED">');
		        			  
		            		}
	            		
	        			  $('td:eq(6)', nRow).html('<a href="#" id="sid" class="btn btn-primary btn-xs"  onclick="action('+aData[0]+' )"> Action </a>')
	            		
	            		
	        		 //<a class="btn btn-danger btn-xs" href="deleteUser?uid=${all.uid}" onclick="return confirm('Are you sure?');">Delete</a>//approveSenderName?sid=${all.sid}
	            
	            
	            
	        },
	 /*        
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
					"t"+
					"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>", */
	   /*      "oTableTools": {
	        	 "aButtons": [
	             "copy",
	             "csv",
	             "xls",
	                {
	                    "sExtends": "pdf",
	                    "sTitle": "SmartSMS_PDF",
	                    "sPdfMessage": "SmartSMS PDF Export",
	                    "sPdfSize": "letter"
	                },
	             	{
	                	"sExtends": "print",
	                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
	            	}
	             ],
	            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
	        }, */
			"autoWidth" : true,	
			"rowCallback" : function(nRow) {
		
			},
			"drawCallback" : function(oSettings) {
		
			}
		});
		
		
	}
	 
			function action(e) {
			
				//	alert(e);
			$.SmartMessageBox({
				title : "Action",
				content : "This is a confirmation box for Perform action.",
				buttons : '[Close][Approve][Rejected][Edit][Delete][Pendding]'}, function(ButtonPressed) {
				if (ButtonPressed === "Many") {
	
					//alert("many");
				$.smallBox({
						title : "Callback function",
						content : "<i class='fa fa-clock-o'></i> <i>You pressed Yes...</i>",
						
						color : "#659265",
						iconSmall : "fa fa-check fa-2x fadeInRight animated",
						timeout : 4000,
						
					}); 
				}
				if (ButtonPressed === "Approve") {
					var sid = $('#sid').val();
					var option="1";
					$.ajax({
						url : "approveSenderName?sid="+e,
						type : "GET",
						//contentType: "application/json; charset=utf-8", 
						data : {
							'sid' : sid,
							'option':option
						},
						beforeSend : function(response) {
							$('#sendit').attr('disabled', true);
						},
						success : function(response) {
								$.smallBox({
									title : response+ButtonPressed,
									content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>",
									color : "#296191",
									iconSmall : "fa fa-thumbs-up bounce animated",
									timeout : 4000
								});
								reloadsendertable();
						},
						error : function(xhr, status, error) {
							//alert(xhr.responseText);
							$('#sendit').attr('disabled', false);
						}
					});
					return false;
				}  
				if (ButtonPressed == "Rejected") {
					var sid = $('#sid').val();
					var option="2";
					$.ajax({
						url : "approveSenderName?sid="+e,
						type : "GET",
						//contentType: "application/json; charset=utf-8", 
						data : {
							'sid' : sid,
							'option':option
						},
						beforeSend : function(response) {
							$('#sendit').attr('disabled', true);
						},
						success : function(response) {
								$.smallBox({
									title : response+ButtonPressed,
									content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>",
									color : "#296191",
									iconSmall : "fa fa-thumbs-up bounce animated",
									timeout : 4000
								});
								reloadsendertable();
						},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
							$('#sendit').attr('disabled', false);
						}
					});
					return false;
				}
				if (ButtonPressed == "Pendding") {
					var sid = $('#sid').val();
					var option="0";
					$.ajax({
						url : "approveSenderName?sid="+e,
						type : "GET",
						//contentType: "application/json; charset=utf-8", 
						data : {
							'sid' : sid,
							'option':option
						},
						beforeSend : function(response) {
							$('#sendit').attr('disabled', true);
						},
						success : function(response) {
								$.smallBox({
									title : response+ButtonPressed,
									content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>",
									color : "#296191",
									iconSmall : "fa fa-thumbs-up bounce animated",
									timeout : 4000
								});
								reloadsendertable();
						},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
							$('#sendit').attr('disabled', false);
						}
					});
					return false;
				}
			
				if (ButtonPressed == "Delete") {
					
					var sid = $('#sid').val();
					var option="2";
					alert(e)
					$.ajax({
						url : "deleteSenderName?sid="+e,
						type : "GET",
						//contentType: "application/json; charset=utf-8", 
						data : {
							'sid' : sid,
							'option':option
						},
						beforeSend : function(response) {
							$('#sendit').attr('disabled', true);
						},
						success : function(response) {
								$.smallBox({
									title : response+ButtonPressed,
									content : "<i class='fa fa-clock-o'></i> <i>Assign Succefully.</i>",
									color : "#296191",
									iconSmall : "fa fa-thumbs-up bounce animated",
									timeout : 4000
								});
								
								reloadsendertable();
					
						},
						error : function(xhr, status, error) {
							alert(xhr.responseText);
							$('#sendit').attr('disabled', false);
						}
					});
					return false;
				}
				
				
				if (ButtonPressed == "Edit") {
					
					
					/* $(this).html("<a href="./group/createGroup.jsp" data-toggle="modal"data-target="#remoteModal" class="btn btn-primary"> <i class="fa fa-circle-arrow-up fa-lg"></i> Create Group</a>"); */
					
	
					$("#remoteModal").modal({remote:"./user/updateSenderName.jsp?sid="+e,show:'true'});
					
					
					
				}
			
			});
			e.preventDefault();
		};
	var pagefunction = function() {
		
		
		 $('body').on('hidden.bs.modal', '.modal', function () {
			  $(this).removeData('bs.modal');
			});
		 getdata();
		 $('#remoteModal').on('hidden.bs.modal', function () {
			 reloadsendertable();	
			});
		
		//console.log("cleared");

		/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
		 */

		/* BASIC ;*/
		//getdata();
		

		/* COLUMN FILTER  */
	


	};

	// load related plugins
	
	
	function reloadsendertable(){
		senderadmintable.ajax.reload();	
	}

	loadScript(
			"js/plugin/datatables/jquery.dataTables.min.js",
			function() {
				loadScript(
						"js/plugin/datatables/dataTables.colVis.min.js",
									function() {
										loadScript(
												"js/plugin/datatables/dataTables.bootstrap.min.js",
												function(){loadScript(
														"js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js",
																	
																	function() {
																		loadScript(
																				"js/plugin/datatable-responsive/datatables.responsive.min.js",pagefunction)
																	});
									});
						});
			});
</script>
<script type="text/javascript">
	

	pageSetUp();


	function noAnswer() {

		$.smallBox({
			title : "Sure, as you wish sir...",
			content : "",
			color : "#A65858",
			iconSmall : "fa fa-times",
			timeout : 5000
		});

	};

	function closedthis() {
		$.smallBox({
			title : "Great! You just closed that last alert!",
			content : "This message will be gone in 5 seconds!",
			color : "#739E73",
			iconSmall : "fa fa-cloud",
			timeout : 5000
		});
	};

	// pagefunction



	// end pagefunction

	// load bootstrap-progress bar script and run pagefunction
//	loadScript("js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js",
		//	pagefunction);
</script>
