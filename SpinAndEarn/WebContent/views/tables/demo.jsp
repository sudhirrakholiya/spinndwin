<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
<form id="test" action="#" method="post">
        <input class="form-control" type="text" name="name" id="name" />
    
    	<input type="checkbox" name="check" value="yes" />
    
    	<select name="select" class="form-control">
    		<option value="none" selected="selected">None</option>
    		<option value="jquery">jQuery</option>
    		<option value="angularjs">Angular</option>
    		<option value="mine">Mine, of course!</option>
    	</select>
    
    	<textarea class="form-control" name="message" rows="15" cols="30"></textarea>
        <label for="email">Email</label>
        <input class="form-control" type="text" name="email" id="email" />
        <input class="form-control" type="password" name="password" id="password" />
    <p>
        <input type="submit" value="Send" class="btn btn-primary btn-block" />
    </p>
</form>	
<pre id="output"></pre>
</body>
<script>

(function() {
	function toJSONString( form ) {
		var obj = {};
		var elements = form.querySelectorAll( "input, select, textarea" );
		for( var i = 0; i < elements.length; ++i ) {
			var element = elements[i];
			var name = element.name;
			var value = element.value;

			if( name ) {
				obj[ name ] = value;
			}
		}

		return JSON.stringify( obj );
	}

	document.addEventListener( "DOMContentLoaded", function() {
		var form = document.getElementById( "test" );
		var output = document.getElementById( "output" );
		form.addEventListener( "submit", function( e ) {
			e.preventDefault();
			var json = toJSONString( this );
			output.innerHTML = json;

		}, false);

	});

})();
</script>
</html>