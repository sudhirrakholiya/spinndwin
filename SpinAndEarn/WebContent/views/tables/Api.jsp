

<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>


<sql:query var="msg" dataSource="SMS">
                         SELECT * FROM apikey where ishidden=0 AND uid=<%=currentUser.getUserid()%>
			</sql:query>

<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<a href="./user/createApiKey.jsp" data-toggle="modal"
				data-target="#remoteModal" class="btn btn-primary"> <i
				class="fa fa-circle-arrow-up fa-lg"></i> Create Api Key
			</a>

			<!-- MODAL PLACE HOLDER -->
			<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
				aria-labelledby="remoteModalLabel" aria-hidden="true"
				style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content">








						<section id="widget-grid" class="ng-scope">
							<!-- START ROW -->
							<div class="row">

								<!-- NEW COL START -->
								<article
									class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

									<script type="text/javascript">
										$(document).ready(function() {

											$('#datepicker').datepicker();
										});

									
									</script>
									<div class="jarviswidget jarviswidget-sortable" id="wid-id-1"
										data-widget-colorbutton="false" data-widget-editbutton="false"
										data-widget-custombutton="false" role="widget" style="">




										<!-- widget div-->
										<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
											<div class="well no-padding">

												<form name="register" id="register"
													class="smart-form client-form">
													<header>Reseller Registration </header>

													<fieldset>
														<div class="row">
															<section class="col col-6">
																<label class="input"> <i
																	class="icon-append fa fa-user"></i> <input type="text"
																	name="senderName" id="senderName"
																	placeholder="senderName"> <b
																	class="tooltip tooltip-bottom-right">Needed to
																		enter the senderName</b>
																</label>
															</section>

														</div>
														<div>

															<section class="col col-6">
																<footer>
																	<button type="button" onclick="formSub()"
																		class="btn btn-primary">Assign</button>
																	<button type="button" class="btn btn-default"
																		data-dismiss="modal">Close</button>
																</footer>
															</section>

														</div>


													</fieldset>
												</form>
											</div>
										</div>
									</div>
								</article>
							</div>


						</section>


					</div>




				</div>
			</div>

		</div>

	</div>
</div>



<div class="row">
	<div class="col-sm-12"></div>
	

</div>


<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
	data-widget-editbutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-table"></i>
		</span>
		<h2>API Key</h2>

	</header>

	<div>

		<div class="jarviswidget-editbox">

		</div>
		<div class="widget-body no-padding">

			<table id="dt_basic"
				class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr role="row">
						<th>ApiKey</th>
						<th>IP</th>
							<th>Created On</th>
						<th>Action</th>
						
					</tr>
				</thead>
				<tbody>  

					<c:forEach var="all" items="${msg.rows}">


						<tr role="row" class="odd">
							<td>${all.keyvalue}</td>
							<td>${all.ip}</td>
							<td>${all.createdate}</td>
							<td><a href="deleteApi?aid=${all.aid}"
								class="btn btn-danger btn-xs">Delete</td>
						</tr>
					</c:forEach>

				</tbody>

			</table>

		</div>
		<!-- end widget content -->

	</div>
	<!-- end widget div -->

</div>



<div class="row">
	<div class="col-sm-12">

		<div class="well">
			<button class="close" data-dismiss="alert">�</button>
			<h1 class="semi-bold">Send Single,Multiple SMS</h1>
			<p>
				http://<%=request.getServerName()%>:<%=request.getServerPort()==80?"":request.getServerPort()%><%=request.getContextPath() %>/sendSMS?username=${pageContext.request.userPrincipal.name}&message=XXXXXXXXXX&sendername=XYZ&smstype=TRANS&numbers=9898294895,8401246699&apikey=d0002689-c347-4b82-af90-9d3188ad6c6e
			</p>


		</div>
		
		<div class="well">
			<button class="close" data-dismiss="alert">�</button>
			<h1 class="semi-bold">Send Schedule Message</h1>
			<p>
				http://<%=request.getServerName()%>:<%=request.getServerPort()==80?"":request.getServerPort()%><%=request.getContextPath() %>/sendSMS?username=${pageContext.request.userPrincipal.name}&apikey=d0002689-c347-4b82-af90-9d3188ad6c6e&scheduled=yyyymmddhhmm&message=XXXXXXXXXX&sendername=XYZ&smstype=TRANS&numbers=9898294895,8401246699
		<br/> <div class="note">
											<strong>Note:</strong> yyyymmddhhmm --> ex. 201512311254
										</div>
			</p>


		</div>

<div class="well">
			<button class="close" data-dismiss="alert">�</button>
			<h1 class="semi-bold">Send GROUP SMS</h1>
			<p>				
			
			http://<%=request.getServerName()%>:<%=request.getServerPort()==80?"":request.getServerPort()%><%=request.getContextPath() %>/sendSMSByGroupname?username=${pageContext.request.userPrincipal.name}&message=xxxxx&sendername=INFORM&apikey=49bdcd4b-7f8c-462a-8eaa-c2c0ab78d82c&gname=&lt;groupname&gt;&smstype=TRANS
			</p> 


		</div>

<div class="well">
			<button class="close" data-dismiss="alert">�</button>
			<h1 class="semi-bold">Balance Check</h1>
			<p>
				http://<%=request.getServerName()%>:<%=request.getServerPort()==80?"":request.getServerPort()%><%=request.getContextPath() %>/getSMSCredit?username=${pageContext.request.userPrincipal.name}&apikey=d0002689-c347-4b82-af90-9d3188ad6c6e
			</p>


		</div>
		
		<div class="well">
			<button class="close" data-dismiss="alert">�</button>
			<h1 class="semi-bold">Delivery Report</h1>
			<p>
				http://<%=request.getServerName()%>:<%=request.getServerPort()==80?"":request.getServerPort()%><%=request.getContextPath() %>/getDLR?username=${pageContext.request.userPrincipal.name}&msgid=<msgid>&apikey=d0002689-c347-4b82-af90-9d3188ad6c6e
			</p>


		</div>

		<div class="well">
			<button class="close" data-dismiss="alert">�</button>
			<h1 class="semi-bold">Datewise Report</h1>
			<p>
				http://<%=request.getServerName()%>:<%=request.getServerPort()==80?"":request.getServerPort()%><%=request.getContextPath() %>/getDLRReport?username=${pageContext.request.userPrincipal.name}&apikey=d0002689-c347-4b82-af90-9d3188ad6c6e&from=yyyy-mm-dd&to=yyyy-mm-dd&sendername={{sendername}}
			</p>


		</div>		
		
		
		
		

	</div>
</div>





<script type="text/javascript">
var apitable;
function reloadtable() {

	apitable.fnReloadAjax();
}


	pageSetUp();

	var pagefunction = function() {
		

		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic')
				.DataTable(
						{
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
							
							}, "language": {
						        "emptyTable":     '<b><a href="./user/createApiKey.jsp" data-toggle="modal" data-target="#remoteModal" > <i	class="fa fa-circle-arrow-up fa-lg"></i> Create Api Key	</a></b>'
						    },
							"rowCallback" : function(nRow) {
								
							},
							"drawCallback" : function(oSettings) {
								
							}
						});

	

	};


loadScript(
			"js/plugin/datatables/jquery.dataTables.min.js",
									function() {
										loadScript(
												"js/plugin/datatables/dataTables.bootstrap.min.js",
														function (){
															loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js",pagefunction);
																	
																	
											});
			});
</script>
