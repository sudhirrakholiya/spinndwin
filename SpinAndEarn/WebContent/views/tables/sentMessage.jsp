<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%
	String name = currentUser.getUsername();
%>
<c:set var="name" scope="session" value="<%=currentUser.getUsername()%>" />
		
		<style type="text/css">
		
		.tables td {
	text-overflow: ellipsis;
	width: 200px;
	white-space: nowrap;
	overflow: hidden;
	
	padding: 10px;
}


		</style>

<sql:query var="msg" dataSource="SMS">
                 	
		SELECT mobileNumber,message ,STATUS,submitdatetime,sentdatetime,hid FROM smshistory WHERE uid=<%=currentUser.getUserid()%>
                           
                           </sql:query>




                           
<script>

	var t_currentPage=1;
	
	var t_maxPage;
	var t_totalRecords;
	var executeFlag = true;
	function createPagination() {
		executeFlag = false;
		opt1 = {
			callback : myFunction
		};

		opt1.items_per_page = 1;
		opt1.num_display_entries = 10;
		opt1.num_edge_entries = 1;
		opt1.prev_text = 'Prev';
		opt1.next_text = 'Next';
		opt1.current_page = t_currentPage;

		$("#Pagination").pagination(t_maxPage, opt1);
		executeFlag = true;
	}
	
	function myFunction() {
		if (current_page != null) {
			if (executeFlag == true) {
				showPageData(parseInt(current_page) + 1);
			}
		}
		return true;
	}
	function showPageData(pageValue) {

		document.getElementById('page').value = pageValue;
		document.forms[0].submit();
	}
	
	</script>
                           
<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<div class="row">
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
				<a href="./user/createTamplate.jsp" data-toggle="modal"
					data-target="#remoteModal" class="btn btn-primary"> <i
					class="fa fa-circle-arrow-up fa-lg"></i> Create Template
				</a>

				<!-- MODAL PLACE HOLDER -->
				<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
					aria-labelledby="remoteModalLabel" aria-hidden="true"
					style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content"></div>
					</div>
				</div>

			</div>

		</div>
	</div>

</div>

<section id="widget-grid" class="">

	<div class="row">

		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">



			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
				data-widget-editbutton="false">
				
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Export to PDF / Excel</h2>

				</header>

				<div>

					<div class="jarviswidget-editbox">

					</div>
					<div class="widget-body no-padding">

						<table id="datatable_tabletools"
							class="table table-striped table-bordered table-hover"
							width="100%">
							<thead>
								<tr>
								   <th>Hid</th>
									<th >Mobile Number</th>
									<th >Message</th>
									<th >Status</th>
									<th >Submit date</th>
									<th >Sent date</th>


								</tr>
							</thead>
							<tbody>

								<c:forEach var="all" items="${msg.rows}">
								

									

									<tr role="row" class="odd">
										<%-- <td class="sorting_1">${all.uid}</td> --%>

										<%-- <td style="width: 30%"> <c:out value="${all}"/></td> --%>
										<td>${all.hid}</td>
										<td>${all.mobileNumber}</td>
										<td style="word-wrap: break-word;">${all.message}</td>
										<td>${all.status}</td>
										<td>${all.sentdatetime}</td>
										<td>${all.submitdatetime}</td>


																			<%-- <td><a href="./views/media/TinyHitList.jsp?mid=${all.mediaid}" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Detail</td>
									<td><a href="./views/quickMessage.jsp?url=${baseURL}/${all.tinyurl}" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Send SMS</td> --%>

									</tr>
								</c:forEach>


							</tbody>
						</table>
						<div style="width:500px; float: center; margin-left: 10px; display: none; background: #D9EAFB;" id="paginationContainer"> 
                        <div id="Pagination" class="pagination" style="width:auto;float:right;">
                        </div>
                 </div>

					</div>

				</div>

			</div>

		</article>

	</div>


</section>

<script type="text/javascript">


	pageSetUp();

	var pagefunction = function() {

		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;

		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic')
				.dataTable(
						{
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
								// Initialize the responsive datatables helper once.
								if (!responsiveHelper_dt_basic) {
									responsiveHelper_dt_basic = new ResponsiveDatatablesHelper(
											$('#dt_basic'),
											breakpointDefinition);
								}
							},
							"rowCallback" : function(nRow) {
								responsiveHelper_dt_basic
										.createExpandIcon(nRow);
							},
							"drawCallback" : function(oSettings) {
								responsiveHelper_dt_basic.respond();
							}
						});

		/* END BASIC */

		/* COLUMN FILTER  */
		var otable = $('#datatable_fixed_column')
				.DataTable(
						{
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
								// Initialize the responsive datatables helper once.
								if (!responsiveHelper_datatable_fixed_column) {
									responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper(
											$('#datatable_fixed_column'),
											breakpointDefinition);
								}
							},
							"rowCallback" : function(nRow) {
								responsiveHelper_datatable_fixed_column
										.createExpandIcon(nRow);
							},
							"drawCallback" : function(oSettings) {
								responsiveHelper_datatable_fixed_column
										.respond();
							}

						});

		// custom toolbar
		$("div.toolbar")
				.html(
						'<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

		$("#datatable_fixed_column thead th input[type=text]").on(
				'keyup change',
				function() {

					otable.column($(this).parent().index() + ':visible')
							.search(this.value).draw();

				});
		$('#datatable_col_reorder')
				.dataTable(
						{
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
								// Initialize the responsive datatables helper once.  
								if (!responsiveHelper_datatable_col_reorder) {
									responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper(
											$('#datatable_col_reorder'),
											breakpointDefinition);
								}
							},
							"rowCallback" : function(nRow) {
								responsiveHelper_datatable_col_reorder
										.createExpandIcon(nRow);
							},
							"drawCallback" : function(oSettings) {
								responsiveHelper_datatable_col_reorder
										.respond();
							}
						});

		/* END COLUMN SHOW - HIDE */

		/* TABLETOOLS */
		$('#datatable_tabletools')
				.dataTable(
						{

							// Tabletools options: 
							//   https://datatables.net/extensions/tabletools/button_options
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
							"oTableTools" : {
								"aButtons" : [
										"copy",
										"csv",
										"xls",
										{
											"sExtends" : "pdf",
											"sTitle" : "SmartAdmin_PDF",
											"sPdfMessage" : "SmartAdmin PDF Export",
											"sPdfSize" : "letter"
										},
										{
											"sExtends" : "print",
											"sMessage" : "Generated by SmartAdmin <i>(press Esc to close)</i>"
										} ],
								"sSwfPath" : "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
							},
							"autoWidth" : true,
							"preDrawCallback" : function() {
								// Initialize the responsive datatables helper once.
								if (!responsiveHelper_datatable_tabletools) {
									responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper(
											$('#datatable_tabletools'),
											breakpointDefinition);
								}
							},
							"rowCallback" : function(nRow) {
								responsiveHelper_datatable_tabletools
										.createExpandIcon(nRow);
							},
							"drawCallback" : function(oSettings) {
								responsiveHelper_datatable_tabletools.respond();
							}
						});

		/* END TABLETOOLS */

	};

	// load related plugins

	loadScript(
			"js/plugin/datatables/jquery.dataTables.min.js",
			function() {
				loadScript(
						"js/plugin/datatables/dataTables.colVis.min.js",
						function() {
							loadScript(
									"js/plugin/datatables/dataTables.tableTools.min.js",
									function() {
										loadScript(
												"js/plugin/datatables/dataTables.bootstrap.min.js",
												function() {
													loadScript(
															"js/plugin/datatable-responsive/datatables.responsive.min.js",
															pagefunction)
												});
									});
						});
			});
</script>
 <script>
	          t_currentPage = <%=request.getAttribute("page")%>;
	          t_currentPage = parseInt(t_currentPage) - 1;
	          t_maxPage = <%=request.getAttribute("totalRecords")%>;		               
	          if(t_maxPage > 1)
	          {
	            document.getElementById('paginationContainer').style.display = '';
	            createPagination();
	          }
	       </script>
