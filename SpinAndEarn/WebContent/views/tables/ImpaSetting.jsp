<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.sudhir.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">


$(document).ready(function() {
	
	$.ajax({
		url : 'GetImapSettingByCompanyId',
		type: 'GET',
		success:function(data)
		{
			var da=data;
			alert('IMap Setting Successfully Added ')
			if(da.length==0)
		{
		var b = document.createElement('button');
		b.setAttribute('content', 'test content');
		b.setAttribute('id','myBtn');
		b.setAttribute('class', 'btn btn-primary');
		b.innerHTML = 'Add Imap Setting';

		var wrapper = document.getElementById("divWrapper");
		wrapper.appendChild(b);
		
		
		document.getElementById("myBtn").addEventListener("click", addImapSetting);
		}
	else
		{
		var b = document.createElement('button');
		b.setAttribute('content', 'test content');
		b.setAttribute('id','myBtn');
		b.setAttribute('class', 'btn btn-primary');
		b.innerHTML = 'Update Imap Setting';

		var wrapper = document.getElementById("divWrapper");
		wrapper.appendChild(b);
		
		
		document.getElementById("myBtn").addEventListener("click",updateImapSetting);
		$('#inputEmail').val(da[0][1]);
		$('#inputsubject').val(da[0][2]);
		}
		},
		error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		
		}); 
	});

function updateImapSetting()
{
	 var email=$('#inputEmail').val();
	 var pass=$('#inputsubject').val();
	 $.ajax({
			url : 'UpdateImapSetting',
			type: 'GET',
			data:{email:email,pass:pass},
			success:function(data)
			{
				alert('IMap Setting Successfully Updated')
			},
			error: function(e) {
				//called when there is an error
				console.log(e.message);
			  }
			
			}); 
	
}



function addImapSetting()
{
//	alert('Hello...')
	 var email=$('#inputEmail').val();
	var pass=$('#inputsubject').val();
	
	$.ajax({
		url : 'addImapSetting',
		type: 'GET',
		data:{email:email,pass:pass},
		success:function(data)
		{
			alert('IMap Setting Successfully Added');
			$('#divWrapper').html("<button class='btn btn-primary' id='btnupd'>Update Imap Setting</button>");
			document.getElementById("btnupd").addEventListener("click",updateImapSetting);
		},
		error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		
		}); 
}

</script>
</head>
<body>
<div class="bs-example">
    <h1>New Imap Setting</h1>
    <form class="form-horizontal">
    <div id="my">
        <div class="form-group">
            <label class="control-label col-xs-3" for="inputEmail"><b>Email Id</b></label>
            <div class="col-xs-4">
                <input type="text" class="form-control" id="inputEmail" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" for="inputPassword"><b>Password</b></label>
            <div class="col-xs-4">
                <input type="password" class="form-control" id="inputsubject" placeholder="******">
            </div>
        </div>
         <br>
        <div class="form-group" >
            <div class="col-xs-offset-3 col-xs-9" id="divWrapper">
                
               
            </div>
        </div>
        
        </div>
        
          <%
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id=currentUser.getUserid();
		String uname=currentUser.getUsername();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
		out.println("<input type='hidden' id='cmp' value='"+uname+"'>");
        
        
        %>
    </form>
</div>
</body>
</html>