<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Manage Campaign</h2>
			<div class="widget-toolbar" role="menu">
					
			<button class="btn btn-sm btn-danger"   data-toggle="modal" data-target="#addModal"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span> New Campaign</button>
					</div>
		</header>
		<div>
			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">
			<br>
				<table id="example" name="demo"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							 <th>Sr.</th> 
							<th>Campaign</th>
							 <th class="col-5" >Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	
	<!-- View All details -->
<div id="addCompainModal" class="modal fade" role="dialog" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width: 650px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modify Campaign</h4>
      </div>
      <div class="modal-body">
      <form id="updateCompaindForm">
      <div class="row">
  <div class="col-md-4">Campaign</div>
  <div class="col-md-4"><input class="form-control" name="campainId" type="hidden"><input class="form-control" id="compainTxt" name="campainName" type="text"></div>
  <div class="col-md-4"></div>
</div>
</form>
      </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="updateCompagin">Save Campaign</button>
      </div>
    </div>
  </div>
</div>

<script>
			var dataTable=null;
	      
	      $(function(){
	  		$("#btnSerialize").on("click", function(){
	  			var obj = $("#myForm").serializeToJSON({
	  				parseFloat: {
	  					condition: ".number,.money"
	  				}			
	  			});
	  			console.log(obj);
	  			
	  			var jsonString = JSON.stringify(obj);
	  			$("#result").val(jsonString);
	  		})
	  	});
			$(document).ready(function() {
				 
			  dataTable=  $('#example').DataTable( {
			        "ajax": {
			            "url": "GetCampain",
			            "dataSrc": ""
			        },
			        "columns": [
			            { "data": "campainId" },
			            { "data": "campainName" },
			            { "data": "null" , "defaultContent": "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#addCompainModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"}
			        ]
			    } );
			    
			    $('#example tbody').on( 'click', '#edit-btn', function () {
		 			  var  data = dataTable.row( $(this).parents('tr') ).data();
		 			  $("#compainTxt").val(data.campainName)
		 			} );
			} );
			
			$( "#updateCompagin" ).click(function() {
				  var data = $("#updateCompaindForm").serializeFormJSON();
				    console.log(JSON.stringify(data));
				    $.ajax({
					  	  url: 'Updatecompain',
					  	  type: 'POST',
					  	 contentType: "application/json; charset=utf-8",  
				            dataType: "json",  
					  	  data: JSON.stringify(data),
					  	  success: function(data) {
					  		alert("Success :: "+data)
					  	  },
						 error: function(e) {
					 		console.log(e.message);
					 	  }
						} );
		});
			
			$( "#addNewLead" ).click(function() {
				    var data = $("#newLeadForm").serializeFormJSON();
				    console.log(JSON.stringify(data));
				    $.ajax({
					  	  url: 'Updatecompain',
					  	  type: 'POST',
					  	 contentType: "application/json; charset=utf-8",  
				            dataType: "json",  
					  	  data: JSON.stringify(data),
					  	  success: function(data) {
					  		alert("Success :: "+data)
					  	  },
						 error: function(e) {
					 		console.log(e.message);
					 	  }
						} );
			});
			 
			</script>

