<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.sudhir.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W2C//DTD HTML 4.01 Transitional//EN" "http://www.w2.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!--   <link rel="stylesheet" href="js/New_dataTable/buttons.dataTables.min.css" />
    <link rel="stylesheet" href="js/New_dataTable/jquery.dataTables.min.css" /> -->
</head>
<body>
<div class="row">
<div class="col-md-6"><b>Searching Criteria</b></div>
</div>
<br>
<div class="row">
  <div class="col-xs-4 col-sm-2">
     <select class="form-control"  id="category" onchange="reloadtable()"></select>
  </div>
  <div class="col-xs-4 col-sm-2">
       <select class="form-control"  id="telecaller" onchange="reloadtable()"></select>
  </div>
  <div class="clearfix visible-xs-block"></div>
  <div class="col-xs-4 col-sm-2">  
         <select class="form-control"  id="LPState" onchange="reloadtable()"></select>
</div>

  <div class="col-xs-4 col-sm-2"><select class="form-control "  id="AUAll" onchange="reloadtable()">
<option value="All">All</option><option value="Unassigned">Unassigned </option>
<option value="Assign">Assign</option></select></div>

   <div class="col-xs-4 col-sm-2"><select class="form-control" id="ocst" onchange="reloadtable()">
   <option value="Open">Open</option>
   <option value="All">All</option>
  <option value="Close">Close</option>
</select></div>

 <div class="col-xs-4 col-sm-2">
      <br/>
<div class="container" style=" margin-left: -957px;margin-top: 29px;">
  <div class="row">
       <div class="col-lg-12">
      <div class="button-group" id="tag1"> 
  </div>
</div>
  </div>
</div>
  <p><br></p>
  </div>
  
<div class="modal" id="TCAT121">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
          <h4 class="modal-title">Modal title12</h4>
        </div><div class="container"></div>
        <div class="modal-body">
        	  <form method="get">
        <div class="row">
  <div class="col-xs-6 col-md-2">Category</div>
  <div class="col-xs-6 col-md-4"><select class="form-control"  id="TraSelcategory" ></select></div>
  <div class="col-xs-6 col-md-4"> 
   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newcat">New Category</button> </div>
</div>

<div class="row">
  <div class="col-xs-6 col-md-2">&nbsp;</div>
  <br>
  <div class="col-xs-6 col-md-4"><span class="label label-danger">Add Leads To Selected Category.</span></div>
  <div class="col-xs-6 col-md-4">&nbsp;</div>
</div>

 </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="AddInNewCategory('All')">Add In New Category</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
<table id="example" class="table"class="table table-striped table-bordered" >
   <thead>
      <tr>
         <th><input name="select_all" value="1" type="checkbox"></th>
         <th >Name</th>
         <th>Email</th>
         <th>Mobile No.</th>
         <th>Category</th>
         <th>Process State</th>
         <th>Lead State</th>
         <th>Telecaller</th>
         <th>Tag</th>
      </tr>
   </thead>
</table>

<p><hr></p>
</div>

 <div class="row">
  <div class="col-xs-6 col-md-4">
  <div class="btn-group">
  <button type="button" class="btn btn-primary" onclick="deleteAll('All')">Delete All</button>
  <button type="button" class="btn btn-primary" onclick="deleteAll('Selected')">Delete Selected</button>
</div>
  </div>
  
  <div class="col-xs-6 col-md-4">
  <div class="btn-group">
  <button type="button" class="btn btn-primary" onclick="setFlag('All')" data-toggle="modal" data-target="#myModalDemo" >Transfer All</button>
  <button type="button" class="btn btn-primary" onclick="setFlag('Selected')" data-toggle="modal" data-target="#myModalDemo">Transfer Selected</button>
</div>
  </div>
  
   <div class="col-xs-6 col-md-4">
  <div class="btn-group">
  <button type="button" class="btn btn-primary" onclick="setFlag('All')" data-toggle="modal" data-target="#report">Export All Follow Up</button>
</div>
  </div>
</div>

<br>
<div class="row">
 <div class="col-xs-6 col-md-4">
   <div class="btn-group">
  <button type="button" class="btn btn-primary" onclick="reOpenAll('All')">Re Open All</button>
  <button type="button" class="btn btn-primary" onclick="reOpenAll('Selected')">Re Open Selected</button>
</div>

  </div>
  <div class="col-xs-6 col-md-4">
 <div class="btn-group">
  <button type="button" class="btn btn-primary" onclick="setFlag('All')" data-toggle="modal" data-target="#report">Export All</button>
  <button type="button" class="btn btn-primary" onclick="setFlag('Selected')" data-toggle="modal" data-target="#report">Export Selected</button>
</div>
  </div>
  
  <div class="col-xs-6 col-md-4">
  <div class="btn-group">
  <button type="button" class="btn btn-primary" onclick="setFlag('All')" data-toggle="modal" data-target="#TCAT121">Add All In New Cat</button>
  <button type="button" class="btn btn-primary" onclick="setFlag('Selected')" data-toggle="modal" data-target="#TCAT121">Add In New Cat Selected</button>
</div>
  </div>
</div>
 
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are You Sure You want to Delete Lead(s)?</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
       <div class="row">
  <div class="col-xs-12 col-sm-12">
  <b>If You Delete Category It Will Delete Following Data.</b><br>
  <b class="text-danger">1. All Leads are Deleted Permanently.</b><br>
  <b class="text-danger">2. All Follow Ups of Leads are Deleted Permanently.</b><br>
  <b class="text-danger">3. Audio Files of Leads are also Deleted Permanently.</b><br><br>
  <b class="text-warning">If  <kbd>Yes</kbd>  Please Enter Transaction Password otherwise Click on  <kbd>No</kbd>  Button.</b>
  </div>
       </div>
        
       <div class="row">
       <p><br></p>
  <div class="col-xs-6 col-sm-4">Transaction Password</div>
  <div class="col-xs-6 col-sm-3"><input type="password" style="width:150%;" id="mstpass" class="form-control" ></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-primary" onclick="deleteLeadData()">Yes</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
  <div class="modal-dialog" role="document" style="width: 75%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Lead Full Detail</h4>
      </div>
      <div class="modal-body" >
      <form method="get">
       <table border="1" class="table table-bordered">
       <tr>
       <th class="">Id</th>
       <td><label id="lid"></label></td>
       <th class="">Company</th>
       <td><label id="lcomp"></label></td>
       </tr>
       
       <tr>
       <th class="">First Name</th>
       <td><label id="lfname"></label></td>
       <th class="">Last Name</th>
       <td><label id="llname"></label></td>
       </tr>
       
         <tr>
       <th class="">Email Id</th>
       <td><label id="lemail"></label></td>
       <th class="">Mobile No</th>
       <td><label id="lcno"></label></td>
       </tr>
       
        <tr>
       <th class="">Lead State</th>
       <td><label id="pstate"></label></td>
       <th class="">Process State</th>
       <td><label id="lst"></label></td>
       </tr>
       
       <tr>
       <th class="">Register Date</th>
       <td><label id="lregdate"></label></td>
       <th class="">Shedule Date</th>
       <td><label id="lshdate"></label></td>
       </tr>
       
       <tr>
       <th class="">Location</th>
       <td><label id="loc">Ahmedabad,Gujarat,India</label></td>
       <th class="">Company Info.</th>
       <td><label id="cinfo">Bonric Software System(www.bonric.co.in)</label></td>
       </tr>
       
        <tr>
       <th class="">Category</th>
      <td><label id="lcat"></label></td>
       <th class="">Follow Up Data</th>
             <td><button type="button" class="btn btn-primary" id="follow" data-toggle="modal" data-target="#myModal44" onclick="showfollowups()">Follow Up </button></td> 
       </tr>
       </table>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal44" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
  <div class="modal-dialog" role="document" style="width: 70%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">FollowUps Detail</h4>
      </div>
      <div class="modal-body" >
		   <table id="hist-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>Calling Time</th>
							<th>Remark</th>
							<th>Schedule Time</th>
							<th>New Status</th>
							<th>Call Duration</th>
							<th>Call Status</th>
							<th>Audio FIle Name</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="width: 115%;" >
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Lead Data</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         <div class="row">
  <div class="col-xs-2 col-sm-2">First Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="fname" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Last Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="lname" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Email</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="email" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Contact No</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="cno" class="form-control"></div>
        </div>
        
        
        
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Category</div>
  <div class="col-xs-6 col-sm-3" id=categoryEdit></div>
  <div class="col-xs-2 col-sm-2">Lead State</div>
  <div class="col-xs-6 col-sm-3" id="lidst"></div>
        </div>
        
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Company</div>
  <div class="col-xs-6 col-sm-3" id="catled"><input type="text" id="comp" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Web Url</div>
  <div class="col-xs-6 col-sm-3" id="lidst"><input type="text" id="web" class="form-control"></div>
        </div>
        
        <hr>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Telecaller</div>
  <div class="col-xs-6 col-sm-3" id="tcalleredit"></div>

  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-4"><b>Assign Lead To Telecaller</b></div>
  <div class="col-xs-6 col-sm-1"><b></b></div>
</div>

         <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Location</div>
  <div class="col-xs-6 col-sm-3" id=""><input type="text" id="cont" class="form-control"></div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="stat" class="form-control"></div>
  <div class="col-xs-6 col-sm-3" id=""><input type="text" id="cty" class="form-control"></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveChangeLead();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>
<!--Demo  -->
<div class="modal" id="myModalDemo" data-backdrop="static">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
          <h4 class="modal-title">Select Category To Transfer Leads</h4>
        </div><div class="container"></div>
        <div class="modal-body">
         
           <form method="get">
        <div class="row">
  <div class="col-xs-6 col-md-2">Category</div>
  <div class="col-xs-6 col-md-4"><select class="form-control"  id="Selcategory" ></select></div>
  <div class="col-xs-6 col-md-4"> 
 <!--   <a  class="btn btn-primary" data-toggle="modal" href="#myModal202" >Save changes</a> -->
   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newcat">New Category</button> </div>
</div>

<div class="row">
  <div class="col-xs-6 col-md-2">&nbsp;</div>
  <br>
  <div class="col-xs-6 col-md-4"><span class="label label-danger">Transfer Leads To Selected Category.</span></div>
  <div class="col-xs-6 col-md-4">&nbsp;</div>
</div>

 </form>
        </div>
        <div class="modal-footer">
          <a href="#" data-dismiss="modal" class="btn">Close</a>
          <a href="#" class="btn btn-primary" onclick="transfreAll('All')">Transfer Leads</a>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="newcat" data-backdrop="static" tabindex="1060" role="dialog" aria-labelledby="AddCategoryLabel12">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Category</h4>
      </div>
      <div class="modal-body">
      <form method="get">
       
        <div class="row">
  <div class="col-xs-6 col-md-3">Category Name</div>
  <div class="col-xs-6 col-md-5"><input type="text" class="form-control"  id="catname" ></div>
 <!--  <div class="col-xs-6 col-md-4"><span class="label label-danger">Transfer All Leads To Selected Category.</span></div> -->
</div>

 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveCategory()">Create Category</button>
      </div>
    </div>
  </div>
</div>
 

 
 <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="width: 115%;" >
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Lead Data</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         <div class="row">
  <div class="col-xs-2 col-sm-2">First Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="fname" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Last Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="lname" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Email</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="email" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Contact No</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="cno" class="form-control"></div>
        </div>
        
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Category</div>
  <div class="col-xs-6 col-sm-3" id=categoryEdit></div>
  <div class="col-xs-2 col-sm-2">Lead State</div>
  <div class="col-xs-6 col-sm-3" id="lidst"></div>
        </div>
        
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Company</div>
  <div class="col-xs-6 col-sm-3" id="catled"><input type="text" id="comp" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Web Url</div>
  <div class="col-xs-6 col-sm-3" id="lidst"><input type="text" id="web" class="form-control"></div>
        </div>
        
        <hr>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Telecaller</div>
  <div class="col-xs-6 col-sm-3" id="tcalleredit"></div>

  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-4"><b>Assign Lead To Telecaller</b></div>
  <div class="col-xs-6 col-sm-1"><b></b></div>
</div>

         <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Location</div>
  <div class="col-xs-6 col-sm-3" id=""><input type="text" id="cont" class="form-control"></div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="stat" class="form-control"></div>
  <div class="col-xs-6 col-sm-3" id=""><input type="text" id="cty" class="form-control"></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveChangeLead();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ADDcat" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabelmn">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Category To Add Leads</h4>
      </div>
      <div class="modal-body">
        <div class="row">
  <div class="col-xs-6 col-md-2">Category</div>
  <div class="col-xs-6 col-md-4"><select class="form-control"  id="SelcategoryForAdd" ></select></div>
  <div class="col-xs-6 col-md-4"> <button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#newcat">New Category</button></div>
</div>

<div class="row">
      </div>
    </div>
  </div>
</div>
</div>

<div id="report" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Export Leads</h4>
      </div>
      <div class="modal-body">
        <div class="row">
  <div class="col-xs-6 col-sm-3"><b>Select Format</b></div>
  <div class="col-xs-6 col-sm-3"><label><input type="radio" name="format" value="pdf">&nbsp;*.pdf</label></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-6 col-sm-3"><label><input type="radio" name="format" value="csv">&nbsp;*.csv</label></div>
  <div class="col-xs-6 col-sm-3"><label><input type="radio" name="format" value="xls">&nbsp;*.xls</label></div>
</div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" onclick="exportLeads()">Export</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="width: 115%;" >
  <div class="modal-dialog" role="document" style="width: 55%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Lead Data</h4>
      </div>
      <div class="modal-body">
      <form method="get">
         <div class="row">
  <div class="col-xs-2 col-sm-2">First Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="Myfname" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Last Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="Mylname" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Email</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="Myemail" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Contact No</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="Mycno" class="form-control"></div>
        </div>
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Category</div>
  <div class="col-xs-6 col-sm-3" id="Mycatled"></div>
  <div class="col-xs-2 col-sm-2">Lead State</div>
  <div class="col-xs-6 col-sm-3" id="Mylidst"></div>
        </div>
        
        <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Company</div>
  <div class="col-xs-6 col-sm-3" id="catled"><input type="text" id="Mycomp" class="form-control"></div>
  <div class="col-xs-2 col-sm-2">Web Url</div>
  <div class="col-xs-6 col-sm-3" id="lidst"><input type="text" id="Myweb" class="form-control"></div>
        </div>
        
        <hr>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Telecaller</div>
  <div class="col-xs-6 col-sm-3" id="Mytcalleredit"></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-4"><b>Assign Lead To Telecaller</b></div>
  <div class="col-xs-6 col-sm-1"><b></b></div>
</div>

         <hr>
         <div class="row">
  <div class="col-xs-2 col-sm-2">Location</div>
  <div class="col-xs-6 col-sm-3" id=""><input type="text" id="Mycont" class="form-control"></div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="Mystat" class="form-control"></div>
  <div class="col-xs-6 col-sm-3" id=""><input type="text" id="Mycty" class="form-control"></div>
        </div>
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveChangeLead();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>

<%
        Authentication auth2 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser2 = (MediUser) auth2.getPrincipal();
		long id=currentUser2.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        %> 
</body>

<!-- <script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/fnReloadAjax.js></script>  -->

<!-- <script src="js/New_dataTable/buttons.html5.min.js"></script>
<script src="js/New_dataTable/dataTables.buttons.min.js"></script>
<script src="js/New_dataTable/jquery-1.12.4.js"></script>
<script src="js/New_dataTable/jquery.dataTables.min.js"></script>
<script src="js/New_dataTable/jszip.min.js"></script>
<script src="js/New_dataTable/pdfmake.min.js"></script>
<script src="js/New_dataTable/vfs_fonts.js"></script> -->

<Script>
var uid=$('#uid').val();
var cat=null;
var assign=null;
var lstate=null;
var lpstate=null;   
var tcaller=null;
var table=null;
var leadId=null;
var rows_selected = [];
var flag=null;
var proceesState=null;
var comatag=null;

$(document).ready(function() {
	$.ajax({
	  	  url: 'GetCategory',
	  	  type: 'GET',
	  	  data: 'compid='+uid,
	  	  success: function(data) {
	  		var msg=data;
	  		var html="<option value='0'>All Category</option>";
	  		var i=0;
	  		for(i=0;i<msg.length;i++)
	  			{
	  			var val='"'+msg[i][0]+'"';
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#category').html(html);
	  		$('#Selcategory').html(html);
	  		$('#SelcategoryForAdd').html(html);
	  		$("#TraSelcategory").html(html);
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );

	$.ajax({
	  	  url: 'GetTelecaller',
	  	  type: 'GET',
	  	  success: function(data) {
	  		var msg=data;
	  		var html="<option value='0'>All Telecaller</option>";
	  		var i=0;
	  		for(i=0;i<msg.length;i++)
	  			{
	  			var val='"'+msg[i][0]+'"';
	  			var tcaller=msg[i][1];
	  			tcaller.toString();
	  			if(tcaller=='Unassign')
	  			{
	  			}	
	  			else
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#telecaller').html(html);
	  		$('#Mytcalleredit').html("<select class='form-control' id='telecallerEdit'>"+html+"</select>");
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );
		
	$.ajax({
		  url:'getLeadProcessState',
		  type: 'GET',
		  success: function(data) {
			  var msg=data;
		  		var html="<option value='Lead Process'>All Process State</option>";
		  		var i=0;
		  		for(i=0;i<msg.length;i++)
		  			{
		  			var val='"'+msg[i][1]+'"';
		  				html+="<option value="+val+">"+msg[i][1]+"</option>";
		  			}
		  		$('#LPState').html(html);
		  		getLeadParameter();
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		});
	
	$.ajax({
		  url:'getAllTag',
		  type: 'GET',
		  success: function(data) {
			  var msg=data;
		  		var html="<select  class='js-example-basic-multiple'   onchange='reloadtable()' multiple='multiple' id='tag' style='width:350px;'>";
		  		var i=0;
		  		for(i=0;i<msg.length;i++)
		  			{
		  			var val='"'+msg[i][1]+'"';
		  				html+="<option value='"+val+"'>"+msg[i][1]+"</option>";
		  			}
		  		html+="</select>";
		  		console.log(html)
		  		$('#tag1').html(html);
		  		$(".js-example-basic-multiple").select2({placeholder: "All Tag",});
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		});
});
	function leaveChange()
	{
		alert("Hello..."+$("#tag").val())
	}
	

function ShowModel(val)
{
	  $('#newcat').modal('show');
}
function saveCategory()
{
	var catname=$('#catname').val();
	var uid=$('#uid').val();

	$.ajax({
		  url: 'AddCategory',
		  type: 'GET',
		  data: {catname:catname,compid :uid},
		  success: function(data) {
			  $("#newcat").modal('hide');
				$.ajax({
				  	  url: 'GetCategory',
				  	  type: 'GET',
				  	  data: 'compid='+uid,
				  	  success: function(data) {
				  		$("#catname").val("");
				  		var msg=data;
				  		var html="<option value='0'>All Category</option>";
				  		var i=0;
				  		for(i=0;i<msg.length;i++)
				  			{
				  			var val='"'+msg[i][0]+'"';
				  				html+="<option value="+val+">"+msg[i][1]+"</option>";
				  			}
				  		$('#category').html(html);
				  		$('#Selcategory').html(html);
				  		$("#TraSelcategory").html(html);
				  	  },
					 error: function(e) {
				 		console.log(e.message);
				 	  }
					} );
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		});
}

function setLeadProcessSate(proceesState)
{
	var proceesState =proceesState.toString();
	 	$.ajax({
			  url: 'GetLeadState',
			  type: 'GET',
			  success: function(data) {  
				var msg=data;
		  		var html="<option>Lead State</option>";
		  		var i=0;
		  		for(i=0;i<data.length;i++)
		  			{
		  	 	if(data[i]==proceesState)
		  			 html+="<option selected='selected' value='"+data[i]+"'>"+data[i]+"</option>";	
		  		else
		  			html+="<option  value='"+data[i]+"'>"+data[i]+"</option>";
		  			}
		  		$('#Mylidst').html("<select class='form-control' id='lpstate'>"+html+"</select>"); 
			  },
			 error: function(e) {
				console.log(e.message);
			  }
			} );
}

function loadcatleadst()
{
	$.ajax({
	  	  url: 'GetCategory',
	  	  type: 'GET',
	  	  data: 'compid='+uid,
	  	  success: function(data) {
	  		var msg=data;
	  		var html="<option>Select Category</option>";
	  		var i=0;
	  		for(i=0;i<msg.length;i++)
	  			{
	  			var val='"'+msg[i][0]+'"';
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#Mycatled').html("<select class='form-control' id='cat'>"+html+"</select>");
	  		console.log("<select class='form-control' id='llstste'>"+html+"</select>");
	  	  },
		 error: function(e) {
	 		console.log(e.message);
	 	  }
		} );
}

function setval(val)
{
	    var textToFind =val;
	    var dd = document.getElementById('cat');
	    for (var i = 0; i < dd.options.length; i++) {
	        if (dd.options[i].text === textToFind) {
	            dd.selectedIndex = i;
	            break;
	        }
	    }
	    var value = document.getElementById("cat").value;
}

function showfollowups()
{
	 dl=leadId;
	 dtable= $('#hist-grid').DataTable( {
 	        "processing": true,
 	        "serverSide": true,
 	       "bPaginate": true,
 	        "ajax":{
 	            "url": "getFollowupsHistory",
 	           " type": "get",
 	           "data": function ( d ) {
 	        	   d.leadid=dl;
 	           }
 	        },  
	"columnDefs": [ {
        "targets": -1,
        "data":null,
        "defaultContent": "<button id='play-btn' class='btn btn-success'>Audio File</button>"
    }],
	   } );
 	       $('#hist-grid tbody').on( 'click', '#play-btn', function () {
 			  var  data = dtable.row( $(this).parents('tr') ).data();
 		 	 var win = window.open('Sound/'+data[7]+''); 
 			} );
}
	
	function getLeadParameter()
	{
		  cat=$("#category").val();
		  assign=$("#AUAll").val();
		  lstate=$("#ocst").val();
		  lpstate=$("#LPState").val();
		  tcaller=$("#telecaller").val();
		  comatag=$("#tag").val();  
	
		 if (typeof(comatag) == "undefined")
			 comatag=null;
		    table = $('#example').DataTable({
		         "ajax": 
		         {
		        	"url":"GetLeadParameterForAdvanceSearch",	      
		 	        "data": function ( d ) {
		 	        	 d.cat=cat;
						  d.assign=assign;
						  d.lstate=lstate;
						  d.lpstate=lpstate;
						  d.comatag=comatag;
						  d.tcaller=tcaller;
		           }
		 	        },
		 	       "lengthMenu": [[10, 25, 50,100,250,500,1000,2000,5000, -1], [10, 25, 50,100,250,500,1000,2000,5000, "All"]],
	 	      'columnDefs': [{
 		         'targets': 0,
		         'searchable':false,
		         'orderable':false,
		         'width':'14%',
		         'className': 'dt-body-center',
		         'render': function (data, type, full, meta){
		             return '<input type="checkbox">  <button class="btn btn-primary btn-xs" id="btn-edit1"  data-toggle="modal" data-target="#myModal" data-toggle="tooltip" data-placement="bottom" title="Change Lead Data." ><span class="glyphicon glyphicon-pencil"></span></button> <button class="btn btn-danger btn-xs" id="btn-delete" data-toggle="tooltip" data-placement="bottom" title="Delete Lead Data." ><span class="glyphicon glyphicon-trash"></span></button> <button class="btn btn-warning btn-xs" id="btn-view"  data-toggle="modal" data-target="#myModal3" data-toggle="tooltip" data-placement="bottom" title="View Full Lead Data." ><span class="glyphicon glyphicon-list-alt"></span></button>';
		         }
		      }],
		      'order': [1, 'asc'],
		      'rowCallback': function(row, data, dataIndex){
		         var rowId = data[0];
		         var tt=data[8] ;
		         var tag="";
		         if(tt!=null)
					{
	  		            	var msg = tt.split(",");
	  		            	for(var i=0;i<msg.length;i++)
			  				{
			  				if(msg.length!=0)
			  					{
			  					tag=tag.concat("<span class='label label-danger'>"+msg[i]+"</span><br>");
			  					}
			  				}
					}
		         $('td:eq(8)', row).html(tag );
		       
		         if($.inArray(rowId, rows_selected) !== -1){
		            $(row).find('input[type="checkbox"]').prop('checked', true);
		         }
		      }
		   });
		    
		    $('#example tbody').on( 'click', '#btn-delete', function () {
	 			  var  data = table.row( $(this).parents('tr') ).data();
	 		 	 leadId=data[0];
	 		 	 $("#mstpass").val('')
	 			 $("#myModal4").modal('show');
	 			} );
		    
	         $('#example tbody').on( 'click', '#btn-view', function () {
	        	   var data = table.row( $(this).parents('tr') ).data();
	        	    var name=data[1];
		        	var res = name.split(" ");
		        	var uname=$('#cmp').val();
		        	leadId=data[ 0 ];
		        	 $.ajax({
		        	  	  url: "getfollowupsCount",
		        	  	  type: "get",
		        	  	  data:"leadid="+data[ 0 ],
		        	  	  success: function(html){
		        	  	   $("#follow").text('FollowsUp '+html);
		        	  	  }
		        	  	});
		        	
		        	  $.ajax({
	        	  	  url: "GetLeadByLeadId",
	        	  	  type: "get",
	        	  	  data:"lId="+data[ 0 ],
	        	  	  success: function(data){
		        	   var data=eval(data);
		        	   proceesState=data[0][7]
		        	   $('#lid').text(data[0][0]);
		        	   $('#lcomp').text(data[0][10]);
		        	   $('#lfname').text(data[0][1]);
		        	   $('#llname').text(data[0][2]);
		        	   $('#lemail').text(data[0][4]==null?"N/A":data[0][4]);
		        	   $('#lcno').text(data[0][5]==null?"N/A":data[0][5]);
		        	   $('#lcat').text(data[0][3]);
		        	   $('#lst').text(data[0][7]);
		        	   $('#lregdate').text(data[0][8]);
		        	   $('#lshdate').text(data[0][9]==null?"N/A":data[0][9]);
		        	   $('#pstate').text(data[0][7]);
		        	   $('#loc').text(data[0][11])
		        	   $('#cinfo').text(data[0][12]);
	        	  	  }
	        	  	}); 
		        	  var i=0;
	        	} );
	         
	         $('#example tbody').on( 'click', '#btn-edit1', function () {
	        	    var data = table.row( $(this).parents('tr') ).data();
	        	    proceesState= data[ 5 ];
	        	    setLeadProcessSate(proceesState);
	        	     tcaltcal=data[7];
	        	    leadId=data[ 0 ];
	        	    $.ajax({
		        	  	  url: "GetLeadByLeadId",
		        	  	  type: "get",
		        	  	  data:"lId="+data[ 0 ],
		        	  	  success: function(data){
			        	var data=eval(data)
			        	var i=0;
			        	for(i=0;i<=data.length;i++)
			        		{
			        		}
			        	var res =data[0][11].split(",");
			        	var loc =data[0][12];
			        	   $('#Myfname').val(data[0][1]);
			        	   $('#Mylname').val(data[0][2]);
			        	   $('#Myemail').val(data[0][3]==null?"N/A":data[0][4]);
			        	   $('#Mycno').val(data[0][5]);
			        	   $('#Mycomp').val(loc.substring(0,loc.indexOf("(")));
			        	   $('#Myweb').val(loc.substring(loc.indexOf("(")+1,loc.indexOf(")")));
			        	   $('#Mycont').val(res[0]);
			        	   $('#Mystat').val(res[1])
			        	   $('#Mycty').val(res[2]);
			        	 
			        	   $.ajax({
			        		  	  url: 'GetLeadCategoryName',
			        		  	  type: 'GET',
			        		  	  data:'lid='+leadId,
			        		  	  success: function(data) {
			        		  		var msg=data[0];
			        		  		setcaval='';
			        		  		setcaval=msg;
			        		  		setval(setcaval);  
			        		  		setTcval(tcaltcal); 
			        		  	  },
			        			 error: function(e) {
			        		 		console.log(e.message);
			        		 	  }
			        			} );
		        	  	  }
		        	  	}); 
	        	    loadcatleadst();
	        	} );
	         
			   $('#example tbody').on('click', 'input[type="checkbox"]', function(e){
				      var $row = $(this).closest('tr');
				      var data = table.row($row).data();
				      var rowId = data[0];
				      console.log(data[0])
				      Ids+=data[0]+","
				      var index = $.inArray(rowId, rows_selected);

				      if(this.checked && index === -1){
				         rows_selected.push(rowId);
				      } else if (!this.checked && index !== -1){
				         rows_selected.splice(index, 1);
				      }

				      if(this.checked){
				       //  $row.addClass('selected');
				      } else {
				         $row.removeClass('selected');
				      }
				      updateDataTableSelectAllCtrl(table);
				      e.stopPropagation();
				   });

				   $('#example').on('click', 'tbody td, thead th:first-child', function(e){
				      $(this).parent().find('input[type="checkbox"]').trigger('click');
				   });

				   $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
				      if(this.checked){
				         $('#example tbody input[type="checkbox"]:not(:checked)').trigger('click');
				      } else {
				         $('#example tbody input[type="checkbox"]:checked').trigger('click');
				      }
				      // Prevent click event from propagating to parent
				      e.stopPropagation();
				   });
				   table.on('draw', function(){
				      updateDataTableSelectAllCtrl(table);
				   });
	}
	function saveChangeLead()
	{
		var lid=leadId;
	    var fname=$('#Myfname').val();
	   var lname=$('#Mylname').val();
	    var email=$('#Myemail').val();
	   var mob=$('#Mycno').val();
	   var cat=$('#cat').val();
	   var status=$('#lpstate').val();
	   var company= $('#Mycomp').val();
	   var website=$('#Myweb').val();
	   var cont=$('#Mycont').val();
	   var state= $('#Mystat').val();
	   var city=$('#Mycty').val();
	   var tcaller=$("#telecallerEdit").val();
	   var comastr=lid+","+fname+","+lname+","+email+","+mob+","+cat+","+status+","+company+","+website+","+cont+","+state+","+city+","+tcaller;
	    	   $.ajax({
		  	  url: "UpdateLead",
		  	  type: "get",
		  	  data:"comastr="+comastr,
		  	  success: function(html){
		  		$('#myModal').modal('hide');
					$.smallBox({
					title : "Lead Successfullt Updated.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					});
					reloadtable();
		  	  }       
		  	});    
	}
	 function setTcval(val)
	{
		    var textToFind =val;
		    var dd = document.getElementById('telecallerEdit');
		    for (var i = 0; i < dd.options.length; i++) {
		        if (dd.options[i].text === textToFind) {
		            dd.selectedIndex = i;
		            break;
		        }
		    }
		    var value = document.getElementById("telecallerEdit").value;
	} 
	
	function deleteLeadData()
	{
		$.ajax({
		  	  url: "GetUserData",
		  	  type: "get",
		  	  data:"mstPass="+$("#mstpass").val(),
		  	  success: function(html){
		  	var data=html.toString();
		  	if(data=="Success")
		  		{
		  	 	$.ajax({
		  	  	  url: "DeleteLeadHistoryByLeadId",
		  	  	  type: "get",
		  	  	  data:"lId="+leadId,
		  	  	  success: function(html){
		  	  		reloadtable();
		  	  	 $.smallBox({
		  				title : "Lead Data are Successfully Deleted.",
		  				color : "#296191",
		  				iconSmall : "fa fa-thumbs-up bounce animated",
		  				timeout : 4000
		  				});
		  	  	  }
		  	  	});  
		  		}
		  	else
		  		{
		  	 $.smallBox({
					title : "Invalid Transaction Password.",
					color : "#8B0000",
					iconSmall : "fa fa-thumbs-o-down animated",
					timeout : 4000
					});
		  	  }
		  	  }
		}); 	
	}
	
	function setFlag(val)
	{
		flag=val;
	}
	function transfreAll(val)
	{	
	 	flag.toString();
	  	  if(flag=="Selected")
	  	{
	  		 $.each(rows_selected, function(index, rowId){
	 	  		var tcat =$("#Selcategory").val();
	 	  	 $.ajax({
		  	  	  url: "transferLead",
		  	  	  type: "get",
		  	  	  data:{lId:rowId,catId:tcat},
		  	  	  success: function(html){
		  	  		$("#category").prop('selectedIndex',0);
			  		  $("#AUAll").prop('selectedIndex',0);
			  		  $("#ocst").prop('selectedIndex',0);
			  		  $("#LPState").prop('selectedIndex',0);
			  		  $("#telecaller").prop('selectedIndex',0);
			  	  		reloadtable();
		  	  	  }
		  	  	});
	 	      }); 
	  		 $('#myModalDemo').modal('hide');
	  		 $.smallBox({
					title : "Lead(s) are Successfully Transfer.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
	    }
	  	  else
	  		  {
	  		var tcat =$("#Selcategory").val();
	  		table.rows().every( function () {
			    var d = this.data();
			    $.ajax({
		  	  	  url: "transferLead",
		  	  	  type: "get",
		  	  	  data:{lId:d[0],catId:tcat},
		  	  	  success: function(html){
		  	  	  $("#Selcategory").prop('selectedIndex',0);
		  	  	  $("#category").prop('selectedIndex',0);
		  		  $("#AUAll").prop('selectedIndex',0);
		  		  $("#ocst").prop('selectedIndex',0);
		  		  $("#LPState").prop('selectedIndex',0);
		  		  $("#telecaller").prop('selectedIndex',0);
		  	  		reloadtable();
		  	  	('input[type="checkbox"]').prop('checked', false);
		  	  	  }
		  	  	});
			 
			    this.invalidate(); // invalidate the data DataTables has cached for this row
			} );
	  		 $('#myModalDemo').modal('hide');
	  		 $.smallBox({
					title : "Lead(s) are Successfully Transfer.",
					
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
			// Draw once all updates are done
			table.draw();
	  		  }
	}
	
	function openAll(val)
	{	
		var myflag=val;
		myflag.toString();
	  	  if(myflag=="Selected")
	  	{
	  		 $.each(rows_selected, function(index, rowId){
	 	  	 $.ajax({
		  	  	  url: "DeleteLeadHistoryByLeadId",
		  	  	  type: "get",
		  	  	  data:{lId:rowId},
		  	  	  success: function(html){
		  	  		$("#category").prop('selectedIndex',0);
			  		  $("#AUAll").prop('selectedIndex',0);
			  		  $("#ocst").prop('selectedIndex',0);
			  		  $("#LPState").prop('selectedIndex',0);
			  		  $("#telecaller").prop('selectedIndex',0);
			  	  		reloadtable();
		  	  	  }
		  	  	});
	 	      }); 
	  		 $.smallBox({
					title : "Lead(s) are Successfully Deleted.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
	    }
	  	  else
	  		  {
	  		
	  		table.rows().every( function () {
			    var d = this.data();
			    $.ajax({
		  	  	  url: "DeleteLeadHistoryByLeadId",
		  	  	  type: "get",
		  	  	  data:{lId:d[0]},
		  	  	  success: function(html){
		  	  	  $("#category").prop('selectedIndex',0);
		  		  $("#AUAll").prop('selectedIndex',0);
		  		  $("#ocst").prop('selectedIndex',0);
		  		  $("#LPState").prop('selectedIndex',0);
		  		  $("#telecaller").prop('selectedIndex',0);
		  	  		reloadtable();
		  	  	('input[type="checkbox"]').prop('checked', false);
		  	  	  }
		  	  	});
			    this.invalidate(); // invalidate the data DataTables has cached for this row
			} );
	  		 $.smallBox({
					title : "Lead(s) are Successfully Deleted.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
			// Draw once all updates are done
			table.draw();
	  		  }
	}
	
	function deleteAll(val)
	{	
		var myflag=val;
		myflag.toString();
	  	  if(myflag=="Selected")
	  	{
	  		 $.each(rows_selected, function(index, rowId){
	 	  	 $.ajax({
		  	  	  url: "DeleteLeadHistoryByLeadId",
		  	  	  type: "get",
		  	  	  data:{lId:rowId},
		  	  	  success: function(html){
		  	  		$("#category").prop('selectedIndex',0);
			  		  $("#AUAll").prop('selectedIndex',0);
			  		  $("#ocst").prop('selectedIndex',0);
			  		  $("#LPState").prop('selectedIndex',0);
			  		  $("#telecaller").prop('selectedIndex',0);
			  	  		reloadtable();
		  	  	  }
		  	  	});
	 	      }); 
	  		 $.smallBox({
					title : "Lead(s) are Successfully Deleted.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
	    }
	  	  else
	  		  {
	  		table.rows().every( function () {
			    var d = this.data();
			    $.ajax({
		  	  	  url: "DeleteLeadHistoryByLeadId",
		  	  	  type: "get",
		  	  	  data:{lId:d[0]},
		  	  	  success: function(html){
		  	  	  $("#category").prop('selectedIndex',0);
		  		  $("#AUAll").prop('selectedIndex',0);
		  		  $("#ocst").prop('selectedIndex',0);
		  		  $("#LPState").prop('selectedIndex',0);
		  		  $("#telecaller").prop('selectedIndex',0);
		  	  		reloadtable();
		  	  	('input[type="checkbox"]').prop('checked', false);
		  	  	  }
		  	  	});
			 
			    this.invalidate(); // invalidate the data DataTables has cached for this row
			} );
	  		 $.smallBox({
					title : "Lead(s) are Successfully Deleted.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
			// Draw once all updates are done
			table.draw();
	  		  }
	}
	
	function transfreAllToNewCat(val)
	{	
	 	flag.toString();
	  	  if(flag=="Selected")
	  	{
	  		 $.each(rows_selected, function(index, rowId){
	 	  		var tcat =$("#Selcategory").val();
	 	  	 $.ajax({
		  	  	  url: "transfreLeadsToNewCat",
		  	  	  type: "get",
		  	  	  data:{lId:rowId,catId:tcat},
		  	  	  success: function(html){
		  	  		$("#category").prop('selectedIndex',0);
			  		  $("#AUAll").prop('selectedIndex',0);
			  		  $("#ocst").prop('selectedIndex',0);
			  		  $("#LPState").prop('selectedIndex',0);
			  		  $("#telecaller").prop('selectedIndex',0);
			  	  		reloadtable();
		  	  	  }
		  	  	});
	 	      }); 
	  		 $('#myModalDemo').modal('hide');
	  		 $.smallBox({
					title : "Lead(s) are Successfully Transfer.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
	    }
	  	  else
	  		  {
	  		var tcat =$("#Selcategory").val();
	  		table.rows().every( function () {
			    var d = this.data();
			    $.ajax({
		  	  	  url: "transferLead",
		  	  	  type: "get",
		  	  	  data:{lId:d[0],catId:tcat},
		  	  	  success: function(html){
		  	  	  $("#Selcategory").prop('selectedIndex',0);
		  	  	  $("#category").prop('selectedIndex',0);
		  		  $("#AUAll").prop('selectedIndex',0);
		  		  $("#ocst").prop('selectedIndex',0);
		  		  $("#LPState").prop('selectedIndex',0);
		  		  $("#telecaller").prop('selectedIndex',0);
		  	  		reloadtable();
		  	  	('input[type="checkbox"]').prop('checked', false);
		  	  	  }
		  	  	});
			 
			    this.invalidate(); // invalidate the data DataTables has cached for this row
			} );
	  		 $('#myModalDemo').modal('hide');
	  		 $.smallBox({
					title : "Lead(s) are Successfully Transfer.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
			// Draw once all updates are done
			table.draw();
	  		  }
	}
	
	function reOpenAll(val)
	{
		var myflag=val;
		myflag.toString();
	  	  if(myflag=="Selected")
	  	{
	  		 $.each(rows_selected, function(index, rowId){
	 	  	 $.ajax({
		  	  	  url: "reOPenLead",
		  	  	  type: "get",
		  	  	  data:{lId:rowId},
		  	  	  success: function(html){
		  	  		$("#category").prop('selectedIndex',0);
			  		  $("#AUAll").prop('selectedIndex',0);
			  		  $("#ocst").prop('selectedIndex',0);
			  		  $("#LPState").prop('selectedIndex',0);
			  		  $("#telecaller").prop('selectedIndex',0);
			  	  		reloadtable();
		  	  	  }
		  	  	});
	 	        
	 	      }); 
	  		 $('#myModalDemo').modal('hide');
	  		 $.smallBox({
					title : "Lead(s) are Re-Opened Successfully.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
	    }
	  	  else
	  		  {
	  		table.rows().every( function () {
			    var d = this.data();
			 console.log(d[0])
			    $.ajax({
		  	  	  url: "reOPenLead",
		  	  	  type: "get",
		  	  	  data:{lId:d[0]},
		  	  	  success: function(html){
		  	  	  $("#category").prop('selectedIndex',0);
		  		  $("#AUAll").prop('selectedIndex',0);
		  		  $("#ocst").prop('selectedIndex',0);
		  		  $("#LPState").prop('selectedIndex',0);
		  		  $("#telecaller").prop('selectedIndex',0);
		  	  		reloadtable();
		  	  	('input[type="checkbox"]').prop('checked', false);
		  	  	  }
		  	  	});
			 
			    this.invalidate(); // invalidate the data DataTables has cached for this row
			} );
	  		 $.smallBox({
					title : "Lead(s) are Re-Opened Successfully.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
			// Draw once all updates are done
			table.draw();
	  		  }
	}
	  		
	function AddInNewCategory()
	{
		var myflag=flag;
		myflag.toString();
	  	  if(myflag=="Selected")
	  	{
		  		$.each(rows_selected, function(index, rowId){
			    var tcat =$("#TraSelcategory").val();
		 	  	 $.ajax({
			  	  	  url: "transfreLeadsToNewCat",
			  	  	  type: "get",
			  	  	 data:{lId:rowId,tcat:tcat},
			  	  	  success: function(html){
			  	  		$("#category").prop('selectedIndex',0);
				  		  $("#AUAll").prop('selectedIndex',0);
				  		  $("#ocst").prop('selectedIndex',0);
				  		  $("#LPState").prop('selectedIndex',0);
				  		  $("#telecaller").prop('selectedIndex',0);
				  	  		reloadtable();
			  	  	  }
			  	  	});
		 		  
		 	      }); 
	  		
	  		 $('#TCAT121').modal('hide');
		 	  	$.smallBox({
					title : "Lead(s) are Created Successfully.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					}); 
	  	}
	  	  else
	  		  {
	  		table.rows().every( function () {
			    var d = this.data();
			    var tcat =$("#TraSelcategory").val();
			 console.log(d[0])
			    $.ajax({
		  	  	  url: "transfreLeadsToNewCat",
		  	  	  type: "get",
		  	  	  data:{lId:d[0],tcat:tcat},
		  	  	  success: function(html){
		  	  	  $("#category").prop('selectedIndex',0);
		  		  $("#AUAll").prop('selectedIndex',0);
		  		  $("#ocst").prop('selectedIndex',0);
		  		  $("#LPState").prop('selectedIndex',0);
		  		  $("#telecaller").prop('selectedIndex',0);
		  	  		reloadtable();
		  	  	  }
		  	  	});
			 
			    this.invalidate(); // invalidate the data DataTables has cached for this row
			} );
	 		 $('#TCAT121').modal('hide');
	  		$.smallBox({
				title : "Lead(s) are Created Successfully.",
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 4000
				}); 
	  		  }
	}
	var Ids="";
	var format=null;
	function exportLeads()
	{
		var myflag=flag;
		myflag.toString();
		var i=0;
		 format=$('input[name=format]:checked').val();
	  	  var csvmyflag=format;
	  	csvmyflag.toString();
	  	 if(csvmyflag=="csv")
	  		 document.location.href = 'download.do?lId='+Ids+'&format='+format+'&flag='+myflag+'';	
	  		 else
			      	document.location.href = 'exportLeads?lId='+Ids+'&format='+format+'&flag='+myflag+'';	
	}

	function updateDataTableSelectAllCtrl(table){
		   var $table             = table.table().node();
		   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
		   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
		   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

		   // If none of the checkboxes are checked
		   if($chkbox_checked.length === 0){
		      chkbox_select_all.checked = false;
		      if('indeterminate' in chkbox_select_all){
		         chkbox_select_all.indeterminate = false;
		      }

		   // If all of the checkboxes are checked
		   } else if ($chkbox_checked.length === $chkbox_all.length){
		      chkbox_select_all.checked = true;
		      if('indeterminate' in chkbox_select_all){
		         chkbox_select_all.indeterminate = false;
		      }

		   // If some of the checkboxes are checked
		   } else {
		      chkbox_select_all.checked = true;
		      if('indeterminate' in chkbox_select_all){
		         chkbox_select_all.indeterminate = true;
		      }
		   }
		}
	
	function reloadtable(){	
		var newcomatag=$("#tag").val();
		var s="";
		 if (typeof(comatag) == "undefined")
			 newcomatag=null;
		  else
			  {
			  newcomatag=$("#tag").val();
			  if(newcomatag!=null)
			  s=newcomatag.join(", ");
			  }
		  cat=$("#category").val();
		  assign=$("#AUAll").val();
		  lstate=$("#ocst").val();
		  lpstate=$("#LPState").val();
		  tcaller=$("#telecaller").val();
		  comatag=s;
		  table.ajax.reload();
		}

</Script>
</html>