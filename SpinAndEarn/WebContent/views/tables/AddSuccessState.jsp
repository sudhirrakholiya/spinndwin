<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.sudhir.model.MediUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
var dataTable=null;
$(document).ready(function() {
	         dataTable = $('#cat-grid').DataTable( {
	            "processing": true,
	            "serverSide": false,
	            "ajax": "GetSuccessStatus",
	                "columnDefs": [ {
	                    "targets": -1,
	                    "data": null,
	                    "defaultContent": "<button class='btn btn-sm btn-success' id='edit-btn' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button>"
	                }],
	                "order": [[ 0, 'desc' ]]
	        } );
	  

$('#cat-grid tbody').on( 'click', '#edit-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
    $('#catid').val(data[0]);
    $('#catval').val(data[1]);
} );

$('#cat-grid tbody').on( 'click', '#delete-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
    $.ajax({
  	  url: "deleteSuccessStae",
  	  type: "get",
  	  data:"catid="+data[ 0 ],
  	  success: function(html){
  		$.smallBox({
			title : "State Successfullt Deleted.",
			color : "#296191",
			iconSmall : "fa fa-thumbs-up bounce animated",
			timeout : 4000
			});
	  dataTable.ajax.reload();
  	  }
  	});
} );
} );

dataTable.on( 'order.dt search.dt', function () {
	 dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
       cell.innerHTML = i+1;
   } );
} ).draw(); 

function saveChangeCategory()
{
var catid=$('#catid').val();
var catname=$('#catval').val();
var uid=$('#uid').val();

$.ajax({
	  url: 'UpdateSuccessState',
	  type: 'GET',
	  data: {catid :catid,catname:catname,compid :uid},
	  success: function(data) {
			$.smallBox({
				title : "State Successfullt Updated.",
				color : "#296191",
				iconSmall : "fa fa-thumbs-up bounce animated",
				timeout : 4000
				});
		  dataTable.ajax.reload();
		  $('#myModal').modal('hide');
	  },
	  error: function(e) {
		console.log(e.message);
	  }
	});
}

function saveCategory()
{
	var catname=$('#catNameAdd').val();
	var uid=$('#uid').val();
	$.ajax({
		  url: 'AddSuccessState',
		  type: 'GET',
		  data: {catname:catname,compid :uid},
		  success: function(data) {
			  $("#catNameAdd").val("");
			  $('#myModal1').modal('hide');
			  dataTable.ajax.reload();
			  $.smallBox({
					title : "Stae Successfully Added.",
					color : "#296191",
					iconSmall : "fa fa-thumbs-up bounce animated",
					timeout : 4000
					});
		  },
		  error: function(e) {
			console.log(e.message);
		  }
		});
}
</script>
</head>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
			
				<span class="widget-icon"> <i class="fa fa-table"></i>
					</span>
					<h2>Manage Success Status</h2>
			<div class="widget-toolbar" role="menu">
						
			<button class="btn btn-sm btn-danger"  data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Success State</button>
					</div>
		</header>
		<div>

			<div class="jarviswidget-editbox"></div>

			<div class="widget-body no-padding">

				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>


						<tr role="row">
							<th>ID</th>
							<th>Name</th>
							<th>Action</th>
							
						</tr>
					</thead>


				</table>

			</div>

		</div>

	</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Status</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         
        <table>
        <tr>
        <td>Id&nbsp;</td>
        <td> <input type="text" id="catid" class="form-control" disabled="disabled"><br></td>
        </tr>
        <tr>
        <td>Name&nbsp;</td>
        <td> <input type="text" id="catval" class="form-control"></td>
        </tr>
        <%
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		long id=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id+"'>");
        
        
        %>
        
        </table>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveChangeCategory();"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Status</h4>
      </div>
      <div class="modal-body">
      <form method="get">
      
         
        <table>
        <tr>
        <td>State Name&nbsp;</td>
        <td> <input type="text" id="catNameAdd" class="form-control"><br></td>
        </tr>
        <%
        Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser1 = (MediUser) auth.getPrincipal();
		long id1=currentUser.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id1+"'>");
        %>
        
        </table>
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveCategory();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Status</button>
      </div>
    </div>
  </div>
</div>

</html>
