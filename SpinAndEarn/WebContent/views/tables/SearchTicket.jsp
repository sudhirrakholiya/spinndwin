<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!-- <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
<script src="js/jquery.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<link href="css/datepicker.css" rel="stylesheet" media="screen"> -->

<script src="js/daterangepicker/daterangepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<style type="text/css">
.timeline {
  list-style: none;
 /*padding: 20px 0 20px;*/
  /*position: relative;*/
  margin-left: -20px;
  
}
.timeline:before {
  top: 0;
  bottom: 0;
  position: absolute;
  content: " ";
  width: 3px;
  background-color: gray;
  left: -0%;
  margin-left:37px;
}
.timeline > li {
  margin-bottom: 20px;
  position: relative;
}
.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}
.timeline > li:after {
  clear: both;
}
.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}
.timeline > li:after {
  clear: both;
}
.timeline > li > .timeline-panel {
  width: 70%;
  float: left;
  left:50px;
  background-color:white;
  border: 1px solid #d4d4d4;
  border-radius: 2px;
  padding: 20px;
  position: relative;
  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
}
.timeline > li > .timeline-panel:before {
  position: absolute;
  top: 26px;
  right: -15px;
  display: inline-block;
  border-top: 15px solid transparent;
  border-left: 15px solid #ccc;
  border-right: 0 solid #ccc;
  border-bottom: 15px solid transparent;
  content: " ";
}
.timeline > li > .timeline-panel:after {
  position: absolute;
  top: 27px;
  right: -14px;
  display: inline-block;
  border-top: 14px solid transparent;
  border-left: 14px solid #fff;
  border-right: 0 solid #fff;
  border-bottom: 14px solid transparent;
  content: " ";
}
.timeline > li > .timeline-badge {
  color: black;
  width: 50px;
  height: 50px;
  line-height: 50px;
  font-size:2em;
  text-align: center;
  position: absolute;
  top: 16px;
  left: ;
  margin-left: -20px;
  background-color: #999999;
  z-index: 100;
  border-top-right-radius: 50%;
  border-top-left-radius: 50%;
  border-bottom-right-radius: 50%;
  border-bottom-left-radius: 50%;
 font-family: "Courier New";
   font-style: normal;
}
.timeline > li.timeline-inverted > .timeline-panel:before {
  border-left-width: 0;
  border-right-width: 15px;
  left: -15px;
  right: auto;
}
.timeline > li.timeline-inverted > .timeline-panel:after {
  border-left-width: 0;
  border-right-width: 14px;
  left: -14px;
  right: auto;
}
</style>
<script>
/* $('#reservation').daterangepicker();
//Date range picker with time picker
$('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'}); */
	
	
	var st=moment().subtract(29, 'days').format('YYYY-MM-DD');
	var ed=moment().format('YYYY-MM-DD');
	console.log(st+"  "+ed);
	 $('#date').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' +moment().format('MMMM D, YYYY'));
$('#daterange-btn').daterangepicker(
        {
        	
        	
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          
          st=start.format('YYYY-MM-DD');
          ed=end.format('YYYY-MM-DD');
          
        }
    );

</script>
<script>
/* $('#datepicker').datepicker({
    format: 'dd-mm-yyyy'
  
})

$('#datepicker1').datepicker({
    format: 'dd-mm-yyyy'
    
}) */
$(document).ready(function() {
	

	$.ajax({
	  	  url: 'GetContactInfo',
	  	  type: 'GET',
	  	  success: function(data) {
	  		
	  		 var msg=data;
	  		var html="<option>--ALL--</option>";
	  		var i=0;
	  		
	  		for(i=0;i<msg.length;i++)
	  			{
	  			//alert(msg[i][0])
	  			console.log('Id='+msg[i][0]);
	  			var val='"'+msg[i][0]+'"';
	  				html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#contactdata').html("Select User<select class='form-control' id='cname'>"+html+"</select>");
	  		
	  		//console.log("<select class='form-control' id='cat'>"+html+"</select>"); */
	  		
	  	  },
		 error: function(e) {
	 		//called when there is an error
	 		console.log(e.message);
	 	  }
		} );
	
	
	 $.ajax({
	  	  url: 'GetAllTickect',
	  	  type: 'GET',
	  	  success: function(data) {
	  	
	  		var data=eval(data)
	  		//	alert(data);
	  		//alert(data.length)
	  		var html="";
	  		var html1="";
	  		html+="<div class='col-md-10'><div class='panel panel-info' style='width:100%;'><div class='panel-heading'>"+name+"&nbsp; Tickets</div><div class='panel-body'>";
	 		html+="<ul class='media-list'>";
	 		var colorArray = ['#9DA7D8', '#4BD0DF', '#9BC86A','#B866C6','#F16091','#E0BFDE','#4E9258','#DCD5BF','#FAEBD7','#C47451'];  
	 		for(i=0;i<data.length;i++)
	 			{
	 			var color = colorArray[Math.floor(Math.random() * colorArray.length)];
	 	
	 			 html1+="<li class='media'><div class='media-body'><div class='media'><a class='pull-left' href='#'><div style=' border-radius: 50%;width:50px;height:50px;padding: 10px;background:"+color+";border: 2px solid "+color+";color:white;text-align: center;font: 25px Courier New, Courier, monospace;'><i class='fa fa-ticket'></i></div>";
	 			 html1+="<a class='pull-right' href='#'><button type='button' class='btn btn-info' data-toggle='modal' data-target='#myModal4' onclick='sowDetail("+data[i][0]+")' ><i class='fa fa-weixin'></i></button></a><div class='media-body' >";
	 			 html1+="</a><div class='media-body' id='chat' >"+data[i][1]+" <br /> <small class='text-muted'>"+data[i][3]+" | "+data[i][2]+"</small>";
				 html1+=" <hr /></div></div></div></li>"; 
				  
	 			}
	 		html+="</ul></div></div></div>";
	 		$('#chat').html(html1);
	 		
	  		
	  	  },
		 error: function(e) {
	 		//called when there is an error
	 		console.log(e.message);
	 	  }
		} );  
});

function findTicket()
{
	var email=$("#cname").val();
	var name=$("#cname :selected").text();
	var status;
	var status=$("#status").val();
	
	var fromdate=$("#datepicker").val();
	/* alert(email)
	alert(todate)
	alert(fromdate) */
	
	//alert(st+"=="+st+" "+ed);
	  $.ajax({
	  	  url: 'GetTickectByEmailId',
	  	  data:{email:email,todate:st,fromdate:ed,status:status},
	  	  type: 'GET',
	  	  success: function(data) {
	  	
	  		var data=eval(data)
	  			alert(data);
	  		alert(data.length)
	  		var html="";
	  		var html1="";
	  		html+="<div class='col-md-10'><div class='panel panel-info' style='width:100%;'><div class='panel-heading'>"+name+"&nbsp; Tickets</div><div class='panel-body'>";
	 		html+="<ul class='media-list'>";
	 		for(i=0;i<data.length;i++)
	 			{
	 	
	 			 html1+="<li class='media'><div class='media-body'><div class='media'><a class='pull-left' href='#'><div style=' border-radius: 50%;width:50px;height:50px;padding: 10px;background:#40b3ff;border: 2px solid #40b3ff;color:white;text-align: center;font: 25px Courier New, Courier, monospace;'><i class='fa fa-ticket'></i></div>";
	 			 html1+="<a class='pull-right' href='#'><button type='button' class='btn btn-info' data-toggle='modal' data-target='#myModal4' onclick='sowDetail("+data[i][0]+")' ><i class='fa fa-weixin'></i></button></a><div class='media-body' >";
	 			 html1+="</a><div class='media-body' id='chat' >"+data[i][1]+" <br /> <small class='text-muted'>"+data[i][3]+" | "+data[i][2]+"</small>";
				 html1+=" <hr /></div></div></div></li>"; 
				  
	 			}
	 		html+="</ul></div></div></div>";
	 		$('#chat').html(html1);
	 		
	  		
	  	  },
		 error: function(e) {
	 		//called when there is an error
	 		console.log(e.message);
	 	  }
		} );   
}

function sowDetail(val)
{
	var tickId=val;
	console.log(tickId)
	
 	$.ajax({
	  	  url: 'GetTicketChatHistory',
	  	  type: 'GET',
	  	  data:'tid='+tickId,
	  	  success: function(data) {
	  		
	  		 var data=eval(data);
	  		var html="";
	  		var i=0;
	  		
	  		for(i=0;i<data.length;i++)
	  			{
	  			console.log(data[i][2])
	  			var start = moment(data[i][2]);
	  			var end=new Date().toJSON().slice(0,10);
	 			var day=Math.abs(start.diff(end, "days"))+2;
	  			var name=data[i][3];
				var firstchar=name.charAt(0).toUpperCase();
	  			html+="<li class='timeline-inverted'>";
	  			html+="<div class='timeline-badge warning'><div style=' border-radius: 50%;width:50px;height:50px;padding: 8px;background:#40b3ff;border: 2px solid #40b3ff;color:white;text-align: center;font: 25px Courier New, Courier, monospace;'><b>"+firstchar+"</b></div></div>";
	  			html+="<div class='timeline-panel'><div class='timeline-heading' style='display: inline-block;'><h4 class='timeline-title' style='display: inline-block;'><font face='Times New Roman'>"+data[i][3]+"</font></h4>";
	  			html+="<div style='display: inline-block;'><span><font size='+0'  color='gray' face='Times New Roman'>&nbsp;reported "+day+" days ago ("+data[i][2]+")</font></span></div></div>";
	  			html+="<div class='timeline-body'><p>"+decodeURI(data[i][1])+"</p> </div>";
	  			html+="<div style='float:right;'><span class='fa fa-pencil' ><font size='2' face='Times New Roman' color='gray'>&nbsp;<b>"+data[i][2]+"</b></font></span></div></div></li>";
	  			}
	  		$('#chat1').html(html)
	  	  },
		 error: function(e) {
	 		//called when there is an error
	 		console.log(e.message);
	 	  }
		} ); 
}
</script>
</head>
<body>
 
                
                <!--  <div class="form-group">
                <label>Date range:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="reservation">
                </div></div> -->
                
                
<!-- <div id="calendar"></div> -->
 <div class="row">
  <div class="col-md-3" id="contactdata">
 
  </div>
  
  <div class="col-md-3"> 
  <div class="input-append date" id="dp3" data-date="09-01-2015" data-date-format="dd-myyyy">
Status
 <select class=form-control  " id='status'>
    <option value="ALL">--ALL--</option>
  <option value="Open">Open</option>
<option value="Pending">Pending</option>
<option value="Resolved">Resolved</option>
<option value="Closed">Closed</option>
<!-- <option value="5">Waiting on Customer</option>
<option value="6">Waiting on Third Party</option>--></select> 
</div>
  
  </div>
  <div class="col-md-3">
  
Date Range<div class="input-group">
                  <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                    <i class="fa fa-calendar"></i> <span id=date></span>
                    <i class="fa fa-caret-down"></i> 
                  </button>
               
</div>
  
  </div>
  <div class="col-md-3">
  <div><br></div><button type="button" class="btn btn-primary" onclick="findTicket()">Find Tickets</button>
  </div>
</div> 
<br><br>
<!-- <div id="tdetail" style="margin-left:120px"  ></div> -->

<div class="panel panel-info" style="width:70%;  margin: 1cm 4cm 3cm 4cm;">
            <div class="panel-heading">
                Ticket List
            </div>
            <div class="panel-body">
             
<ul class="media-list" id="chat"></ul>
            </div>
        </div>
 
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
  <div class="modal-dialog" role="document" style="width: 70%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">FollowUps Detail</h4>
      </div>
      <div class="modal-body" >
		  <ul class="timeline" id="chat1">
    
				
    
   
  </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>
</body>
</html>