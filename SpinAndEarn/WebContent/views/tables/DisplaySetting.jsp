
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/includes/SessionCheck.jsp"%>

<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
			aria-labelledby="remoteModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content"></div>
			</div>
		</div>

<section id="widget-grid" class="">
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-displaysetting"
		data-widget-editbutton="false">
<header role="heading">
						<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2><strong>Display Setting</strong> </h2>		
							
					<div class="widget-toolbar" role="menu">
						<a href="./user/createDisplaySetting.jsp" data-toggle="modal"
			data-target="#remoteModal" class="btn btn-primary"> <i
			class="fa fa-circle-arrow-up fa-lg"></i> Create Display Setting
		</a>
					</div>
				<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
		<div>

			<div class="jarviswidget-editbox">

			</div>
			<div class="widget-body no-padding">

				<table id="displaytable_tabletools"
					class="table table-striped table-bordered table-hover" width="100%">
					<thead>


						<tr role="row">
							<th data-hide="phone" 
								style="width: 27px;">ID</th>
							<th data-class="expand" >User
								Name</th>
							<th>Title</th>
							<th data-hide="phone" >URL</th>
							<th data-hide="phone,tablet" >Image</th>
							<th data-hide="phone,tablet">Home URL</th>
							<th>Action</th>
						</tr>
					</thead>

				</table>

			</div>

		</div>

	</div>

</section>

<script type="text/javascript">

	var displaytable = null;
	pageSetUp();
	function reloadtable() {
		 displaytable.ajax.reload();	
	}
	var pagefunction = function() {
		$('body').on('hidden.bs.modal', '.modal', function() {
			$(this).removeData('bs.modal');
		});
		getdata();
		$('#remoteModal').on('hidden.bs.modal', function() {
			reloadtable();
		});
	};

	loadScript(
			"js/plugin/datatables/jquery.dataTables.min.js",
									function() {
										loadScript(
												"js/plugin/datatables/dataTables.bootstrap.min.js",
															function() {
																loadScript(
																		"js/plugin/datatable-responsive/datatables.responsive.min.js",
																		pagefunction)
															});
										
			});

	function getdata() {
		if (displaytable)
			displaytable.fnDestroy();
		displaytable = $('#displaytable_tabletools')
				.DataTable(
						{
							"processing" : false,
							"serverSide" : false,
							"bServerSide" : false,
							"ajax" : "getDisplaySettings",
							"fnCreatedRow" : function(nRow, aData, iDataIndex) {
								$('td:eq(0)', nRow).html(aData[0]);
								$('td:eq(1)', nRow).html(aData[1]);
								$('td:eq(2)', nRow).html(aData[2]);
								$('td:eq(3)', nRow).html(aData[3]);
								$('td:eq(4)', nRow).html(aData[4]);
								$('td:eq(5)', nRow).html(aData[5]);

								$('td:eq(6)', nRow)
										.html(
												'<div class="btn-group btn-group-justified" style="width:150px;"><a href="./user/updateDisplaySetting.jsp?id='
														+ aData[0]
														+ '" data-toggle="modal" data-target="#remoteModal" class="btn btn-primary btn-xs">Edit</a><a  class="btn btn-danger btn-xs" onclick="delhttp('
														+ aData[0]
														+ ')");">Delete</a></div>');

							},
							"autoWidth" : true,
							"rowCallback" : function(nRow) {

							},
							"drawCallback" : function(oSettings) {

							}
						});

	}

	function delhttp(id) {
		var conf = confirm('Are you sure you want to delete this?');
		if (conf) {
			$.get("deleteDisplaySetting?id=" + id, function(msg) {
				reloadtable();
			});
		}

	}
</script>
