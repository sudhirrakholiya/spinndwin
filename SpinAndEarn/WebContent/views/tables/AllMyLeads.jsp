<style>
#asectDate, #afollow {
	background-color: #C0C0C0;
	color: #C0C0C0;
}
</style>
<div class="jarviswidget jarviswidget-color-blueDark"
	id="wid-id-adduser" data-widget-editbutton="false">
	<header>

		<span class="widget-icon"> <i class="fa fa-table"></i>
		</span>
		<h2>Lead Manage</h2>
		<div class="widget-toolbar" role="menu">

			<button class="btn btn-sm btn-danger" data-toggle="modal"
				data-target="#addModal">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				Add Lead
			</button>
		</div>
	</header>
	<div>
		<div class="jarviswidget-editbox"></div>

		<div class="widget-body no-padding">
			<br>
			<table id="example" name="demo"
				class="table table-striped table-bordered">
				<thead>
					<tr role="row">
						<th>ID</th>
						<th>Client</th>
						<th>Email</th>
						<th>Firm</th>
						<th>Entry</th>
						<th>Login Amt</th>
						<th>Mobile</th>
						<th>Campaign</th>
						<th>Stage</th>
						<th>Priority</th>
						<th>Agent</th>
						<th>Product</th>
						<th>Reference</th>
						<th>Section Date</th>
						<th>Section Amt</th>
						<th class="col-5">Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<!-- View All details -->
<div id="showAllModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content" style="width: 650px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Full Lead Details</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered">
					<tr>
						<td class="success">Client</td>
						<td id="lstname">Lastname</td>
						<td class="success">Email</td>
						<td id="email">Email</td>
					</tr>
					<tr>
						<td class="danger">Firm Name</td>
						<td id="firmname">Doe</td>
						<td class="danger">Mobile</td>
						<td id="mobile"></td>
					</tr>
					<tr>
						<td class="info">Login Date</td>
						<td id="loginyDate">Moe</td>
						<td class="info">Login Loan Amt.</td>
						<td id="lgnAmt"></td>
					</tr>
					<tr>
						<td class="success">Section date</td>
						<td id="sectDate"></td>
						<td class="success">Section Loan Amt.</td>
						<td id="sectAmt"></td>
					</tr>
					<tr>
						<td class="danger">Product</td>
						<td id="product"></td>
						<td class="danger">Reference</td>
						<td id="refrence"></td>
					</tr>
					<tr>
						<td class="info">Priority</td>
						<td id="priority"></td>
						<td class="info">Stage</td>
						<td id="stage"></td>
					</tr>
					<tr>
						<td class="success">Agent</td>
						<td id="agent"></td>
						<td class="success">Entry Date</td>
						<td id="entryDate"></td>
					</tr>
					<tr>
					<tr>
						<td class="danger">Compain</td>
						<td id="compain"></td>
						<td class="danger">Follw-Up Date</td>
						<td id="follow"></td>
					</tr>
					<tr>
						<td class="info">Extra-1</td>
						<td id="extra1"></td>
						<td class="info">Extra-2</td>
						<td id="extra2"></td>
					</tr>
					<tr>
						<td class="success">Extra-3</td>
						<td id="extra3"></td>
						<td class="success">Extra-4</td>
						<td id="extra4"></td>
					</tr>
					<tr>
						<td class="danger">Extra-5</td>
						<td id="extra5"></td>
						<td class="danger">Extra-6</td>
						<td id="extra6"></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Edit All details -->
<div id="changeModal" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width: 1060px;">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modify Lead Details</h4>
			</div>
			<div class="modal-body">
			<form id="updateLeadForm">
				<table class="table table-bordered" border=0>
									<tr>
						<td class="success">Client</td>
						<td id=""><input class="form-control" id="elstname" name="clientName"
							type="text">
							<input class="form-control" id="lId" name="id"
							type="hidden">
							
							</td>
						<td class="success">Email</td>
						<td id=""><input class="form-control" id="eemail" type="text" name="email"></td>
						<td class="danger">Firm Name</td>
						<td id=""><input class="form-control" id="efirmname" name="firmname"
							type="text"></td>
					</tr>
					<tr>

						<td class="danger">Mobile</td>
						<td id=""><input class="form-control" id="emobile" name="mobile"
							type="text"></td>
						<td class="info">Login Date</td>
						<td id=""><input class="form-control" id="eloginyDate" 
							type="text"></td>
						<td class="info">Login Loan Amt.</td>
						<td id=""><input class="form-control" id="elgnAmt" name="lgnAmt"
							type="text"></td>
					</tr>
					<tr>


						<td class="success">Section date</td>
						<td id=""><input class="form-control" id="esectDate" name="sectionDate"
							type="text"></td>
						<td class="success">Section Loan Amt.</td>
						<td id=""><input class="form-control" id="esectAmt" name="sectionAmt"
							type="text"></td>
						<td class="danger">Product</td>
						<td id=""><input class="form-control" id="eproduct" name="product"
							type="text"></td>
					</tr>
					<tr>
						<td class="danger">Reference</td>
						<td id=""><input class="form-control" id="erefrence" name="refrence"
							type="text"></td>
						<td class="info">Priority</td>
						<td id=""><select class="form-control" name="priority"
							id="epriority"></select></td>
						<td class="info">Stage</td>
						<td id=""><select class="form-control" name="stage"
							id="estage"></select></td>
					</tr>
					<tr>
						<td class="success">Agent</td>
						<td id=""><select class="form-control" name="agent"  id="eagent" name="agent"></select></td>
						<td class="success">Entry Date</td>
						<td id=""><input class="form-control" id="eentryDate"
							type="text"></td>
						<td class="danger">Campaign</td>
						<td id=""><select class="form-control" name="compain" name="compain"
							id="ecompain"></select></td>
					</tr>
					<tr>
						<td class="danger">Follw-Up Date</td>
						<td id=""><input class="form-control" id="efollow" name="followUp"
							type="text"></td>
						<td class="info">Extra-1</td>
						<td id=""><input class="form-control" id="eextra1" name="extra1"
							type="text"></td>
						<td class="info">Extra-2</td>
						<td id=""><input class="form-control" id="eextra2" name="extra2"
							type="text"></td>
					</tr>
					<tr>
						<td class="success">Extra-3</td>
						<td id=""><input class="form-control" id="eextra3" name="extra3"
							type="text"></td>
						<td class="success">Extra-4</td>
						<td id=""><input class="form-control" id="eextra4" name="extra4"
							type="text"></td>
						<td class="danger">Extra-5</td>
						<td id=""><input class="form-control" id="eextra5" name="extra5"
							type="text"></td>
					</tr>
					<tr>
					<tr class="noBorder">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							
						</td>
						<td>
						<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						<button id="updateLead" type="button" class="btn btn-primary">Update
								Lead Info</button></td>
					</tr>
				</table>
				</form>
			</div>
		</div>
	</div>
</div>


<!-- Add New details -->
<div id="addModal" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width: 1060px;">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">New Lead</h4>
			</div>
			<div class="modal-body">
				<form action="#" method="post" id="newLeadForm">
					<table class="table table-bordered" border=0>
						<tr>
							<td class="success">Client</td>
							<td id=""><input class="form-control" id="alstname"
								name="clientName" type="text"></td>
							<td class="success">Email</td>
							<td id=""><input class="form-control" for="email"
								name="email" id="aemail" type="email"></td>
							<td class="danger">Firm Name</td>
							<td id=""><input class="form-control" name="firmname"
								id="afirmname" type="text"></td>
						</tr>
						<tr>

							<td class="danger">Mobile</td>
							<td id=""><input class="form-control"
								onkeypress="validate(event)" name="mobile" id="amobile"
								type="text"></td>
							<td class="info">Client Type</td>
							<td id="">
							<select name="clientType" class="form-control">
							<option value="None">Client Type</option>
  <option value="Client">Client</option>
  <option value="Builder">Builder</option>
  <option value="Real Estate Broker">Real Estate Broker</option>
  <option value="Connector">Connector</option>
   <option value="CA">CA</option>
</select>
							</td>
							<td class="info">Login Loan Amt.</td>
							<td id=""><input class="form-control" value="0"
								onkeypress="validate(event)" name="lgnAmt" id="algnAmt"
								type="text"></td>
						</tr>
						<tr>


							<td class="success">Section date</td>
							<td id=""><input type="checkbox" value="" id="sectcheck"><input
								name="sectionDate" class="form-control" id="asectDate"
								type="text"></td>
							<td class="success">Section Loan Amt.</td>
							<td id=""><input class="form-control" value="0"
								onkeypress="validate(event)" name="sectionAmt" id="asectAmt"
								type="text"></td>
							<td class="danger">Product</td>
							<td id="">
							<select name="product" class="form-control">
							<option value="None">Product</option>
  <option value="HL">HL</option>
  <option value="LAP">LAP</option>
  <option value="LRD">LRD</option>
  <option value="CF">CF</option>
   <option value="NRP">NRP</option>
   <option value="BT">BT</option>
   <option value="Top Up">Top Up</option>
</select>
							
							</td>
						</tr>
						<tr>
							<td class="danger">Reference</td>
							<td id=""><input class="form-control" name="refrence"
								id="arefrence" type="text"></td>
							<td class="info">Priority</td>
							<td id=""><select class="form-control" name="priority"
								id="apriority"></select></td>
							<td class="info">Stage</td>
							<td id=""><select class="form-control" name=stage
								id="astage"></select></td>
						</tr>
						<tr>
							<td class="success">Agent</td>
							<td id=""><select class="form-control" name="agent"
								id="aagent"></select></td>
							<td class="success">Entry Date</td>
							<td id=""><input class="form-control" id="aentryDate"
								type="text"></td>
							<td class="danger">Campaign</td>
							<td id=""><select class="form-control" name="compain"
								id="acompain"></select></td>
						</tr>
						<tr>
							<td class="danger">Follw-Up</td>
							<td id=""><input type="checkbox" id="follwcheck" value=""><input
								name="followUp" class="form-control" id="afollow" type="text"></td>
							<td class="info">Extra-1</td>
							<td id=""><input class="form-control" id="aextra1"
								name="extra1" type="text"></td>
							<td class="info">Extra-2</td>
							<td id=""><input class="form-control" id="aextra2"
								name="extra2" type="text"></td>
						</tr>
						<tr>
							<td class="success">Extra-3</td>
							<td id=""><input class="form-control" id="aextra3"
								name="extra3" type="text"></td>
							<td class="success">Extra-4</td>
							<td id=""><input class="form-control" id="aextra4"
								name="extra4" type="text"></td>
							<td class="danger">Extra-5</td>
							<td id=""><input class="form-control" id="aextra5"
								name="extra5" type="text"></td>
						</tr>
						<tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>
								<div class="form-group"></div>
							</td>
							<td><button type="button" class="btn btn-default"
									data-dismiss="modal">Close</button>&nbsp;&nbsp;
								<button type="button" class="btn btn-primary" id="addNewLead">Save
									Lead</button></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Update Schedule Modal -->
<div id="updateScheduleModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Follow Up Date</h4>
      </div>
      <div class="modal-body">
       <div class="row">
  <div class="col-md-4">Follow Up Date</div>
  <div class="col-md-5">
  
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
    <input id="updFolloDate" type="text" class="form-control" name="email" placeholder="Email">
  </div>
  
 </div>
  <div class="col-md-3"></div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="updateFollowUp">Update Follow Up Date</button>
      </div>
    </div>

  </div>
</div>


<script>
	var dataTable = null;
	var leadId ="";
	var tt = '<div class="btn-group">'
			+ ' <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">'
			+ '<i class="fa fa-cog" aria-hidden="flase"></i> <span class="caret"></span>'
			+ '</button>'
			+ ' <ul class="dropdown-menu" role="menu" style="right: 0; left: auto;">'
			+ ' <li><a href="#" id="show-btn" data-toggle="modal" data-target="#showAllModal"><i class="fa fa-arrows-alt" aria-hidden="true"></i>&nbsp;Details</a></li>'
			+ ' <li><a href="#" id="edit-btn"  data-toggle="modal" data-target="#changeModal"><i class="fa fa-pencil-square" aria-hidden="true"></i>&nbsp;Change Lead</a></li>'
			+ '</ul>' + '</div>';

	$(function() {
		$("#btnSerialize").on("click", function() {
			var obj = $("#myForm").serializeToJSON({
				parseFloat : {
					condition : ".number,.money"
				}
			});
			console.log(obj);

			var jsonString = JSON.stringify(obj);
			$("#result").val(jsonString);
		})
	});
	$(document).ready(
			function() {
				
				
				$('#updFolloDate').datetimepicker({
					format : 'YYYY-MM-DD hh:mm',
					defaultDate : new Date(),
				});

				$('#esectDate').datetimepicker({
					format : 'YYYY-MM-DD hh:mm',
					//defaultDate : new Date(),
				});

				$('#efollow').datetimepicker({
					format : 'YYYY-MM-DD hh:mm',
					//defaultDate : new Date(),
				});
				
				$('#asectDate').datetimepicker({
					format : 'YYYY-MM-DD hh:mm',
					defaultDate : new Date(),
				});

				$('#afollow').datetimepicker({
					format : 'YYYY-MM-DD hh:mm',
					defaultDate : new Date(),
				});

				$('#asectDate').on('changeDate', function(ev) {
					$(this).datepicker('hide');
				});

				$('#afollow').on('changeDate', function(ev) {
					$(this).datepicker('hide');
				});

				$('#afollow').attr('readonly', true);
				$('#follwcheck').prop('checked', true);
				$('#follwcheck').on(
						'change',
						function() { // on change of state
							if (this.checked) // if changed state is "CHECKED"
							{
								$("#afollow").css("color", "#C0C0C0");
								$("#afollow")
										.css("background-color", "#C0C0C0");
								$('#afollow').data("DateTimePicker").date(
										'1992-05-29 10:10')
								$('#afollow').attr('readonly', true);
							} else {
								$("#afollow").css("color", "black");
								$("#afollow").css("background-color", "white");
								$('#afollow').data("DateTimePicker").date(
										new Date())
								$('#afollow').attr('readonly', false);
							}

						});

				$('#asectDate').attr('readonly', true);
				$('#sectcheck').prop('checked', true);
				$('#sectcheck').on(
						'change',
						function() { // on change of state
							if (this.checked) // if changed state is "CHECKED"
							{
								$("#asectDate").css("color", "#C0C0C0");
								$("#asectDate").css("background-color",
										"#C0C0C0");
								$('#asectDate').data("DateTimePicker").date(
										'1992-05-29 10:10')
								$('#asectDate').attr('readonly', true);
							} else {
								$("#asectDate").css("color", "black");
								$("#asectDate")
										.css("background-color", "white");
								$('#asectDate').data("DateTimePicker").date(
										new Date())
								$('#asectDate').attr('readonly', false);
							}
						});

				$("#aagent").html(getAgents());
				$("#acompain").html(getCompain())
				$("#apriority").html(getpriority())
				$("#astage").html(getStage())

				$("#eagent").html(getAgents());
				$("#ecompain").html(getCompain())
				$("#epriority").html(getpriority())
				$("#estage").html(getStage())
				dataTable = $('#example').DataTable({
					//  "processing": true,
					//   "serverSide": true,
					//  "bPaginate": true,
					"ajax" : {
						"url" : "GetFinanceLead",
						" type" : "get",
						"data" : function(d) {
							d.name = "sajan";
						}
					},
					"columnDefs" : [ {
						"targets" : -1,
						"data" : null,
						"defaultContent" : tt
					} ],
					/*   "columns": [
				            { data: 4 },
				            { data: 9 },
				            { data: 1 },
				            { data: 6 },
				            { data: 8 },
				            { data: 2 }
				        ]  */
				});

				$('#example tbody').on('click', '#show-btn', function() {
					var data = dataTable.row($(this).parents('tr')).data();
					$("#lstname").html(data[1])
					$("#email").html(data[2])
					$("#firmname").html(data[3])
					$("#mobile").html(data[6])
					$("#loginyDate").html(data[4])
					$("#lgnAmt").html(data[5])
					$("#sectDate").html(data[13])
					$("#sectAmt").html(data[14])
					$("#product").html(data[11])
					$("#refrence").html(data[12])
					$("#project").html(data[1])
					$("#priority").html(data[9])
					$("#stage").html(data[8])
					$("#agent").html(data[10])
					$("#entryDate").html(data[1])
					$("#compain").html(data[8])
					$("#follow").html(data[15])
					$("#extra1").html(data[16])
					$("#extra2").html(data[17])
					$("#extra3").html(data[18])
					$("#extra4").html(data[18])
					$("#extra5").html(data[19])
				});
				
				$('#example tbody').on('click', '#upd-btn', function() {
					var data = dataTable.row($(this).parents('tr')).data();
					leadId=data[0];
				});

				$('#example tbody').on(
						'click',
						'#edit-btn',
						function() {
							var data = dataTable.row($(this).parents('tr'))
									.data();
							$("#lId").val(data[0])
							$("#elstname").val(data[1])
							$("#eemail").val(data[2])
							$("#efirmname").val(data[3])
							$("#emobile").val(data[6])
							$("#eloginyDate").val(data[4])
							$("#elgnAmt").val(data[5])
							$("#esectDate").val(data[13])
							$("#esectAmt").val(data[14])
							$("#eproduct").val(data[11])
							$("#erefrence").val(data[12])
							$("#eproject").val(data[1])
							$(
									"#epriority option:contains("
											+ data[9].toString() + ")").attr(
									"selected", true);
							$(
									"#estage option:contains("
											+ data[8].toString() + ")").attr(
									"selected", true);
							$(
									"#ecompain option:contains("
											+ data[7].toString() + ")").attr(
									"selected", true);
							$(
									"#eagent option:contains("
											+ data[10].toString() + ")").attr(
									"selected", true);
							$("#eentryDate").val(data[1])
							$("#efollow").val(data[15])
							$("#eextra1").val(data[16])
							$("#eextra2").val(data[17])
							$("#eextra3").val(data[18])
							$("#eextra4").val(data[18])
							$("#eextra5").val(data[19])
						});
				
				
			});
	dataTable.on( 'order.dt search.dt', function () {
		dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	    	     cell.innerHTML = i+1;
	    	 } );
	    	} ).draw();
	$("#addNewLead").click(function() {
		if ($('#amobile').val().length != 10) {
			alert('Phone number must be 10 digits.');
		} else {
			var data = $("#newLeadForm").serializeFormJSON();
			console.log(JSON.stringify(data));
			$.ajax({
				url : 'AddLead',
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				data : JSON.stringify(data),
				success : function(data) {
					dataTable.ajax.reload();
					$('#addModal').modal('hide');
					
				},
				error : function(e) {
					dataTable.ajax.reload();
					$('#addModal').modal('hide');
					$('#newLeadForm')[0].reset();
					console.log(e.message);
					
				}
			});
		}
	});
	
	$("#updateLead").click(function() {
		if ($('#emobile').val().length != 10) {
			alert('Phone number must be 10 digits.');
		} else {
			var data = $("#updateLeadForm").serializeFormJSON();
			console.log(JSON.stringify(data));
			  $.ajax({
				url : 'updateLead',
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				data : JSON.stringify(data),
				success : function(data) {
					dataTable.ajax.reload();
					$('#changeModal').modal('hide');
				},
				error : function(e) {
					console.log(e.message);
				}
			});  
		}
	});
	
</script>