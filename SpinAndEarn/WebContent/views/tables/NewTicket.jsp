<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
 <script src="js/plugin/select2/select2.min.js"></script> 
 <script src="js/jquery.form.js"></script> 

<script>
$(document).ready(function() {
	
	
	$.ajax({
	  	  url: 'GetContactInfoDetailList',
	  	  type: 'GET',
	  	  success: function(data) {
	  		  var msg=data;
	  		var html="<option value='' selected disabled>Email Address</option>"; 
	  		for(i=0;i<msg.length;i++)
	  			{
	  			var val='"'+msg[i][3]+'"';
	  			html+="<option value="+val+">"+msg[i][3]+"</option>";
	  			}
	  		$('#ticket_emailAdd').html("<select class='form-control' id='ticket_email'>"+html+"</select>");
	  	$("#ticket_emailAdd").select2();
	  	  },
		 error: function(e) {
	 		//called when there is an error
	 		console.log(e.message);
	 	  }
		} );
	
	
	$.ajax({
	  	  url: 'getDepartment',
	  	  type: 'GET',
	  	  success: function(data) {
	  		  var msg=data;
	  		var html="<option selected disabled>Select Group</option>"; 
	  		for(i=0;i<msg.length;i++)
	  			{
	  			var val='"'+msg[i][0]+'"';
	  			html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#group').html("<select class='form-control' id='ticket_group'>"+html+"</select>");
	  	  },
		 error: function(e) {
	 		//called when there is an error
	 		console.log(e.message);
	 	  }
		} );
	
	
	
	$.ajax({
	  	  url: 'getAgent',
	  	  type: 'GET',
	  	  success: function(data) {
	  		  var msg=data;
	  		 // console.log(msg.toString)
	  		 var html="<option selected disabled>Select Agent</option>"; 
	  		for(i=0;i<msg.length;i++)
	  			{
	  			var val='"'+msg[i][0]+'"';
	  			html+="<option value="+val+">"+msg[i][1]+"</option>";
	  			}
	  		$('#agent').html("<select class='form-control' id='ticket_agent'>"+html+"</select>"); 
	  	  },
		 error: function(e) {
	 		//called when there is an error
	 		console.log(e.message);
	 	  }
		} );
	
	
	/*  $('#zipcode').typeahead({
	      name: 'accounts',
	      local: ['Audi', 'BMW', 'Bugatti', 'Ferrari', 'Ford', 'Lamborghini', 'Mercedes Benz', 'Porsche', 'Rolls-Royce', 'Volkswagen']
	    });
	 */
    
	
	
});


function addTicket(){
	var email=$("#ticket_emailAdd").val();
	var subject=$("#ticket_subject").val();
	var type=$("#ticket_type").val();
	var status=$("#ticket_status").val();
	var priority=$("#ticket_priority").val();
	var group=$("#ticket_group").val();
	var agent=$("#ticket_agent").val();
	var attach=$("#filename").val();
	var desc=encodeURI($("#ticket_desc").val());
	console.log(desc)
	var fullPath =document.getElementById('ticket_exampleInputFile').value;
	var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
	var filename = fullPath.substring(startIndex);
	if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
		filename = filename.substring(1);
	}
	filepath=filename;
	
	console.log("Email : "+email+" "+subject+" "+type+" "+status+" "+priority+" "+group+" "+agent+" "+" "+desc)
	
	   $.ajax({
		url:"addFullTicket",
		type:"GET",
		data:{email:email,subject:subject,type:type,status:status,priority:priority,group:group,agent:agent,filepath:attach,desc:desc},
		success:function(d)
		{
			alert('Ticket Added Successfully...!')
		},
			error:function(f,d,e)
			{
				console.log(f.responseText)
				}
			});   
	 
		 
	     }
	     

$('#ticket_exampleInputFile').change(function() {

	 $('#myForm1').ajaxSubmit({
         success: function(data) { 
       	  $("#filename").val(data)
       }  
   }); 

});
 
 

</script>
</head>
<body>
 <div class="panel panel-default">
  <div class="panel-heading">Create a New Ticket</div>
  <div class="panel-body">
 
 
  
  <div class="row">
  <div class="col-md-6">
 
  <label for="exampleInputPassword1">Search a Requester<span class="required_star" style="color:red; font-size: 20px;">*</span>&nbsp;&nbsp;Or <a href="/helpdesk/tickets/add_requester" class="add_requester_button dialog2" data-width="500" id="add_requester_btn" title="Add New Requester">Add New Requester</a> </label>
   <!--  <input type="text" class="form-control" id="request" placeholder="Email Address"> -->
    <select id="ticket_emailAdd" class="form-control"></select>
  </div>
  
    
  <div class="col-md-6">
  <label for="exampleInputPassword1">Subject<span class="required_star" style="color:red; font-size: 20px;">*</span> </label>
    <input type="text" class="form-control" id="ticket_subject" placeholder="Subject">
  </div>
</div>
<div class="row"><br></div>
 <div class="row">
 <div class="col-md-6">
 <input type="hidden" name="filename" id="filename">
  <label for="exampleInputPassword1">Type<span class="required_star" style="color:red; font-size: 20px;"></span></label>
    <select class="form-control" id="ticket_type" name="helpdesk_ticket[ticket_type]"><option value="">...</option>
<option value="Question" data-id="Question">Question</option>
<option value="Incident" data-id="Incident">Incident</option>
<option value="Problem" data-id="Problem">Problem</option>
<option value="Feature Request" data-id="Feature Request">Feature Request</option>
<option value="Lead" data-id="8000009345">Lead</option></select>
  </div>
  <div class="col-md-6">
 
  <label for="exampleInputPassword1">Status<span class="required_star" style="color:red; font-size: 20px;">*</span> </label>
   <select class="form-control" id="ticket_status" name="helpdesk_ticket[status]" aria-required="true">
<option value="Open" selected="selected">Open</option>
<option value="Pending">Pending</option>
<option value="Resolved">Resolved</option>
<option value="Closed">Closed</option>
<!-- <option value="6">Waiting on Customer</option>
<option value="7">Waiting on Third Party</option> --></select>
  </div>
</div>

<div class="row"><br></div>
 <div class="row">
 <div class="col-md-6">
  <label for="exampleInputPassword1">Priority<span class="required_star" style="color:red; font-size: 20px;">*</span></label>
   <select class="form-control" id="ticket_priority" name="helpdesk_ticket[priority]" aria-required="true">
   <option value="Low" selected="selected">Low</option>
<option value="Medium">Medium</option>
<option value="High">High</option>
<option value="Urgent">Urgent</option></select>
  </div>
  <div class="col-md-6">
  <label for="exampleInputPassword1">Group<span class="required_star" style="color:red; font-size: 20px;"></span> </label>
   <div id="group"></div>
  </div>
</div>
  
  
  <div class="row"><br></div>
 <div class="row">
 <div class="col-md-6">
  <label for="exampleInputPassword1">Agent<span class="required_star" style="color:red; font-size: 20px;">*</span></label>
   <div id="agent"></div>
  </div>
  <div class="col-md-6">
  <label for="exampleInputPassword1">Attachment<span class="required_star" style="color:red; font-size: 20px;"></span> </label>
    <form accept-charset="UTF-8" action="fileUpload"  enctype="multipart/form-data" id="myForm1"  method="post">
    <input type="file" class="form-control" id="ticket_exampleInputFile" name="ticket_exampleInputFile">
   
    </form>
  </div>
</div>

 <div class="row"><br></div>
 <div class="row">
 <div class="col-md-8">
  <label for="exampleInputPassword1">Description<span class="required_star" style="color:red; font-size: 20px;">*</span></label>
   <textarea class="form-control"  rows="5" id="ticket_desc"  ></textarea>
 </div>
</div>
 
 <div><hr></div>
  <button type="button"  onclick="addTicket()"   class="btn btn-default" >Submit</button>

  </div>
</div> 


</body>
</html>