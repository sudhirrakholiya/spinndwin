<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.sudhir.model.MediUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<div class="row">
     <button type="button" class="btn btn-default pull-right" id="daterange-btn"  >
                    <i class="fa fa-calendar"></i> chose Date Range For Log
                    <i class="fa fa-caret-down"></i>
                  </button>
                  
                   
     <div class="col-lg-4" style="margin-left: 3px;"> 
  <div class="row"  >
    <div class="col-xs-6 col-sm-8">  <input type="text" id="searchData" class="form-control"></div> 
  <div class="col-xs-4 col-sm-2" ><button type="button" class="btn btn-primary" onclick="reloadtable()">Search</button>
  </div>
</div>
  </div>
  </div> 
  <br>
<!-- MODAL PLACE HOLDER -->
<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog"
	aria-labelledby="remoteModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 800px;"></div>
	</div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
		data-widget-editbutton="false">
		<header>
			&nbsp;&nbsp;&nbsp;<b> Mobile Call Log</b>

		</header>



		<!-- widget div-->
		<div>

			<!-- widget edit box -->
			<div class="jarviswidget-editbox">
				<!-- This area used as dropdown edit box -->

			</div>
			<!-- end widget edit box -->

			<!-- widget content -->
			<div class="widget-body no-padding">
<table id="example" class="table table-striped table-bordered" style="font-weight:normal;" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Telcaller</th>
                <th>Contact Person</th>
                <th>Mobile No.</th>
                 <th>Call Duration</th>
                <th>Call Status</th>
                 <th>Call Time</th>
                <th>Action</th>
                 <!--  <th>View</th> -->
            </tr>
            </thead>
            <tbody id="data" >
            
              </tbody>
       
       
    </table>   
					
				<table id="example"
					class="table table-striped table-bordered table-hover">
					<thead>	
				</table>

			</div>
			<!-- end widget content -->

		</div>
		<!-- end widget div -->

	</div>
	<!-- end widget -->

	</article>
	<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->


<!-- View Modal -->
 <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel" >
  <div class="modal-dialog" role="document" style="width:60%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Lead</h4>
      </div>
      <div class="modal-body">
      <form method="get" id="lead_from">
    
       <div class="row"  >
  <div class="col-xs-4 col-sm-2">First Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="fname1" class="form-control" placeholder="First Name"></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-2">Last Name</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="lname1" class="form-control" placeholder="Last Name"></div>
</div>
<br>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Email Id</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="eid" class="form-control" placeholder="Email Address"></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-2">Contact No&nbsp;<b style="color:red;">*</b>&nbsp;</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="mobno" readonly="readonly" class="form-control" placeholder="Mobile/Landline No."></div>
</div>
<br>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Category&nbsp;<b style="color:red;">*</b>&nbsp;</div>
  <div class="col-xs-6 col-sm-3" id="category">
  

</div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-2">Shedule Date</div>
  <div class="col-xs-6 col-sm-3">
            <input class="form-control" value="01-01-2016" name="validity" id="validity" type="text" placeholder="Shedule Date">
											
											
  </div>
  
</div>

<br>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Company</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="comp1" class="form-control" placeholder="Company Name"></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-2">Web URL</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="url" class="form-control" placeholder="Website"></div>
</div>

<br>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Telecaller</div>
  <div class="col-xs-6 col-sm-3" id="tcaller"></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-4"><b>Assign Lead To Telecaller</b></div>
  <div class="col-xs-6 col-sm-1"><b></b></div>
</div>

<br>
 <div class="row">
  <div class="col-xs-4 col-sm-2">Location</div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="cont11" class="form-control" placeholder="Country"></div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-4 col-sm-4"><input type="text" id="stat11" class="form-control" placeholder="State"></div>
  <div class="col-xs-6 col-sm-3"><input type="text" id="cty11" class="form-control" placeholder="City"></div>
</div>
      
      
       
        <%
        Authentication auth1 = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser1 = (MediUser) auth1.getPrincipal();
		long id1=currentUser1.getUserid();
		out.println("<input type='hidden' id='uid' value='"+id1+"'>");
        %>
        
       <!--  </table> -->
        
 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveLead();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Lead</button>
      </div>
    </div>
  </div>
</div>

<!-- View Leads Modal -->
<div id="myViewLeadsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Leads</h4>
      </div>
      <div class="modal-body">
       <table id="view_tbl" class="table table-striped table-bordered" style="font-weight:normal;" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Mobile No.</th>
                 <th>Category</th>
                 <th>Email</th>
            </tr>
            </thead>
            <tbody id="data" >
            
              </tbody>
       
       
    </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</section>

<script src="js/daterangepicker.js"></script>
<script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script>

<script>
 var name="NA";
 var mobileNo="NA";
 var email="NA";
 var telecaller="NA"
 var stdate="NA";
 var endate="NA";
 var search="false";
 var dataTable=null;
 var uname="NA";
 var callDue="NA";
 var callSta="NA";
 var date = new Date();
 var dataTable=null;
 var dt=null;
 $('#daterange-btn').daterangepicker(
	        {
	        	format: "yyyy-mm-dd",
	          ranges: {
	            'Today': [moment(), moment()],
	            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	            'This Month': [moment().startOf('month'), moment().endOf('month')],
	            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	          },
	           startDate: moment().subtract(29, 'days'),
	          endDate: moment() 
	        
	       
	        },
	        function (start, end) {
	          $('#daterange-btn').text(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	           stdate=start;
	           endate=end;
	        }
	    );
 
 $('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {
	 
	 if($("#searchData").val().toString()=="")
		 {
	    name="NA";
     	mobileNo="NA";
     	telecaller="NA";
     	uname="NA";
     	callDue="NA";
     	callSta="NA";
		 }
	 else
		 {
		 mobileNo=$("#searchData").val();
	     	telecaller=$("#searchData").val();
	     	uname=$("#searchData").val();
	     	callDue=$("#searchData").val();
	     	callSta=$("#searchData").val();
		 }
		 stdate=picker.startDate.format('YYYY-MM-DD');
		 endate=picker.endDate.format('YYYY-MM-DD');
		 dataTable.ajax.reload();
	});
 
 
  $(document).ready(function() {
	 var date=new Date();
	     
	     dataTable=  $('#example').DataTable( {
		  "processing": true,
	        "serverSide": true, 
	          "searching": false,  
	         // "bLengthChange": false, 
	        
	        "ajax": {
	        	"url":"getAndroidCallLogByCompany",	      
	 	        "data": function ( d ) {
	 	        	d.name=name;
	 	        	d.mobileNo=mobileNo;
	 	        	d.telecaller=telecaller;
	 	        	d.search=search;
	 	        	d.uname=uname;
	 	        	d.callDue=callDue;
	 	        	d.callSta=callSta;
	 	        	d.stdate=stdate;
	 	        	d.endate=endate;
	           }  
	        }, 
	        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	        	
            	$('td:eq(6)', nRow).html('<button class="btn btn-warning" id="edit-btn" data-toggle="modal" data-target="#myModal1" data-placement="bottom">Add To Lead</button>&nbsp;<button type="button" id="view-btn" data-toggle="modal" data-target="#myViewLeadsModal" class="btn btn-info">View Leads</button>');

            	if(aData[4].toString()=="INCOMING")
                	$('td:eq(4)', nRow).html('<span class="label label-primary">INCOMING</span>');
            	else if(aData[4].toString()=="OUTGOING")
                	$('td:eq(4)', nRow).html('<span class="label label-success">OUTGOING</span>');
            	else if(aData[4].toString()=="MISSCALL")
                	$('td:eq(4)', nRow).html('<span class="label label-danger">MISSCALL</span>');

		        }, 
	         "sPaginationType" : "full_numbers",
			  "oLanguage" : {
			  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
			  },
			   "sDom": 'T<"clear">lfrtip', 
			   
			  "oTableTools" : {
			  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			  "aButtons": [
				  {
	 				  "sExtends": "pdf",
	 				  "sTitle" : "Mobile Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
	 				  },
	 				  {
	 					  "sExtends": "csv",
	 					  "sTitle" : "Mobile Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
	 					},
	 					{
	 						  "sExtends": "xls",
	 						  "sTitle" : "Mobile Log  Report"+" On "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
	 					}
				  ],
			  } 
			   
			 
	    } );
	     
	     $('#example tbody').on( 'click', '#edit-btn', function () {
	     	    var data = dataTable.row( $(this).parents('tr') ).data();
	     	   $('#fname1').val(data[1]);
	      	  $('#mobno').val(data[2]);
	     	} );
	     
	     $('#example tbody').on( 'click', '#view-btn', function () {
	     	    var data = dataTable.row( $(this).parents('tr') ).data();
	     	    var mobileNo=data[2];
	     	   if(dt!=null)
	     		   {
	     		  mobileNo=mobileNo;
	     		 dt.ajax.reload();
	 	    	return;
	     		   }
	     	    dt=$('#view_tbl').DataTable( {
	     		  "processing": true,
	     	        "serverSide": true, 
	     	          "searching": false,  
	     	         // "bLengthChange": false, 
	     	        
	     	        "ajax": {
	     	        	"url":"geLeadByMobileNumber",	      
	     	 	        "data": function ( d ) {
	     	 	        	d.mobileNo=mobileNo;
	     	           }  
	     	        }, 
	     	    } );
	     	} );
	     
	     $.ajax({
	    	 url: 'GetCategory',
		  	  type: 'GET',
		  	  data: 'compid='+$("#uid").val(),
			  success: function(msg) {
				  
			//	  alert(msg)
				  var html="<option value='Select Category'>Category</option>";
				 
				  for(i=0;i<msg.length;i++)
		  			{
					  var val='"'+msg[i][0]+'"';
		  				html+="<option value="+val+">"+msg[i][1]+"</option>";
		  			}
				//  console.log("<select class='form-control' id='tcId'>"+html+"</select>")
		  		$('#category').html("<select class='form-control' id='addCat'>"+html+"</select>");
				 
			  },
			  error: function(e) {
				 
				//called when there is an error
				console.log(e.message);
			  }
			});
	     
	     $.ajax({
			  url:'GetTellyCallerBycompanyId',
			  type: 'GET',
			  success: function(msg) {
				  var sthtml="<option value='0'>Unassign</option>";
				  
				  for(i=0;i<msg.length;i++)
		  			{
		  			
		  		sthtml+="<option value='"+msg[i][0]+"'>"+msg[i][2]+" "+msg[i][3]+"</option>";
		  			}
				  console.log("<select class='form-control' id='tcId'>"+sthtml+"</select>")
		  		$('#tcaller').html("<select class='form-control' id='tcId'>"+sthtml+"</select>");
				 
			  },
			  error: function(e) {
				 
				//called when there is an error
				console.log(e.message);
			  }
			});
	  
	} );
 
 function ViewMessagw(text)
 {
	 $("#modal-body").html(decodeURI(text))
 }
  function reloadtable(){
	  
	      name=$("#searchData").val();
	      mobileNo=$("#searchData").val();
	      telecaller=$("#searchData").val();
	      uname=$("#searchData").val();
	      callDue=$("#searchData").val();
	      callSta=$("#searchData").val();
	      stdate=stdate;
			 endate=endate;
		 dataTable.ajax.reload();    
		}
  
  function saveLead()
  {
  	   var country="N/A",state="N/A",city="N/A",cname="N/A",cweb="N/A",company="N/A",web="N/A";
  	
  	    var uid=$('#uid').val();
  	    var fname=$('#fname1').val();
  	   var lname=$('#lname1').val();
  	    var email=$('#eid').val();
  	   var mob=$('#mobno').val();
  	   var cat=$('#addCat').val();
  	   var date=$('#validity').val(); 
  	   var tcaler=$('#tcId').val();
  	    country=$('#cont11').val();  
  	    state=$('#stat11').val();  
  	    city=$('#cty11').val(); 
  	    company=$('#comp1').val(); 
  	    web=$('#url').val(); 

  	    if(web=="")
  			 web="N/A";
  			 if(company=="")
  				company="N/A";
  			 if(city=="")
  				city="N/A";
  			 if(state=="")
  				state="N/A";
  			 if(web=="")
  				web="N/A";
  			 if(country=="")
  				 country="N/A";
  			if(email=="")
  				email="N/A";
  			if(fname=="")
  				fname="N/A";
  			if(lname=="")
  				lname="N/A";
  	
  		mob=mob.replace(/^\s+|\s+$/gm,'');
  		var comastr=uid+","+fname+","+lname+","+email+","+mob+","+cat+","+date+","+country+","+state+","+city+","+company+","+web+","+tcaler; 
  	 if(mob=="")
  		 {
  		 document.getElementById('mobno').style.borderColor='red';
  	return;
  		 }
  	 else if(cat.toString()=="Select Category")
  		 {
  		 $.smallBox({
  				title : "Select Category.",
  				color : "#5b0506",
  				timeout : 4000
  				});
  		 }
  	 else
  		 {
  		 document.getElementById('mobno').style.borderColor='';

  	   $.ajax({
  	  	  url: "AddLead",
  	  	  type: "get",
  	  	  data:"comastr="+comastr,
  	  	  success: function(msg){
  	  		
  	  	  
  	  	   if(msg.toString()=="Lead Successfullt Added.")
  	  		   {
    	  	  $('#lead_from').trigger("reset");
  	  		dataTable.ajax.reload();
  	  		$.smallBox({
  				title : msg,
  				color : "#296191",
  				iconSmall : "fa fa-thumbs-up bounce animated",
  				timeout : 4000
  				});
  	  		 $('#myModal1').modal('hide');
  	  		   }
  	  	   else
  	  		   {
  	  		$('#addCat').prop('selectedIndex',0);
  	  		$.smallBox({
  				title : msg,
  				color : "#296191",
  				iconSmall : "fa fa-thumbs-down bounce animated",
  				timeout : 4000
  				});
  	  		   }
  	  	  }
  	  	}); 
  		 }
  }
</script>
</body>
</html>