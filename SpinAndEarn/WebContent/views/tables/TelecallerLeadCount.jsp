<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
<div id="OCLByTelecaller"></div>

</body>
<!-- <script src=js/jquery.dataTables.js></script>
<script src=js/dataTables.bootstrap.js></script>
<script src=js/dataTables.tableTools.js></script> --> 
<script>
$(document).ready(function() {
	getSuccesFailCallLogCountByTelecaller();
   getOpenCloseLeadCountByCategory();
	//generateSuccessFailChart();
	
});

function getOpenCloseLeadCountByCategory()
{
	$.ajax({
		  url: 'getOpenCloseLeadCountByCategory',
		  type: 'GET',
		  success: function(Mydata) {
			  var html="";
			  var i=0;
			  var status="";
			  var calltype="";
			  var audioPath="";
			  var data=eval(Mydata);
			  html+="<h3>Category Wise Lead Cont</h3>";
			  	html+="<table id='cat-gridCat' name='sa'class='table table-bordered' >";
			  html+="<thead>";
					html+="<tr>";
						 
							html+="<th>Category Name</th>";
							html+="<th>Open Lead Count</th>";
							html+="<th>Colse Lead Count</th>";	
							html+="<th>Total Lead Count</th>";	
							html+="</tr>";
						html+="</thead>";
					html+="<tbody id='folloBody1'>";
			  for(i=0;i<data.length;i++)
				  {
					var sum=Number(data[i][0])+Number(data[i][1]);

				  html+="<tr>";
				  html+="<td>"+data[i][2]+"</td>";
				  html+="<td>"+data[i][0]+"</td>";
				  html+="<td>"+data[i][1]+"</td>";
				  html+="<td>"+sum+"</td>";
				 
				  html+="</tr>";
				  }
			  html+="</tbody></table>";
			  $("#alltime").html(html);
			  $('#cat-gridCat').dataTable( {
			       
			        "sPaginationType" : "full_numbers",
					  "oLanguage" : {
					  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
					  },
					  //"sDom" : '<"tbl-tools-searchbox"fl<"clear">>,<"tbl_tools"CT<"clear">>,<"table_content"t>,<"widget-bottom"p<"clear">>',
					   "sDom": 'T<"clear">lfrtip',
					  "oTableTools" : {
					  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
					  }
			    } );    
					  
		  },
		  error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		});	
}
 function getSuccesFailCallLogCountByTelecaller()
{
	$.ajax({
		  url: 'getOpenColseLeadCountByTelecaller',
		  type: 'GET',
		  success: function(Mydata) {
			  var html="";
			  var i=0;
			  var status="";
			  var calltype="";
			  var audioPath="";
			  var data=eval(Mydata);
				html+="<h3>Telecaller Wise Lead Cont</h3>";
			  	html+="<table id='cat-grid' name='demo' class='table table-bordered' >";
			  html+="<thead>";
					html+="<tr>";
						  /*   html+="<th>Id</th>"; */
							html+="<th>Telecaller Name</th>";
							html+="<th>Open Lead Count</th>";
							html+="<th>Colse Lead Count</th>";	
							html+="<th>Total Lead Count</th>";
							html+="</tr>";
						html+="</thead>";
					html+="<tbody id='folloBody'>";
					
			  for(i=0;i<data.length;i++)
				  {
				  var sum=Number(data[i][1])+Number(data[i][2]);
				  html+="<tr>";
			/* 	  html+="<td>"+data[i][0]+"</td>"; */
				  html+="<td>"+data[i][3]+"</td>";
				  html+="<td>"+data[i][1]+"</td>";
				  html+="<td>"+data[i][2]+"</td>";
				  html+="<td>"+sum+"</td>";
				 
				  html+="</tr>";
				  }
			  html+="</tbody></table>";
			  $("#OCLByTelecaller").html(html);
			  $('#cat-grid').dataTable( {
				  
				  "paging": true,	
				  "sPaginationType" : "full_numbers",
				  "oLanguage" : {
				  "sLengthMenu" : "Data Length <span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'></span>"
				  },
				  //"sDom" : '<"tbl-tools-searchbox"fl<"clear">>,<"tbl_tools"CT<"clear">>,<"table_content"t>,<"widget-bottom"p<"clear">>',
				   "sDom": 'T<"clear">lfrtip',
				  "oTableTools" : {
				  "sSwfPath" : "plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
				  }
			    } );    
					  
		  },
		  error: function(e) {
			//called when there is an error
			console.log(e.message);
		  }
		});	
} 
</script>
</html>