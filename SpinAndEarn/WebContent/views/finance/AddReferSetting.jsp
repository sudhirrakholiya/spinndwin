<%@page import="com.sudhir.model.MediUser"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<sql:query var="msg" dataSource="SMS"> SELECT * FROM refersetting </sql:query>
			
<script type="text/javascript">

var dataTable=null;
$(document).ready(function() {
	// getdata();
});

function formSub(event) {
	
var codeLimit = $('#codeLimit').val();
var newUserAmt = $('#newUserAmt').val();
var oldUserAmt = $('#oldUserAmt').val();
var spinReqAmt = $('#spinReqAmt').val();
var signUpBonus = $('#signUpBonus').val();
var withdrawAmt = $('#withdrawAmt').val();
var betValues = $('#betValues').val();
if(codeLimit !== "" && newUserAmt !== "" && oldUserAmt !== "" && spinReqAmt !== "" && signUpBonus !== ""){
	$.ajax({
		url : "addReferSetting",
		type : "POST",
		data : {
			'codeLimit' : codeLimit,
			'newUserAmt' : newUserAmt,
			'oldUserAmt' : oldUserAmt,
			'spinReqAmt' : spinReqAmt,
			'signUpBonus' : signUpBonus,
			'withdrawAmt' :withdrawAmt,
			'betAmtValues' : betValues
		},
		success : function(response) {
			$('#remoteModal').modal('hide');
			$.smallBox({
                title : "Success",
                content : "<i class='fa fa-clock-o'></i> <i>Setting Added Successfully...</i>",
                color : "#659265",
                iconSmall : "fa fa-check fa-2x fadeInRight animated",
                timeout : 3000
            });
			parent.location.reload();
			//reloadtable();
		},
		error : function(xhr, status, error) {
			alert("get error");
			alert(xhr.responseText);
		}
	});
	
	}else{
		//alert("Please Check Input Details")
		 $.smallBox({
             title : "Please Check Input Details",
             content : "<i class='fa fa-clock-o'></i> <i>Please Check Input Details..</i>",
             color : "#C46A69",
             iconSmall : "fa fa-times fa-2x fadeInRight animated",
             timeout : 4000
         });
	}
	return false;
}

function editmodel(id) { 
	$("#imeiupdate").val(id); 
	var srno=$("#imeiupdate").val();
	 $('#imeiupdt').modal({
	        'remote': "views/finance/EditReferalSetting.jsp?srno="+srno,          //views/finance/EditSecratery.jsp
			'show':true		
	}); 
}

function enableit(setId,flag){
	var contents = "";
	if (flag == 1) {
		contents = "Are your Sure To Active this Setting ?"
	} else {
		contents = "Are your Sure To Deactive this Setting ?"
	}
	
	$.SmartMessageBox({
        title : "Alert!",
       // content : "Are your Sure To " + stat == 1 ? "Active": "Deactive" + " this user ?",
    	content : 	contents,   
        buttons : '[No][Yes]'
    },function(ButtonPressed) {
        if (ButtonPressed === "Yes") {
        	if (flag == 0) {
        		$.smallBox({
                    title : "You Can't Disable All Setting",
                    content : "<i class='fa fa-clock-o'></i> <i>You Can't Disable All Setting..</i>",
                    color : "#C46A69",
                    iconSmall : "fa fa-times fa-2x fadeInRight animated",
                    timeout : 4000
                });
        	} else {
        		 $.ajax({
           	  	  url: "enableRefSetting?setId="+setId+"&flag="+flag,
           	  	  type: "POST",
           	  	  success: function(html){
   					  var stats = "";
   						if (flag == 1) {
   							stats = "<i class='fa fa-clock-o'></i> <i>Setting Enabled Successfully...</i>"
   						} else {
   							stats = "<i class='fa fa-clock-o'></i> <i>Setting Disabled Successfully...</i>"
   						}
           	  		$.smallBox({
           				title : "Success",
           				content : stats,
           				color : "#00802b",
           				iconSmall : "fa fa-thumbs-up bounce animated",
           				timeout : 4000
           				});
           	  		parent.location.reload();
           	  	  }
           	  	});
        	}
        }if (ButtonPressed === "No") {
            $.smallBox({
                title : "OK Sir",
                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
        }
       });
	
}
</script>		

<input type="hidden" id="imeiupdate">

 <div class="modal fade" id="imeiupdt" tabindex="-1" role="dialog"
    aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" align="center">
        <div class="modal-content" style="width: 750px;">
        </div>
    </div>
</div> 	

<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<!-- <a  data-toggle="modal" data-target="#remoteModal" class="btn btn-primary"> <i
				class="fa fa-circle-arrow-up fa-lg"></i> Add Setting
			</a> -->

<div class="modal fade" id="remoteModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Referral Setting</h4>
        </div>
        <div class="modal-body">
          <form name="add" id="add" class="smart-form client-form">
           <fieldset>
           		<div class="row">
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-users"></i>
								<input type="text" name="codeLimit" id="codeLimit"
								placeholder="Enter Code Limit" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Code Limit</b>
							</label>
						</section>
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="newUserAmt" id="newUserAmt"
								placeholder="Enter Register User Amount" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									New User Amount</b>
							</label>
						</section>
					</div>
					<div class="row">
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="oldUserAmt" id="oldUserAmt"
								placeholder="Enter Referral User Amount" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Referral User Amount</b>
							</label>
						</section>
						
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="spinReqAmt" id="spinReqAmt"
								placeholder="Enter Spin Request Amount" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Spin Request Amount</b>
							</label>
						</section>
					</div>
					
					<div class="row">
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="signUpBonus" id="signUpBonus"
								placeholder="Enter SignUp Bonus Amount" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									SignUp Bonus Amount</b>
							</label>
						</section>
						
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="withdrawAmt" id="withdrawAmt"
								placeholder="Enter Minimum Withdraw Amount" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Withdraw Amount</b>
							</label>
						</section>
					</div>
					
					<div class="row">
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="betValues" id="betValues"
								placeholder="Enter Bet Amount Values" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Bet Amount Values</b>
							</label>
						</section>
					</div>
					
           </fieldset>
         </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="formSub()">Add</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>
  </div>

		</div>

	</div>

 


<div class="row">
	<div class="col-sm-12"></div>
	

</div>


<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-table"></i></span>
		<h2>Referral Settings</h2>
		<div class="widget-toolbar" role="menu">
			   <button class="btn btn-sm btn-danger" id="btn-addcol" data-toggle="modal" data-target="#remoteModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Referral Settings</button>
		</div>
	</header>

	<div>
		<div class="jarviswidget-editbox">
		</div>
		<div class="widget-body no-padding">
			<table id="dt_basic"
				class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr role="row">
					 <th scope="col">SrNo</th>
                     <th scope="col">Code Limit</th>
                     <th scope="col">New User Amount</th>
                     <th scope="col">Referral Amount</th>
                     <th scope="col">Spin Request Amount</th>
                     <th scope="col">SignUp Bonus</th>
                     <th scope="col">Min. Withdraw Amount</th>
                     <th scope="col">AddedOn</th>
                     <th scope="col">Bet Amount</th>
                     <th scope="col">isEnable</th>
					 <th>Action</th>
					</tr>
				</thead>
					<tbody>  
					<c:forEach var="all" items="${msg.rows}">
						<tr role="row" class="odd">
							<td>${all.settid}</td>
							<td>${all.codeLimit}</td>
							<td>${all.newUserAmt}</td>
							<td>${all.oldUserAmt}</td>
							<td>${all.spinReqAmt}</td>
							<td>${all.signUpBonus}</td>
							<td>${all.withdrawAmt}</td>
							<td>${all.addedOn}</td>
							<td>${all.betValues}</td>
							<c:choose>
						      <c:when test="${all.isEnable == true}">
							      <td><a class="btn btn-success btn-xs" href="#" onclick="enableit('${all.settid}',0)" >Enable &nbsp;<i class="icon-append fa fa-toggle-on"></i></a> </td>
						      </c:when>	
						      <c:when test="${all.isEnable == false}">
							     <td><a class="btn btn-danger btn-xs" href="#" onclick="enableit('${all.settid}',1)">Disable &nbsp;<i class="icon-append fa fa-toggle-off"></i></a> </td>
						      </c:when>	
						    </c:choose>	
							<td><a onclick="deleteSett('${all.settid}')" class="btn btn-danger btn-md">Delete &nbsp;<i class="icon-append fa fa-trash"></i></a>
								<a class="btn btn-primary btn-md" onclick="editmodel(${all.settid})">Edit &nbsp;<i class="icon-append fa fa-pencil"></i></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- end widget content -->

	</div>
	<!-- end widget div -->
</div>



<script type="text/javascript">
var apitable;
function reloadtable() {
	apitable.fnReloadAjax();
}

	pageSetUp();
	var pagefunction = function() {

		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').DataTable({
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
							
							}, "language": {
						       
						    },
							"rowCallback" : function(nRow) {
								
							},
							"drawCallback" : function(oSettings) {
								
							}
						});

	

	};

			loadScript(
					"js/plugin/datatables/jquery.dataTables.min.js",
					function() {
						loadScript(
								"js/plugin/datatables/dataTables.colVis.min.js",
								function() {
									loadScript(
											"js/plugin/datatables/dataTables.tableTools.min.js",
											function() {
												loadScript(
														"js/plugin/datatables/dataTables.bootstrap.min.js",
														function() {
															loadScript(
																	"js/plugin/datatable-responsive/datatables.responsive.min.js",
																	pagefunction)
														});
											});
								});
					});
			
			 function deleteSett(refId){
		            $.SmartMessageBox({
		                title : "Alert!",
		                content : "Are your sure to Delete this Setting?",
		                buttons : '[No][Yes]'
		            }, function(ButtonPressed) {
		                if (ButtonPressed === "Yes") {
		                    $.get("deleteRefSetting?srno="+refId,function(msg){
		                     //   reloadtable();
		                        $.smallBox({
		                            title : "Success",
		                            content : "<i class='fa fa-clock-o'></i> <i>Setting Deleted Successfully...</i>",
		                            color : "#659265",
		                            iconSmall : "fa fa-check fa-2x fadeInRight animated",
		                            timeout : 10000
		                        });
		                     //   $('#dt_basic').DataTable().reload();
		                        location.reload();
		                    });
		                }
		                if (ButtonPressed === "No") {
		                    $.smallBox({
		                        title : "OK Sir",
		                        content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
		                        color : "#C46A69",
		                        iconSmall : "fa fa-times fa-2x fadeInRight animated",
		                        timeout : 4000
		                    });
		                }
		            });
		         //   
		        }
</script>
