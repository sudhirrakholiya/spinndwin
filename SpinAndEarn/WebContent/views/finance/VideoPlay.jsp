<%@page import="com.sudhir.model.MediUser"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

		
<script type="text/javascript">

var dataTable=null;
$(document).ready(function() {
	// getdata();
});

</script>		

<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-table"></i></span>
		<h2>Video Player</h2>
		<div class="widget-toolbar" role="menu">
			  <!--  <button class="btn btn-sm btn-danger" id="btn-addcol" data-toggle="modal" data-target="#remoteModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Referal Settings</button> -->
		</div>
	</header>
	<div>
		<div class="jarviswidget-editbox">
		</div>
		<div class="widget-body no-padding">
			<video width="300" height="300" controls>
			       <source src="http://95.217.8.207/UploadFile/bx.mp4" type="video/mp4">
			</video>
		</div>
		<!-- end widget content -->

	</div>
	<!-- end widget div -->
</div>



<script type="text/javascript">
var apitable;
function reloadtable() {
	apitable.fnReloadAjax();
}

	pageSetUp();
	var pagefunction = function() {

		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').DataTable({
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
							
							}, "language": {
						       
						    },
							"rowCallback" : function(nRow) {
								
							},
							"drawCallback" : function(oSettings) {
								
							}
						});

	

	};

			loadScript(
					"js/plugin/datatables/jquery.dataTables.min.js",
					function() {
						loadScript(
								"js/plugin/datatables/dataTables.colVis.min.js",
								function() {
									loadScript(
											"js/plugin/datatables/dataTables.tableTools.min.js",
											function() {
												loadScript(
														"js/plugin/datatables/dataTables.bootstrap.min.js",
														function() {
															loadScript(
																	"js/plugin/datatable-responsive/datatables.responsive.min.js",
																	pagefunction)
														});
											});
								});
					});
</script>
