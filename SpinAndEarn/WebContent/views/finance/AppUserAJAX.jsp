<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.sudhir.model.MediUser"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/includes/SessionCheck.jsp"%>


<section id="widget-grid" class="">
    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-servicecomm"  data-widget-editbutton="false">
        <header>
                <span class="widget-icon"><i class="fa fa-table"></i></span>
                    <h2>Manage Users</h2>
        </header>
        <div class="col-lg-4" style="margin-left:1px;"> 
              <div class="row"  style="margin-bottom: 5px;">
                   <div class="col-xs-4 col-sm-3"><input type="text" id="searchData" class="form-control" placeholder="Enter Email Id For Search"></div> 
                   <div class="col-xs-6 col-sm-6" ><button type="button" class="btn btn-primary" onclick="getdata1()">Search</button> 
                   <button type="button" class="btn btn-success" onclick="setWinAll()">Win All</button>
                   	<button type="button" class="btn btn-danger" onclick="setLoseAll()">Lose All</button>
                   </div>
                   
			 </div>
        </div><br><br>
        <div>
            <div class="jarviswidget-editbox"></div>
			<input type="hidden" name="userIdTemp" id="userIdTemp">
            <div class="widget-body no-padding">
                <table id="servicecommission_tabletools"
                    class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr role="row">
                            <th style="width: 2%;">ID</th>
							<th data-hide="phone">Username</th>
							<th data-hide="phone,tablet">Email</th>
							<th data-hide="phone,tablet">DateOfBirth</th>
							<th data-hide="phone,tablet">SpinWallet Amt</th>
							<th data-hide="phone,tablet">WinWallet Amt</th>
							<th data-hide="phone,tablet">RefCode UsedBy</th>
							<th data-hide="phone,tablet">My RefCode</th>
							<th data-hide="phone,tablet">Code Used</th>
							<th data-hide="phone,tablet">AddedOn</th>
							<th data-hide="phone,tablet">Enabled</th>
							<th data-hide="phone,tablet">isWin</th>
							<th data-hide="phone,tablet">Add Money</th>
							<th data-hide="phone,tablet">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

 <div class="modal fade" id="moneyModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Money To User Spin Wallet</h4>
        </div>
        <div class="modal-body">
           <form name="add" id="add" class="smart-form client-form">
           <fieldset>
           		<div class="row">
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="adminAmt" id="adminAmt"
								placeholder="Enter Amount" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Amount</b>
							</label>
						</section>
					</div>
           </fieldset>
         </form>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-primary" onclick="addMoneyUser()">Add</button> 
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>
    
    <div class="modal fade" id="remoteModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">User Info</h4>
        </div>
        <div class="modal-body">
           <div class="box-body">
          	<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>UserName</th>
							<th>Email</th>
							<th>WinAmt</th>
							<th>WalletAmt</th>
							<th>UsedOn</th>
							<th>RefCode</th>
						</tr>
					</thead>
				</table>
			</div>	
        </div>
        <div class="modal-footer">
     <!-- <button type="button" class="btn btn-primary" onclick="formSub()">Add</button> -->
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>
</section>

<script type="text/javascript">
var servicecommission=null;
var searchStr='NA';
    function reloadtable(){
        servicecommission.ajax.reload();    
    }
    var pagefunction = function() {
         $('body').on('hidden.bs.modal', '.modal', function () {
              $(this).removeData('bs.modal');
            });
         getdata1()
    };
    $(document).ready(function() {
    	
    	$('#servicecommission_tabletools tbody').on('click', '#delete-btn', function () {
    	    var data = servicecommission.row( $(this).parents('tr') ).data();
    	    $.SmartMessageBox({
    	        title : "Alert!",
    	        content : "Are your Sure To Delete User ?",
    	        buttons : '[No][Yes]'
    	    },function(ButtonPressed) {
    	        if (ButtonPressed === "Yes") {
    	        	 $.ajax({
    	        	  	  url: "deleteAppUserByAdmin?srno="+data[0],
    	        	  	  type: "get",
    	        	  	  success: function(html){
    	        	  		  if(html=='1'){
    	        	  			$.smallBox({
    	            				title : "Success",
    	            				content : "User Successfully Deleted.",
    	            				color : "#00802b",
    	            				iconSmall : "fa fa-thumbs-up bounce animated",
    	            				timeout : 4000
    	            				});
    	        	  		  }else{
    	        	  			$.smallBox({
    	        	                title : "Fail",
    	        	                content : "SomethingWent Wrong",
    	        	                color : "#C46A69",
    	        	                iconSmall : "fa fa-times fa-2x fadeInRight animated",
    	        	                timeout : 4000
    	        	            });
    	        	  		  }
    	        	  		dataTable.ajax.reload();
    	        	  	  }
    	        	  	});
    	        }if (ButtonPressed === "No") {
    	            $.smallBox({
    	                title : "OK Sir",
    	                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
    	                color : "#C46A69",
    	                iconSmall : "fa fa-times fa-2x fadeInRight animated",
    	                timeout : 4000
    	            });
    	        }
    	       });
    	    });
    });
    
    
    loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
        loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
                loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
                    //loadScript("js/fnReloadAjax.js", function(){
                    loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", pagefunction)
            //  });
        
            });
        });
    });

    function getdata1(){
    	searchStr = $("#searchData").val();
    	$("#servicecommission_tabletools").dataTable().fnDestroy(); 
        servicecommission=   $('#servicecommission_tabletools').DataTable({
        	"processing": true,
		    "serverSide": true, 
	        "searching": false,
	         "ajax":{
	        	"url":"getAppUsersAdmin",
	        	"data": function ( d ) {
	 	        	  d.searchStr=searchStr;
	             },
	            "dataType": "jsonp"
	         },
	 	   "lengthMenu": [[10, 25, 50,100,250,500,1000,2000,5000, -1], [10, 25, 50,100,250,500,1000,2000,5000, "All"]],
	 	  "order": [1, 'asc'],
          "fnCreatedRow": function( nRow, aData, iDataIndex ) {
          	$('td:eq(0)', nRow).html(aData[0]);
          	$('td:eq(1)', nRow).html(aData[6]);
          	$('td:eq(2)', nRow).html(aData[2]);
          	$('td:eq(3)', nRow).html(aData[1]);
          	$('td:eq(4)', nRow).html(aData[7]);
          	$('td:eq(5)', nRow).html(aData[11]);
          	$('td:eq(6)', nRow).html(aData[5]);
          	$('td:eq(7)', nRow).html(aData[8]);
          	$('td:eq(8)', nRow).html('<a href="#" onclick=refInfo("'+aData[8]+'",'+aData[9]+')>'+aData[9]+' </a>');
          	$('td:eq(9)', nRow).html(aData[16]);
          	
          	var stat=aData[3];
          	if(stat==true){
          		$('td:eq(10)', nRow).html('<button class="btn btn-xs btn-success" id="view-btn" onclick=activeDe("'+aData[0]+'","0")>Active</button>');
          	}else{
          		$('td:eq(10)', nRow).html('<button class="btn btn-xs btn-danger" id="view-btn" onclick=activeDe("'+aData[0]+'","1")>DeActive</button>');
          	}
          	
          	if(aData[10]==true){
          		$('td:eq(11)', nRow).html('<button class="btn btn-xs btn-success" id="view-btn" onclick=changeWin("'+aData[0]+'","0")>Win</button>');
          	}else{
          		$('td:eq(11)', nRow).html('<button class="btn btn-xs btn-danger" id="view-btn" onclick=changeWin("'+aData[0]+'","1")>Not WIn</button>');
          	}
          	
          	$('td:eq(12)', nRow).html('<a class="btn btn-primary btn-xs" href="#" onclick="addMoney('+aData[0]+')">Add &nbsp;<i class="icon-append fa fa-money"></i></a> ');
          	
          	$('td:eq(13)', nRow).html('<button class="btn btn-sm btn-danger" id="del-btn" onclick=delUser("'+aData[0]+'","'+aData[5]+'")>DELETE</button>');
          },
          "autoWidth" : true
      });
  }
    
    function activeDe(uid,stat){
    	var contents = "";
    	if (stat == 1) {
    		contents = "Are your Sure To Active this user ?"
    	} else {
    		contents = "Are your Sure To Deactive this user ?"
    	}
    	
    	$.SmartMessageBox({
            title : "Alert!",
           // content : "Are your Sure To " + stat == 1 ? "Active": "Deactive" + " this user ?",
        	content : 	contents,   
            buttons : '[No][Yes]'
        },function(ButtonPressed) {
            if (ButtonPressed === "Yes") {
            	 $.ajax({
            	  	  url: "updateUserStat?stat="+stat+"&uid="+uid,
            	  	  type: "get",
            	  	  success: function(html){
    					  var stats = "";
    						if (stat == 1) {
    							stats = "<i class='fa fa-clock-o'></i> <i>User Activated Successfully...</i>"
    						} else {
    							stats = "<i class='fa fa-clock-o'></i> <i>User Deactivated Successfully...</i>"
    						}
            	  		$.smallBox({
            				title : "Success",
            				content : stats,
            				color : "#00802b",
            				iconSmall : "fa fa-thumbs-up bounce animated",
            				timeout : 4000
            				});
            	  		servicecommission.ajax.reload();
            	  	  }
            	  	});
            }if (ButtonPressed === "No") {
                $.smallBox({
                    title : "OK Sir",
                    content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                    color : "#C46A69",
                    iconSmall : "fa fa-times fa-2x fadeInRight animated",
                    timeout : 4000
                });
            }
           });
    }
    
    function changeWin(uid,stat){
    	
    	var contents = "";
    	if (stat == 1) {
    		contents = "Are your Sure To Set Win this user ?"
    	} else {
    		contents = "Are your Sure To CSet Loose this user ?"
    	}
    	
    	$.SmartMessageBox({
            title : "Alert!",
         // content : "Are your Sure To " + stat == 1 ? "Active": "Deactive" + " this user ?",
        	content : 	contents,   
            buttons : '[No][Yes]'
        },function(ButtonPressed) {
            if (ButtonPressed === "Yes") {
            	 $.ajax({
            	  	  url: "changeWinnerStat?stat="+stat+"&uid="+uid,
            	  	  type: "get",
            	  	  success: function(html){
    					  var stats = "";
    					  var colors = "#00802b";
    						if (stat == 1) {
    							stats = "<i class='fa fa-clock-o'></i> <i>User Winner Stat Set Successfully...</i>"
    								colors = "#00802b";
    						} else {
    							stats = "<i class='fa fa-clock-o'></i> <i>User Looser Stat Set Successfully...</i>"
    								colors = "#ff9407";     // ffc107
    						}
            	  		$.smallBox({
            				title : "Success",
            				content : stats,
            				color : colors,
            				iconSmall : "fa fa-thumbs-up bounce animated",
            				timeout : 4000
            				});
            	  		servicecommission.ajax.reload();
            	  	  }
            	  	});
            }if (ButtonPressed === "No") {
                $.smallBox({
                    title : "OK Sir",
                    content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                    color : "#C46A69",
                    iconSmall : "fa fa-times fa-2x fadeInRight animated",
                    timeout : 4000
                });
              }
           });
    }
    
    function addMoney(userId){
    	$('#moneyModal').modal();
    	$('#userIdTemp').val(userId);
    }

    function addMoneyUser(){
    	var adminAmt =$('#adminAmt').val();
    	var  userIdTemp= $('#userIdTemp').val();
    	
    	  if(adminAmt !== ""){
    		  if(adminAmt < 0){
    			  $.smallBox({
    		             title : "Please Check Input Details",
    		             content : "<i class='fa fa-clock-o'></i> <i>Invalid Amount..</i>",
    		             color : "#C46A69",
    		             iconSmall : "fa fa-times fa-2x fadeInRight animated",
    		             timeout : 4000
    		         });
    			}else{
    				$.ajax({
    					url : "addMoneyAdmin",
    					type : "POST",
    					data : {
    						'adminAmt' : adminAmt,
    						'uid' : userIdTemp
    					},
    					success : function(response) {
    						$('#moneyModal').modal('hide');
    						$.smallBox({
    			                title : "Success",
    			                content : "<i class='fa fa-clock-o'></i> <i>Money Added Successfully...</i>",
    			                color : "#659265",
    			                iconSmall : "fa fa-check fa-2x fadeInRight animated",
    			                timeout : 3000
    			            });
    					//	parent.location.reload();
    						servicecommission.ajax.reload();
    					},error : function(xhr, status, error) {
    						//alert("get error");
    						alert(xhr.responseText);
    					}
    				});
    			}
    		}else{
    			//alert("Please Check Input Details")
    			 $.smallBox({
    	             title : "Please Check Input Details",
    	             content : "<i class='fa fa-clock-o'></i> <i>Please Check Input Details..</i>",
    	             color : "#C46A69",
    	             iconSmall : "fa fa-times fa-2x fadeInRight animated",
    	             timeout : 4000
    	         });
    		} 
    	
    }
    
    function refInfo(refCode,codeCount){
    	//alert("codeCount::"+codeCount)
    	
    	if(codeCount=='0'){
    		$.smallBox({
                title : "No Data Found",
                content : "<i class='fa fa-clock-o'></i> <i>No Data Found..</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
    	}else{
    		$('#remoteModal').modal();
    		$("#cat-grid").dataTable().fnDestroy(); 
    		 dataTable = $('#cat-grid').DataTable({
    	         "processing": true,
    	         "serverSide": false,
    	         "ajax": "getRefUserUsed?refCode="+refCode,
    	         "columnDefs": [{
    	                 "targets": -1,
    	                 "data": null,
    	                 "defaultContent": ""
    	           }],
    	             "order": [[ 0, 'desc' ]],
    	             "fnCreatedRow": function( nRow, aData, iDataIndex ) {
    	            	 $('td:eq(6)', nRow).html(aData[6]);
    	             }
    	     });
    	}
    }
    
    function delUser(userId,reflCode){
    	 $.SmartMessageBox({
 	        title : "Alert!",
 	        content : "Are your Sure To Delete User ?",
 	        buttons : '[No][Yes]'
 	    },function(ButtonPressed) {
 	        if (ButtonPressed === "Yes") {
 	        	 $.ajax({
 	        	  	  url: "deleteAppUserByAdmin?srno="+userId+"&reflCode="+reflCode,
 	        	  	  type: "GET",
 	        	  	  success: function(html){
 	        	  		  if(html=='1'){
 	        	  			$.smallBox({
 	            				title : "Success",
 	            				content : "User Successfully Deleted.",
 	            				color : "#00802b",
 	            				iconSmall : "fa fa-thumbs-up bounce animated",
 	            				timeout : 4000
 	            				});
 	        	  		  }else{
 	        	  			$.smallBox({
 	        	                title : "Fail",
 	        	                content : "SomethingWent Wrong",
 	        	                color : "#C46A69",
 	        	                iconSmall : "fa fa-times fa-2x fadeInRight animated",
 	        	                timeout : 4000
 	        	            });
 	        	  		  }
 	        	  		servicecommission.ajax.reload();
 	        	  		//parent.location.reload();
 	        	  	  }
 	        	  	});
 	        }if (ButtonPressed === "No") {
 	            $.smallBox({
 	                title : "OK Sir",
 	                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
 	                color : "#C46A69",
 	                iconSmall : "fa fa-times fa-2x fadeInRight animated",
 	                timeout : 4000
 	            });
 	        }
 	       });
    }
    
    function setWinAll(){
    	$.SmartMessageBox({
            title : "Alert!",
            content : "Are your Sure To Set All To Win?",
            buttons : '[No][Yes]'
        },function(ButtonPressed) {
            if (ButtonPressed === "Yes") {
            	 $.ajax({
            	  	  url: "adminSetWinAll",
            	  	  type: "get",
            	  	  success: function(html){
            	  		var stats = "<i class='fa fa-clock-o'></i> <i>All User Set to Win Successfully...</i>"
						var	colors = "#00802b";
            	  		$.smallBox({
            				title : "Success",
            				content : stats,
            				color : colors,
            				iconSmall : "fa fa-thumbs-up bounce animated",
            				timeout : 4000
            			});
            	  		servicecommission.ajax.reload();
            	  	  }
            	  	});
            }if (ButtonPressed === "No") {
                $.smallBox({
                    title : "OK Sir",
                    content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                    color : "#C46A69",
                    iconSmall : "fa fa-times fa-2x fadeInRight animated",
                    timeout : 4000
                });
              }
           });
    }
    
    function setLoseAll(){
    	$.SmartMessageBox({
            title : "Alert!",
            content : "Are your Sure To Set All To Win?",
            buttons : '[No][Yes]'
        },function(ButtonPressed) {
            if (ButtonPressed === "Yes") {
            	 $.ajax({
            	  	  url: "adminSetLoseAll",
            	  	  type: "get",
            	  	  success: function(html){
            	  		var stats = "<i class='fa fa-clock-o'></i> <i>All User Set to Lose Successfully...</i>"
						var	colors = "#00802b";
            	  		$.smallBox({
            				title : "Success",
            				content : stats,
            				color : colors,
            				iconSmall : "fa fa-thumbs-up bounce animated",
            				timeout : 4000
            			});
            	  		servicecommission.ajax.reload();
            	  	  }
            	  	});
            }if (ButtonPressed === "No") {
                $.smallBox({
                    title : "OK Sir",
                    content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                    color : "#C46A69",
                    iconSmall : "fa fa-times fa-2x fadeInRight animated",
                    timeout : 4000
                });
              }
           });
    }
</script>
