<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.sudhir.model.MediUser"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<sql:query var="msg" dataSource="SMS"> SELECT COUNT(*) AS total FROM appuser </sql:query>

<sql:query var="msg1" dataSource="SMS"> SELECT IFNULL(SUM(withAmt),0) AS total FROM withdrawreq WHERE rqStatus=TRUE </sql:query>

<sql:query var="msg2" dataSource="SMS"> SELECT IFNULL(SUM(amount),0) AS total FROM usermoneytrans  </sql:query>

<sql:query var="msg3" dataSource="SMS"> SELECT IFNULL(SUM(withAmt),0) AS total FROM withdrawreq WHERE rqStatus=FALSE </sql:query>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SpinAndWin DashBoard</title>

<style>
</style>

<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css' />
<link href="css/img/styles.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet" />
</head>

<body>
<navigation>
	<div class="row">
		<div class="col-md-3">
			<div class="panel panel-primary text-center no-boder bg-color-red">
				<div class="panel-body">
					<i class="fa fa-users fa-3x"></i>
					<c:forEach items="${msg.rows}" var="s">
						<h3 style="color: white;" value="${s.total}">${s.total}</h3>
					</c:forEach>
				</div>
			<!-- 	<div class="panel-footer back-footer-red" ><button type="button" class="btn btn-sm btn-danger" onclick="getUserFun()">Total Users</button></div> -->
				<div class="panel-footer back-footer-red" style="color: white;">
					 <nav:item data-view="/finance/AppUserAJAX" data-icon="fa fa-users" title="Total Users" />
				</div>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="panel panel-primary text-center no-boder bg-color-green">
				<div class="panel-body">
					<i class="fa fa-money fa-3x"></i>
					<c:forEach items="${msg2.rows}" var="s2">
						<h3 style="color: white;" value="${s2.total}">${s2.total}</h3>
					</c:forEach>
				</div>
				<!-- <div class="panel-footer back-footer-red">Approved Requests</div> -->
				<div class="panel-footer back-footer-green">
					 <nav:item data-view="/finance/UserMoneyTransaction" data-icon="fa fa-money" title="Total Earning" />
				</div>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="panel panel-primary text-center no-boder bg-color-red">
				<div class="panel-body">
					<i class="fa fa-money fa-3x"></i>
					<div class="row">
						<c:forEach items="${msg1.rows}" var="s1">
							<h3 style="color: white;" value="${s1.total}">${s1.total}</h3>
						</c:forEach>
					</div>
				</div>
				<!-- <div class="panel-footer back-footer-green">Total Withdraw Requests</div> -->
				<div class="panel-footer back-footer-red" >
					 <nav:item data-view="/finance/WithdrawReq" data-icon="fa fa-money" title="Withdraw Amount" />
				</div>
			</div>
		</div>
		
		

		<div class="col-md-3">
			<div class="panel panel-primary text-center no-boder bg-color-green">
				<div class="panel-body">
					<i class="fa fa-clock-o fa-3x"></i>
					<div class="row">
						<c:forEach items="${msg3.rows}" var="s3">
							<h3 style="color: white;" value="${s3.total}">${s3.total}</h3>
						</c:forEach>
					</div>
				</div>
				<!-- <div class="panel-footer back-footer-green">Pending Requests</div> -->
				<div class="panel-footer back-footer-green" >
					 <nav:item data-view="/finance/WithdrawReq" data-icon="fa fa-money" title="Pending Amount" />
				</div>
			</div>
		</div>
		
	</div>
</navigation>
	<!-- Stage Wise Lead -->

	<div class="col-lg-12">
		<div class="panel panel-warning">
			<div class="panel-heading"></div>
			<div class="panel-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div id="bar-example2" style="width: 100%; heighr: 100%"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<br>
	</div>

	<!-- Agent Wise Lead -->

	<div class="col-lg-12">
		<div class="panel panel-warning">
			<div class="panel-heading"></div>
			<div class="panel-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div id="bar-agent" style="width: 100%; heighr: 100%"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<br>
	</div>
</body>

<script>
	$(document).ready(function() {
		console.log("invoked")
	});
	

</script>
</html>