<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.sudhir.model.MediUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
var dataTable=null;
$(document).ready(function() {
	          dataTable = $('#cat-grid').DataTable({
	            "processing": true,
	            "serverSide": false,
	            "ajax": "getReferalSetting",
	            "columnDefs": [{
	                    "targets": -1,
	                    "data": null,
	                    "defaultContent": "<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button> <button class='btn btn-sm btn-primary' id='edit-btn'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>  Edit  </button>&nbsp;&nbsp;&nbsp;&nbsp;"
	              }],
	                "order": [[ 0, 'desc' ]]
	        }); 
	  

$('#cat-grid tbody').on('click', '#edit-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
   console.log("jio::"+data[0])
    $('#imeiupdt').modal({
        'remote': "views/finance/EditReferalSetting.jsp?srno="+data[0],          //views/finance/EditSecratery.jsp
		'show':true		
    });
});


$('#cat-grid tbody').on( 'click', '#delete-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
    $.SmartMessageBox({
        title : "Alert!",
        content : "Are your Sure To Delete Setting ?",
        buttons : '[No][Yes]'
    },function(ButtonPressed) {
        if (ButtonPressed === "Yes") {
        	 $.ajax({
        	  	  url: "deleteRefSetting?srno="+data[0],
        	  	  type: "get",
        	  	  success: function(html){
					  dataTable.ajax.reload();
        	  		$.smallBox({
        				title : "Setting Successfully Deleted.",
        				color : "#00802b",
        				iconSmall : "fa fa-thumbs-up bounce animated",
        				timeout : 4000
        				});
        	  	  }
        	  	});
        }if (ButtonPressed === "No") {
            $.smallBox({
                title : "OK Sir",
                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
        }
       });
    });
});

dataTable.on( 'order.dt search.dt', function () {
	   dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
       cell.innerHTML = i+1;
   });
}).draw(); 

function saveSetting(){
	var codeLimit = $('#codeLimit').val();
	var newUserAmt = $('#newUserAmt').val();
	var oldUserAmt = $('#oldUserAmt').val();
	if(codeLimit !== "" ){
		$.ajax({
			url : "addReferSetting",
			type : "POST",
			data : {
				'codeLimit' : codeLimit,
				'newUserAmt' : newUserAmt,
				'oldUserAmt' : oldUserAmt
			},
			success : function(response) {
				$('#myModal1').modal('hide');
				$.smallBox({
	                title : "Success",
	                content : "<i class='fa fa-clock-o'></i> <i>Setting Added Successfully...</i>",
	                color : "#659265",
	                iconSmall : "fa fa-check fa-2x fadeInRight animated",
	                timeout : 3000
	            });
				dataTable.ajax.reload();
			},
			error : function(xhr, status, error) {
				alert("get error");
				alert(xhr.responseText);
			}
		});
		}else{
			 $.smallBox({
	             title : "Please Check Input Details",
	             content : "<i class='fa fa-clock-o'></i> <i>Please Check Input Details..</i>",
	             color : "#C46A69",
	             iconSmall : "fa fa-times fa-2x fadeInRight animated",
	             timeout : 4000
	         });
		}
}
</script>
</head>
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser" data-widget-editbutton="false">
		<header>
		    <span class="widget-icon"> <i class="fa fa-table"></i></span>
			<h2>Manage Referal Settings</h2>
			<div class="widget-toolbar" role="menu">
			   <button class="btn btn-sm btn-danger" id="btn-addcol" data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Referal Settings</button>
			</div>
		</header>
		
		<div>
		<div class="jarviswidget-editbox"></div>
			<div class="widget-body no-padding">
				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>AddedOn</th>
							<th>Limit</th>
							<th>New User Amount</th>
							<th>Existing User Amount</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>

	</div>

<div class="modal fade" id="imeiupdt" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 850px;"></div>
    </div>
</div> 

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="AddCategoryLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Fields</h4>
      </div>
      <div class="modal-body">
       <form name="add" id="add" class="smart-form client-form">
           <fieldset>
           		<div class="row">
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-user"></i>
								<input type="text" name="codeLimit" id="codeLimit"
								placeholder="Enter Code Limit" > <b
								class="tooltip tooltip-bottom-right">Needed to enter
									Code Limit</b>
							</label>
						</section>
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-user"></i>
								<input type="text" name="newUserAmt" id="newUserAmt"
								placeholder="Enter Register User Amount" > <b
								class="tooltip tooltip-bottom-right">Needed to enter
									New User Amount</b>
							</label>
						</section>
					</div>
					<div class="row">
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-user"></i>
								<input type="text" name="oldUserAmt" id="oldUserAmt"
								placeholder="Enter Existing User Amount" > <b
								class="tooltip tooltip-bottom-right">Needed to enter
									Existing User Amount</b>
							</label>
						</section>
					</div>
           </fieldset>
         </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveSetting();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Setting</button>
      </div>
    </div>
  </div>
</div>

</html>


<script type="text/javascript">
	pageSetUp();
	var pagefunction = function() {
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};
	};

			loadScript(
					"js/plugin/datatables/jquery.dataTables.min.js",
					function() {
						loadScript(
								"js/plugin/datatables/dataTables.colVis.min.js",
								function() {
									loadScript(
											"js/plugin/datatables/dataTables.tableTools.min.js",
											function() {
												loadScript(
														"js/plugin/datatables/dataTables.bootstrap.min.js",
														function() {
															loadScript(
																	"js/plugin/datatable-responsive/datatables.responsive.min.js",
																	pagefunction)
														});
											});
								});
					});
</script>
