<%@page import="com.sudhir.model.MediUser"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<sql:query var="msg" dataSource="SMS"> SELECT wr.rqid,wr.accName,wr.accNo,wr.ifsc,wr.mobileUPI,wr.rqDate,wr.rqStatus,wr.rqType,wr.userId,au.username,wr.apprvDate,wr.withAmt FROM withdrawreq wr JOIN appuser au ON wr.userId=au.uid ORDER BY wr.rqDate DESC </sql:query>
			
<script type="text/javascript">

var dataTable=null;
$(document).ready(function() {
	// getdata();
});

function approveit(uid,stat,rqid){
	
	var contents = "";
	if (stat == 1) {
		contents = "Are your Sure To Approve this request ?"
	} else {
		contents = "Are your Sure To Approve this request ?"
	}
	
	$.SmartMessageBox({
        title : "Alert!",
       // content : "Are your Sure To " + stat == 1 ? "Active": "Deactive" + " this user ?",
    	content : 	contents,   
        buttons : '[No][Yes]'
    },function(ButtonPressed) {
        if (ButtonPressed === "Yes") {
        	 $.ajax({
        	  	  url: "approveWithdrawReq?stat="+stat+"&uid="+uid+"&rqid="+rqid,
        	  	  type: "get",
        	  	  success: function(html){
					  var stats = "";
						if (stat == 1) {
							stats = "<i class='fa fa-clock-o'></i> <i>Request Approved Successfully...</i>"
						} else {
							stats = "<i class='fa fa-clock-o'></i> <i>Request Rejected...</i>"
						}
        	  		$.smallBox({
        				title : "Success",
        				content : stats,
        				color : "#00802b",
        				iconSmall : "fa fa-thumbs-up bounce animated",
        				timeout : 4000
        				});
        	  		 //dataTable.ajax.reload();
        	  		parent.location.reload();
        	  	  }
        	  	});
        }if (ButtonPressed === "No") {
            $.smallBox({
                title : "OK Sir",
                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
        }
       });
}

function deleteUser(id){
	 $.SmartMessageBox({
	        title : "Alert!",
	        content : "Are your Sure To Delete User ?",
	        buttons : '[No][Yes]'
	    },function(ButtonPressed) {
	        if (ButtonPressed === "Yes") {
	        	 $.ajax({
	        	  	  url: "deleteAppUserByAdmin?srno="+id,
	        	  	  type: "get",
	        	  	  success: function(html){
	        	  		  if(html=='1'){
	        	  			$.smallBox({
	            				title : "Success",
	            				content : "User Successfully Deleted.",
	            				color : "#00802b",
	            				iconSmall : "fa fa-thumbs-up bounce animated",
	            				timeout : 4000
	            				});
	        	  		  }else{
	        	  			$.smallBox({
	        	                title : "Fail",
	        	                content : "SomethingWent Wrong",
	        	                color : "#C46A69",
	        	                iconSmall : "fa fa-times fa-2x fadeInRight animated",
	        	                timeout : 4000
	        	            });
	        	  		  }
	        	  		//dataTable.ajax.reload();
	        	  		parent.location.reload();
	        	  	  }
	        	  	});
	        }if (ButtonPressed === "No") {
	            $.smallBox({
	                title : "OK Sir",
	                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
	                color : "#C46A69",
	                iconSmall : "fa fa-times fa-2x fadeInRight animated",
	                timeout : 4000
	            });
	        }
	       });
}


</script>		

<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-table"></i></span>
		<h2>Withdraw Requests</h2>
		<div class="widget-toolbar" role="menu">
			  <!--  <button class="btn btn-sm btn-danger" id="btn-addcol" data-toggle="modal" data-target="#remoteModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Referal Settings</button> -->
		</div>
	</header>
	<div>
		<div class="jarviswidget-editbox">
		</div>
		<div class="widget-body no-padding">
			<table id="dt_basic"
				class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr role="row">
					        <th>ID</th>
					        <th>UserName</th>
					        <th>Req Type</th>
					        <th>Account Name</th>
							<th>Account No</th>
							<th>IFSC Code</th>
							<th>Mobile UPI</th>
							<th>Withdraw Amount</th>
							<th>Req Status</th>
							<th>Req Date</th>
							<th>Approve Date</th>
							<th>Action</th>
					</tr>
				</thead>
					<tbody>  
					<c:forEach var="all" items="${msg.rows}">
						<tr role="row" class="odd">
							<td>${all.rqid}</td>
							<td>${all.username}</td>
							<td>${all.rqType}</td>
							<td>${all.accName}</td>
							<td>${all.accNo}</td>
							<td>${all.ifsc}</td>
							<td>${all.mobileUPI}</td>
							<td>${all.withAmt}</td>
						<c:choose>
						  <c:when test="${all.rqStatus == true}">
							<td><a class="btn btn-success btn-xs" href="#">APPROVED &nbsp;<i class="icon-append fa fa-thumbs-up"></i></a> </td>
						  </c:when>	
						  <c:when test="${all.rqStatus == false}">
							 <td><a class="btn btn-warning btn-xs" href="#" onclick="approveit('${all.userId}',1,'${all.rqid}')">PENDING &nbsp;<i class="icon-append fa fa-clock-o"></i></a> </td>
						  </c:when>	
						</c:choose>	
						<td>${all.rqDate}</td>
						<td>${all.apprvDate}</td>
							<td><a onclick="deleteUser('${all.rqid}')" class="btn btn-danger btn-md">Delete &nbsp;<i class="icon-append fa fa-trash"></i></a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- end widget content -->

	</div>
	<!-- end widget div -->
</div>



<script type="text/javascript">
var apitable;
function reloadtable() {
	apitable.fnReloadAjax();
}

	pageSetUp();
	var pagefunction = function() {

		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').DataTable({
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
							
							}, "language": {
						       
						    },
							"rowCallback" : function(nRow) {
								
							},
							"drawCallback" : function(oSettings) {
								
							}
						});

	

	};

			loadScript(
					"js/plugin/datatables/jquery.dataTables.min.js",
					function() {
						loadScript(
								"js/plugin/datatables/dataTables.colVis.min.js",
								function() {
									loadScript(
											"js/plugin/datatables/dataTables.tableTools.min.js",
											function() {
												loadScript(
														"js/plugin/datatables/dataTables.bootstrap.min.js",
														function() {
															loadScript(
																	"js/plugin/datatable-responsive/datatables.responsive.min.js",
																	pagefunction)
														});
											});
								});
					});
</script>
