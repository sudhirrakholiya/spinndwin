<%@page import="com.sudhir.model.MediUser"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<section id="widget-grid" class="">
    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-servicecomm"  data-widget-editbutton="false">
        <header>
                <span class="widget-icon"> <i class="fa fa-table"></i>    </span>
                    <h2>User Money Transactions</h2>
        </header>
         <div class="col-lg-4" style="margin-left: 3px;"> 
              <div class="row"  style="margin-bottom: 5px;">
                   <div class="col-xs-4 col-sm-3"><input type="text" id="searchData" class="form-control" placeholder="Enter Email Id For Search"></div> 
                   <div class="col-xs-2 col-sm-2" ><button type="button" class="btn btn-primary" onclick="getdata()">Search</button> </div>
			 </div>
            </div><br><br>
        <div>
            <div class="jarviswidget-editbox"></div>
			<input type="hidden" name="userIdTemp" id="userIdTemp">
            <div class="widget-body no-padding">
                <table id="dt_basic"
                    class="table table-striped table-bordered" width="100%">
                    <thead>
                       <tr role="row">
					        <th data-hide="phone,tablet">Sr.No</th>
					        <th data-hide="phone,tablet">UserName</th>
					        <th data-hide="phone,tablet">Email</th>
					        <th data-hide="phone,tablet" >OrderId</th>
					        <th data-hide="phone,tablet" >Transaction Id </th>
					        <th data-hide="phone,tablet" >Transaction Amount </th>
					        <th data-hide="phone,tablet" >Source</th>
							<th data-hide="phone,tablet">Transactions Date</th>
					</tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
pageSetUp();
var searchStr='NA';
var servicecommission=null;
var pagefunction = function() {
       $('#dt_basic').DataTable({
					"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"
							+ "t"
							+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
					
					}, "language": {
				       
				    },
					"rowCallback" : function(nRow) {
						
					},
					"drawCallback" : function(oSettings) {
						
					}
				});
         getdata();
	};
	
	function getdata(){
		searchStr = $("#searchData").val();
		$("#dt_basic").dataTable().fnDestroy();
	    servicecommission=   $('#dt_basic').DataTable({
	    	"processing": true,
		    "serverSide": true, 
	        "searching": false,
	         "ajax":{
	        	"url":"getUserMoneyTransAdmin",	
	        	"data": function ( d ) {
	 	        	  d.searchStr=searchStr;
	             },
	           "dataType": "jsonp"
	         },
	 	   "lengthMenu": [[10, 25, 50,100,250,500,1000,2000,5000, -1], [10, 25, 50,100,250,500,1000,2000,5000, "All"]],
	 	  "order": [1, 'asc'],
	      "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	      },
	      "autoWidth" : true
	  });
	}		

loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
        loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
            //loadScript("js/fnReloadAjax.js", function(){
            loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", pagefunction)
    //  });

    });
});
});
</script>
