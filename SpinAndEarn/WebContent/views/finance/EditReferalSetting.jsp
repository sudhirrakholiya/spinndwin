<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<sql:query var="msg" dataSource="SMS">SELECT * FROM refersetting WHERE settid='${param.srno}' </sql:query>

<style type="text/css">


.control-label.bld{
padding-top: 0px;
}
</style>

<script type="text/javascript">

function formSub(event) {
var codeLimit = $('#codeLimit').val();
var newUserAmt = $('#newUserAmt').val();
var oldUserAmt = $('#oldUserAmt').val();
var spinReqAmt = $('#spinReqAmt').val();
var signUpBonus = $('#signUpBonus').val();
var withdrawAmt = $('#withdrawAmt').val();
var isEnable = $('#isEnable').val();
var settid = $('#settid').val();
var betValues = $('#betValues').val();
if(codeLimit !== "" && newUserAmt !== "" && oldUserAmt !== "" && spinReqAmt !== "" && signUpBonus !== ""){
	 $.ajax({
		url : "updateReferSetting",
		type : "POST",
		data : {
			'codeLimit' : codeLimit,
			'newUserAmt' : newUserAmt,
			'oldUserAmt' : oldUserAmt,
			'spinReqAmt' : spinReqAmt,
			'signUpBonus' : signUpBonus,
			'withdrawAmt' :withdrawAmt,
			'isEnable' : isEnable,
			'settid' : settid,
			'betAmtValues' : betValues
		},
		success : function(response) {
			$('#remoteModal').modal('hide');
			//alert(response);
			$.smallBox({
                title : "Success",
                content : "<i class='fa fa-clock-o'></i> <i>Setting Updated Successfully...</i>",
                color : "#659265",
                iconSmall : "fa fa-check fa-2x fadeInRight animated",
                timeout : 5000
            });
			parent.location.reload();
		},error : function(xhr, status, error) {
			alert("get error");
			alert(xhr.responseText);
		}
	}); 
}else{
	 $.smallBox({
         title : "Please Check Input Details",
         content : "<i class='fa fa-clock-o'></i> <i>Please Check Input Details..</i>",
         color : "#C46A69",
         iconSmall : "fa fa-times fa-2x fadeInRight animated",
         timeout : 5000
     });
}

}
</script>
<c:forEach items="${msg.rows}" var="s"> 
<section id="widget-grid" class=""> 
	<div class="row">
		<article class="col-sm-12">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-provider-detail"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i>
					</span>
					<button type="button" class="close" data-dismiss="modal" style="color:white;width:20px">&times;</button>
					<h2>Edit Referal Setting</h2>
				</header>
				<div>
			
					<div class="widget-body">
					     <input type="hidden" name="settid" id="settid" value="${s.settid}">
					     <input type="hidden" name="isEnable" id="isEnable" value="${s.isEnable}">
		<form name="add" id="add" class="smart-form client-form">
           <fieldset>
           		<div class="row">
           		        <label class="col-md-3 control-label bld">Code Limit:</label>
						<section class="col col-3">
							<label class="input"> <i class="icon-append fa fa-users"></i>
								<input type="text" name="codeLimit" id="codeLimit"
								placeholder="Enter Code Limit" value="${s.codeLimit}" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Code Limit</b>
							</label>
						</section>
						
						<label class="col-md-3 control-label bld">Spin Request Amount:</label>
				         <section class="col col-3">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="spinReqAmt" id="spinReqAmt"
								placeholder="Enter Spin Request Amount" value="${s.spinReqAmt}" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Spin Request Amount</b>
							</label>
						</section>
				</div>
				<div class="row">
           		        <label class="col-md-3 control-label bld">Referral Amount:</label>
						<section class="col col-3">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="oldUserAmt" id="oldUserAmt"
								placeholder="Enter Referral Amount" value="${s.oldUserAmt}" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Referral Amount</b>
							</label>
						</section>
						
						<label class="col-md-3 control-label bld">Register User Amount:</label>
						<section class="col col-3">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="newUserAmt" id="newUserAmt"
								placeholder="Enter Register User Amount" value="${s.newUserAmt}" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Register User Amount</b>
							</label>
						</section>
				</div>
				
				<div class="row">
           		        <label class="col-md-3 control-label bld">SignUp Amount:</label>
						<section class="col col-3">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="signUpBonus" id="signUpBonus"
								placeholder="Enter SignUp Amount" value="${s.signUpBonus}"> <b
								class="tooltip tooltip-bottom-right">Need to enter
									SignUp Amount</b>
							</label>
						</section>
						
						 <label class="col-md-3 control-label bld">Withdraw Amount:</label>
						<section class="col col-3">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="withdrawAmt" id="withdrawAmt"
								placeholder="Enter Withdraw Amount" value="${s.withdrawAmt}"> <b
								class="tooltip tooltip-bottom-right">Need to enter
									Withdraw Amount</b>
							</label>
						</section>
				</div>
				
				<div class="row">
           		        <label class="col-md-3 control-label bld">Bet Amount:</label>
						<section class="col col-5">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="betValues" id="betValues"
								placeholder="Enter Bet Amount Values" value="${s.betValues}"> <b
								class="tooltip tooltip-bottom-right">Need to enter
									Bet Amount Values</b>
							</label>
						</section>
				</div>
				
           </fieldset>
         </form>
					</div>
					 <div class="modal-footer">
          					<button type="button" class="btn btn-primary" onclick="formSub()">Update</button>
         					 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       				 </div>

				</div>

			</div>

		</article>
	</div>

</section>
 </c:forEach> 


