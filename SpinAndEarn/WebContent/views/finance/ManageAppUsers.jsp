<%@page import="com.sudhir.model.MediUser"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<sql:query var="msg" dataSource="SMS"> SELECT * FROM appuser </sql:query>
			
<script type="text/javascript">

var dataTable=null;
$(document).ready(function() {
	// getdata();
});

function deactiveit(uid,stat){
	
	var contents = "";
	if (stat == 1) {
		contents = "Are your Sure To Active this user ?"
	} else {
		contents = "Are your Sure To Deactive this user ?"
	}
	
	$.SmartMessageBox({
        title : "Alert!",
       // content : "Are your Sure To " + stat == 1 ? "Active": "Deactive" + " this user ?",
    	content : 	contents,   
        buttons : '[No][Yes]'
    },function(ButtonPressed) {
        if (ButtonPressed === "Yes") {
        	 $.ajax({
        	  	  url: "updateUserStat?stat="+stat+"&uid="+uid,
        	  	  type: "get",
        	  	  success: function(html){
					  var stats = "";
						if (stat == 1) {
							stats = "<i class='fa fa-clock-o'></i> <i>User Activated Successfully...</i>"
						} else {
							stats = "<i class='fa fa-clock-o'></i> <i>User Deactivated Successfully...</i>"
						}
        	  		$.smallBox({
        				title : "Success",
        				content : stats,
        				color : "#00802b",
        				iconSmall : "fa fa-thumbs-up bounce animated",
        				timeout : 4000
        				});
        	  		 //dataTable.ajax.reload();
        	  		parent.location.reload();
        	  	  }
        	  	});
        }if (ButtonPressed === "No") {
            $.smallBox({
                title : "OK Sir",
                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
        }
       });
}

function deleteUser(id,reflCode){
	 $.SmartMessageBox({
	        title : "Alert!",
	        content : "Are your Sure To Delete User ?",
	        buttons : '[No][Yes]'
	    },function(ButtonPressed) {
	        if (ButtonPressed === "Yes") {
	        	 $.ajax({
	        	  	  url: "deleteAppUserByAdmin?srno="+id+"&reflCode="+reflCode,
	        	  	  type: "GET",
	        	  	  success: function(html){
	        	  		  if(html=='1'){
	        	  			$.smallBox({
	            				title : "Success",
	            				content : "User Successfully Deleted.",
	            				color : "#00802b",
	            				iconSmall : "fa fa-thumbs-up bounce animated",
	            				timeout : 4000
	            				});
	        	  		  }else{
	        	  			$.smallBox({
	        	                title : "Fail",
	        	                content : "SomethingWent Wrong",
	        	                color : "#C46A69",
	        	                iconSmall : "fa fa-times fa-2x fadeInRight animated",
	        	                timeout : 4000
	        	            });
	        	  		  }
	        	  		//dataTable.ajax.reload();
	        	  		parent.location.reload();
	        	  	  }
	        	  	});
	        }if (ButtonPressed === "No") {
	            $.smallBox({
	                title : "OK Sir",
	                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
	                color : "#C46A69",
	                iconSmall : "fa fa-times fa-2x fadeInRight animated",
	                timeout : 4000
	            });
	        }
	       });
}

function changeWin(uid,stat){
	
	var contents = "";
	if (stat == 1) {
		contents = "Are your Sure To Set Win this user ?"
	} else {
		contents = "Are your Sure To CSet Loose this user ?"
	}
	
	$.SmartMessageBox({
        title : "Alert!",
     // content : "Are your Sure To " + stat == 1 ? "Active": "Deactive" + " this user ?",
    	content : 	contents,   
        buttons : '[No][Yes]'
    },function(ButtonPressed) {
        if (ButtonPressed === "Yes") {
        	 $.ajax({
        	  	  url: "changeWinnerStat?stat="+stat+"&uid="+uid,
        	  	  type: "get",
        	  	  success: function(html){
					  var stats = "";
					  var colors = "#00802b";
						if (stat == 1) {
							stats = "<i class='fa fa-clock-o'></i> <i>User Winner Stat Set Successfully...</i>"
								colors = "#00802b";
						} else {
							stats = "<i class='fa fa-clock-o'></i> <i>User Looser Stat Set Successfully...</i>"
								colors = "#ff9407";     // ffc107
						}
        	  		$.smallBox({
        				title : "Success",
        				content : stats,
        				color : colors,
        				iconSmall : "fa fa-thumbs-up bounce animated",
        				timeout : 4000
        				});
        	  		parent.location.reload();
        	  	  }
        	  	});
        }if (ButtonPressed === "No") {
            $.smallBox({
                title : "OK Sir",
                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
          }
       });
}

function refInfo(refCode,codeCount){
	//alert("codeCount::"+codeCount)
	
	if(codeCount=='0'){
		$.smallBox({
            title : "No Data Found",
            content : "<i class='fa fa-clock-o'></i> <i>No Data Found..</i>",
            color : "#C46A69",
            iconSmall : "fa fa-times fa-2x fadeInRight animated",
            timeout : 4000
        });
	}else{
		$('#remoteModal').modal();
		$("#cat-grid").dataTable().fnDestroy(); 
		 dataTable = $('#cat-grid').DataTable({
	         "processing": true,
	         "serverSide": false,
	         "ajax": "getRefUserUsed?refCode="+refCode,
	         "columnDefs": [{
	                 "targets": -1,
	                 "data": null,
	                 "defaultContent": ""
	           }],
	             "order": [[ 0, 'desc' ]],
	             "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	            	 $('td:eq(6)', nRow).html(aData[6]);
	             }
	     });
	}
	
}


function addMoney(userId){
	$('#moneyModal').modal();
	$('#userIdTemp').val(userId);
}

function addMoneyUser(){
	var adminAmt =$('#adminAmt').val();
	var  userIdTemp= $('#userIdTemp').val();
	
	  if(adminAmt !== ""){
		  if(adminAmt < 0){
			  $.smallBox({
		             title : "Please Check Input Details",
		             content : "<i class='fa fa-clock-o'></i> <i>Invalid Amount..</i>",
		             color : "#C46A69",
		             iconSmall : "fa fa-times fa-2x fadeInRight animated",
		             timeout : 4000
		         });
			}else{
				$.ajax({
					url : "addMoneyAdmin",
					type : "POST",
					data : {
						'adminAmt' : adminAmt,
						'uid' : userIdTemp
					},
					success : function(response) {
						$('#moneyModal').modal('hide');
						$.smallBox({
			                title : "Success",
			                content : "<i class='fa fa-clock-o'></i> <i>Money Added Successfully...</i>",
			                color : "#659265",
			                iconSmall : "fa fa-check fa-2x fadeInRight animated",
			                timeout : 3000
			            });
						parent.location.reload();
					},error : function(xhr, status, error) {
						//alert("get error");
						alert(xhr.responseText);
					}
				});
			}
		}else{
			//alert("Please Check Input Details")
			 $.smallBox({
	             title : "Please Check Input Details",
	             content : "<i class='fa fa-clock-o'></i> <i>Please Check Input Details..</i>",
	             color : "#C46A69",
	             iconSmall : "fa fa-times fa-2x fadeInRight animated",
	             timeout : 4000
	         });
		} 
	
}
</script>		

<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-table"></i></span>
		<h2>Manage Users</h2>
		<div class="widget-toolbar" role="menu">
			  <!--  <button class="btn btn-sm btn-danger" id="btn-addcol" data-toggle="modal" data-target="#remoteModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Referal Settings</button> -->
		</div>
	</header>
	<div>
		<div class="jarviswidget-editbox">
		</div>
		<div class="widget-body no-padding">
			<table id="dt_basic"
				class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr role="row">
					        <th>ID</th>
					        <th>Username</th>
					        <th>Email</th>
							<th>DateOfBirth</th>
							<th>SpinWallet Amt</th>
							<th>WinWallet Amt</th>
							<th>RefCode UsedBy</th>
							<th>My RefCode</th>
							<th>Code Used</th>
							<th>Enabled</th>
							<th>isWin</th>
							<th>Add Money</th>
							<th>Action</th>
					</tr>
				</thead>
					<tbody>  
					<c:forEach var="all" items="${msg.rows}">
						<tr role="row" class="odd">
							<td>${all.uid}</td>
							<td>${all.username}</td>
							<td>${all.email}</td>
							<td>${all.dateofbirth}</td>
							<td>${all.walletamount}</td>
							<td>${all.winamount}</td>
							<td>${all.reflCode}</td>
							<td>${all.myReflCode}</td>
							<%-- <td>${all.myReflCodeLimit}</td> --%>
							<td><a  href="#" onclick="refInfo('${all.myReflCode}',${all.myReflCodeLimit})" >${all.myReflCodeLimit}</a> </td>
							<%-- <td>${all.enabled}</td> --%>
						<c:choose>
						  <c:when test="${all.enabled == true}">
							<td><a class="btn btn-success btn-xs" href="#" onclick="deactiveit('${all.uid}',0)" >Active &nbsp;<i class="icon-append fa fa-unlock"></i></a> </td>
						  </c:when>	
						  <c:when test="${all.enabled == false}">
							 <td><a class="btn btn-danger btn-xs" href="#" onclick="deactiveit('${all.uid}',1)">DeActive &nbsp;<i class="icon-append fa fa-lock"></i></a> </td>
						  </c:when>	
						</c:choose>	
						
						<c:choose>
						  <c:when test="${all.isWin == true}">
							<td><a class="btn btn-success btn-xs" href="#" onclick="changeWin('${all.uid}',0)" >Win &nbsp;<i class="icon-append fa fa-trophy"></i></a> </td>
						  </c:when>	
						  <c:when test="${all.isWin == false}">
							 <td><a class="btn btn-danger btn-xs" href="#" onclick="changeWin('${all.uid}',1)">Not WIn &nbsp;<i class="icon-append fa fa-thumbs-down"></i></a> </td>
						  </c:when>	
						</c:choose>	
							
							<td><a class="btn btn-primary btn-xs" href="#" onclick="addMoney('${all.uid}')">Add &nbsp;<i class="icon-append fa fa-money"></i></a> </td>
							<td><a onclick="deleteUser('${all.uid}','${all.reflCode}')" class="btn btn-danger btn-md">Delete &nbsp;<i class="icon-append fa fa-trash"></i></a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- end widget content -->
	</div>
	<!-- end widget div -->
</div>

<div class="modal fade" id="remoteModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">User Info</h4>
        </div>
        <div class="modal-body">
           <div class="box-body">
          	<table id="cat-grid" name="demo"
					class="table table-striped table-bordered" >
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>UserName</th>
							<th>Email</th>
							<th>WinAmt</th>
							<th>WalletAmt</th>
							<th>UsedOn</th>
							<th>RefCode</th>
						</tr>
					</thead>
				</table>
			</div>	
        </div>
        <div class="modal-footer">
     <!-- <button type="button" class="btn btn-primary" onclick="formSub()">Add</button> -->
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>
    
    
    <div class="modal fade" id="moneyModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Money To User Spin Wallet</h4>
        </div>
        <div class="modal-body">
           <form name="add" id="add" class="smart-form client-form">
           <fieldset>
           		<div class="row">
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="adminAmt" id="adminAmt"
								placeholder="Enter Amount" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Amount</b>
							</label>
						</section>
					</div>
           </fieldset>
         </form>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-primary" onclick="addMoneyUser()">Add</button> 
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>

<input type="hidden" name="userIdTemp" id="userIdTemp">
<script type="text/javascript">
var apitable;
function reloadtable() {
	apitable.fnReloadAjax();
}

	pageSetUp();
	var pagefunction = function() {

		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').DataTable({
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
							
							}, "language": {
						       
						    },
							"rowCallback" : function(nRow) {
								
							},
							"drawCallback" : function(oSettings) {
								
							}
						});

	

	};

			loadScript(
					"js/plugin/datatables/jquery.dataTables.min.js",
					function() {
						loadScript(
								"js/plugin/datatables/dataTables.colVis.min.js",
								function() {
									loadScript(
											"js/plugin/datatables/dataTables.tableTools.min.js",
											function() {
												loadScript(
														"js/plugin/datatables/dataTables.bootstrap.min.js",
														function() {
															loadScript(
																	"js/plugin/datatable-responsive/datatables.responsive.min.js",
																	pagefunction)
														});
											});
								});
					});
</script>
