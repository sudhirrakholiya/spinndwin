<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="com.sudhir.model.MediUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
var dataTable=null;
$(document).ready(function() {
	          dataTable = $('#cat-grid').DataTable({
	            "processing": true,
	            "serverSide": false,
	            "ajax": "getAppUsers",
	            "scrollX": true,
		        "scrollCollapse": true,
	            "columnDefs": [{
	                    "targets": -1,
	                    "data": null,
	                    "defaultContent": "<button class='btn btn-sm btn-danger' id='delete-btn'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>  Delete  </button> "
	              }],
	                "order": [[ 0, 'desc' ]],
	                "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	                	var stat=aData[9];
	                	if(stat==true){
	                		$('td:eq(9)', nRow).html('<button class="btn btn-xs btn-success" id="view-btn" onclick=activeDe("'+aData[0]+'","0")>Active</button>');
	                	}else{
	                		$('td:eq(9)', nRow).html('<button class="btn btn-xs btn-danger" id="view-btn" onclick=activeDe("'+aData[0]+'","1")>DeActive</button>');
	                	}
	                	
	                	if(aData[10]==true){
	                		$('td:eq(10)', nRow).html('<button class="btn btn-xs btn-success" id="view-btn" onclick=changeWin("'+aData[0]+'","0")>Win</button>');
	                	}else{
	                		$('td:eq(10)', nRow).html('<button class="btn btn-xs btn-danger" id="view-btn" onclick=changeWin("'+aData[0]+'","1")>Not WIn</button>');
	                	}
	                	
	                	$('td:eq(11)', nRow).html('<a class="btn btn-primary btn-xs" href="#" onclick="addMoney("'+aData[0]+'")">Add &nbsp;<i class="icon-append fa fa-money"></i></a> ');
	                }
	        }); 
	  
$('#cat-grid tbody').on( 'click', '#delete-btn', function () {
    var data = dataTable.row( $(this).parents('tr') ).data();
    $.SmartMessageBox({
        title : "Alert!",
        content : "Are your Sure To Delete User ?",
        buttons : '[No][Yes]'
    },function(ButtonPressed) {
        if (ButtonPressed === "Yes") {
        	 $.ajax({
        	  	  url: "deleteAppUserByAdmin?srno="+data[0],
        	  	  type: "get",
        	  	  success: function(html){
        	  		  if(html=='1'){
        	  			$.smallBox({
            				title : "Success",
            				content : "User Successfully Deleted.",
            				color : "#00802b",
            				iconSmall : "fa fa-thumbs-up bounce animated",
            				timeout : 4000
            				});
        	  		  }else{
        	  			$.smallBox({
        	                title : "Fail",
        	                content : "SomethingWent Wrong",
        	                color : "#C46A69",
        	                iconSmall : "fa fa-times fa-2x fadeInRight animated",
        	                timeout : 4000
        	            });
        	  		  }
        	  		dataTable.ajax.reload();
        	  	  }
        	  	});
        }if (ButtonPressed === "No") {
            $.smallBox({
                title : "OK Sir",
                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
        }
       });
    });
});

dataTable.on( 'order.dt search.dt', function () {
	   dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
       cell.innerHTML = i+1;
   });
}).draw(); 


function activeDe(uid,stat){
	var contents = "";
	if (stat == 1) {
		contents = "Are your Sure To Active this user ?"
	} else {
		contents = "Are your Sure To Deactive this user ?"
	}
	
	$.SmartMessageBox({
        title : "Alert!",
       // content : "Are your Sure To " + stat == 1 ? "Active": "Deactive" + " this user ?",
    	content : 	contents,   
        buttons : '[No][Yes]'
    },function(ButtonPressed) {
        if (ButtonPressed === "Yes") {
        	 $.ajax({
        	  	  url: "updateUserStat?stat="+stat+"&uid="+uid,
        	  	  type: "get",
        	  	  success: function(html){
					  var stats = "";
						if (stat == 1) {
							stats = "<i class='fa fa-clock-o'></i> <i>User Activated Successfully...</i>"
						} else {
							stats = "<i class='fa fa-clock-o'></i> <i>User Deactivated Successfully...</i>"
						}
        	  		$.smallBox({
        				title : "Success",
        				content : stats,
        				color : "#00802b",
        				iconSmall : "fa fa-thumbs-up bounce animated",
        				timeout : 4000
        				});
        	  		 dataTable.ajax.reload();
        	  	  }
        	  	});
        }if (ButtonPressed === "No") {
            $.smallBox({
                title : "OK Sir",
                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
        }
       });
}


function changeWin(uid,stat){
	
	var contents = "";
	if (stat == 1) {
		contents = "Are your Sure To Set Win this user ?"
	} else {
		contents = "Are your Sure To CSet Loose this user ?"
	}
	
	$.SmartMessageBox({
        title : "Alert!",
     // content : "Are your Sure To " + stat == 1 ? "Active": "Deactive" + " this user ?",
    	content : 	contents,   
        buttons : '[No][Yes]'
    },function(ButtonPressed) {
        if (ButtonPressed === "Yes") {
        	 $.ajax({
        	  	  url: "changeWinnerStat?stat="+stat+"&uid="+uid,
        	  	  type: "get",
        	  	  success: function(html){
					  var stats = "";
					  var colors = "#00802b";
						if (stat == 1) {
							stats = "<i class='fa fa-clock-o'></i> <i>User Winner Stat Set Successfully...</i>"
								colors = "#00802b";
						} else {
							stats = "<i class='fa fa-clock-o'></i> <i>User Looser Stat Set Successfully...</i>"
								colors = "#ff9407";     // ffc107
						}
        	  		$.smallBox({
        				title : "Success",
        				content : stats,
        				color : colors,
        				iconSmall : "fa fa-thumbs-up bounce animated",
        				timeout : 4000
        				});
        	  		 dataTable.ajax.reload();
        	  	  }
        	  	});
        }if (ButtonPressed === "No") {
            $.smallBox({
                title : "OK Sir",
                content : "<i class='fa fa-clock-o'></i> <i>Ok Sir as You Wish..</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
          }
       });
}

function addMoney(userId){
	$('#moneyModal').modal();
	$('#userIdTemp').val(userId);
}

function addMoneyUser(){
	var adminAmt =$('#adminAmt').val();
	var  userIdTemp= $('#userIdTemp').val();
	
	  if(adminAmt !== ""){
		  if(adminAmt < 0){
			  $.smallBox({
		             title : "Please Check Input Details",
		             content : "<i class='fa fa-clock-o'></i> <i>Invalid Amount..</i>",
		             color : "#C46A69",
		             iconSmall : "fa fa-times fa-2x fadeInRight animated",
		             timeout : 4000
		         });
			}else{
				$.ajax({
					url : "addMoneyAdmin",
					type : "POST",
					data : {
						'adminAmt' : adminAmt,
						'uid' : userIdTemp
					},
					success : function(response) {
						$('#moneyModal').modal('hide');
						$.smallBox({
			                title : "Success",
			                content : "<i class='fa fa-clock-o'></i> <i>Money Added Successfully...</i>",
			                color : "#659265",
			                iconSmall : "fa fa-check fa-2x fadeInRight animated",
			                timeout : 3000
			            });
						parent.location.reload();
					},error : function(xhr, status, error) {
						//alert("get error");
						alert(xhr.responseText);
					}
				});
			}
		}else{
			//alert("Please Check Input Details")
			 $.smallBox({
	             title : "Please Check Input Details",
	             content : "<i class='fa fa-clock-o'></i> <i>Please Check Input Details..</i>",
	             color : "#C46A69",
	             iconSmall : "fa fa-times fa-2x fadeInRight animated",
	             timeout : 4000
	         });
		} 
	
}
</script>
</head>
<input type="hidden" name="userIdTemp" id="userIdTemp">
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-adduser"
		data-widget-editbutton="false">
		<header>
				<span class="widget-icon"> <i class="fa fa-table"></i>	</span>
				<h2>Manage Users</h2>
			    <div class="widget-toolbar" role="menu"></div>
		</header>
		<div>

		<div class="jarviswidget-editbox"></div>
			<div class="widget-body no-padding">
				<table id="cat-grid" name="demo"
					class="table table-striped table-bordered table-hover" width="100%">
					<thead>
						<tr role="row">
							<th>ID</th>
							<th>Username</th>
							<th>Email</th>
							<th>DateOfBirth</th>
							<th>SpinWallet Amt</th>
							<th>WinWallet Amt</th>
							<th>RefCode UsedBy</th>
							<th>My RefCode</th>
							<th>Code Used</th>
							<th>Enabled</th>
							<th>isWin</th>
							<th>Add Money</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>


 <div class="modal fade" id="moneyModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Money To User Spin Wallet</h4>
        </div>
        <div class="modal-body">
           <form name="add" id="add" class="smart-form client-form">
           <fieldset>
           		<div class="row">
						<section class="col col-6">
							<label class="input"> <i class="icon-append fa fa-money"></i>
								<input type="text" name="adminAmt" id="adminAmt"
								placeholder="Enter Amount" > <b
								class="tooltip tooltip-bottom-right">Need to enter
									Amount</b>
							</label>
						</section>
					</div>
           </fieldset>
         </form>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-primary" onclick="addMoneyUser()">Add</button> 
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>
	</div>
</html>

<script type="text/javascript">
	pageSetUp();
	var pagefunction = function() {
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};
	};

			loadScript(
					"js/plugin/datatables/jquery.dataTables.min.js",
					function() {
						loadScript(
								"js/plugin/datatables/dataTables.colVis.min.js",
								function() {
									loadScript(
											"js/plugin/datatables/dataTables.tableTools.min.js",
											function() {
												loadScript(
														"js/plugin/datatables/dataTables.bootstrap.min.js",
														function() {
															loadScript(
																	"js/plugin/datatable-responsive/datatables.responsive.min.js",
																	pagefunction)
														});
											});
								});
					});
</script>
