<%@page import="com.sudhir.model.MediUser"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@include file="/includes/SessionCheck.jsp"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<sql:query var="msg" dataSource="SMS"> SELECT sw.swid,au.username,au.email,au.myReflCode,sw.winAmt,sw.trDate FROM spinwallet sw JOIN appuser au ON sw.userId=au.uid WHERE sw.trType='By Admin'  </sql:query>
			
<script type="text/javascript">

var dataTable=null;
$(document).ready(function() {
	// getdata();
});

</script>		

<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-editbutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-table"></i></span>
		<h2>Admin Transactions</h2>
		<div class="widget-toolbar" role="menu">
			  <!--  <button class="btn btn-sm btn-danger" id="btn-addcol" data-toggle="modal" data-target="#remoteModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Referal Settings</button> -->
		</div>
	</header>
	<div>
		<div class="jarviswidget-editbox">
		</div>
		<div class="widget-body no-padding">
			<table id="dt_basic"
				class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr role="row">
					        <th>Sr.No</th>
					        <th>UserName</th>
							<th>Email</th>
					        <th>User Ref. Code</th>
					        <th>Admin Amount</th>
							<th>Transactions Date</th>
					</tr>
				</thead>
					<tbody>  
					<c:forEach var="all" items="${msg.rows}">
						<tr role="row" class="odd">
							<td>${all.swid}</td>
							<td>${all.username}</td>
							<td>${all.email}</td>
							<td>${all.myReflCode}</td>
							 <td>${all.winAmt}</td>
							<td>${all.trDate}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- end widget content -->

	</div>
	<!-- end widget div -->
</div>



<script type="text/javascript">
var apitable;
function reloadtable() {
	apitable.fnReloadAjax();
}

	pageSetUp();
	var pagefunction = function() {

		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').DataTable({
							"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"
									+ "t"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"autoWidth" : true,
							"preDrawCallback" : function() {
							
							}, "language": {
						       
						    },
							"rowCallback" : function(nRow) {
								
							},
							"drawCallback" : function(oSettings) {
								
							}
						});

	

	};

			loadScript(
					"js/plugin/datatables/jquery.dataTables.min.js",
					function() {
						loadScript(
								"js/plugin/datatables/dataTables.colVis.min.js",
								function() {
									loadScript(
											"js/plugin/datatables/dataTables.tableTools.min.js",
											function() {
												loadScript(
														"js/plugin/datatables/dataTables.bootstrap.min.js",
														function() {
															loadScript(
																	"js/plugin/datatable-responsive/datatables.responsive.min.js",
																	pagefunction)
														});
											});
								});
					});
</script>
