<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.security.Principal"%>
<%@page import="com.sudhir.model.MediUser"%>
<%@page
	import="org.springframework.security.core.authority.SimpleGrantedAuthority"%>
<%@page import="java.util.Collection"%>
<%@page
	import="org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper"%>
<%@page
	import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<section id="widget-grid" class="">
	<div class="row">
		<article class="col-sm-12">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0"
				data-widget-colorbutton="false" data-widget-editbutton="false"
				data-widget-custombutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i>
					</span>
					
					<h2>Application Upload </h2>
				
				</header>
			
				<div>
					<div class="jarviswidget-editbox"></div>

					<div class="widget-body">
						<form class="form-horizontal"  enctype="multipart/form-data" method="POST" id="idUploadLogoForm1">
							<fieldset>
							 
							 <div class="form-group">
							  <label class="col-md-2 control-label bld">Upload File:</label>
							    <div class="col-md-3">
							     <input type="file" class="form-control" name="image" id="image">
								</div>
								<div class="col-md-4">
							     Please Choose APK File
								</div>
						    </div>
					
							 <div class="form-group">
							  <label class="col-md-2 control-label bld">App Version:</label>
							  <div class="col-md-3">
								<input type="text" class="form-control"  name="appVersion" id="appVersion"
								placeholder="Enter Application Version" >
								</div>	
							 </div> 
							<div class="form-group">
									<label class="col-md-2 control-label bld"></label>
									<div class="col-md-4">
										<input type="button" id="sendnotification" value="Upload"	onclick="formSub();" class="btn btn-primary btn-sm">
									</div>
							</div>
								</fieldset>
						</form>
								</div>
								</div>
								
					</div>

				</div>

			</div>

		</article>

	</div>

</section>
<!-- Select2 -->
<!--   <link rel="stylesheet" href="https://almsaeedstudio.com/themes/AdminLTE/plugins/select2/select2.min.css">
  
  
  Select2
<script src="https://almsaeedstudio.com/themes/AdminLTE/plugins/select2/select2.full.min.js"></script> -->
<style type="text/css">
border-style:solid;
border-color:#287EC7;
</style>
<script type="text/javascript">

function formSub(){
	var appVersion = $('#appVersion').val();
	if(appVersion==''){
		$.smallBox({
		                     title : "Please Check Input Details",
		                     content : "Please Choose Application Version",
		                     color : "#C46A69",
		                     iconSmall : "fa fa-times fa-2x fadeInRight animated",
		                     timeout : 4000
		                 }); 
	}else{
		 var data = new FormData(document.getElementById('idUploadLogoForm1'));
                $.ajax({
                     type: "POST",
         	         enctype: 'multipart/form-data',
         	         url: "uploadApp",
         	         data: data,
         	         crossDomain: true,
         	         processData: false,
         	         contentType: false,
         	         cache: false,
         	         success: function(data, textStatus, jqXHR) {		
         	        	 $.smallBox({
		                     title : "Success",
		                     content : "Successfully Uploaded",
		                     color : "#659265",
		                     iconSmall : "fa fa-check fa-2x fadeInRight animated",
		                     timeout : 4000
		                 }); 
						location.reload();
         	 },
         	 error: function(jqXHR, textStatus, errorThrown)
         	 {         
         		alert('ERRORS: ' + textStatus+" :: "+jqXHR+" :: "+errorThrown);
         	     console.log('ERRORS: ' + textStatus+" :: "+jqXHR+" :: "+errorThrown);
         	 }
         	     });
	}
	     
         		 
         		 
        }

</script>
