<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

 <style>


 </style>
 
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
 <link href="css/img/styles.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet" />  
</head>

<body>
    
                           <div class="row">
  <div class="col-md-3">
  
  
  
                    <div class="panel panel-primary text-center no-boder bg-color-red">
                        <div class="panel-body">
                            <i class="fa fa-edit fa-3x"></i>
                            <h3 style="color: white;" id="totallead">00</h3>
                        </div>
                        <div class="panel-footer back-footer-red">
                            Total Leads
                            
                        </div>
                    </div> 
  
  </div>
  <div class="col-md-3">
  
  <div class="panel panel-primary text-center no-boder bg-color-green">
                        <div class="panel-body">
                            <i class="fa fa-bar-chart-o fa-3x"></i>
                            <div class="row">
   							 <h3 style="color: white;" id="todaylLead">00</h3>
  </div>
                        </div>
                        <div class="panel-footer back-footer-green">
                          Today Leads
                            
                        </div>
                    </div>
  
  </div>
  <div class="col-md-3">
  
 <div class="panel panel-primary text-center no-boder bg-color-red">
                        <div class="panel-body">
                            <i class="fa fa-edit fa-3x"></i>
                            <h3 style="color: white;" id="todayfollow">00</h3>
                        </div>
                        <div class="panel-footer back-footer-red">
                            Today's Follow-up
                            
                        </div>
                    </div>
  
  </div>
  
   <div class="col-md-3">
  
  <div class="panel panel-primary text-center no-boder bg-color-green">
                        <div class="panel-body">
                            <i class="fa fa-bar-chart-o fa-3x"></i>
                            <div class="row">
   							 <h3 style="color: white;" id="closeLead">00</h3>
  </div>
                        </div>
                        <div class="panel-footer back-footer-green">
                          Close Leads
                            
                        </div>
                    </div>
  
  </div>
</div> 
   
   <!-- Stage Wise Lead -->
   
   	<div class="col-lg-12" >
				<div class="panel panel-warning">
					<div class="panel-heading" >Stage Wise Lead Overview</div>
					<div class="panel-body">
						 <div class="container-fluid">
  <div class="row" >
    <div class="col-md-12">
        <div  id="bar-example2" style="width:100%;heighr:100%">
        </div>
    </div>
  </div>
</div> 
					</div>
				</div>
				<br><br>
			</div> 
			
			<!-- Agent Wise Lead -->
   
   	<div class="col-lg-12" >
				<div class="panel panel-warning">
					<div class="panel-heading" >RM Wise Lead Overview</div>
					<div class="panel-body">
						 <div class="container-fluid">
  <div class="row" >
    <div class="col-md-12">
        <div  id="bar-agent" style="width:100%;heighr:100%">
        </div>
    </div>
  </div>
</div> 
					</div>
				</div>
				<br><br>
			</div>            
</body>

<script>

$(document).ready(function() {
	stageChart();
	agentChart();
	
	$.ajax({
		  url: 'GetDashboardCount',
		  type: 'GET',
		  success: function(data) {
				  $("#totallead").html(data[0][0])
				  $("#todaylLead").html(data[0][1])
				  $("#todayfollow").html(data[0][2])
				  $("#").html()
		  },
		 error: function(e) {
			console.log(e.message);
		  }
		} );
});


function agentChart()
{
	 var dd 	=[];
	$.ajax({
		  url: 'GetAgentLeadCount',
		  type: 'GET',
		  success: function(data) {
			  console.log(data)
			  for(i=0;i<data.length;i++)
	  			{
				for(var i=0;i<data.length;i++)
					{
				  var obj = {y: data[i][0], a: parseInt(data[i][1])};
				    dd.push(obj);
	  			}
	  			}
		  },
		 error: function(e) {
			console.log(e.message);
		  }
		} );
	$.getScript('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',function(){
		$.getScript('http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js',function(){
    Morris.Bar({
   	 
        element: 'bar-agent',
        data:dd,
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Lead'],
        barColors:['#31C0BE'],
        hideHover: 'auto',
        resize: true ,
        barSizeRatio:0.10,
       // xLabelAngle: 50
     }); 
		});
	}); 
	
}


function stageChart()
{
	 var dd 	=[];
	$.ajax({
		  url: 'GetStageLeadCount',
		  type: 'GET',
		  success: function(data) {
			  console.log(data)
			  for(i=0;i<data.length;i++)
	  			{
				for(var i=0;i<data.length;i++)
					{
				  var obj = {y: data[i][0], a: parseInt(data[i][1])};
				    dd.push(obj);
	  			}
	  			}
		  },
		 error: function(e) {
			console.log(e.message);
		  }
		} );
	$.getScript('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',function(){
		$.getScript('http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js',function(){
    Morris.Bar({
   	 
        element: 'bar-example2',
        data:dd,
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Lead'],
        barColors:['#C7254E'],
        hideHover: 'auto',
        resize: true ,
        xLabelAngle: 50
     }); 
		});
	}); 
	
}

</script>
</html>