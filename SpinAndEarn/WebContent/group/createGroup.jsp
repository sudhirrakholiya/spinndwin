<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/includes/SessionCheck.jsp"%>

	

			<script type="text/javascript">
				$(document).ready(function() {

					$('#datepicker').datepicker();
				});

				function formSub(event) {
 
				//	alert("asda");
 
					var groupname = $('#groupname').val();
					var uid = $('#uid').val();
					var type= $('#type').val();
					$.ajax({
						url : "createGroup",
						type : "GET",
						data : {
							'groupname' : groupname,
							'type' : type,
							'uid' : uid

						},

						success : function(response) {
							alert(response);
							 $('#remoteModal').modal('toggle');
							 
						},
						error : function(xhr, status, error) {

							alert(xhr.responseText);
						}
					});
					return false;
				}
			</script>
			
			

	<!-- row -->
	
	<div class="row">
		
		<article class="col-sm-12 col-md-12 col-lg-12">

			<div class="jarviswidget jarviswidget-color-purple" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-fullscreenbutton="false" data-widget-sortable="false">
			
				<header>
					<span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
					<h2>Add Group</h2>

				</header>


				<div>

					<div class="jarviswidget-editbox">

					</div>
					<div class="widget-body">

                     <form class="form-inline" role="form">							

							

                               <fieldset>
								<div class="form-group">
									<label class="sr-only">Group Name</label>
									<input  class="form-control" id="groupname"  name="groupname" placeholder="Enter GroupName">
								</div>
								
									<div class="form-group">
									<label class="control-label col-md-2">Type</label>
									<div class="col-md-10">
										<select class="form-control" id="type">
										<option value="Normal">Normal</option>
											<option value="Optin">Optin</option>
										</select>
									</div>
								</div>
									
								
							
								<button type="button"  onclick="formSub()" class="btn btn-primary">
									SAVE
								</button>
								<button type="button" class="btn btn-default"
												data-dismiss="modal">Close</button>
							</fieldset>

							
								
						</form>
					</div>
				</div>
			</div>
			</article>
			</div>
		


