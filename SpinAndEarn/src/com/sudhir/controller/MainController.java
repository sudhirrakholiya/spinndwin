package com.sudhir.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sudhir.model.DisplaySetting;
import com.sudhir.service.AdminService;
import com.sudhir.service.UserService;


@Controller
public class MainController {

	@Autowired
	AdminService adminService;
	@Autowired
	UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, HttpServletRequest request) {

		System.out.println("in login" + new Date() + "::" + request.getServerName());

		DisplaySetting ds = adminService.getDisplaySettingByURL(request.getServerName());

		ModelAndView model = new ModelAndView();
		if (ds != null) {
			System.out.println("DS::" + ds.getUserName());
			model.addObject("mtitle", ds.getTitle());
			model.addObject("logoImage", ds.getLogoImage());

			model.addObject("sub_title", ds.getSubTitle());
			model.addObject("image2", ds.getImage2());
			model.addObject("address", ds.getAddress());
			model.addObject("aboutus", ds.getAboutus());
			model.addObject("extraInfo1", ds.getExtraInfo1());
			model.addObject("extraInfo2", ds.getExtraInfo2());
			model.addObject("aboutus", ds.getAboutus());
			model.addObject("aboutus", ds.getAboutus());
			model.addObject("aboutus", ds.getAboutus());

		}
		if (error != null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}

	// customize the error message
	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid username and password!";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else if (exception instanceof UsernameNotFoundException) {
			error = exception.getMessage();
		} else {
			error = exception.getMessage();
		}

		return "<div class='alert alert-danger fade in'>"
				+ "<button class='close' data-dismiss='alert'></button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>"
				+ error + "</div>";

	}

	// for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();

			System.out.println(userDetail);

			model.addObject("username", userDetail.getUsername());
			model.addObject("password", userDetail.getPassword());

		}

		model.setViewName("views/403");
		return model;

	}

}