package com.sudhir.controller;

import javax.servlet.annotation.MultipartConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.sudhir.service.AdminService;
import com.sudhir.service.UserService;

@Controller
@MultipartConfig
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;

	/*@Autowired
	SMSService smsService;*/

/*	@RequestMapping(value = "RestoreSMS", produces = "text/plain;charset=UTF-8")
	public @ResponseBody String RestoreSMS(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "cid", required = true) int cid,
			@RequestParam(value = "addmin", required = true) int addmin) {
		int sizes = 0;
		if (cid == 1) {
			sizes = QueueUltraHigh.getInstance().ultraHigh1.size();
		} else if (cid == 2) {
			sizes = QueueUltraHigh.getInstance().ultraHigh2.size();
		} else if (cid == 3) {
			sizes = QueueUltraHigh.getInstance().ultraHigh3.size();
		} else if (cid == 4) {
			sizes = QueueUltraHigh.getInstance().ultraHigh4.size();
		}
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		List<SmsQueue> lstsmsqueue = new ArrayList<SmsQueue>();
		if (currentUser.getUserid() == 1) {

			Calendar cal = Calendar.getInstance();
			for (int k = 0; k < sizes; k++) {
				SmsQueue sq = QueueUltraHigh.getInstance().getSmsObject(cid);
				if (sq != null) {
					cal.setTime(sq.getCreatDate());
					cal.add(12, addmin);
					sq.setCreatDate(cal.getTime());
					lstsmsqueue.add(sq);
				} else {
					break;
				}

			}

		//	smsService.saveSmsQueue(lstsmsqueue);

		}

		return "Channel #" + cid + "'s total " + lstsmsqueue.size() + "SMS Restored And schedule after " + addmin
				+ " Minuts";
	}*/

	/*@RequestMapping(value = "getUsersParent", method = RequestMethod.GET)
	public ModelAndView UserDetail(HttpServletRequest request,
			@RequestParam(value = "uid", required = true) String uid) {
		User us = adminService.getUserByUserName(uid);

		Long parentid = us.getUid();
		List<String[]> lst = new ArrayList<String[]>();

		while (parentid != 1) {
			User usr = adminService.getUserByUid(parentid);
			lst.add(new String[] { usr.getUid() + "", usr.getUsername(), usr.getMobileNumber(), usr.getEmail() });
			parentid = usr.getParentId();
		}

		ModelAndView model = new ModelAndView("parentList");
		model.addObject("lists", lst);

		return model;

	}*/

	// for send message at user createtaion time

	/* @Secured("ROLE_ADMIN") */
	/* @PreAuthorize("hasRole('ROLE_ADMIN')") */

	/*
	 * @RequestMapping(value = "sendMessageApi", produces = "application/json")
	 * public @ResponseBody String sendMessageApi(
	 * 
	 * @RequestParam(value = "username", required = true) String userName,
	 * 
	 * @RequestParam(value = "apikey", required = true) String key,
	 * 
	 * @RequestParam(value = "message", required = true) String message,
	 * 
	 * @RequestParam(value = "numbers", required = true) String mobileNumbers
	 * ,HttpServletRequest request, HttpServletResponse response) throws
	 * Exception { //System.out.println("sendMessageApi"); SmsCredit sc=null;
	 * 
	 * String senderName = request.getParameter("senderName"); String smsType =
	 * request.getParameter("smsType"); String[] numbers =
	 * mobileNumbers.split(","); String serverIP = request.getRemoteAddr();
	 * JsonObject jo=new JsonObject();
	 * 
	 * if (!userName.equalsIgnoreCase("") || !key.equalsIgnoreCase("") ||
	 * !senderName.equalsIgnoreCase("") || !message.equalsIgnoreCase("")) { User
	 * userObj = adminService .getUserByUserName(userName);
	 * 
	 * if(!userObj.isActive()){ jo.addProperty("error",
	 * "User Account Deactivated"); return jo.toString();
	 * 
	 * };
	 * 
	 * if (userObj.getUsername() != null ||
	 * !userObj.getUsername().equalsIgnoreCase("")) {
	 * 
	 * // System.out.print("UID:"+userObj.getUid()+"::KEY:"+key); ApiKey apiKey
	 * = userService.gettAPiByUid(userObj.getUid(), key,false);
	 * 
	 * if (apiKey != null) {
	 * 
	 * if (!apiKey.getIp().contains("*") &&
	 * !apiKey.getIp().equalsIgnoreCase(serverIP)) { jo.addProperty("error",
	 * "invalid IP address"); return jo.toString(); }
	 * 
	 * ServiceType serviceTypes = userService .getServiceTypeByName(smsType);
	 * 
	 * if(serviceTypes==null){ jo.addProperty("error", "Service not available");
	 * return jo.toString(); }
	 * 
	 * try{ sc = adminService.getSmsCredit( userObj.getUid(),
	 * serviceTypes.getStid()); }catch(NullPointerException ex){
	 * jo.addProperty("error",
	 * "No Credit in Given Service ? Please Contact customer Care"); return
	 * jo.toString(); } boolean isUnicode = StringUtils.isUnicode(message); int
	 * crdeduct=StringUtils.SMSCounter(message.length(),isUnicode);
	 * 
	 * Long bal = sc.getSmsCredit();
	 * 
	 * ServiceType serviceType = userService .getServiceTypeById(serviceTypes
	 * .getStid()); if (serviceType.getTemplatecheck() &&
	 * !isvalidtemplate(message,userObj.getUid())) { jo.addProperty("error",
	 * "Message Not Match With Template"); return jo.toString(); }
	 * 
	 * 
	 * int crdup =
	 * smsService.creditUpdate(crdeduct*numbers.length,userObj.getUid(),
	 * serviceType.getStid()); if (crdup == 0) { System.out.print(
	 * "Insuffician credit of User id:" + userObj.getUid() +
	 * "::Message cannot be sent" + new Date()); jo.addProperty("error",
	 * "insufficiant credit"); return jo.toString();
	 * 
	 * }
	 * 
	 * 
	 * if (bal > (numbers.length*crdeduct)) {
	 * 
	 * for (int i = 0; i < numbers.length; i++) { SmsQueue smsQueue = new
	 * SmsQueue(); smsQueue.setCreatDate(new Date());
	 * smsQueue.setMessage(message);
	 * smsQueue.setPriority(serviceType.getPriority());
	 * smsQueue.setRetryCount(1); smsQueue.setStatus(1);
	 * smsQueue.setSenderid(senderName); smsQueue.setUid(userObj.getUid());
	 * smsQueue.setStid(serviceType.getStid());
	 * smsQueue.setDndChecked(!serviceType.getDndcheck());
	 * smsQueue.setNumber(numbers[i]); smsQueue.setMsgcredit(crdeduct);
	 * userService.saveSms(smsQueue); int crdup =
	 * smsService.creditUpdate(crdeduct, userObj.getUid(),
	 * serviceTypes.getStid());
	 * 
	 * } } else { jo.addProperty("error", "Not Enough SMS Credit."); return
	 * jo.toString();
	 * 
	 * } } else { return "Check Ip Address."; }
	 * 
	 * } else { jo.addProperty("error","Wrong Api Key."); return jo.toString();
	 * }
	 * 
	 * }
	 * 
	 * } else { jo.addProperty("error","Not Enough Perameter"); return
	 * jo.toString(); }
	 * 
	 * jo.addProperty("success","Message SuccessFully Submitted"); return
	 * jo.toString(); }
	 */


	/*@RequestMapping(value = "deleteApi")
	public String deleteApi(HttpServletRequest request, HttpServletResponse response) {
		String aid = request.getParameter("aid");
		userService.deteleApiKey(aid);
		return "redirect:/#/Api";

	}*/

	/*public boolean isvalidtemplate(String sms, Long userid) throws Exception {

		List<Tamplate> spamText = null;

		spamText = userService.getTemplateByuser(userid);
		if (spamText != null && spamText.size() > 0) {

			for (Tamplate spmText : spamText) {
				Pattern pattern = Pattern.compile(spmText.getTname(), Pattern.CASE_INSENSITIVE);

				Matcher matcher = pattern.matcher(sms);
				if (matcher.find())
					return true;
			}
		}
		return false;
	}*/

	/*@RequestMapping(value = "SaveUrlSetting", method = RequestMethod.POST)
	public @ResponseBody String SaveUrlSetting(HttpServletRequest request, HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();

		if (currentUser.getUserid() != 1) {
			return "Unauthorized";
		}

		String apiText = request.getParameter("apitext");
		String priority = request.getParameter("priority");
		String successMeassage = request.getParameter("successMeassage");
		int uids = Integer.parseInt(request.getParameter("uid"));
		int sid = Integer.parseInt(request.getParameter("sid"));
		int isfailover = Integer.parseInt(request.getParameter("isfailover"));

		SmsApi smsApi = new SmsApi();
		smsApi.setApiText(apiText);
		smsApi.setIsFailOver(isfailover);

		smsApi.setPriorty(Integer.parseInt(priority));
		smsApi.setSuccessMsg(successMeassage);
		smsApi.setuId(uids);
		smsApi.setServiceid(sid);

		userService.saveHttpGateway(smsApi);

		
		 * if (!altapiText.equalsIgnoreCase("") ||
		 * !altpriority.equalsIgnoreCase("") ||
		 * !altsuccessMeassage.equalsIgnoreCase("")) { SmsApi smsApi2 = new
		 * SmsApi(); smsApi2.setApiText(altapiText);
		 * smsApi2.setPriorty(Integer.parseInt(altpriority));
		 * smsApi2.setSuccessMsg(altsuccessMeassage); smsApi2.setuId(uids);
		 * userService.saveSmsApi(smsApi2); }
		 

		return "URL SAVED";

	}*/

/*	@RequestMapping(value = "findHttpGateway", method = RequestMethod.GET)
	public @ResponseBody String findHttpGateway(@RequestParam(value = "t", required = true) int type,
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		if (currentUser.getUserid() != 1) {
			return "Unauthorized";
		}

		return userService.findHttpGateway(type);
	}

	@RequestMapping(value = "deleteHttpGatewaybyid", method = RequestMethod.GET)
	public @ResponseBody String deleteHttpGatewaybyid(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		if (currentUser.getUserid() != 1) {
			return "Unauthorized";
		}
		Long hid = Long.parseLong(request.getParameter("hid"));
		userService.deleteHttpGateway(hid);
		return 1 + "";
	}*/

	/*@RequestMapping(value = "getCountSmsQueue", method = RequestMethod.GET)
	public @ResponseBody String getCountSmsQueue(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();

		String cnt = userService.getQueueCount(currentUser.getUserid());
		System.out.println("Cnt ------ > " + cnt);
		return cnt;
	}

	@RequestMapping(value = "getCountSentSms", method = RequestMethod.GET)
	public @ResponseBody String getCountSentSms(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		System.out.println(currentUser.getUserid());
		String cnt = userService.getSentSms(currentUser.getUserid());
		System.out.println("Cnt ------ > " + cnt);
		return cnt;
	}
*/
	/*@RequestMapping(value = "getCountTodaySentSms", method = RequestMethod.GET)
	public @ResponseBody String getCountTodaySentSms(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		String cnt = userService.getSentSmsToday(currentUser.getUserid(), sdf.format(date));
		System.out.println("Cnt ------ > " + cnt);
		return cnt;
	}

	@RequestMapping(value = "getSentMessage", method = RequestMethod.POST)
	public @ResponseBody String getSentMessage(HttpServletRequest request, HttpServletResponse response) {

		int page = 1;
		int listSize = 10;
		int ttlpage = 10;

		List jsonList = userService.getSmsHistory(page, listSize);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		MediUser currentUser = (MediUser) auth.getPrincipal();
		System.out.println("In Sent message ------ > " + request.getParameter("t_currentPage"));

		return new Gson().toJson(jsonList);
	}*/

	/*@RequestMapping(value = "user/activedeactive", method = RequestMethod.GET)
	public @ResponseBody int activeDeactive(HttpServletRequest request, HttpServletResponse response) {

		Long id = Long.parseLong(request.getParameter("uid"));

		int act = Integer.parseInt(request.getParameter("isactive"));

		// System.out.println("delete id" + id);
		
		 * if(act==0){ smsService.moveToFailedSmsQueue(id); }
		 
		return adminService.updateStatusUser(id, act);

	}*/

	/*@RequestMapping(value = "user/defaultSender/{sid}", method = RequestMethod.GET)
	public @ResponseBody int defaultSender(@PathVariable("sid") int sid) {
		// Long id = Long.parseLong(request.getParameter("uid"));
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal();

		return adminService.updateDefaultSender(currentUser.getUserid(), sid);

	}*/

}
