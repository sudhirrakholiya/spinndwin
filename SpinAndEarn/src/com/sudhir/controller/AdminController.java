package com.sudhir.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sudhir.config.SecurityConfig;
import com.sudhir.model.AppUser;
import com.sudhir.model.AppVersionInfo;
import com.sudhir.model.DatatableJsonObject;
import com.sudhir.model.MediUser;
import com.sudhir.model.ReferSetting;
import com.sudhir.model.SpinWallet;
import com.sudhir.model.WinWallet;
import com.sudhir.service.AdminService;
import com.sudhir.service.CommonService;
import com.sudhir.service.UserService;


@Controller
public class AdminController {

	@Autowired
	UserService userService;
	
	@Autowired
	AdminService adminService;
	
	@Autowired
	CommonService cservice;
	
	@Autowired
	SecurityConfig secconfig;
	
	@Autowired
	CommonService commonservice;

	LocalDateTime now = LocalDateTime.now();
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	final SimpleDateFormat df4 = new SimpleDateFormat("yyyyMMddHHmmss");
	final SimpleDateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
	
	
	@RequestMapping(value = "/addReferSetting", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String addReferSetting(HttpServletRequest request, HttpServletResponse response,@RequestParam("codeLimit") int codeLimit, 
			@RequestParam("newUserAmt") double newUserAmt,@RequestParam("oldUserAmt") double oldUserAmt,@RequestParam("spinReqAmt") double spinReqAmt,
			@RequestParam("signUpBonus") double signUpBonus,@RequestParam("withdrawAmt") double withdrawAmt,@RequestParam("betAmtValues") String betAmtValues) throws ParseException {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		 
		/* int cutt = 0;
			List<Object[]> data = adminService.getMerchantKey();
			for(Object[] result : data) {
				cutt = Integer.parseInt(result[8].toString());
			}*/
		 
		 System.out.println("addReferSetting call");
		 
		ReferSetting rs = new ReferSetting();
		rs.setCodeLimit(codeLimit);
		rs.setNewUserAmt(newUserAmt);
		rs.setOldUserAmt(oldUserAmt);
		rs.setAddedOn(new Date());
		rs.setSpinReqAmt(spinReqAmt);
		rs.setSignUpBonus(signUpBonus);
		rs.setWithdrawAmt(withdrawAmt);
		rs.setEnable(false);
		rs.setBetValues(betAmtValues);
		adminService.saveReferSetting(rs);
		return "1";
	}
	
	@RequestMapping({ "deleteRefSetting" })
	public String deleteRefSetting(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		 
		 System.out.println("deleteRefSetting call");
		 
		String srn = request.getParameter("srno");
		int srnm = Integer.parseInt(srn);
		long srnum = srnm;
		System.out.println("srnum::" + srnum + " " + " srn::" + srn);
		adminService.deleteRefSetting(srnum);
		return "redirect:/#/finance/AddReferSetting";
	}
	
	@RequestMapping(value = "/getReferalSetting", method = RequestMethod.GET)
	public @ResponseBody String getReferalSetting(HttpServletRequest request, HttpServletResponse response) {
		
		 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		
		 System.out.println("getReferalSetting call");
		List list = adminService.getRerferalSetting();
		DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
		datatableJsonObject.setRecordsFiltered(list.size());
		datatableJsonObject.setRecordsTotal(list.size());
		datatableJsonObject.setData(list);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(datatableJsonObject);
	}
	
	
	@RequestMapping(value = "/updateReferSetting", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String updateReferSetting(HttpServletRequest request, HttpServletResponse response,@RequestParam("codeLimit") int codeLimit, 
			@RequestParam("newUserAmt") double newUserAmt,@RequestParam("oldUserAmt") double oldUserAmt,
			@RequestParam("spinReqAmt") double spinReqAmt,@RequestParam("signUpBonus") double signUpBonus,@RequestParam("settid") long settid
			,@RequestParam("withdrawAmt") double withdrawAmt,@RequestParam("isEnable") boolean isEnable,@RequestParam("betAmtValues") String betAmtValues) throws ParseException {
		
		 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		 
		 System.out.println("updateReferSetting call");
		ReferSetting rs = new ReferSetting();
		rs.setCodeLimit(codeLimit);
		rs.setNewUserAmt(newUserAmt);
		rs.setOldUserAmt(oldUserAmt);
		rs.setAddedOn(new Date());
		rs.setSpinReqAmt(spinReqAmt);
		rs.setSignUpBonus(signUpBonus);
		rs.setWithdrawAmt(withdrawAmt);
		rs.setEnable(isEnable);
		rs.setBetValues(betAmtValues);
		rs.setSettid(settid);
		adminService.updateRefSetting(rs);
		return "1";
	}
	
	@RequestMapping(value = "/enableRefSetting", method = RequestMethod.POST)
	public @ResponseBody String enableRefSetting(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("setId") long setId,@RequestParam("flag") boolean flag) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		 
		 System.out.println("enableRefSetting call");
		int stat = adminService.enableRefSett(setId, flag);
		return String.valueOf(stat);
	}
	
	@RequestMapping(value = "/getAppUsers", method = RequestMethod.GET)
	public @ResponseBody String getAppUsers(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();

		 System.out.println("getAppUsers call");
		List list = adminService.getAppUsers();
		DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
		datatableJsonObject.setRecordsFiltered(list.size());
		datatableJsonObject.setRecordsTotal(list.size());
		datatableJsonObject.setData(list);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(datatableJsonObject);
	}
	
	@RequestMapping(value = "/getAppUsersAdmin", method = RequestMethod.GET)
	public @ResponseBody String getAppUsersAdmin(HttpServletRequest request, HttpServletResponse response) {
		String orderBy = request.getParameter("orderBy");
		String searchData = request.getParameter("searchStr");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal(); 
		long id = currentUser.getUserid();
		
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		
		try {
			if (request.getParameter("start") != null) {
				start = Integer.parseInt(request.getParameter("start"));
			}else {

			}
			if (request.getParameter("length") != null) {
				len = Integer.parseInt(request.getParameter("length"));
				listSize = len;
			} else {
				// System.out.println("LENGTH : " + len);
			}
			page = start / len + 1;
		} catch (Exception ex) {

		}
		StringBuilder query = new StringBuilder("SELECT * FROM appuser");
		StringBuilder queryCount = new StringBuilder("SELECT COUNT(*) FROM appuser");
		if(!searchData.equals("NA") && !searchData.isEmpty()) {
			query.append(" WHERE email='"+searchData+"' ");
			queryCount.append(" WHERE email='"+searchData+"' ");
		}
		System.out.println("Query::"+query.toString());
		String list = "";
		list = adminService.ConvertDataToWebJSON(queryCount.toString(), "" + query.toString(), page, listSize);
		if (list.length() != 0) {
			if (request.getParameter("callback") != null) {
				// System.out.println(request.getParameter("callback") +
				// "(" + list + ");");
				return request.getParameter("callback") + "(" + list + ");";
			} else {
				// System.out.println("IN LIST");
				return list;
			}
		} else {
			// return "[{\"responceCode\":\"No Data Found\"}]";
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		}
	}
	
	@RequestMapping(value = "/updateUserStat", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String updateUserStat(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("uid") long uid,@RequestParam("stat") boolean stat) throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		 
		 System.out.println("updateUserStat call");
		System.out.println("stat:::"+stat);
		int statnw = adminService.updateUserStatus(uid,stat);
		return "1";
	}
	
	@RequestMapping(value = "/adminSetWinAll", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String adminSetWinAll(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		int statnw = adminService.adminSetWinAll();
		return "1";
	}
	
	@RequestMapping(value = "/adminSetLoseAll", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String adminSetLoseAll(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		int statnw = adminService.adminSetLoseAll();
		return "1";
	}
	
	@RequestMapping(value = "/deleteAppUserByAdmin",method = RequestMethod.GET)
	public @ResponseBody String deleteAppUserByAdmin(HttpServletRequest request, HttpServletResponse response) {
	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		
		 System.out.println("deleteAppUserByAdmin call");
		 
		String reflCode = request.getParameter("reflCode");
		String srn = request.getParameter("srno");  
		int srnm = Integer.parseInt(srn);
		long srnum = srnm;
		adminService.deleteAppUserByAdmin(srnum);
		if(reflCode.equalsIgnoreCase("NA")) {
			System.out.println("");
		}else {
			adminService.updateClientReflLimit(reflCode);
		}
		return "1";
	}
	
	@RequestMapping(value = "/changeWinnerStat", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String changeWinnerStat(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("uid") long uid,@RequestParam("stat") boolean stat) throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		 
		 System.out.println("changeWinnerStat call");
		int statnw = adminService.changeWinnerFlag(uid,stat);
		return "1";
	}
	
	@RequestMapping(value = "/approveWithdrawReq", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String approveWithdrawReq(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("uid") long uid,@RequestParam("rqid") long rqid,@RequestParam("stat") boolean stat) throws ParseException {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		 
		 System.out.println("approveWithdrawReq call");
		double minWithdrawAmt=0;
		List<Object[]> settingListNw = adminService.getWidAmountByUser(uid,rqid);
		for(Object[] result : settingListNw) {
			minWithdrawAmt= Double.parseDouble(result[10].toString());
		}
		double walletAmt = 0;
		List<Object[]> userList = userService.getUserDetailsById(uid);
		for(Object[] result : userList) {
			walletAmt = (double) result[11];
		}
		double newWalletAmt = walletAmt - minWithdrawAmt;
		if(newWalletAmt < 0) {
			newWalletAmt = 0;
		}
		
		System.out.println("newWalletAmt::::"+newWalletAmt);
		int statnw = adminService.approveWithdrawReq(uid,stat,newWalletAmt,rqid);
		
		WinWallet sw = new WinWallet();
		sw.setWinAmt(minWithdrawAmt);
		sw.setTrDate(new Date());
		sw.setUserId(uid);
		sw.setRemark("Withdraw By User");
		sw.setTrType("Dr");
		userService.saveWinWallet(sw);
		return "1";
	}
	
	@RequestMapping(value = "/getRefUserUsed", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getRefUserUsed(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("refCode") String refCode) throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		
		 System.out.println("getRefUserUsed call");
		List<Object[]> list = adminService.getUsersByReferalUsed(refCode);
		DatatableJsonObject datatableJsonObject = new DatatableJsonObject();
		datatableJsonObject.setRecordsFiltered(list.size());
		datatableJsonObject.setRecordsTotal(list.size());
		datatableJsonObject.setData(list);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(datatableJsonObject);
	}
	
	@RequestMapping(value = "/addMoneyAdmin", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String addMoneyAdmin(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("uid") long uid,@RequestParam("adminAmt") double adminAmt) throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		 
		 System.out.println("addMoneyAdmin call");
		SpinWallet ww2 = new SpinWallet();
		ww2.setTrDate(new Date());
		ww2.setTrType("By Admin");
		ww2.setUserId(uid);
		ww2.setWinAmt(adminAmt);
		ww2.setTransType("Cr");
		ww2.setUsedRefCode("NA");
		userService.saveSpinWallet(ww2);
		int stat = userService.updateWalletaBalance(uid, adminAmt);
		return "1";
	}
	
	@RequestMapping(value = "/resetPass", method = RequestMethod.GET)
	public @ResponseBody String resetPass(@RequestParam("email") String email,@RequestParam("password") String password,
			@RequestParam("resetTocken") String resetTocken) throws ParseException, Exception {
		
		 System.out.println("resetPass call");
		AppUser us = (AppUser) cservice.getSingleObject("from AppUser Where email='"+email+"' ");
		if (us == null) {
			return "Invalid EmailId";
		} else {
			String dbToc = us.getResetTocken();
			if(dbToc.equals(resetTocken)) {
				String pass =secconfig.passwordEncoder().encode(password);
				int stat = adminService.updatePasswordByEmail(email, password);
				userService.updateResetTocketByUser("NA", email);
				return "Password Reset Successfully";
			}else {
			   return "Password Reset Link Expired !";
			}
		   
		}
     }
	
	@RequestMapping(value = "uploadApp", method = RequestMethod.POST)
	@ResponseBody public String uploadApp(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("image") MultipartFile file,@RequestParam("appVersion") String appVersion) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 MediUser currentUser = (MediUser) auth.getPrincipal(); 
		 long id = currentUser.getUserid();
		 
		 System.out.println("uploadApp call");
		 
		String replaceStrinng = "SpinWinAPP";
		String fileName = null;
		String filePath = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String offerdate = format.format( new Date());
		String logopath = null;
		if (!file.isEmpty()) {
			try {
				String SEP = System.getProperty("file.separator");
				fileName = file.getOriginalFilename();
				byte[] bytes = file.getBytes();
				int byteLen = bytes.length;
				String s1 = fileName.substring(fileName.indexOf(".") + 1);

				File convFile = new File(file.getOriginalFilename());
				convFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(convFile);
				fos.write(file.getBytes());
				fos.close();

				String clientSideFileName = convFile.getName();
				File uploadDirectory = convFile.getParentFile();
				String newFileName = replaceStrinng + "." + s1;
				File newFile = new File(uploadDirectory, newFileName);
				convFile.renameTo(newFile);

				filePath = request.getServletContext().getRealPath("//");
				System.out.println("Real_filePath---"+filePath);
				filePath = filePath + "UploadFile/apk";
				System.out.println("Real_filePath2---"+filePath);
				filePath = filePath + SEP;
				
				System.out.println(filePath);
				File directory = new File(filePath);
				if (!directory.exists()) {
					directory.mkdir();
				}
				BufferedOutputStream buffStream = new BufferedOutputStream(new FileOutputStream(new File(filePath + newFileName)));
				buffStream.write(bytes);
				System.out.println("File PAth : " + filePath);
				buffStream.close();
				String s = "You have successfully uploaded " + filePath;
				logopath = "UploadFile/apk/" + newFileName;
				System.out.println("logopath::" + logopath);
				
				AppVersionInfo avi = new AppVersionInfo();
				avi.setVersion(appVersion);
				avi.setAddedon(new Date());
				adminService.saveAppDetails(avi);
			}catch(Exception e){
				e.printStackTrace();
			}
		}else {
		}
		return "";
	}
	
	
	
	/*
	
	 @RequestMapping(value = "uploadApp", method = RequestMethod.POST)
	@ResponseBody public String uploadApp(HttpServletRequest request, HttpServletResponse response, Authentication auth,
			@RequestParam("image") MultipartFile[] files,@RequestParam("appVersion") String appVersion) {

		String replaceStrinng = "SpinWinAPP";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String offerdate = format.format( new Date());
		
	//	if (!file.isEmpty()) {
		Arrays.asList(files).stream().forEach(file -> {
			String fileName = null;
			String filePath = null;
			String logopath = null;
			if (!file.isEmpty()) {
			try {
				String SEP = System.getProperty("file.separator");
				fileName = file.getOriginalFilename();
				byte[] bytes = file.getBytes();
				int byteLen = bytes.length;
				String s1 = fileName.substring(fileName.indexOf(".") + 1);
				
				Path path = Paths.get(fileName);
				
				System.out.println("Real_filePath0---"+path.toAbsolutePath()+"----"+path.toRealPath());
				
				File convFile = new File(file.getOriginalFilename());
				convFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(convFile);
				fos.write(file.getBytes());
				fos.close();
				String clientSideFileName = convFile.getName();
				File uploadDirectory = convFile.getParentFile();
				String newFileName = replaceStrinng + "." + s1;
				File newFile = new File(uploadDirectory, newFileName);
				convFile.renameTo(newFile);

				filePath = request.getServletContext().getRealPath("//");
				System.out.println("Real_filePath---"+filePath);
				filePath = filePath + "UploadFile/apk";
				System.out.println("Real_filePath2---"+filePath);
				filePath = filePath + SEP;
				
				System.out.println(filePath);
				File directory = new File(filePath);
				if (!directory.exists()) {
					directory.mkdir();
				}
				BufferedOutputStream buffStream = new BufferedOutputStream(new FileOutputStream(new File(filePath + newFileName)));
				buffStream.write(bytes);
				System.out.println("File PAth : " + filePath);
				buffStream.close();
				String s = "You have successfully uploaded " + filePath;
				logopath = "UploadFile/apk/" + newFileName;
				System.out.println("logopath::" + logopath);
				
				AppVersionInfo avi = new AppVersionInfo();
				avi.setVersion(appVersion);
				avi.setAddedon(new Date());
				adminService.saveAppDetails(avi);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			}else {
				System.out.println("Empty FILE");
			}
		});
			
		//}else {
		//}
		return "";
	}
	  
	 */
	
	
	@RequestMapping(value = "/getWinWalletTransAdmin", method = RequestMethod.GET)
	public @ResponseBody String getWinWalletTransAdmin(HttpServletRequest request, HttpServletResponse response) {
		String orderBy = request.getParameter("orderBy");
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal(); 
		long id = currentUser.getUserid();
		
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		
		try {
			if (request.getParameter("start") != null) {
				start = Integer.parseInt(request.getParameter("start"));
			}else {

			}
			if (request.getParameter("length") != null) {
				len = Integer.parseInt(request.getParameter("length"));
				listSize = len;
			} else {
				// System.out.println("LENGTH : " + len);
			}
			page = start / len + 1;
		} catch (Exception ex) {

		}
		String query = "SELECT ww.wwid,au.username,ww.winAmt,ww.trType,ww.remark,ww.trDate FROM winwallet ww JOIN appuser au ON ww.userId=au.uid ORDER BY ww.trDate DESC";
		String queryCount = "SELECT COUNT(*) FROM winwallet ww JOIN appuser au ON ww.userId=au.uid ORDER BY ww.trDate DESC";
		
		String list = "";
		list = adminService.ConvertDataToWebJSON(queryCount.toString(), "" + query, page, listSize);
		if (list.length() != 0) {
			if (request.getParameter("callback") != null) {
				// System.out.println(request.getParameter("callback") +
				// "(" + list + ");");
				return request.getParameter("callback") + "(" + list + ");";
			} else {
				// System.out.println("IN LIST");
				return list;
			}
		} else {
			// return "[{\"responceCode\":\"No Data Found\"}]";
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		}
	}
	
	@RequestMapping(value = "/getSpinWalletTransAdmin", method = RequestMethod.GET)
	public @ResponseBody String getSpinWalletTransAdmin(HttpServletRequest request, HttpServletResponse response) {
		String orderBy = request.getParameter("orderBy");
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal(); 
		long id = currentUser.getUserid();
		
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		
		try {
			if (request.getParameter("start") != null) {
				start = Integer.parseInt(request.getParameter("start"));
			}else {

			}
			if (request.getParameter("length") != null) {
				len = Integer.parseInt(request.getParameter("length"));
				listSize = len;
			} else {
				// System.out.println("LENGTH : " + len);
			}
			page = start / len + 1;
		} catch (Exception ex) {

		}
		String query = "SELECT sw.swid,au.username,sw.winAmt,sw.transType,sw.trType,sw.usedRefCode,sw.trDate FROM spinwallet sw JOIN appuser au ON sw.userId=au.uid ORDER BY sw.trDate DESC";
		String queryCount = "SELECT COUNT(*) FROM spinwallet sw JOIN appuser au ON sw.userId=au.uid ORDER BY sw.trDate DESC";
		
		String list = "";
		list = adminService.ConvertDataToWebJSON(queryCount.toString(), "" + query, page, listSize);
		if (list.length() != 0) {
			if (request.getParameter("callback") != null) {
				// System.out.println(request.getParameter("callback") +
				// "(" + list + ");");
				return request.getParameter("callback") + "(" + list + ");";
			} else {
				// System.out.println("IN LIST");
				return list;
			}
		} else {
			// return "[{\"responceCode\":\"No Data Found\"}]";
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		}
	}
	
	
	@RequestMapping(value = "/getUserMoneyTransAdmin", method = RequestMethod.GET)
	public @ResponseBody String getUserMoneyTransAdmin(HttpServletRequest request, HttpServletResponse response) {
		String orderBy = request.getParameter("orderBy");
		String searchData = request.getParameter("searchStr");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal(); 
		long id = currentUser.getUserid();
		
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		
		try {
			if (request.getParameter("start") != null) {
				start = Integer.parseInt(request.getParameter("start"));
			}else {

			}
			if (request.getParameter("length") != null) {
				len = Integer.parseInt(request.getParameter("length"));
				listSize = len;
			} else {
				// System.out.println("LENGTH : " + len);
			}
			page = start / len + 1;
		} catch (Exception ex) {

		}
		
	//	String query = "SELECT um.srno,au.username,au.email,um.orderId,um.transcId,um.amount,um.source,um.addedon FROM usermoneytrans um JOIN appuser au ON um.userId=au.uid ORDER BY um.addedon DESC";
	//	String queryCount = "SELECT COUNT(*) FROM usermoneytrans um JOIN appuser au ON um.userId=au.uid ORDER BY um.addedon DESC";
		StringBuilder query = new StringBuilder("SELECT um.srno,au.username,au.email,um.orderId,um.transcId,um.amount,um.source,um.addedon FROM usermoneytrans um JOIN appuser au ON um.userId=au.uid ");
		StringBuilder queryCount = new StringBuilder("SELECT COUNT(*) FROM usermoneytrans um JOIN appuser au ON um.userId=au.uid ");
		if(!searchData.equals("NA") && !searchData.isEmpty()) {
			query.append(" WHERE au.email='"+searchData+"' ");
			queryCount.append(" WHERE email='"+searchData+"' ");
		}
		query.append(" ORDER BY um.addedon DESC");
		queryCount.append(" ORDER BY um.addedon DESC ");
		
		String list = "";
		list = adminService.ConvertDataToWebJSON(queryCount.toString(), "" + query.toString(), page, listSize);
		if (list.length() != 0) {
			if (request.getParameter("callback") != null) {
				// System.out.println(request.getParameter("callback") +
				// "(" + list + ");");
				return request.getParameter("callback") + "(" + list + ");";
			} else {
				// System.out.println("IN LIST");
				return list;
			}
		} else {
			// return "[{\"responceCode\":\"No Data Found\"}]";
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		}
	}
	
	@RequestMapping(value = "/getContactDetailsAdmin", method = RequestMethod.GET)
	public @ResponseBody String getContactDetailsAdmin(HttpServletRequest request, HttpServletResponse response) {
		String orderBy = request.getParameter("orderBy");
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MediUser currentUser = (MediUser) auth.getPrincipal(); 
		long id = currentUser.getUserid();
		
		org.json.simple.JSONArray jarray = new org.json.simple.JSONArray();
		int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		
		try {
			if (request.getParameter("start") != null) {
				start = Integer.parseInt(request.getParameter("start"));
			}else {

			}
			if (request.getParameter("length") != null) {
				len = Integer.parseInt(request.getParameter("length"));
				listSize = len;
			} else {
				// System.out.println("LENGTH : " + len);
			}
			page = start / len + 1;
		} catch (Exception ex) {

		}
		String query = "SELECT ct.cntid,au.username,au.email,ct.message FROM contact ct JOIN appuser au ON ct.userId= au.uid ";
		String queryCount = "SELECT COUNT(*) FROM contact ct JOIN appuser au ON ct.userId= au.uid";
		
		String list = "";
		list = adminService.ConvertDataToWebJSON(queryCount.toString(), "" + query, page, listSize);
		if (list.length() != 0) {
			if (request.getParameter("callback") != null) {
				// System.out.println(request.getParameter("callback") +
				// "(" + list + ");");
				return request.getParameter("callback") + "(" + list + ");";
			} else {
				// System.out.println("IN LIST");
				return list;
			}
		} else {
			// return "[{\"responceCode\":\"No Data Found\"}]";
			return request.getParameter("callback") + "(" + jarray.toJSONString() + ");";
		}
	}
	
	@RequestMapping(value = "/getAdminDashboardData", method = RequestMethod.GET)
	public @ResponseBody String getAdminDashboardData() {
	
		String appuserQuery = "SELECT COUNT(*) AS total FROM appuser";
		List appUserList = commonservice.createSqlQuery(appuserQuery);
		
		String totalEarnQuery = "SELECT FORMAT((SELECT IFNULL(SUM(amount),0) AS total FROM usermoneytrans),2) AS total";
		List totalEarnrList = commonservice.createSqlQuery(totalEarnQuery);
		
		String withdrawQuery = "SELECT IFNULL(SUM(withAmt),0) AS total FROM withdrawreq WHERE rqStatus=TRUE";
		List withdrawList = commonservice.createSqlQuery(withdrawQuery);
		
		String pendingQuery = "SELECT IFNULL(SUM(withAmt),0) AS total FROM withdrawreq WHERE rqStatus=FALSE";
		List pendingList = commonservice.createSqlQuery(pendingQuery);
		
		System.out.println("Data::"+appUserList.get(0));
		System.out.println("Data::"+totalEarnrList.get(0));
		System.out.println("Data::"+withdrawList.get(0));
		System.out.println("Data::"+pendingList.get(0));
		
		return "";
	}
	
}
