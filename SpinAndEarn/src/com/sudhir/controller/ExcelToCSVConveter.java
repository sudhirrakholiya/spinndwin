package com.sudhir.controller;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public class ExcelToCSVConveter {

	public static void main(String[] args) throws Exception {
        //
        // An excel file name. You can create a file name with a full path
        // information.
        //
        String filename = "D:/02.xls";

        //
        // Create an ArrayList to store the data read from excel sheet.
        //
        List sheetData = new ArrayList();

        FileInputStream fis = null;
        try {
            //
            // Create a FileInputStream that will be use to read the excel file.
            //
            fis = new FileInputStream(filename);

            //
            // Create an excel workbook from the file system.
            //
            HSSFWorkbook workbook = new HSSFWorkbook(fis);
            //
            // Get the first sheet on the workbook.
            //
            HSSFSheet sheet = workbook.getSheetAt(0);

            //
            // When we have a sheet object in hand we can iterator on each
            // sheet's rows and on each row's cells. We store the data read
            // on an ArrayList so that we can printed the content of the excel
            // to the console.
            //
            Iterator rows = sheet.rowIterator();
            while (rows.hasNext()) {
                HSSFRow row = (HSSFRow) rows.next();
                Iterator cells = row.cellIterator();

                List data = new ArrayList();
                while (cells.hasNext()) {
                    HSSFCell cell = (HSSFCell) cells.next();
                    cell.setCellType ( cell.CELL_TYPE_STRING );
                    data.add(cell);
                }

                sheetData.add(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                fis.close();
            }
        }

        showExelData(sheetData);
    }

    private static void showExelData(List sheetData) {
        //
        // Iterates the data and print it out to the console.
        //
    	try {
			int colsize=0;
			File file = new File("D:/02.csv");
			
			// if file doesnt exists, then create it
						if (!file.exists()) {
							file.createNewFile();
						}
						
						FileWriter fw = new FileWriter(file.getAbsoluteFile());
						BufferedWriter bw = new BufferedWriter(fw);
						 String rowData="";
			for (int i = 0; i < sheetData.size(); i++) {
				
			    List list = (List) sheetData.get(i);
			   
			    for (int j = 0; j < list.size(); j++) {
			    	
			    	if(colsize<list.size())
			    	{
			    		colsize=list.size();
			    	}
			        HSSFCell cell = (HSSFCell) list.get(j);
			        rowData+=cell.getRichStringCellValue().toString();
			       // System.out.print(cell.getRichStringCellValue().toString());
			        if (j < colsize-1) {
			        	rowData+=",";
			           // System.out.print(", ");
			        }
			       
			    }
	        	rowData+="\n";


			}
			int k=0;
		    String headerData="";
		    for(k=0;k<colsize;k++)
		    {
		    	headerData+="Column"+k;
		    	 if (k < colsize-1) {
			        	headerData+=",";
			           // System.out.print(", ");
			        }
		    	
		    }
		    bw.write(headerData);
		    bw.write("\n");
		    bw.write(rowData);
	       // bw.write("\n");
			System.out.println("Done");
		    System.out.print(rowData);
		    System.out.println("");
	        rowData="";
			  bw.close();
		} catch (IOException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
}
