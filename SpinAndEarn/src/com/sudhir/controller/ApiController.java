package com.sudhir.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TreeMap;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paytm.pg.merchant.PaytmChecksum;
import com.sudhir.config.SecurityConfig;
import com.sudhir.model.AppUser;
import com.sudhir.model.Contact;
import com.sudhir.model.EmailLogs;
import com.sudhir.model.SpinWallet;
import com.sudhir.model.UserMoneyTrans;
import com.sudhir.model.WinWallet;
import com.sudhir.model.WithdrawReq;
import com.sudhir.service.AdminService;
import com.sudhir.service.CommonService;
import com.sudhir.service.UserService;

@Controller
@MultipartConfig
public class ApiController
{
	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;
	
	@Autowired
	CommonService cs;
	
	@Autowired
	SecurityConfig secconfig;
	
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    
    @RequestMapping(value = { "/register" }, method = { RequestMethod.POST }, produces = { "application/json" })
    @ResponseBody
    public String register(HttpServletRequest request,HttpServletResponse response, @RequestParam("fullName")  String userName,
    		                 @RequestParam("password")  String password, @RequestParam("email")  String email,
    		                 @RequestParam("dob")  String dateOfBirth, @RequestParam("referalCode")  String referalCode, 
    		                 @RequestParam("imeiNo")  String imeiNo) throws ParseException {
         JSONObject jobj = new JSONObject();
         JSONArray arryyyyyy = new JSONArray();
        System.out.println("referalCode::" + referalCode);
         double winAmt = 0.0;
         double walletAmt = 0.0;
         double newUserAmt = 0.0;
         double extUserAmt = 0.0;
         double signUpBonus = 0.0;
         String ipAdd = request.getRemoteAddr();
        System.out.println("ipAdd:::" + ipAdd);
        AppUser us09 = (AppUser)cs.getSingleObject("from AppUser Where ipAddress='" + ipAdd + "'");
        if (us09 != null) {
        	 System.out.println("Sorry You Are Not Allowed");
            jobj.put("sucess", false);
            jobj.put("message", "Sorry You Are Not Allowed");
            return jobj.toJSONString();
        }
         AppUser us10 = (AppUser)cs.getSingleObject("from AppUser Where imeiNo='" + imeiNo + "'");
        if (us10 != null) {
            jobj.put("sucess", false);
            jobj.put("message", "User Already Register With This Device");
            return jobj.toJSONString();
        }
         AppUser us11 = (AppUser)cs.getSingleObject("from AppUser Where email='" + email + "'");
        if (us11 == null) {
            long refUserId = 0L;
            if (referalCode.equalsIgnoreCase("")) {
                System.out.println("Referal Code Not Avalible");
            }
            else {
                 AppUser us12 = (AppUser)cs.getSingleObject("from AppUser Where myReflCode='" + referalCode + "'");
                if (us12 == null) {
                    jobj.put("sucess", false);
                    jobj.put("message", "Invalid Referal Code");
                    return jobj.toJSONString();
                }
                refUserId = us12.getUid();
                System.out.println("Ref_userId:::" + us12.getUid());
            }
             List<Object[]> settingList = (List<Object[]>)adminService.getRerferalSetting();
            for ( Object[] result : settingList) {
                signUpBonus = Double.parseDouble(result[3].toString());
                extUserAmt = Double.parseDouble(result[4].toString());
            }
             String myReferalCode = RandomStringUtils.randomAlphanumeric(6).toLowerCase();
             AppUser au = new AppUser();
            au.setUsername(userName);
            au.setPassword(password);
            au.setEmail(email);
            au.setDateofbirth(dateOfBirth);
            au.setReflCode(referalCode);
            au.setEnabled(true);
            au.setWalletamount(walletAmt);
            au.setWinamount(winAmt);
            au.setMyReflCode(myReferalCode);
            au.setWin(false);
            au.setResetTocken("");
            au.setImeiNo(imeiNo);
            au.setIpAddress(ipAdd);
            au.setAddedOn(new Date());
            au.setWinExtra(false);
            userService.saveAppUsers(au);
             SpinWallet ww2 = new SpinWallet();
            ww2.setTrDate(new Date());
            ww2.setTrType("SignUp Bonus");
            ww2.setUserId(au.getUid());
            ww2.setWinAmt(signUpBonus);
            ww2.setTransType("Cr");
            ww2.setUsedRefCode(referalCode);
            userService.saveSpinWallet(ww2);
            int stat = userService.updateWalletaBalance(au.getUid(), signUpBonus);
            if (!referalCode.equalsIgnoreCase("")) {
                 SpinWallet ww3 = new SpinWallet();
                ww3.setTrDate(new Date());
                ww3.setTrType("EU Referal");
                ww3.setUserId(refUserId);
                ww3.setWinAmt(extUserAmt);
                ww3.setTransType("Cr");
                ww3.setUsedRefCode("NA");
                userService.saveSpinWallet(ww3);
                userService.updateWalletaBalance(refUserId, extUserAmt);
            }
             List<Object[]> userList = (List<Object[]>)userService.getUserDetailsById(au.getUid());
            for ( Object[] result2 : userList) {
                walletAmt = (double)result2[7];
            }
            jobj.put("sucess", true);
            jobj.put("message", "User Registered Successfully");
            jobj.put("userid", au.getUid());
            jobj.put("win_amt", winAmt);
            jobj.put("wallet_amt", walletAmt);
            jobj.put("username", au.getUsername());
            jobj.put("refferal", au.getMyReflCode());
            if (!referalCode.equalsIgnoreCase("")) {
                userService.updateClientRefl(referalCode);
            }
            return jobj.toJSONString();
        }else {
        	jobj.put("sucess", false);
            jobj.put("message", "User Already Register With Email");
            return jobj.toJSONString();
        }
        
    }
    
    @RequestMapping(value = { "/loginAppUser" }, method = { RequestMethod.GET }, produces = { "application/json" })
    @ResponseBody
    public String loginAppUser( HttpServletRequest request,  HttpServletResponse response, @RequestParam("email")  String email, @RequestParam("password")  String password, @RequestParam("imeiNo")  String imeiNo) throws ParseException {
         JSONObject jobj = new JSONObject();
         JSONArray arryyyyyy = new JSONArray();
         AppUser us = (AppUser)cs.getSingleObject("from AppUser Where email='" + email + "' AND  imeiNo='" + imeiNo + "'");
        if (us == null) {
            jobj.put("sucess", false);
            jobj.put("message", "You are not Authorised User");
            return jobj.toJSONString();
        }
         AppUser us2 = (AppUser)cs.getSingleObject("from AppUser Where email='" + email + "' AND  password='" + password + "' ");
        if (us2 == null) {
            jobj.put("sucess", false);
            jobj.put("message", "Login Details Not Matched");
            return jobj.toJSONString();
        }
         boolean enableFlag = true;
         long uid = us2.getUid();
         AppUser us3 = (AppUser)cs.getSingleObject("from AppUser Where email='" + email + "' AND enabled=" + enableFlag + " ");
        if (us3 == null) {
            jobj.put("sucess", false);
            jobj.put("message", "User Blocked By Admin");
            return jobj.toJSONString();
        }
         double winAmount = userService.getWinAmountByUser(uid);
         List<Object[]> userList = (List<Object[]>)userService.getUserDetails(email);
        for ( Object[] result : userList) {
            jobj.put("userid", result[0].toString());
            jobj.put("dob", result[1].toString());
            jobj.put("win_amt", Double.parseDouble(result[11].toString()));
            jobj.put("wallet_amt", Double.parseDouble(result[7].toString()));
            jobj.put("username", result[6].toString());
            jobj.put("refferal", result[8].toString());
        }
        
     /*  String betAmount = "";
       List<Object[]> settingList = (List<Object[]>)adminService.getRerferalSetting();
       for ( Object[] result : settingList) {
    	   betAmount = result[9].toString();
       }*/
        
    //    jobj.put("betAmount", betAmount);
        jobj.put("sucess", true);
        jobj.put("message", "User login Successfully");
        return jobj.toJSONString();
    }
   
   // Lucky11 
/*  @RequestMapping(value = { "/getSpin" }, method = { RequestMethod.GET }, produces = { "application/json" })
    @ResponseBody
    public String getSpin(HttpServletRequest request, HttpServletResponse response, @RequestParam("userid") long userid, @RequestParam("imeiNo") String imeiNo, @RequestParam("userGSNumber") String userGSNumber, @RequestParam("userGSAmt") double userGSAmt) throws ParseException {
        JSONObject jobj = new JSONObject();
        JSONArray arryyyyyy = new JSONArray();
        AppUser us = (AppUser)cs.getSingleObject("from AppUser Where uid='" + userid + "'");
        if (us == null) {
            jobj.put("sucess", false);
            jobj.put("message", "No User Found");
            return jobj.toJSONString();
        }
        AppUser us2 = (AppUser)cs.getSingleObject("from AppUser Where uid=" + userid + " AND imeiNo='" + imeiNo + "' ");
        if (us2 == null) {
            jobj.put("sucess", false);
            jobj.put("message", "You are not Authorised User");
            return jobj.toJSONString();
        }
        double spinAmt = 0.0;
        final List<Object[]> userList = (List<Object[]>)userService.getUserDetailsById(userid);
        double walletAmt = 0.0;
        boolean canWin = false;
        boolean isFirstSpin = false;
        double wimAmt = 0.0;
        for (Object[] result : userList) {
            walletAmt = (double)result[7];
            wimAmt = (double)result[11];
        }
        int countStat = this.userService.getSpinCountByUser(userid);
        if (countStat == 0) {
            isFirstSpin = true;
        }
        String[] numArray1 = userGSNumber.split(",");
        System.out.println("Length:::" + numArray1.length + "----Amt::" + numArray1.length * userGSAmt);
        spinAmt = numArray1.length * userGSAmt;
        if (walletAmt + wimAmt < spinAmt) {
            jobj.put("sucess", false);
            jobj.put("isFirstSpin", isFirstSpin);
            jobj.put("canWin", false);
            jobj.put("message", "Yo don't have amount to spin .would you like to buy some amount ?");
            return jobj.toJSONString();
        }
        if (walletAmt < spinAmt) {
            double diff = spinAmt - walletAmt;
            wimAmt -= diff;
            int winwalletStat = userService.updateWinWalletaBalByUser(userid, wimAmt);
            int updateStat = userService.updateWalletaBalByUser(userid, 0.0);
            List<Object[]> userListNew = (List<Object[]>)userService.getUserDetailsById(userid);
            double newwalletBal = 0.0;
            for (Object[] result2 : userListNew) {
                newwalletBal = (double)result2[7] + (double)result2[11];
            }
            boolean finalcanwin = false;
            String[] numArray2 = userGSNumber.split(",");
            for (int i = 0; i < numArray2.length; ++i) {
                System.out.println("Num::" + numArray2[i] + "---i::" + i);
                final SpinWallet ww = new SpinWallet();
                ww.setTrDate(new Date());
                ww.setTrType("Spin");
                ww.setUserId(userid);
                ww.setWinAmt(userGSAmt);
                ww.setTransType("Dr");
                ww.setUsedRefCode("NA");
                ww.setUserGSAmt(userGSAmt);
                ww.setUserGSNumber(Double.parseDouble(numArray2[i].toString()));
                userService.saveSpinWallet(ww);
                int countGuessStat = userService.userGSNumberCount(userGSAmt);
                System.out.println("countGuessStat::" + countGuessStat);
                canWin = (countGuessStat % 15 == 0);
                System.out.println("canWin::" + canWin);
                if (canWin) {
                    finalcanwin = true;
                }
            }
            jobj.put("sucess", true);
            jobj.put("isFirstSpin", isFirstSpin);
            jobj.put("canWin", finalcanwin);
            jobj.put("wallet_amt", newwalletBal);
            jobj.put("message", "Yo are allow to spin");
            return jobj.toJSONString();
        }
        boolean finalcanwin2 = false;
        String[] numArray3 = userGSNumber.split(",");
        for (int j = 0; j < numArray3.length; ++j) {
            System.out.println("Num::" + numArray3[j] + "---i::" + j);
            final SpinWallet ww2 = new SpinWallet();
            ww2.setTrDate(new Date());
            ww2.setTrType("Spin");
            ww2.setUserId(userid);
            ww2.setWinAmt(userGSAmt);
            ww2.setTransType("Dr");
            ww2.setUsedRefCode("NA");
            ww2.setUserGSAmt(userGSAmt);
            ww2.setUserGSNumber(Double.parseDouble(numArray3[j].toString()));
            this.userService.saveSpinWallet(ww2);
            final int countGuessStat2 = userService.userGSNumberCount(userGSAmt);
            System.out.println("countGuessStat::" + countGuessStat2);
            canWin = (countGuessStat2 % 20 == 0);
            System.out.println("canWin::" + canWin);
            if (canWin) {
                finalcanwin2 = true;
            }
        }
        walletAmt -= spinAmt;
        final int updateStat2 = userService.updateWalletaBalByUser(userid, walletAmt);
        final List<Object[]> userListNew2 = (List<Object[]>)this.userService.getUserDetailsById(userid);
        double newwalletBal2 = 0.0;
        for (final Object[] result3 : userListNew2) {
            newwalletBal2 = (double)result3[7] + (double)result3[11];
        }
        jobj.put("sucess", true);
        jobj.put("isFirstSpin", isFirstSpin);
        jobj.put("canWin", finalcanwin2);
        jobj.put("wallet_amt", newwalletBal2);
        jobj.put("message", "Yo are allow to spin");
        return jobj.toJSONString();
    } */
    
  // Lucky11 OLD
    
    
/*    @RequestMapping(value = { "/getSpin" }, method = { RequestMethod.GET }, produces = { "application/json" })
    @ResponseBody
    public String getSpin(final HttpServletRequest request, final HttpServletResponse response, @RequestParam("userid") final long userid, @RequestParam("imeiNo") final String imeiNo, @RequestParam("userGSNumber") final String userGSNumber, @RequestParam("userGSAmt") final double userGSAmt) throws ParseException {
        final JSONObject jobj = new JSONObject();
        final JSONArray arryyyyyy = new JSONArray();
        final AppUser us = (AppUser)this.cs.getSingleObject("from AppUser Where uid='" + userid + "'");
        if (us == null) {
            jobj.put((Object)"sucess", (Object)false);
            jobj.put((Object)"message", (Object)"No User Found");
            return jobj.toJSONString();
        }
//      final AppUser us2 = (AppUser)this.cs.getSingleObject("from AppUser Where uid=" + userid + " AND imeiNo='" + imeiNo + "' ");
//        if (us2 == null) {
//            jobj.put((Object)"sucess", (Object)false);
//            jobj.put((Object)"message", (Object)"You are not Authorised User");
//            return jobj.toJSONString();
//        }
        double spinAmt = 0.0;
        final List<Object[]> userList = (List<Object[]>)this.userService.getUserDetailsById(userid);
        double walletAmt = 0.0;
        boolean canWin = false;
        boolean isFirstSpin = false;
        double wimAmt = 0.0;
        for (final Object[] result : userList) {
            walletAmt = (double)result[7];
            wimAmt = (double)result[11];
        }
        final int countStat = this.userService.getSpinCountByUser(userid);
        if (countStat == 0) {
            isFirstSpin = true;
        }
        final String[] numArray1 = userGSNumber.split(",");
        System.out.println("Length:::" + numArray1.length + "----Amt::" + numArray1.length * userGSAmt);
        spinAmt = numArray1.length * userGSAmt;
        if (walletAmt + wimAmt < spinAmt) {
            jobj.put((Object)"sucess", (Object)false);
            jobj.put((Object)"isFirstSpin", (Object)isFirstSpin);
            jobj.put((Object)"canWin", (Object)false);
            jobj.put((Object)"message", (Object)"Yo don't have amount to spin .would you like to buy some amount ?");
            return jobj.toJSONString();
        }
        if (walletAmt < spinAmt) {
            final double diff = spinAmt - walletAmt;
            wimAmt -= diff;
            final int winwalletStat = this.userService.updateWinWalletaBalByUser(userid, wimAmt);
            final int updateStat = this.userService.updateWalletaBalByUser(userid, 0.0);
            final List<Object[]> userListNew = (List<Object[]>)this.userService.getUserDetailsById(userid);
            double newwalletBal = 0.0;
            for (final Object[] result2 : userListNew) {
                newwalletBal = (double)result2[7] + (double)result2[11];
            }
            boolean finalcanwin = false;
            final String[] numArray2 = userGSNumber.split(",");
            for (int i = 0; i < numArray2.length; ++i) {
                System.out.println("Num::" + numArray2[i] + "---i::" + i);
                final SpinWallet ww = new SpinWallet();
                ww.setTrDate(new Date());
                ww.setTrType("Spin");
                ww.setUserId(userid);
                ww.setWinAmt(userGSAmt);
                ww.setTransType("Dr");
                ww.setUsedRefCode("NA");
                ww.setUserGSAmt(userGSAmt);
                ww.setUserGSNumber(Double.parseDouble(numArray2[i].toString()));
                this.userService.saveSpinWallet(ww);
                final int countGuessStat = this.userService.userGSNumberCount(userGSAmt);
                System.out.println("countGuessStat::" + countGuessStat);
                canWin = (countGuessStat % 20 == 0);
                System.out.println("canWin::" + canWin);
                if (canWin) {
                    finalcanwin = true;
                }
            }
            jobj.put((Object)"sucess", (Object)true);
            jobj.put((Object)"isFirstSpin", (Object)isFirstSpin);
            jobj.put((Object)"canWin", (Object)finalcanwin);
            jobj.put((Object)"wallet_amt", (Object)newwalletBal);
            jobj.put((Object)"message", (Object)"Yo are allow to spin");
            return jobj.toJSONString();
        }
        boolean finalcanwin2 = false;
        final String[] numArray3 = userGSNumber.split(",");
        for (int j = 0; j < numArray3.length; ++j) {
            System.out.println("Num::" + numArray3[j] + "---i::" + j);
            final SpinWallet ww2 = new SpinWallet();
            ww2.setTrDate(new Date());
            ww2.setTrType("Spin");
            ww2.setUserId(userid);
            ww2.setWinAmt(userGSAmt);
            ww2.setTransType("Dr");
            ww2.setUsedRefCode("NA");
            ww2.setUserGSAmt(userGSAmt);
            ww2.setUserGSNumber(Double.parseDouble(numArray3[j].toString()));
            this.userService.saveSpinWallet(ww2);
            final int countGuessStat2 = this.userService.userGSNumberCount(userGSAmt);
            System.out.println("countGuessStat::" + countGuessStat2);
            canWin = (countGuessStat2 % 15 == 0);
            System.out.println("canWin::" + canWin);
            if (canWin) {
                finalcanwin2 = true;
            }
        }
        walletAmt -= spinAmt;
        final int updateStat2 = this.userService.updateWalletaBalByUser(userid, walletAmt);
        final List<Object[]> userListNew2 = (List<Object[]>)this.userService.getUserDetailsById(userid);
        double newwalletBal2 = 0.0;
        for (final Object[] result3 : userListNew2) {
            newwalletBal2 = (double)result3[7] + (double)result3[11];
        }
        jobj.put((Object)"sucess", (Object)true);
        jobj.put((Object)"isFirstSpin", (Object)isFirstSpin);
        jobj.put((Object)"canWin", (Object)finalcanwin2);
        jobj.put((Object)"wallet_amt", (Object)newwalletBal2);
        jobj.put((Object)"message", (Object)"Yo are allow to spin");
        return jobj.toJSONString();
    }*/
    
    // Spinndwin  & winzo_spinwin
  @RequestMapping(value = { "/getSpin" }, method = { RequestMethod.GET }, produces = { "application/json" })
    @ResponseBody
    public String getSpin( HttpServletRequest request,  HttpServletResponse response, @RequestParam("userid")  long userid, @RequestParam("imeiNo")  String imeiNo) throws ParseException {
         JSONObject jobj = new JSONObject();
         JSONArray arryyyyyy = new JSONArray();
         AppUser us = (AppUser)cs.getSingleObject("from AppUser Where uid='" + userid + "'");
        if (us == null) {
            jobj.put("sucess", false);
            jobj.put("message", "No User Found");
            return jobj.toJSONString();
        }
      AppUser us2 = (AppUser)cs.getSingleObject("from AppUser Where uid=" + userid + " AND imeiNo='" + imeiNo + "' ");
        if (us2 == null) {
            jobj.put("sucess", false);
            jobj.put("message", "You are not Authorised User");
            return jobj.toJSONString();
        }
        double spinAmt = 0.0;
         List<Object[]> settingList = (List<Object[]>)adminService.getRerferalSetting();
        for ( Object[] result : settingList) {
            spinAmt = Double.parseDouble(result[5].toString());
        }
         List<Object[]> userList = (List<Object[]>)userService.getUserDetailsById(userid);
        double walletAmt = 0.0;
        boolean canWin = false;
        boolean isFirstSpin = false;
        double wimAmt = 0.0;
        for ( Object[] result2 : userList) {
            walletAmt = (double)result2[7];
            canWin = (boolean)result2[10];
            wimAmt = (double)result2[11];
        }
         int countStat = userService.getSpinCountByUser(userid);
        if (countStat == 0) {
            isFirstSpin = true;
        }
        if (!canWin) {
            if (wimAmt < 8500.0) {
                canWin = true;
                adminService.changeWinnerFlagByApp(userid, canWin);
            }else {
                canWin = false;
                adminService.changeWinnerFlagByApp(userid, canWin);
            }
        }
        else {
            adminService.changeWinnerFlagByApp(userid, true);
        }
        if (walletAmt < spinAmt) {
            jobj.put("sucess", false);
            jobj.put("isFirstSpin", isFirstSpin);
            jobj.put("canWin", canWin);
            jobj.put("message", "Yo don't have amount to spin .would you like to buy some amount ?");
            return jobj.toJSONString();
        }
         SpinWallet ww = new SpinWallet();
        ww.setTrDate(new Date());
        ww.setTrType("Spin");
        ww.setUserId(userid);
        ww.setWinAmt(spinAmt);
        ww.setTransType("Dr");
        ww.setUsedRefCode("NA");
        userService.saveSpinWallet(ww);
        walletAmt -= spinAmt;
         int updateStat = userService.updateWalletaBalByUser(userid, walletAmt);
        jobj.put("sucess", true);
        jobj.put("isFirstSpin", isFirstSpin);
        jobj.put("canWin", canWin);
        jobj.put("wallet_amt", walletAmt);
        jobj.put("message", "Yo are allow to spin");
        return jobj.toJSONString();
    } 
    
 // Toss
    
 /* @RequestMapping(value = { "/getSpin" }, method = { RequestMethod.GET }, produces = { "application/json" })
    @ResponseBody
    public String getSpin( HttpServletRequest request,  HttpServletResponse response, @RequestParam("userid")  long userid, @RequestParam("imeiNo")  String imeiNo,
    		@RequestParam("betAmt") double betAmt) throws ParseException {
         JSONObject jobj = new JSONObject();
         JSONArray arryyyyyy = new JSONArray();
         AppUser us = (AppUser)cs.getSingleObject("from AppUser Where uid='" + userid + "'");
        if (us == null) {
            jobj.put("sucess", false);
            jobj.put("message", "No User Found");
            return jobj.toJSONString();
        }
         AppUser us2 = (AppUser)cs.getSingleObject("from AppUser Where uid=" + userid + " AND imeiNo='" + imeiNo + "' ");
        if (us2 == null) {
            jobj.put("sucess", false);
            jobj.put("message", "You are not Authorised User");
            return jobj.toJSONString();
        }
        double spinAmt = betAmt;
        double witdrawAmt=0.0;
         List<Object[]> settingList = (List<Object[]>)adminService.getRerferalSetting();
        for ( Object[] result : settingList) {
        //    spinAmt = Double.parseDouble(result[5].toString());
            witdrawAmt = Double.parseDouble(result[7].toString());
        }
        if (betAmt < 10.0) {
            jobj.put("sucess", false);
            jobj.put("message", "Invalid Bet Amount");
            return jobj.toJSONString();
        }
        
        List<Object[]> userList = (List<Object[]>)userService.getUserDetailsById(userid);
        double walletAmt = 0.0;
        boolean canWinAdmin = false;
        boolean isFirstSpin = false;
        double wimAmt = 0.0;
        for ( Object[] result2 : userList) {
            walletAmt = (double)result2[7];
            canWinAdmin = (boolean)result2[10];
            wimAmt = (double)result2[11];
        }
        int countStat = userService.getSpinCountByUser(userid);
        if (countStat == 0) {
            isFirstSpin = true;
        }
        
        boolean canWin = RandomResult.getRandomBoolean();
        System.out.println("canWin_Main:::"+canWin);
        if(canWin) {
        	betAmt = betAmt * 2;
        	wimAmt = wimAmt + betAmt;
        	if(wimAmt >= witdrawAmt) {
        		if(canWinAdmin) {
        			canWin = true;
				}else if(!canWinAdmin){
					canWin = false; 
				}
        	}
        }
        
        if (walletAmt < spinAmt) {
            jobj.put("sucess", false);
            jobj.put("isFirstSpin", isFirstSpin);
            jobj.put("canWin", canWin);
            jobj.put("message", "Yo don't have amount to toss .would you like to buy some amount ?");
            return jobj.toJSONString();
        }
        SpinWallet ww = new SpinWallet();
        ww.setTrDate(new Date());
        ww.setTrType("Toss");
        ww.setUserId(userid);
        ww.setWinAmt(betAmt);
        ww.setTransType("Dr");
        ww.setUsedRefCode("NA");
        userService.saveSpinWallet(ww);
        walletAmt -= spinAmt;
        int updateStat = userService.updateWalletaBalByUser(userid, walletAmt);
         
        System.out.println("canWin_Final ::"+canWin);
        jobj.put("sucess", true);
        jobj.put("isFirstSpin", isFirstSpin);
        jobj.put("canWin", canWin);
        jobj.put("wallet_amt", walletAmt);
        jobj.put("message", "Yo are allow to toss");
        return jobj.toJSONString();
    }*/
    
  @RequestMapping(value = { "/spinned" }, method = { RequestMethod.GET }, produces = { "application/json" })
    @ResponseBody public String spinned( HttpServletRequest request,  HttpServletResponse response, @RequestParam("winamt")  double winamt,
    		@RequestParam("userid")  long userid, @RequestParam("isWin")  boolean isWin, @RequestParam("imeiNo")  String imeiNo) throws ParseException {
         JSONObject jobj = new JSONObject();
         JSONArray arryyyyyy = new JSONArray();
         AppUser us = (AppUser)cs.getSingleObject("from AppUser Where uid='" + userid + "'");
        if (us == null) {
            jobj.put("sucess", false);
            jobj.put("message", "No User Found");
            return jobj.toJSONString();
        }
        AppUser us2 = (AppUser)cs.getSingleObject("from AppUser Where uid=" + userid + " AND imeiNo='" + imeiNo + "' ");
        if (us2 == null) {
            jobj.put("sucess", false);
            jobj.put("message", "You are not Authorised User");
            return jobj.toJSONString();
        }
        double witdrawAmt=0.0;
        List<Object[]> settingList = (List<Object[]>)adminService.getRerferalSetting();
       for ( Object[] result : settingList) {
           witdrawAmt = Double.parseDouble(result[7].toString());
       }
         List<Object[]> userList = (List<Object[]>)userService.getUserDetailsById(userid);
        double walletAmt = 0.0;
        double winAmount = 0.0;
        boolean isWinFlag = false;
        String trType = "NA";
        for ( Object[] result : userList) {
            walletAmt = (double)result[7];
            winAmount = (double)result[11];
            isWinFlag = (boolean)result[10];
        }
        System.out.println("isWin_spinned  ::"+userid+"--"+ isWin);
        if(isWin) {
        	winAmount += winamt;
        	trType = "Cr";
        }else {
        	//winAmount -= winamt; 
         	trType = "Dr";
        }
//         if(winAmount >= witdrawAmt) {
//            int stat = adminService.changeWinnerFlag(userid, false);
//         }
        WinWallet sw = new WinWallet();
        sw.setWinAmt(winamt);
        sw.setTrDate(new Date());
        sw.setUserId(userid);
        sw.setTrType(trType);
        sw.setRemark("Toss Win");
        userService.saveWinWallet(sw);
         int updateStat = userService.updateWalletaBalAfterSpin(userid, winAmount);
        jobj.put("sucess", true);
        jobj.put("wallet_amt", walletAmt);
        jobj.put("isWin", isWinFlag);
        jobj.put("win_amt", winAmount);
        return jobj.toJSONString();
    }
    
    
   // Lucky11 OLD spinned
    
    
/*    @RequestMapping(value = { "/spinned" }, method = { RequestMethod.GET }, produces = { "application/json" })
    @ResponseBody
    public String spinned(final HttpServletRequest request, final HttpServletResponse response, @RequestParam("winamt") final double winamt, @RequestParam("userid") final long userid, @RequestParam("isWin") final boolean isWin, @RequestParam("imeiNo") final String imeiNo) throws ParseException {
        final JSONObject jobj = new JSONObject();
        final JSONArray arryyyyyy = new JSONArray();
        final AppUser us = (AppUser)this.cs.getSingleObject("from AppUser Where uid='" + userid + "'");
        if (us == null) {
            jobj.put((Object)"sucess", (Object)false);
            jobj.put((Object)"message", (Object)"No User Found");
            return jobj.toJSONString();
        }
//        final AppUser us2 = (AppUser)this.cs.getSingleObject("from AppUser Where uid=" + userid + " AND imeiNo='" + imeiNo + "' ");
//        if (us2 == null) {
//            jobj.put((Object)"sucess", (Object)false);
//            jobj.put((Object)"message", (Object)"You are not Authorised User");
//            return jobj.toJSONString();
//        }
        final List<Object[]> userList = (List<Object[]>)this.userService.getUserDetailsById(userid);
        double walletAmt = 0.0;
        double winAmount = 0.0;
        boolean isWinFlag = false;
        String trType = "NA";
        boolean canWinExt = false;
        for (final Object[] result : userList) {
            walletAmt = (double)result[7];
            winAmount = (double)result[11];
            isWinFlag = (boolean)result[10];
            canWinExt = (boolean)result[15];
        }
        if (isWin) {
            System.out.println("is win True");
            winAmount += winamt;
            trType = "Cr";
        }else {
            winAmount += winamt;
            trType = "Dr";
        }
        if (winAmount < 0.0) {
            winAmount = 0.0;
        }
        if (winamt < 0.0) {
            trType = "Dr";
        }else {
            trType = "Cr";
        }
        final WinWallet sw = new WinWallet();
        sw.setWinAmt(winamt);
        sw.setTrDate(new Date());
        sw.setUserId(userid);
        sw.setTrType(trType);
        sw.setRemark("Spin Win");
        this.userService.saveWinWallet(sw);
        final int updateStat = this.userService.updateWalletaBalAfterSpin(userid, winAmount);
        jobj.put((Object)"sucess", (Object)true);
        jobj.put((Object)"wallet_amt", (Object)(walletAmt + winAmount));
        jobj.put((Object)"isWin", (Object)isWinFlag);
        jobj.put((Object)"win_amt", (Object)winAmount);
        return jobj.toJSONString();
    }*/
    
    @RequestMapping(value = { "/withdrawReq" }, method = { RequestMethod.GET }, produces = { "application/json" })
    @ResponseBody
    public String withdrawReq( HttpServletRequest request,  HttpServletResponse response, @RequestParam("userid")  long userid, 
    		@RequestParam("rqType")  String rqType, @RequestParam("accName")  String accName, 
    		@RequestParam("accNo")  String accNo, @RequestParam("ifsc")  String ifsc, 
    		@RequestParam("mobileUPI")  String mobileUPI, @RequestParam("widAmt")  double widAmt, 
    		@RequestParam("imeiNo")  String imeiNo) throws ParseException {
         JSONObject jobj = new JSONObject();
         JSONArray arryyyyyy = new JSONArray();
         AppUser us = (AppUser)cs.getSingleObject("from AppUser Where uid=" + userid + " ");
        if (us == null) {
            jobj.put("sucess", false);
            jobj.put("message", "No User Found");
            return jobj.toJSONString();
        }
        AppUser us2 = (AppUser)cs.getSingleObject("from AppUser Where uid=" + userid + " AND imeiNo='" + imeiNo + "' ");
        if (us2 == null) {
            jobj.put("sucess", false);
            jobj.put("message", "You are not Authorised User");
            return jobj.toJSONString();
        }
        double minWithdrawAmt = 0.0;
         List<Object[]> settingListNw = (List<Object[]>)adminService.getRerferalSetting();
        for ( Object[] result : settingListNw) {
            minWithdrawAmt = Double.parseDouble(result[7].toString());
        }
         List<Object[]> userList = (List<Object[]>)userService.getUserDetailsById(userid);
        double walletAmt = 0.0;
        for ( Object[] result2 : userList) {
            walletAmt = (double)result2[11];
        }
        System.out.println("walletAmt::"+walletAmt+"-----minWithdrawAmt:::"+minWithdrawAmt);
        if(walletAmt < minWithdrawAmt) {
        	jobj.put("sucess", false);
            jobj.put("message", "Your Wallet Amount Is not Match with Minimum withdraw amount");
            jobj.put("totalAmount", us.getWalletamount());
            jobj.put("winingAmount", us.getWinamount());
            return jobj.toJSONString();
        }
        
        if (walletAmt >= minWithdrawAmt) {
             WithdrawReq wr = new WithdrawReq();
            if (rqType.equalsIgnoreCase("Bank")) {
                wr.setAccName(accName);
                wr.setAccNo(accNo);
                wr.setIfsc(ifsc);
                wr.setMobileUPI("NA");
                wr.setRqType(rqType);
                wr.setRqStatus(false);
                wr.setRqDate(new Date());
                wr.setUserId(userid);
                wr.setWithAmt(widAmt);
                wr.setApprvDate("");
                userService.saveWithdrawReq(wr);
            }else {
                if (!rqType.equalsIgnoreCase("UPI") && !rqType.equalsIgnoreCase("Paytm")) {
                    jobj.put("sucess", false);
                    jobj.put("message", "Invalid Request Type");
                    return jobj.toJSONString();
                }
                wr.setAccName("NA");
                wr.setAccNo("NA");
                wr.setIfsc("NA");
                wr.setMobileUPI(mobileUPI);
                wr.setRqType(rqType);
                wr.setRqStatus(false);
                wr.setRqDate(new Date());
                wr.setUserId(userid);
                wr.setWithAmt(widAmt);
                wr.setApprvDate("");
                userService.saveWithdrawReq(wr);
            }
            double newWalletAmt = walletAmt - widAmt;
            if (newWalletAmt < 0.0) {
                newWalletAmt = 0.0;
            }
             int updateStat = userService.updateWalletaBalAfterSpin(userid, newWalletAmt);
             adminService.changeWinnerFlag(userid, false);
            jobj.put("sucess", true);
            jobj.put("message", "Withdraw request sent Successfully.");
            jobj.put("totalAmount", us.getWalletamount());
            jobj.put("winingAmount", newWalletAmt);
            return jobj.toJSONString();
        }else {
        	jobj.put("sucess", false);
            jobj.put("message", "Your Wallet Amount Is not Match with Minimum withdraw amount");
            jobj.put("totalAmount", us.getWalletamount());
            jobj.put("winingAmount", us.getWinamount());
            return jobj.toJSONString();
        }
    }
    
    @RequestMapping(value = { "/contact" }, method = { RequestMethod.GET }, produces = { "application/json" })
    @ResponseBody
    public String contact( HttpServletRequest request,  HttpServletResponse response, @RequestParam("message")  String message, @RequestParam("userid")  long userid, @RequestParam("imeiNo")  String imeiNo) throws ParseException {
         JSONObject jobj = new JSONObject();
         JSONArray arryyyyyy = new JSONArray();
         AppUser us = (AppUser)cs.getSingleObject("from AppUser Where uid=" + userid + " ");
        if (us == null) {
            jobj.put("sucess", false);
            jobj.put("message", "No User Found");
            return jobj.toJSONString();
        }
        AppUser us2 = (AppUser)cs.getSingleObject("from AppUser Where uid=" + userid + " AND imeiNo='" + imeiNo + "' ");
        if (us2 == null) {
            jobj.put("sucess", false);
            jobj.put("message", "You are not Authorised User");
            return jobj.toJSONString();
        }
        Contact ct = new Contact();
        ct.setMessage(message);
        ct.setUserId(userid);
        userService.saveContactReq(ct);
        jobj.put("sucess", true);
        jobj.put("message", "contact request sent Successfully.");
        return jobj.toJSONString();
    }
    
    @RequestMapping(value = "/userTransactions", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String userTransactions(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("userId") long userId ,@RequestParam("imeiNo") String imeiNo) throws ParseException {
		
		JSONObject jobj = new JSONObject();
		AppUser us = (AppUser) cs.getSingleObject("from AppUser Where uid="+userId+" ");
		if (us == null) {
			jobj.put("sucess", false);
			jobj.put("message", "No User Found");
			return jobj.toJSONString();
		} else {
			AppUser us2 = (AppUser) cs.getSingleObject("from AppUser Where uid="+userId+" AND imeiNo='"+imeiNo+"' ");
			if(us2==null) {
				jobj.put("sucess", false);
				jobj.put("message", "You are not Authorised User");
				return jobj.toJSONString();
			}			
			JSONArray referalArray = new JSONArray();
			List<Object[]> refTrans = userService.getUserRefTransById(userId);
			for(Object[] result : refTrans) {
				JSONObject reflObj = new JSONObject();
				reflObj.put("trDate", result[1].toString());
				reflObj.put("trName", result[2].toString());
				reflObj.put("winAmt", result[4].toString());
				reflObj.put("trType", result[6].toString());
				referalArray.add(reflObj);
			}
			jobj.put("ReferalTransactions", referalArray);
			
			JSONArray spinArray = new JSONArray();
			List<Object[]> spinTrans = userService.getUserSpinTransById(userId);
			for(Object[] result : spinTrans) {
				JSONObject spinObj = new JSONObject();
				spinObj.put("trDate", result[1].toString());
				spinObj.put("trName", result[2].toString());
				spinObj.put("spinAmt", result[4].toString());
				spinObj.put("trType", result[6].toString());
				spinArray.add(spinObj);
			}
			jobj.put("SpinTransactions", spinArray);
			
			JSONArray signupArray = new JSONArray();
			List<Object[]> signupTrans = userService.getUserSignUpTransById(userId);
			for(Object[] result : signupTrans) {
				JSONObject signupObj = new JSONObject();
				signupObj.put("trDate", result[1].toString());
				signupObj.put("trName", result[2].toString());
				signupObj.put("winAmt", result[4].toString());
				signupObj.put("usedRef", result[5].toString());
				signupObj.put("trType", result[6].toString());
				signupArray.add(signupObj);
			}
			jobj.put("SignUpTransactions", signupArray);
			
			JSONArray adminArray = new JSONArray();
			List<Object[]> adminTrans = userService.getUserAdminTransById(userId);
			for(Object[] result : adminTrans) {
				JSONObject adminObj = new JSONObject();
				adminObj.put("trDate", result[1].toString());
				adminObj.put("trName", result[2].toString());
				adminObj.put("winAmt", result[4].toString());
				adminObj.put("trType", result[6].toString());
				adminArray.add(adminObj);
			}
			jobj.put("AdminTransactions", adminArray);
			
			JSONArray winWalletArray = new JSONArray();
			List<Object[]> winWalletTrans = userService.getUserWinTransById(userId);
			for(Object[] result : winWalletTrans) {
				JSONObject winWalletObj = new JSONObject();
				winWalletObj.put("trDate", result[1].toString());
				winWalletObj.put("trType", result[2].toString());
				winWalletObj.put("winAmt", result[4].toString());
				winWalletObj.put("trName", result[5].toString());
				winWalletArray.add(winWalletObj);
			}
			jobj.put("WinWalletTransactions", winWalletArray);
			
			JSONArray paytmWalletArray = new JSONArray();
			List<Object[]> paytmWalletTrans = userService.getUserPaytmTransById(userId);
			for(Object[] result : paytmWalletTrans) {
				JSONObject paytmWalletObj = new JSONObject();
				paytmWalletObj.put("trDate", result[1].toString());
				paytmWalletObj.put("trType", "Cr");
				paytmWalletObj.put("winAmt", result[2].toString());
				paytmWalletObj.put("trName", result[4].toString());
				paytmWalletArray.add(paytmWalletObj);
			}
			jobj.put("PaytmWalletTransactions", paytmWalletArray);
			
			return jobj.toJSONString();
		}
	}
    
    @RequestMapping(value = "/paytmCheckSum", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String paytmCheckSum(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("mId") String YOUR_MID,@RequestParam("orderId") String YOUR_ORDERID,
			@RequestParam("CUST_ID") String CUST_ID,@RequestParam("INDUSTRY_TYPE_ID") String INDUSTRY_TYPE_ID,
			@RequestParam("TXN_AMOUNT") String TXN_AMOUNT,@RequestParam("CHANNEL_ID") String CHANNEL_ID,
			@RequestParam("WEBSITE") String WEBSITE,@RequestParam("EMAIL") String EMAIL,
			@RequestParam("CALLBACK_URL") String CALLBACK_URL) throws Exception {
		
		JSONObject jobj = new JSONObject();
		String YOUR_MERCHANT_KEY = "";
		List<Object[]> data = adminService.getMerchantKey();
		for(Object[] result : data) {
			YOUR_MERCHANT_KEY = result[5].toString();
		//	YOUR_MID =  result[3].toString();
		}
		System.out.println("YOUR_MERCHANT_KEY::"+YOUR_MERCHANT_KEY+"----YOUR_MID::"+YOUR_MID);
		//String YOUR_ORDERID="";
		//String YOUR_MID = "YqgyOp11131432491538";        // TEST_KEY  : uKNJbv68577684387916   PRODUCTION_KEY : YqgyOp11131432491538
		//String YOUR_MERCHANT_KEY = "!jjRPTgJF12oulSb";   // TEST_KEY  : T!4lzmc@f3L3uS%c       PRODUCTION_KEY : !jjRPTgJF12oulSb
		/* initialize an hash */
        TreeMap<String, String> params = new TreeMap<String, String>();
        params.put("MID", YOUR_MID);
        params.put("ORDERID", YOUR_ORDERID);
        params.put("CUST_ID", CUST_ID);
        params.put("INDUSTRY_TYPE_ID", INDUSTRY_TYPE_ID);
        params.put("TXN_AMOUNT", TXN_AMOUNT);
        params.put("CHANNEL_ID", CHANNEL_ID);
        params.put("WEBSITE", WEBSITE);
        params.put("EMAIL", EMAIL);
        params.put("CALLBACK_URL", CALLBACK_URL);
        /**
         * Generate checksum by parameters we have
         * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
         */
        
        String paytmChecksum = PaytmChecksum.generateSignature(params, YOUR_MERCHANT_KEY);
        boolean verifySignature = PaytmChecksum.verifySignature(params, YOUR_MERCHANT_KEY, paytmChecksum);
        System.out.println("generateSignature Returns: " + paytmChecksum);
        System.out.println("verifySignature Returns: " + verifySignature);

        /* initialize JSON String */
        String body = "{\"mid\":"+YOUR_MID+",\"orderId\":"+YOUR_ORDERID+"}";
        System.out.println("body::"+body);

        /**
         * Generate checksum by parameters we have in body
         * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
         */
        jobj.put("paytmChecksum", paytmChecksum);
        jobj.put("verifySignature", verifySignature);
		
		return jobj.toJSONString();
	}
    
    @RequestMapping(value = "/forgotPass", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String forgotPass(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("email") String email,@RequestParam("imeiNo") String imeiNo) throws ParseException, Exception {
		
		JSONObject jobj = new JSONObject();
		JSONArray arryyyyyy = new JSONArray();
		AppUser us = (AppUser) cs.getSingleObject("from AppUser Where email='"+email+"' ");
		if (us == null) {
			jobj.put("sucess", false);
			jobj.put("message", "No User Found");
			return jobj.toJSONString();
		} else {
			AppUser us2 = (AppUser) cs.getSingleObject("from AppUser Where email='"+email+"' AND imeiNo='"+imeiNo+"' ");
			if(us2==null) {
				jobj.put("sucess", false);
				jobj.put("message", "You are not Authorised User");
				return jobj.toJSONString();
			}
			
//			 String fromEmail = "hello@mementotech.in";
//			 String password = "satish@123";
//			Properties props = new Properties();
//			props.put("mail.smtp.host", "mail.mementotech.in");
//			props.put("mail.smtp.port", "587");
//			props.put("mail.smtp.auth", "true");
//			props.put("mail.smtp.starttls.enable", "true");
			
			System.out.println("Email Start");
			
			 String fromEmail = "tp223588@gmail.com";
			 String password = "tusharpatel@2235";
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			
			
			Authenticator auth = new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(fromEmail, password);
				}
			};
			String resetTocken = RandomStringUtils.randomAlphanumeric(6).toLowerCase();
			Session session = Session.getInstance(props, auth);
		      try {
		    	  String Link = "http://104.237.9.106/forgotpassword.jsp?toc="+resetTocken;    // SpinnWin Link
		    //	  String Link = "http://104.237.9.106:9091/forgotpassword.jsp?toc="+resetTocken;  //winzo Link
		    //	  String Link = "http://185.183.182.43:8082/forgotpassword.jsp?toc="+resetTocken;  // Lucky11
		   // 	  String Link = "http://104.237.9.106:9099/forgotpassword.jsp?toc="+resetTocken;  // Lucky11  104.237.9.106
		    	    MimeMessage msg = new MimeMessage(session);
					  msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
				      msg.addHeader("format", "flowed");
				      msg.addHeader("Content-Transfer-Encoding", "8bit");
//				      msg.setFrom(new InternetAddress("hello@mementotech.in", "NoReply-Memento"));
//				      msg.setReplyTo(InternetAddress.parse("hello@mementotech.in", false));
				      msg.setFrom(new InternetAddress("tp223588@gmail.com", "NoReply-Lucky11"));
				      msg.setReplyTo(InternetAddress.parse("tp223588@gmail.com", false));
				      msg.setSubject("Password Reset Link", "UTF-8");
				      msg.setText(Link, "UTF-8");
				      msg.setSentDate(new Date());
				      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
				      
				      MimeBodyPart p1 = new MimeBodyPart();
			          p1.setText(Link);
			          
			          Transport.send(msg);
			          System.out.println("Email Successfully Sent to:"+email);
			          
			          EmailLogs logs = new EmailLogs();
			          logs.setEmailId(email);
			          logs.setEmailOn(new Date());
			          logs.setEmailStatus("Sent");
			          adminService.saveEmailLogs(logs);
			          userService.updateResetTocketByUser(resetTocken, email);
			          
			          jobj.put("sucess", true);
					  jobj.put("message", "Email Successfully Sent to :"+ email);
					  return jobj.toJSONString();
		      } catch (MessagingException e) {
		    	  EmailLogs logs = new EmailLogs();
		    	  logs.setEmailId(email);
		          logs.setEmailOn(new Date());
		          logs.setEmailStatus("Failed");
		          adminService.saveEmailLogs(logs);
				  e.printStackTrace();
				  jobj.put("sucess", false);
				  jobj.put("message", e.getMessage());
				  return jobj.toJSONString();
			}
		}
     }
    
    @RequestMapping(value = "/addMoneyUser", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String addMoneyAdmin(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("uid") long uid,@RequestParam("orderAmt") double orderAmt,
			@RequestParam("orderId") String orderId,@RequestParam("tnxId") String tnxId,@RequestParam("imeiNo") String imeiNo) throws ParseException {

		JSONObject jobj = new JSONObject();
		JSONArray arryyyyyy = new JSONArray();
		AppUser us = (AppUser) cs.getSingleObject("from AppUser Where uid="+uid+" ");
		if (us == null) {
			jobj.put("sucess", false);
			jobj.put("message", "No User Found");
			return jobj.toJSONString();
		}else {
			AppUser us2 = (AppUser) cs.getSingleObject("from AppUser Where uid="+uid+" AND imeiNo='"+imeiNo+"' ");
			if(us2==null) {
				jobj.put("sucess", false);
				jobj.put("message", "You are not Authorised User");
				return jobj.toJSONString();
			}
			UserMoneyTrans umt = new UserMoneyTrans();
			umt.setAmount(orderAmt);
			umt.setOrderId(orderId);
			umt.setSource("Paytm");
			umt.setTranscId(tnxId);
			umt.setUserId(uid);
			umt.setAddedon(new Date());
			userService.saveUserMoneyLogs(umt);
		
		    SpinWallet ww2 = new SpinWallet();
		    ww2.setTrDate(new Date());
		    ww2.setTrType("By User Paytm");
		    ww2.setUserId(uid);
		    ww2.setWinAmt(orderAmt);
		    ww2.setTransType("Cr");
		    ww2.setUsedRefCode("NA");
		    userService.saveSpinWallet(ww2);
		    int stat = userService.updateWalletaBalance(uid, orderAmt);
		    List<Object[]> userList = userService.getUserDetailsById(uid);
			double walletAmt = 0;
			for(Object[] result : userList) {
				walletAmt = (double) result[7];
			}
		    
		    jobj.put("sucess", true);
			jobj.put("message", "Details Saved");
			jobj.put("walletAmt", walletAmt);
			return jobj.toJSONString();
	      }
   }
    
    @RequestMapping(value = "/latestVersion", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String latestVersion(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		JSONObject jobj = new JSONObject();
		List<Object[]> infoList = userService.getAppVersionInfo();
		String latestVersion = "";
		for(Object[] result : infoList) {
			latestVersion= result[2].toString();
		}
		jobj.put("latest_version", latestVersion);
		return jobj.toJSONString();
	}
	
	@RequestMapping(value = "/getPaytmKeys", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getPaytmKeys(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		JSONObject jobj = new JSONObject();
		List<Object[]> infoList = adminService.getMerchantKey();
		for(Object[] result : infoList) {
			jobj.put("YOUR_MID", result[3].toString());
			jobj.put("YOUR_MERCHANT_KEY", result[5].toString());
		}
		return jobj.toJSONString();
	}
	
	
}