package com.sudhir.dto;

import java.math.BigInteger;
import java.util.Date;

public class My_Lead_DTO {
	
	int id;
	String clientName;
	String emailId;
	String firm_name;
	String login_date;
	BigInteger login_loan_amt;
	String  mobileNo;
	String priority;
	String name;
	String product;
	String reference_by;
	String sanction_date;
	BigInteger sanction_loan_amt;
	String extre1;
	String extre2;
	String extre3;
	String extre4;
	String extre5;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getFirm_name() {
		return firm_name;
	}
	public void setFirm_name(String firm_name) {
		this.firm_name = firm_name;
	}
	public String getLogin_date() {
		return login_date;
	}
	public void setLogin_date(String login_date) {
		this.login_date = login_date;
	}
	public BigInteger getLogin_loan_amt() {
		return login_loan_amt;
	}
	public void setLogin_loan_amt(BigInteger login_loan_amt) {
		this.login_loan_amt = login_loan_amt;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getReference_by() {
		return reference_by;
	}
	public void setReference_by(String reference_by) {
		this.reference_by = reference_by;
	}
	public String getSanction_date() {
		return sanction_date;
	}
	public void setSanction_date(String sanction_date) {
		this.sanction_date = sanction_date;
	}
	public BigInteger getSanction_loan_amt() {
		return sanction_loan_amt;
	}
	public void setSanction_loan_amt(BigInteger sanction_loan_amt) {
		this.sanction_loan_amt = sanction_loan_amt;
	}
	public String getExtre1() {
		return extre1;
	}
	public void setExtre1(String extre1) {
		this.extre1 = extre1;
	}
	public String getExtre2() {
		return extre2;
	}
	public void setExtre2(String extre2) {
		this.extre2 = extre2;
	}
	public String getExtre3() {
		return extre3;
	}
	public void setExtre3(String extre3) {
		this.extre3 = extre3;
	}
	public String getExtre4() {
		return extre4;
	}
	public void setExtre4(String extre4) {
		this.extre4 = extre4;
	}
	public String getExtre5() {
		return extre5;
	}
	public void setExtre5(String extre5) {
		this.extre5 = extre5;
	}
	
	
	

}
