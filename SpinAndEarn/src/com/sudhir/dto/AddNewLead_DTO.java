package com.sudhir.dto;

public class AddNewLead_DTO {

	int id;
	String clientName;
	String email;
	String firmname;
	String lgnAmt;
	String mobile;
	String compain;
	String stage;
	String priority;
	String agent;
	String product;
	String refrence;
	String sectionDate;
	String sectionAmt;
	String followUp;
	String clientType;
	String extra1;
	String extra2;
	String extra3;
	String extra4;
	String extra5;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirmname() {
		return firmname;
	}

	public void setFirmname(String firmname) {
		this.firmname = firmname;
	}

	public String getLgnAmt() {
		return lgnAmt;
	}

	public void setLgnAmt(String lgnAmt) {
		this.lgnAmt = lgnAmt;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCompain() {
		return compain;
	}

	public void setCompain(String compain) {
		this.compain = compain;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}


	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getRefrence() {
		return refrence;
	}

	public void setRefrence(String refrence) {
		this.refrence = refrence;
	}

	public String getSectionDate() {
		return sectionDate;
	}

	public void setSectionDate(String sectionDate) {
		this.sectionDate = sectionDate;
	}

	public String getSectionAmt() {
		return sectionAmt;
	}

	public void setSectionAmt(String sectionAmt) {
		this.sectionAmt = sectionAmt;
	}

	public String getFollowUp() {
		return followUp;
	}

	public void setFollowUp(String followUp) {
		this.followUp = followUp;
	}

	public String getExtra1() {
		return extra1;
	}

	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}

	public String getExtra2() {
		return extra2;
	}

	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}

	public String getExtra3() {
		return extra3;
	}

	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}

	public String getExtra4() {
		return extra4;
	}

	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}

	public String getExtra5() {
		return extra5;
	}

	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	
	

}
