package com.sudhir.dto;

import java.util.Date;

public class GetScheduleWiseLead {

	String lastName;
	String website;
	String leadProcessStatus;
	String city;
	boolean autoDial;
	String companyName;
	String leadState;
	String mobileNo;
	String categoryName;
	String firstName;
	Date sheduleDate;
	String csvData;
	String leadType;
	int leaadId;
	String Country;
	String state;
	String tag;
	String email;
	Date createDate;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getLeadProcessStatus() {
		return leadProcessStatus;
	}

	public void setLeadProcessStatus(String leadProcessStatus) {
		this.leadProcessStatus = leadProcessStatus;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public boolean isAutoDial() {
		return autoDial;
	}

	public void setAutoDial(boolean autoDial) {
		this.autoDial = autoDial;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getLeadState() {
		return leadState;
	}

	public void setLeadState(String leadState) {
		this.leadState = leadState;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getSheduleDate() {
		return sheduleDate;
	}

	public void setSheduleDate(Date sheduleDate) {
		this.sheduleDate = sheduleDate;
	}

	public String getCsvData() {
		return csvData;
	}

	public void setCsvData(String csvData) {
		this.csvData = csvData;
	}

	public String getLeadType() {
		return leadType;
	}

	public void setLeadType(String leadType) {
		this.leadType = leadType;
	}

	public int getLeaadId() {
		return leaadId;
	}

	public void setLeaadId(int leaadId) {
		this.leaadId = leaadId;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
