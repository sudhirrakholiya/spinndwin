package com.sudhir.dto;

public class My_Lead {
	int id;

	String clientNam;

	String emailId;

	String enrtyDate;

	String mobileNo;

	String firm_name;

	String reference_by;

	String followup;

	String product;

	String login_date;

	long login_loan_amt;

	long sanction_loan_amt;

	String sanction_date;

	String extre1;

	String extre2;

	String extre3;

	String extre4;

	String extre5;

	String priority;

	String stage;

	//String user;

	String campain;

	String agent;
	
	String clientType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClientNam() {
		return clientNam;
	}

	public void setClientNam(String clientNam) {
		this.clientNam = clientNam;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getEnrtyDate() {
		return enrtyDate;
	}

	public void setEnrtyDate(String enrtyDate) {
		this.enrtyDate = enrtyDate;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getFirm_name() {
		return firm_name;
	}

	public void setFirm_name(String firm_name) {
		this.firm_name = firm_name;
	}

	public String getReference_by() {
		return reference_by;
	}

	public void setReference_by(String reference_by) {
		this.reference_by = reference_by;
	}

	public String getFollowup() {
		return followup;
	}

	public void setFollowup(String followup) {
		this.followup = followup;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getLogin_date() {
		return login_date;
	}

	public void setLogin_date(String login_date) {
		this.login_date = login_date;
	}

	public long getLogin_loan_amt() {
		return login_loan_amt;
	}

	public void setLogin_loan_amt(long login_loan_amt) {
		this.login_loan_amt = login_loan_amt;
	}

	public long getSanction_loan_amt() {
		return sanction_loan_amt;
	}

	public void setSanction_loan_amt(long sanction_loan_amt) {
		this.sanction_loan_amt = sanction_loan_amt;
	}

	public String getSanction_date() {
		return sanction_date;
	}

	public void setSanction_date(String sanction_date) {
		this.sanction_date = sanction_date;
	}

	public String getExtre1() {
		return extre1;
	}

	public void setExtre1(String extre1) {
		this.extre1 = extre1;
	}

	public String getExtre2() {
		return extre2;
	}

	public void setExtre2(String extre2) {
		this.extre2 = extre2;
	}

	public String getExtre3() {
		return extre3;
	}

	public void setExtre3(String extre3) {
		this.extre3 = extre3;
	}

	public String getExtre4() {
		return extre4;
	}

	public void setExtre4(String extre4) {
		this.extre4 = extre4;
	}

	public String getExtre5() {
		return extre5;
	}

	public void setExtre5(String extre5) {
		this.extre5 = extre5;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	/*public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}*/

	public String getCampain() {
		return campain;
	}

	public void setCampain(String campain) {
		this.campain = campain;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	
}
