package com.sudhir.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usermoneytrans")
public class UserMoneyTrans {
	
	@Id
	@GeneratedValue
	@Column(name = "srno")
	private long srno;
	
	@Column(name = "orderId")
	private String orderId;

	@Column(name = "transcId")
	private String transcId;
	
	@Column(name = "userId")
	private long userId;

	@Column(name = "amount")
	private double amount;
	
	@Column(name = "source")
	private String source;

	@Column(name = "addedon")
	private Date addedon;
	
	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTranscId() {
		return transcId;
	}

	public void setTranscId(String transcId) {
		this.transcId = transcId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getAddedon() {
		return addedon;
	}

	public void setAddedon(Date addedon) {
		this.addedon = addedon;
	}

}
