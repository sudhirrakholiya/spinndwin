package com.sudhir.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "users")
public class User {

	@Id
	@GeneratedValue
	@Column(name = "uid")
	private long uid;

	@Column(name = "username", unique = true, nullable = false, length = 45)
	private String username;

	@Column(name = "password", nullable = false, length = 60)
	private String password;

	@Column(name = "enabled", nullable = false)
	private boolean enabled;

	@Column(name = "parentid")
	private long parentId;

	@Column(name = "name")
	private String name;

	@Column(name = "mobilenumber")
	private String mobileNumber;

	@Column(name = "email")
	private String email;

	@Column(name = "cmpname")
	private String cmpName;

	@Column(name = "address")
	private String address;

	@Column(name = "defaultsenderid")
	private long defaultSenderId;

	@Column(name = "regdate")
	private Date regDate;

	@Column(name = "expiredate")
	private Date expireDate;

	@Column(name = "priority")
	private int priority;

	@Column(name = "dlrreport")
	private String dlrReport;

	@Column(name = "maxuser")
	private long maxUser;

	@Column(name = "addby")
	private long addBy;

	@Column(name = "city")
	private String city;

	@Column(name = "country")
	private String country;

	@Column(name = "isactive")
	private boolean isActive;

	@Column(name = "min_number")
	private int minNumber;

	@Column(name = "cutting")
	private int cutting;

	@Column(name = "limit_staff")
	private int limit_staff;

	@Column(name = "limit_tally_caller")
	private int limit_tally_caller;

	@Column(name = "companyId")
	private int companyId;
	
	@Column(name = "apiKey")
	private String apiKey;
	
	@Column(name = "masterPassword")
	private String masterPassword;

	public int getLimit_staff() {
		return limit_staff;
	}

	public void setLimit_staff(int limit_staff) {
		this.limit_staff = limit_staff;
	}

	public int getLimit_tally_caller() {
		return limit_tally_caller;
	}

	public void setLimit_tally_caller(int limit_tally_caller) {
		this.limit_tally_caller = limit_tally_caller;
	}

	public int getMinNumber() {
		return minNumber;
	}

	public void setMinNumber(int minNumber) {
		this.minNumber = minNumber;
	}

	public int getCutting() {
		return cutting;
	}

	public void setCutting(int cutting) {
		this.cutting = cutting;
	}

	public User() {
	}

	public User(String username, String password, boolean enabled) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
	}

	public User(String username, String password, boolean enabled, Set<UserRole> userRole) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.userRole = userRole;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<UserRole> userRole = new HashSet<UserRole>(0);

	public Set<UserRole> getUserRole() {
		return this.userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCmpName() {
		return cmpName;
	}

	public void setCmpName(String cmpName) {
		this.cmpName = cmpName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getDefaultSenderId() {
		return defaultSenderId;
	}

	public void setDefaultSenderId(long defaultSenderId) {
		this.defaultSenderId = defaultSenderId;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getDlrReport() {
		return dlrReport;
	}

	public void setDlrReport(String dlrReport) {
		this.dlrReport = dlrReport;
	}

	public long getMaxUser() {
		return maxUser;
	}

	public void setMaxUser(long maxUser) {
		this.maxUser = maxUser;
	}

	public long getAddBy() {
		return addBy;
	}

	public void setAddBy(long addBy) {
		this.addBy = addBy;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	/**
	 * 
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}

	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * 
	 * @return the masterPassword
	 */
	public String getMasterPassword() {
		return masterPassword;
	}

	/**
	 * @param masterPassword the masterPassword to set
	 */
	public void setMasterPassword(String masterPassword) {
		this.masterPassword = masterPassword;
	}
	
	
	
	

}
