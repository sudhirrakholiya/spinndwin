/**
 * 
 */
package com.sudhir.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Niraj Thakar
 *
 */
@Entity
@Table(name = "apikey")
public class ApiKey implements Serializable {
	
	@GeneratedValue
	@Column(name = "aid")
	private long aid;

	@Id
	@Column(name = "uid")
	private long uid;

	@Column(name = "keyvalue")
	private String keyValue;

	@Column(name = "createdate")
	private Date createDate;

	@Column(name = "ip")
	private String ip;

	@Column(name = "ishidden")
	private Boolean ishidden;

	@Column(name = "accessDate")
	private Date accessDate;
	
	public Boolean getIshidden() {
		return ishidden;
	}

	public void setIshidden(Boolean ishidden) {
		this.ishidden = ishidden;
	}

	/**
	 * 
	 * @return the uid
	 */
	public long getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(long uid) {
		this.uid = uid;
	}

	/**
	 * 
	 * @return the keyValue
	 */
	public String getKeyValue() {
		return keyValue;
	}

	/**
	 * @param keyValue
	 *            the keyValue to set
	 */
	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}

	/**
	 * 
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate
	 *            the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip
	 *            the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * 
	 * @return the aid
	 */
	public long getAid() {
		return aid;
	}

	/**
	 * @param aid
	 *            the aid to set
	 */
	public void setAid(long aid) {
		this.aid = aid;
	}

	public Date getAccessDate() {
		return accessDate;
	}

	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}
	
	
	
	

}
