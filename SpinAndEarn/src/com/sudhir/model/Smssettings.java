package com.sudhir.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "smssettings")
public class Smssettings {

	@Id
	@GeneratedValue
	@Column(name = "url_Id")
	private int url_Id;

	@Column(name = "url")
	private String url;

	@Column(name = "responce")
	private String responce;

	@Column(name = "provider")
	private String provider;

	@Column(name = "comp_Id")
	private int comp_Id;

	@Column(name = "staff_Id")
	private int staff_Id;

	@Column(name = "API_Status")
	private boolean API_Status;
	
	
	/**
	 * 
	 * @return the url_Id
	 */
	public int getUrl_Id() {
		return url_Id;
	}

	/**
	 * @param url_Id
	 *            the url_Id to set
	 */
	public void setUrl_Id(int url_Id) {
		this.url_Id = url_Id;
	}

	/**
	 * 
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * 
	 * @return the responce
	 */
	public String getResponce() {
		return responce;
	}

	/**
	 * @param responce
	 *            the responce to set
	 */
	public void setResponce(String responce) {
		this.responce = responce;
	}

	/**
	 * 
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * @param provider
	 *            the provider to set
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}

	/**
	 * 
	 * @return the comp_Id
	 */
	public int getComp_Id() {
		return comp_Id;
	}

	/**
	 * @param comp_Id
	 *            the comp_Id to set
	 */
	public void setComp_Id(int comp_Id) {
		this.comp_Id = comp_Id;
	}

	/**
	 * 
	 * @return the staff_Id
	 */
	public int getStaff_Id() {
		return staff_Id;
	}

	/**
	 * @param staff_Id
	 *            the staff_Id to set
	 */
	public void setStaff_Id(int staff_Id) {
		this.staff_Id = staff_Id;
	}

	/**
	 * 
	 * @return the status
	 */
	public boolean getStatus() {
		return API_Status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(boolean API_Status) {
		this.API_Status = API_Status;
	}

	/**
	 * 
	 * @return the cat_Id
	 */


	
}
