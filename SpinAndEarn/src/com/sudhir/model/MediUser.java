package com.sudhir.model;

import java.util.Collection;
import org.springframework.security.core.userdetails.User;

public class MediUser extends User {

	private static final long serialVersionUID = -3531439484732724601L;

	/*
	 * private final String firstName; private final String middleName;
	 */
	private final int priority;
	private final long userid;
	private final long parentid;
	private final long defaltsenderid;
	private final int minnumber;
	private final int cutting;

	public MediUser(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked, Collection authorities, int priority, long userid,
			long parentid, long defaltsenderid, int minnumber, int cutting) {

		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);

		this.priority = priority;
		this.userid = userid;
		this.parentid = parentid;
		this.defaltsenderid = defaltsenderid;
		this.minnumber = minnumber;
		this.cutting = cutting;

		// this.altPhNumber = altPhNumber;
	}

	public int getMinnumber() {
		return minnumber;
	}

	public int getCutting() {
		return cutting;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * 
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * 
	 * @return the userid
	 */
	public long getUserid() {
		return userid;
	}

	/**
	 * 
	 * @return the parentid
	 */
	public long getParentid() {
		return parentid;
	}

	/**
	 * 
	 * @return the defaltsenderid
	 */
	public long getDefaltsenderid() {
		return defaltsenderid;
	}

}