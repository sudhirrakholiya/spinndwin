package com.sudhir.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "agents")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Agents {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "agentId")
	int agentId;

	@Column(name = "name", nullable = false, length = 100)
	String name;

	@JsonIgnore
	@Column(name = "regDate", insertable = false, updatable = false, nullable = false, columnDefinition = "datetime default NOW()")
	Date regDate;

	@Column(name = "mobileNo", nullable = false, length = 100)
	String mobileNo;

	@JsonIgnore
	@Column(name = "userId", nullable = false, length = 10)
	int userId;
	
	@Column(name = "email", nullable = false, length = 100)
	String email;

	@Column(name = "username", nullable = false, length = 100)
	String username;

	//@JsonIgnore    
	@Column(name = "password", nullable = false, length = 100)
	String password;
	
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

}
