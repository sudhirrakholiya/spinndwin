package com.sudhir.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "campain")
public class Campain {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "campainId")
	int campainId;
	
	@Column(name = "campainName")
	String campainName;
	
	@Column(name = "uid")
	int uid;


	public int getCampainId() {
		return campainId;
	}

	public void setCampainId(int campainId) {
		this.campainId = campainId;
	}

	public String getCampainName() {
		return campainName;
	}

	public void setCampainName(String campainName) {
		this.campainName = campainName;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}
	
	
}
