package com.sudhir.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "emaillog")
public class EmailLogs {
	
	
	@Id
	@GeneratedValue
	@Column(name = "srno")
	private long srno;

	@Column(name = "emailOn")
	private Date emailOn;

	@Column(name = "emailId")
	private String emailId;

	@Column(name = "emailStatus")
	private String emailStatus;

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public Date getEmailOn() {
		return emailOn;
	}

	public void setEmailOn(Date emailOn) {
		this.emailOn = emailOn;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}

}
