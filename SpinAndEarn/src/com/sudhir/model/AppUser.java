package com.sudhir.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "appuser")
public class AppUser {
	
	
	@Id
	@GeneratedValue
	@Column(name = "uid")
	private long uid;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "enabled")
	private boolean enabled;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "dateofbirth")
	private String dateofbirth;
	
	@Column(name = "reflCode")
	private String reflCode;
	
	@Column(name = "myReflCode")
	private String myReflCode;
	
	@Column(name = "winamount") 
	private double winamount;
	
	@Column(name = "walletamount")
	private double walletamount;
	
	@Column(name = "myReflCodeLimit")
	private int myReflCodeLimit;
	
	@Column(name = "isWin")
	private boolean isWin;
	
	@Column(name = "resetTocken")
	private String resetTocken;
	
	@Column(name = "imeiNo")
	private String imeiNo;
	
	@Column(name = "ipAddress")
	private String ipAddress;
	
	@Column(name = "isWinExtra")
	private boolean isWinExtra;
	
	@Column(name = "addedOn")
	private Date addedOn;
	
	
	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDateofbirth() {
		return dateofbirth;
	}

	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}

	public String getReflCode() {
		return reflCode;
	}

	public void setReflCode(String reflCode) {
		this.reflCode = reflCode;
	}
	
	
	
	 public double getWinamount() { 
		 return winamount; 
	}
	 
	 public void setWinamount(double winamount) { 
		 this.winamount = winamount; 
    }
	

	public double getWalletamount() {
		return walletamount;
	}

	public void setWalletamount(double walletamount) {
		this.walletamount = walletamount;
	}

	public String getMyReflCode() {
		return myReflCode;
	}

	public void setMyReflCode(String myReflCode) {
		this.myReflCode = myReflCode;
	}

	public int getMyReflCodeLimit() {
		return myReflCodeLimit;
	}

	public void setMyReflCodeLimit(int myReflCodeLimit) {
		this.myReflCodeLimit = myReflCodeLimit;
	}

	public boolean isWin() {
		return isWin;
	}

	public void setWin(boolean isWin) {
		this.isWin = isWin;
	}

	public String getResetTocken() {
		return resetTocken;
	}

	public void setResetTocken(String resetTocken) {
		this.resetTocken = resetTocken;
	}

	public String getImeiNo() {
		return imeiNo;
	}

	public void setImeiNo(String imeiNo) {
		this.imeiNo = imeiNo;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public boolean isWinExtra() {
		return isWinExtra;
	}

	public void setWinExtra(boolean isWinExtra) {
		this.isWinExtra = isWinExtra;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}


}
