/**
 * 
 */
package com.sudhir.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Bhavesh Patel
 *
 */
@Entity
@Table(name = "login_log")
public class LoginLog {

	@Id
	@GeneratedValue
	@Column(name = "srno")
	private long srno;

	@Column(name = "login_on")
	private Date loginon;

	@Column(name = "logout_on")
	private Date logouton;

	@Column(name = "ipaddress")
	private String ipaddress;

	@Column(name = "notes")
	private String notes;

	@Column(name = "uid")
	private long uid;

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public Date getLoginon() {
		return loginon;
	}

	public void setLoginon(Date date) {
		this.loginon = date;
	}

	public Date getLogouton() {
		return logouton;
	}

	public void setLogouton(Date date) {
		this.logouton = date;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

}
