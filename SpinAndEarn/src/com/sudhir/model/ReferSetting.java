package com.sudhir.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "refersetting")
public class ReferSetting {
	
	@Id
	@GeneratedValue
	@Column(name = "settid")
	private long settid;

	@Column(name = "codeLimit")
	private int codeLimit;

	@Column(name = "newUserAmt")
	private double newUserAmt;
	
	@Column(name = "oldUserAmt")
	private double oldUserAmt;
	
	@Column(name = "spinReqAmt")
	private double spinReqAmt;
	
	@Column(name = "signUpBonus")
	private double signUpBonus;
	
	@Column(name = "withdrawAmt")
	private double withdrawAmt;
	
	@Column(name = "addedOn")
	private Date addedOn;
	
	@Column(name = "isEnable")
	private boolean isEnable;
	
	@Column(name = "betValues")
	private String betValues;

	public long getSettid() {
		return settid;
	}

	public void setSettid(long settid) {
		this.settid = settid;
	}

	public int getCodeLimit() {
		return codeLimit;
	}

	public void setCodeLimit(int codeLimit) {
		this.codeLimit = codeLimit;
	}

	public double getNewUserAmt() {
		return newUserAmt;
	}

	public void setNewUserAmt(double newUserAmt) {
		this.newUserAmt = newUserAmt;
	}

	public double getOldUserAmt() {
		return oldUserAmt;
	}

	public void setOldUserAmt(double oldUserAmt) {
		this.oldUserAmt = oldUserAmt;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public double getSpinReqAmt() {
		return spinReqAmt;
	}

	public void setSpinReqAmt(double spinReqAmt) {
		this.spinReqAmt = spinReqAmt;
	}

	public double getSignUpBonus() {
		return signUpBonus;
	}

	public void setSignUpBonus(double signUpBonus) {
		this.signUpBonus = signUpBonus;
	}

	public double getWithdrawAmt() {
		return withdrawAmt;
	}

	public void setWithdrawAmt(double withdrawAmt) {
		this.withdrawAmt = withdrawAmt;
	}

	public boolean isEnable() {
		return isEnable;
	}

	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;
	}

	public String getBetValues() {
		return betValues;
	}

	public void setBetValues(String betValues) {
		this.betValues = betValues;
	}

}
