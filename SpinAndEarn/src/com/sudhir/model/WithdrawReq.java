package com.sudhir.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "withdrawreq")
public class WithdrawReq {
	
	@Id
	@GeneratedValue
	@Column(name = "rqid")
	private long rqid;
	
	@Column(name = "userId")
	private long userId;

	@Column(name = "rqType")
	private String rqType;

	@Column(name = "accName")
	private String accName;
	
	@Column(name = "accNo")
	private String accNo;
	
	@Column(name = "ifsc")
	private String ifsc;
	
	@Column(name = "mobileUPI")
	private String mobileUPI;
	
	@Column(name = "rqStatus")
	private boolean rqStatus;
	
	@Column(name = "rqDate")
	private Date rqDate;
	
	@Column(name = "withAmt")
	private double withAmt;
	
	@Column(name = "apprvDate")
	private String apprvDate;

	public long getRqid() {
		return rqid;
	}

	public void setRqid(long rqid) {
		this.rqid = rqid;
	}

	public String getRqType() {
		return rqType;
	}

	public void setRqType(String rqType) {
		this.rqType = rqType;
	}

	public String getAccName() {
		return accName;
	}

	public void setAccName(String accName) {
		this.accName = accName;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getMobileUPI() {
		return mobileUPI;
	}

	public void setMobileUPI(String mobileUPI) {
		this.mobileUPI = mobileUPI;
	}

	public Date getRqDate() {
		return rqDate;
	}

	public void setRqDate(Date rqDate) {
		this.rqDate = rqDate;
	}

	public boolean isRqStatus() {
		return rqStatus;
	}

	public void setRqStatus(boolean rqStatus) {
		this.rqStatus = rqStatus;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getApprvDate() {
		return apprvDate;
	}

	public void setApprvDate(String apprvDate) {
		this.apprvDate = apprvDate;
	}

	public double getWithAmt() {
		return withAmt;
	}

	public void setWithAmt(double withAmt) {
		this.withAmt = withAmt;
	}
}
