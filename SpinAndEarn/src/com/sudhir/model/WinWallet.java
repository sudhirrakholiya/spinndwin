package com.sudhir.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "winwallet")
public class WinWallet {

	@Id
	@GeneratedValue
	@Column(name = "wwid")
	private long wwid;

	@Column(name = "winAmt")
	private double winAmt;

	@Column(name = "userId")
	private long userId;
	
	@Column(name = "trType")
	private String trType;
	
	@Column(name = "trDate")
	private Date trDate;
	
	@Column(name = "remark")
	private String remark;
	
	@Column(name = "userGSNumber")
	private double userGSNumber;
	
	@Column(name = "userGSAmt")
	private double userGSAmt;

	public long getWwid() {
		return wwid;
	}

	public void setWwid(long wwid) {
		this.wwid = wwid;
	}


	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getTrType() {
		return trType;
	}

	public void setTrType(String trType) {
		this.trType = trType;
	}

	public Date getTrDate() {
		return trDate;
	}

	public void setTrDate(Date trDate) {
		this.trDate = trDate;
	}

	public double getWinAmt() {
		return winAmt;
	}

	public void setWinAmt(double winAmt) {
		this.winAmt = winAmt;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public double getUserGSNumber() {
		return userGSNumber;
	}

	public void setUserGSNumber(double userGSNumber) {
		this.userGSNumber = userGSNumber;
	}

	public double getUserGSAmt() {
		return userGSAmt;
	}

	public void setUserGSAmt(double userGSAmt) {
		this.userGSAmt = userGSAmt;
	}

	
}
