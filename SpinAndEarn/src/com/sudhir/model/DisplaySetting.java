package com.sudhir.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "displaysetting")
public class DisplaySetting {

	private long id;
	private String url;
	private String title;
	private String subTitle;
	private String address;
	private String aboutus;
	private String logoImage;
	private String image2;
	private String userName;
	private String homeurl;
	private String extraInfo1;
	private String extraInfo2;

	@Id
	@GeneratedValue
	@Column(name = "id")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "url")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "aboutus")
	public String getAboutus() {
		return aboutus;
	}

	public void setAboutus(String aboutus) {
		this.aboutus = aboutus;
	}

	@Column(name = "user")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "homeUrl")
	public String getHomeurl() {
		return homeurl;
	}

	public void setHomeurl(String homeurl) {
		this.homeurl = homeurl;
	}

	@Column(name = "sub_title")
	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	/**
	 * 
	 * @return the logoImage
	 */
	@Column(name = "logoImage")
	public String getLogoImage() {
		return logoImage;
	}

	/**
	 * @param logoImage
	 *            the logoImage to set
	 */
	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}

	/**
	 * 
	 * @return the image2
	 */
	@Column(name = "image2")
	public String getImage2() {
		return image2;
	}

	/**
	 * @param image2
	 *            the image2 to set
	 */
	public void setImage2(String image2) {
		this.image2 = image2;
	}

	/**
	 * 
	 * @return the extraInfo1
	 */
	@Column(name = "extraInfo1")
	public String getExtraInfo1() {
		return extraInfo1;
	}

	/**
	 * @param extraInfo1
	 *            the extraInfo1 to set
	 */
	public void setExtraInfo1(String extraInfo1) {
		this.extraInfo1 = extraInfo1;
	}

	/**
	 * 
	 * @return the extraInfo2
	 */
	@Column(name = "extraInfo2")
	public String getExtraInfo2() {
		return extraInfo2;
	}

	/**
	 * @param extraInfo2
	 *            the extraInfo2 to set
	 */
	public void setExtraInfo2(String extraInfo2) {
		this.extraInfo2 = extraInfo2;
	}

}
