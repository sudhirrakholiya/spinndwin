package com.sudhir.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "my_leads")
public class My_Leads {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	int id;

	@Column(name = "clientName", length = 100, columnDefinition = "varchar(255) default 'NA'")
	String clientName = "NA";
	
	@Column(name = "emailId", length = 100, columnDefinition = "varchar(255) default 'NA'")
	String emailId = "NA";

	@Column(name = "mobileNo", nullable = false, length = 100)
	String mobileNo;

	@Column(name = "firm_name", length = 100, nullable = false, columnDefinition = "varchar(255) default 'No Refrence'")
	String firm_name = "NA";

	@Column(name = "reference_by", length = 100, columnDefinition = "varchar(255) default 'No Refrence'")
	String reference_by = "No Refrence";

	@Column(name = "followup", length = 100)
	Date followup;

	@Column(name = "product", length = 100, columnDefinition = "varchar(255) default 'NA'")
	String product = "NA";

	@Column(name = "login_date", columnDefinition = "datetime default '1993-05-29 00:00:00'")
	Date login_date;

	@Column(name = "login_loan_amt", length = 10, columnDefinition = "BIGINT(10) default 00")
	long login_loan_amt;

	@Column(name = "sanction_loan_amt", length = 10, columnDefinition = "BIGINT(10) default 00")
	long sanction_loan_amt;

	@Column(name = "sanction_date", columnDefinition = "datetime default 0")
	Date sanction_date;

	@Column(name = "extre1", length = 100, columnDefinition = "varchar(255) default 'NA'")
	String extre1 = "NA";

	@Column(name = "extre2", length = 100, columnDefinition = "varchar(255) default 'NA'")
	String extre2 = "NA";

	@Column(name = "extre3", length = 100, columnDefinition = "varchar(255) default 'NA'")
	String extre3 = "NA";

	@Column(name = "extre4", length = 100, columnDefinition = "varchar(255) default 'NA'")
	String extre4 = "NA";

	@Column(name = "extre5", length = 100, columnDefinition = "varchar(255) default 'NA'")
	String extre5 = "NA";
	
	@Column(name = "userId", length = 10)
	long  userId;

	@Column(name = "clientType", length = 10)
	String  clientType;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "priorityid")
	@NotFound(action=NotFoundAction.IGNORE)
	Priority priority;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "stageid")
	@Fetch(FetchMode.JOIN)
	@NotFound(action=NotFoundAction.IGNORE)
	Stage stage;

	

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "campainId")
	@Fetch(FetchMode.JOIN)
	@NotFound(action=NotFoundAction.IGNORE)
	Campain campain;

	@OneToOne(cascade = CascadeType.ALL)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "agentId", columnDefinition = "int(10) default 0")
	@NotFound(action=NotFoundAction.IGNORE)
	Agents agent;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getFirm_name() {
		return firm_name;
	}

	public void setFirm_name(String firm_name) {
		this.firm_name = firm_name;
	}

	public String getReference_by() {
		return reference_by;
	}

	public void setReference_by(String reference_by) {
		this.reference_by = reference_by;
	}

	public Date getFollowup() {
		return followup;
	}

	public void setFollowup(Date followup) {
		this.followup = followup;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public Date getLogin_date() {
		return login_date;
	}

	public void setLogin_date(Date login_date) {
		this.login_date = login_date;
	}

	public long getLogin_loan_amt() {
		return login_loan_amt;
	}

	public void setLogin_loan_amt(long login_loan_amt) {
		this.login_loan_amt = login_loan_amt;
	}

	public long getSanction_loan_amt() {
		return sanction_loan_amt;
	}

	public void setSanction_loan_amt(long sanction_loan_amt) {
		this.sanction_loan_amt = sanction_loan_amt;
	}

	public Date getSanction_date() {
		return sanction_date;
	}

	public void setSanction_date(Date sanction_date) {
		this.sanction_date = sanction_date;
	}

	public String getExtre1() {
		return extre1;
	}

	public void setExtre1(String extre1) {
		this.extre1 = extre1;
	}

	public String getExtre2() {
		return extre2;
	}

	public void setExtre2(String extre2) {
		this.extre2 = extre2;
	}

	public String getExtre3() {
		return extre3;
	}

	public void setExtre3(String extre3) {
		this.extre3 = extre3;
	}

	public String getExtre4() {
		return extre4;
	}

	public void setExtre4(String extre4) {
		this.extre4 = extre4;
	}

	public String getExtre5() {
		return extre5;
	}

	public void setExtre5(String extre5) {
		this.extre5 = extre5;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public Campain getCampain() {
		return campain;
	}

	public void setCampain(Campain campain) {
		this.campain = campain;
	}

	public Agents getAgent() {
		return agent;
	}

	public void setAgent(Agents agent) {
		this.agent = agent;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	
	

	
	
}
