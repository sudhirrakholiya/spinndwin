package com.sudhir.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sentsmslog")
public class Sentsmslog {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "mobileNo")
	private long mobileNo;

	@Column(name = "message")
	private String message;

	@Column(name = "sendDate")
	private Date sendDate;

	@Column(name = "tallycallerId")
	private int tallycallerId;

	@Column(name = "staffId")
	private int staffId;

	@Column(name = "compayId")
	private int compayId;

	@Column(name = "APIResponse")
	private String APIResponse;

	@Column(name = "MSGStatus")
	private String MSGStatus;

	/**
	 * 
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the mobileNo
	 */
	public long getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo
	 *            the mobileNo to set
	 */
	public void setMobileNo(long mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @return the sendDate
	 */
	public Date getSendDate() {
		return sendDate;
	}

	/**
	 * @param sendDate
	 *            the sendDate to set
	 */
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	/**
	 * 
	 * @return the tallycallerId
	 */
	public int getTallycallerId() {
		return tallycallerId;
	}

	/**
	 * @param tallycallerId
	 *            the tallycallerId to set
	 */
	public void setTallycallerId(int tallycallerId) {
		this.tallycallerId = tallycallerId;
	}

	/**
	 * 
	 * @return the staffId
	 */
	public int getStaffId() {
		return staffId;
	}

	/**
	 * @param staffId
	 *            the staffId to set
	 */
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	/**
	 * 
	 * @return the compayId
	 */
	public int getCompayId() {
		return compayId;
	}

	/**
	 * @param compayId
	 *            the compayId to set
	 */
	public void setCompayId(int compayId) {
		this.compayId = compayId;
	}

	/**
	 * 
	 * @return the messageStatus
	 */
	public String getAPIResponse() {
		return APIResponse;
	}

	/**
	 * @param messageStatus
	 *            the messageStatus to set
	 */
	public void setAPIResponse(String APIResponse) {
		this.APIResponse = APIResponse;
	}

	/**
	 * 
	 * @return the mSGStatus
	 */
	public String getMSGStatus() {
		return MSGStatus;
	}

	/**
	 * @param mSGStatus
	 *            the mSGStatus to set
	 */
	public void setMSGStatus(String mSGStatus) {
		MSGStatus = mSGStatus;
	}

	@Override
	public String toString() {
		return "Sentsmslog [id=" + id + ", mobileNo=" + mobileNo + ", message=" + message + ", sendDate=" + sendDate
				+ ", tallycallerId=" + tallycallerId + ", staffId=" + staffId + ", compayId=" + compayId
				+ ", APIResponse=" + APIResponse + ", MSGStatus=" + MSGStatus + "]";
	}

}
