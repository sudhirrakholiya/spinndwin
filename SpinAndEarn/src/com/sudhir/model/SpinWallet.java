package com.sudhir.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "spinwallet")
public class SpinWallet {
	
	@Id
	@GeneratedValue
	@Column(name = "swid")
	private long swid;

	@Column(name = "winAmt")
	private double winAmt;

	@Column(name = "userId")
	private long userId;
	
	@Column(name = "trType")
	private String trType;
	
	@Column(name = "usedRefCode")
	private String usedRefCode;
	
	@Column(name = "transType")
	private String transType;

	@Column(name = "trDate")
	private Date trDate;
	
	@Column(name = "userGSNumber")
	private double userGSNumber;
	
	@Column(name = "userGSAmt")
	private double userGSAmt;

	public long getSwid() {
		return swid;
	}

	public void setSwid(long swid) {
		this.swid = swid;
	}


	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getTrType() {
		return trType;
	}

	public void setTrType(String trType) {
		this.trType = trType;
	}

	public Date getTrDate() {
		return trDate;
	}

	public void setTrDate(Date trDate) {
		this.trDate = trDate;
	}

	public double getWinAmt() {
		return winAmt;
	}

	public void setWinAmt(double winAmt) {
		this.winAmt = winAmt;
	}

	public String getUsedRefCode() {
		return usedRefCode;
	}

	public void setUsedRefCode(String usedRefCode) {
		this.usedRefCode = usedRefCode;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public double getUserGSNumber() {
		return userGSNumber;
	}

	public void setUserGSNumber(double userGSNumber) {
		this.userGSNumber = userGSNumber;
	}

	public double getUserGSAmt() {
		return userGSAmt;
	}

	public void setUserGSAmt(double userGSAmt) {
		this.userGSAmt = userGSAmt;
	}

}
