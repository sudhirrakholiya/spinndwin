package com.sudhir.utils;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

//import com.sun.xml.internal.ws.wsdl.writer.document.StartWithExtensionsType;

public class StringUtils {

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// String
		// tte="<smslist><error><smsclientid>0</smsclientid><error-code>-10002</error-code>
		// <error-description>Invalid Username Or Password</error-description>
		// <error-action>1</error-action></error></smslist>";
		// System.out.println(parseXmlAruhat(tte,"messageid"));

		// System.out.println(SMSCounter(759));

		// System.out.println(unicodeEscape("नमस�?कार@#$%$^&&^*%^*((*_+@~@@$"));

		// System.out.println(unicodeEscape("this istest
		// msadgdsagdasgdas??!@#$%^&*()"));

		// System.out.println(StringUtils.isNumeric(null));
		/*
		 * String example =
		 * "United an Arab Emirates for Dirham  (AED) AND anasdgasdg for <d>";
		 * Matcher m = Pattern.compile("<([^<>]+)>").matcher(example);
		 * 
		 * while(m.find()) { System.out.println(m.group(0)); }
		 */
		/*
		 * System.out.println(convertMobileno("09925548847"));
		 * System.out.println(convertMobileno("917405402722"));
		 * System.out.println(convertMobileno("9 9925548847"));
		 * 
		 * System.out.println(convertMobileno("92554 8847"));
		 * System.out.println(convertMobileno("+919925548849"));
		 */

		// String ss="Tamara juna TATA SKY MPEG 2 Box same medvo navu letest
		// technology vadu MPEG 4 Box tadan FREE... Visit & Call. Uma
		// Communication F/3, Classic Empire, opp. Shilpa garage, Highway road,
		// Mehsana Con. no. 9723405566 / 9723407788 / 9723408899 / 8401893872
		// Patan No.9173075501 Palanpur no.9925789702 / 9558782980";
		/*
		 * String ss="Dept.of Pali-SPPU\n"+
		 * "for admissions pls visit the website http://www.unipune.ac.in\n"+
		 * "last date is 30th June 2015\n"+
		 * "for details pls cont.Mr.Gunthal -020 25601344/1389";
		 * 
		 * System.out.println(ss.length());
		 * System.out.println(SMSCounter(ss.length(), false));
		 */
		// System.out.println(SMSCounter(201,true));

	}

	public static int countLines(String str) {
		String[] lines = str.split("\r\n|\r|\n");
		return str.length() - (lines.length - 1);
	}

	public static boolean isEmailValid(String str) {
		if (str == null) {
			return false;
		} else {
			return Pattern.compile(EMAIL_PATTERN).matcher(str).matches();
		}
	}

	public static List<String[]> getTagArray(String str) { // get Word between <
															// >

		List<String[]> lsts = new ArrayList<String[]>();
		Matcher m = Pattern.compile("<([^<>]+)>").matcher(str);
		while (m.find()) {
			lsts.add(new String[] { m.group(0).toUpperCase(), getColumnPosition(m.group(1).toUpperCase()) + "" });
		}
		return lsts;
	}

	public static int SMSCounter(int smslen, boolean isunicode) {
		int smscount = 0;
		if (isunicode) {
			if (smslen < 71) {
				smscount = 1;
			} else if (smslen < 135) {
				smscount = 2;
			} else {
				smslen = smslen - 134;
				smscount = 2 + (smslen / 67);
				if (smslen % 67 > 0) {
					smscount++;
				}
			}
			return smscount;
		}

		if (smslen < 161) {
			smscount = 1;
		} else if (smslen < 307) {
			smscount = 2;
		} else {
			smslen = smslen - 307;
			smscount = 2 + (smslen / 152);
			if (smslen % 152 > 0) {
				smscount++;
			}
		}

		return smscount;
	}

	public static boolean isEmpty(final String cs) {
		return cs == null || cs.length() == 0;
	}

	public static boolean isUnicode(String str) {
		boolean isunicode = false;
		int sz = str.length();
		for (int i = 0; i < sz; i++) {
			if ((Character.UnicodeBlock.of(str.charAt(i)) != Character.UnicodeBlock.BASIC_LATIN)) {
				isunicode = true;
				break;
			}
		}
		return isunicode;
	}

	public static String parseXmlAruhat(String xml, String tagname) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		InputSource is;
		String resp = null;
		try {
			builder = factory.newDocumentBuilder();
			is = new InputSource(new StringReader(xml));
			Document doc = builder.parse(is);
			NodeList list = doc.getElementsByTagName(tagname);
			// System.out.println(list.item(0).getTextContent());
			resp = list.item(0).getTextContent();
			System.out.println(resp);

		} catch (Exception e) {

			System.out.println(e.getMessage());
		}

		return resp;
	}

	public static String unicodeEscape(String s) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if ((c >> 7) > 0) {
				sb.append("\\u");
				sb.append(hexChar[(c >> 12) & 0xF]); // append the hex character
														// for the left-most
														// 4-bits
				sb.append(hexChar[(c >> 8) & 0xF]); // hex for the second group
													// of 4-bits from the left
				sb.append(hexChar[(c >> 4) & 0xF]); // hex for the third group
				sb.append(hexChar[c & 0xF]); // hex for the last group, e.g.,
												// the right most 4-bits
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	public static boolean isNumeric(String str) {
		if (str == null) {
			return false;
		}
		int sz = str.length();
		for (int i = 0; i < sz; i++) {
			if (Character.isDigit(str.charAt(i)) == false) {
				return false;
			}
		}

		return true;
	}

	public static String convertMobileno(String str) {
		str = str.trim();
		if (str.matches("\\d{10}")) { // for validate 1-10 number
			return str;
		} else if (str.matches("91\\d{10}")) {
			return str.substring(2);
		} else if (str.matches("0\\d{10}")) {
			return str.substring(1);
		} else if (str.matches("\\+91\\d{10}")) {
			return str.substring(3);
		} else {
			return null;
		}

	}

	public static boolean isAlphaNumeric(String s) {
		String pattern = "^[a-zA-Z0-9]*$";
		if (s.matches(pattern)) {
			return true;
		}
		return false;
	}

	public static int getColumnPosition(String s) {
		String myArray[] = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
				"S", "T", "U", "V", "W", "X", "Y", "Z" };
		return Arrays.asList(myArray).indexOf(s);
	}

	private static final char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E',
			'F' };

}
