package com.sudhir.utils;
public class Config {

    public static String  ZAAKPAY_SECRET_KEY = "9282454ddf424f3ca0d400f5f240d8f0";

    public static String  ENVIRONMENT = "https://zaakstaging.zaakpay.com";

    public static String  ZAAKPAY_MERCHANT_IDENTIFIER = "fe36ee0d8dcc4548af968cd527b75a83";

    public static String  TRANSACTION_API_URL = "/api/paymentTransact/V8";

    public static String  UPDATE_API_URL = "/updatetransaction";

    public static String  TRANSACTION_STATUS_API_URL = "/checkTxn?v=5";

    public static String  RETURN_URL = "https://personalizedframe.in";

}