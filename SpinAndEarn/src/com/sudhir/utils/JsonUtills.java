package com.sudhir.utils;

import java.lang.reflect.Type;
import java.util.List;

import org.json.JSONObject;
import org.springframework.scheduling.config.Task;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("unchecked")
public class JsonUtills {

	private static ObjectMapper mapper;
	
	static {
		try {
			
			mapper = new ObjectMapper();
			mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String JavaToJson(Object object) {
		String jsonResult = "";
		try {
			jsonResult = mapper.writeValueAsString(object);
			System.out.println(jsonResult);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonResult;
	}

	public static String convertJsonToJava() {
		return "";
	}

	public static String ListToDataTable(List list) {
		JSONObject json = new JSONObject();
		json.put("data", list);
		return json.toString();
	}

	public static String ListToJava(List list) {
		Type type = new TypeToken<List<Task>>() {
		}.getType();
		String json = new GsonBuilder().serializeNulls().create().toJson(list, type);
		return json;
	}

}
