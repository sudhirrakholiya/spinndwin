package com.sudhir.utils;
import java.io.IOException;
 
public class IMEIValidator {
   public String validateIMEINum(String imeiStr) {
	   System.out.println("Input 15 digit IMEI number");
		try {
		    // initialize input reader
		    //inputReader = new BufferedReader(new InputStreamReader(System.in));
		    // read IMEI number from user input
		    //String imeiStr = inputReader.readLine();
		    
		   // String imeiStr = "861894041241617";
		    // convert it to a long
		    long imei = Long.parseLong(imeiStr);
		    // check the length which is the first criteria
		    if (imeiStr.length() != 15) {
			System.out.println("Invalid IMEI number: does not contain 15 digits");
	         	return "Invalid IMEI number: does not contain 15 digits";
		    }
		    // variable to hold the sum
		    int sum = 0;
		    // start from the rightmost digit
		    for (int i = 0; i < imeiStr.length(); i++) { 
	                 int digit = (int) (imei % 10); 
	                 // check if the digit is second, fourth and so on. These digits 
	                 // will be at odd positions since number positions start from 0 
	                 if (i % 2 != 0) { 
	                     digit = 2 * digit; 
	                 } 
	                 // get sum of digits and add it to sum 
	                 sum += getSumOfDigits(digit); 
	                 // remove the last digit of the number 
	                 imei = imei / 10; 
	             } 
	             // check if sum is divisible by 10 
	             if (sum % 10 == 0) { 
	                 System.out.println("IMEI is valid"); 
	                 return "1";
	             } else { 
	                 System.out.println("IMEI not valid"); 
	                 return "0";
	             }
	         }catch(NumberFormatException ex) { 
	        	 return "-1";
	         }
		     finally { 
	             // close input reader 
//	             if (inputReader != null) { 
//	                inputReader.close(); 
//	             } 
	         }
		
   }

   static int getSumOfDigits(int number) { 
      // hold sum of digits 
      int sumOfDigits = 0; 
      while (number > 0) {
        // get the last digit
	int digit = number % 10;
	// add it to sum
	sumOfDigits += digit;
	// remove the last digit
	number = number / 10;
      }
      return sumOfDigits;
   }
}