package com.sudhir.dao;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sudhir.dto.Lead;
import com.sudhir.dto.My_Lead;
import com.sudhir.model.Agents;
import com.sudhir.model.AppUser;
import com.sudhir.model.AppVersionInfo;
import com.sudhir.model.Campain;
import com.sudhir.model.DisplaySetting;
import com.sudhir.model.EmailLogs;
import com.sudhir.model.My_Leads;
import com.sudhir.model.Priority;
import com.sudhir.model.ReferSetting;
import com.sudhir.model.Sentsmslog;
import com.sudhir.model.Smssettings;
import com.sudhir.model.Smstemplate;
import com.sudhir.model.Stage;
import com.sudhir.model.User;



@SuppressWarnings("unchecked")
@Repository("AdminDao")
public class AdminDaoImpl implements AdminDao {
	@Autowired
	SessionFactory sessionFactory;
	final SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	public Object saveObject(Object obj) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(obj);
			return obj;
		} catch (Exception e) {
			return obj;
		}
	}

	@Override
	public void executeQuery(String query) {
		sessionFactory.getCurrentSession().createQuery(query).executeUpdate();
	}

	@Override
	public String ConvertDataToWebJSON(String queryCount, String query, int page, int listSize) {
		Query q = sessionFactory.getCurrentSession().createSQLQuery(queryCount);
		BigInteger count = (BigInteger) q.uniqueResult();

		List lst = sessionFactory.getCurrentSession().createSQLQuery(query).setMaxResults(listSize)
				.setFirstResult((page - 1) * listSize).list();

		com.sudhir.model.DatatableJsonObject pjo = new com.sudhir.model.DatatableJsonObject();
		pjo.setRecordsFiltered(count.intValue());
		pjo.setRecordsTotal(count.intValue());
		pjo.setData(lst);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(pjo).toString();
	}

	@Override
	public DisplaySetting getDisplaySettingByURL(String url) {
		return (DisplaySetting) sessionFactory.getCurrentSession().createCriteria(DisplaySetting.class)
				.add(Restrictions.eq("url", url)).uniqueResult();
	}

	@Override
	public void saveReferSetting(ReferSetting rs) {
		sessionFactory.getCurrentSession().save(rs);
	}

	@Override
	public void deleteRefSetting(long srnum) {
		ReferSetting rf = new ReferSetting();
		rf=(ReferSetting) sessionFactory.getCurrentSession().get(ReferSetting.class,srnum);
		sessionFactory.getCurrentSession().delete(rf);
	}

	@Override
	public List<Object[]> getRerferalSetting() {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM refersetting WHERE isEnable=TRUE ").list();
	}

	@Override
	public void updateRefSetting(ReferSetting rs) {
		sessionFactory.getCurrentSession().update(rs);
	}

	@Override
	public List<Object[]> getAppUsers() {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM appuser").list();
	/*	return sessionFactory.getCurrentSession().createSQLQuery(" SELECT au.uid,au.username,au.email,au.dateofbirth,au.walletamount,au.winamount,"
			 											+ "au.reflCode,au.myReflCode,au.myReflCodeLimit,au.enabled,au.isWin FROM appuser au ").list(); */
	}

	@Override
	public int updateUserStatus(long uid, boolean stat) {
		String hql2 = " UPDATE appuser SET enabled="+stat+" WHERE uid="+uid+" ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public void deleteAppUserByAdmin(long uid) {
		AppUser rf = new AppUser();
		rf=(AppUser) sessionFactory.getCurrentSession().get(AppUser.class,uid);
		sessionFactory.getCurrentSession().delete(rf);
		
		int result = 0;
		String hql2 = "DELETE FROM spinwallet WHERE userId="+uid+" ";
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		String hql22 = " DELETE FROM winwallet WHERE userId="+uid+" ";
		int resultNew = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql22);
			resultNew = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		String hql23 = " DELETE FROM withdrawreq WHERE userId="+uid+" ";
		int resultNew23 = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql23);
			resultNew23 = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Override
	public int changeWinnerFlag(long uid, boolean winFlag) {
		String hql2 = " UPDATE appuser SET isWin="+winFlag+" WHERE uid="+uid+" ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	@Override
	public int approveWithdrawReq(long uid, boolean status,double newWalletAmt,long rqid) {
		Date date = Calendar.getInstance().getTime();  
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
        String strDate = dateFormat.format(date);  
        
		String hql2 = "UPDATE withdrawreq SET rqStatus="+status+",apprvDate='"+strDate+"' WHERE userId="+uid+" AND rqid="+rqid+" ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
	/*	String hql1 = "UPDATE withdrawreq SET apprvDate='"+strDate+"' WHERE rqid="+rqid+"";
		int result1 = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql1);
			result1 = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		if(status==true) {
			String hql = " UPDATE appuser SET winamount="+newWalletAmt+" WHERE uid="+uid+" ";
			int resultnew = 0;
			try {
				Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
				resultnew = query.executeUpdate();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} 
		
		if(newWalletAmt < 10000) {
			boolean winFlag =true;
			String hql45 = " UPDATE appuser SET isWin="+winFlag+" WHERE uid="+uid+" ";
			int result45 = 0;
			try {
				Query query = sessionFactory.getCurrentSession().createSQLQuery(hql45);
				result45 = query.executeUpdate();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} */
		
		
		return result;
	}

	@Override
	public List<Object[]> getUsersByReferalUsed(String refCode) {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT sw.swid,au.username,au.email,au.winamount,au.walletamount,sw.trDate,"
			+ "sw.usedRefCode FROM spinwallet sw JOIN appuser au ON sw.userId=au.uid WHERE sw.usedRefCode='"+refCode+"' AND sw.trType='NU Referal'").list();
	}

	@Override
	public int enableRefSett(long setId, boolean status) {
		
		if(status == true) {
			String hql = " UPDATE refersetting SET isEnable=FALSE ";
			int result0 = 0;
			try {
				Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
				result0 = query.executeUpdate();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}else {
		}
		
		String hql2 = " UPDATE refersetting SET isEnable="+status+" WHERE settid="+setId+" ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public int updateClientReflLimit(String reflCode) {
		String hql2 = " UPDATE appuser SET myReflCodeLimit=myReflCodeLimit-1 WHERE myReflCode='"+reflCode+"' ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Object[]> getWidAmountByUser(long uid, long rqid) {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM withdrawreq WHERE rqid="+rqid+" AND userId="+uid+" ").list();
	}

	@Override
	public void saveEmailLogs(EmailLogs logs) {
		sessionFactory.getCurrentSession().save(logs);
	}

	@Override
	public int updatePasswordByEmail(String emailId, String Password) {
		String hql2 = " UPDATE appuser SET password='"+Password+"' WHERE email='"+emailId+"' ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public void saveAppDetails(AppVersionInfo ad) {
		sessionFactory.getCurrentSession().save(ad);
	}

	@Override
	public int changeWinnerFlagByApp(long uid, boolean winFlag) {
		String hql2 = " UPDATE appuser SET isWinExtra="+winFlag+" WHERE uid="+uid+" ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Object[]> getMerchantKey() {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM users").list();
	}

	@Override
	public int getReferalCodeCount(String reflCode) {
		List refCodeCount=null;
		refCodeCount = sessionFactory.getCurrentSession().createSQLQuery("SELECT COUNT(*) FROM appuser WHERE reflCode='"+reflCode+"' ").list();
		return Integer.parseInt(refCodeCount.get(0).toString());
	}

	@Override
	public int adminSetWinAll() {
		String hql2 = " UPDATE appuser SET isWin=TRUE ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public int adminSetLoseAll() {
		String hql2 = " UPDATE appuser SET isWin=FALSE ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
}
