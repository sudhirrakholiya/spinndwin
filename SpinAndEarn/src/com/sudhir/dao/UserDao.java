package com.sudhir.dao;

import java.util.List;

import com.sudhir.model.AppUser;
import com.sudhir.model.Contact;
import com.sudhir.model.LoginLog;
import com.sudhir.model.SpinWallet;
import com.sudhir.model.User;
import com.sudhir.model.UserMoneyTrans;
import com.sudhir.model.UserRole;
import com.sudhir.model.WinWallet;
import com.sudhir.model.WithdrawReq;

public interface UserDao {

	void saveLoginLog(LoginLog lg);

	public void addUserRole(UserRole role);


	User findByUserName(String username);

	Long saveRegUser(User user);
	
// =============================================================
	
	void saveAppUsers(AppUser au);
	
	public List<Object[]> getUserDetails(String email);
	
	public List<Object[]> getUserDetailsById(long userId);
	
	public List<Object[]> getUserDetailsByRefCode(String refCode);
	
	public int updateWalletaBalance(long uid, double winAmt);
	
	public int updateClientRefl(String reflCode);
	
	public void saveSpinWallet(SpinWallet sw);
	
	public void saveWinWallet(WinWallet ww);
	
	public int updateWalletaBalAfterSpin(long uid,double winAmt);
	
	public double getWinAmountByUser(long uid);
	
	public int updateWalletaBalByUser(long uid, double walAmt);
	
	public int updateWinWalletaBalByUser(long uid, double amt);
	
	public int getSpinCountByUser(long uid);
	
	public void saveWithdrawReq(WithdrawReq wr);
	
	public void saveContactReq(Contact ct);
	
    public List<Object[]> getUserSpinTransById(long userId);
	
	public List<Object[]> getUserRefTransById(long userId);
	
	public List<Object[]> getUserAdminTransById(long userId);
	
	public List<Object[]> getUserSignUpTransById(long userId);
	
	public List<Object[]> getUserWinTransById(long userId);
	
	public List<Object[]> getUserPaytmTransById(long userId);
	
	public void saveUserMoneyLogs(UserMoneyTrans umt);
	
	public int updateResetTocketByUser(String tocken, String email);
	
	public List<Object[]> getAppVersionInfo();
	
	int updateUserCutting(long uid,int cutt);
	
	int userGSNumberCount(double amt);
}
