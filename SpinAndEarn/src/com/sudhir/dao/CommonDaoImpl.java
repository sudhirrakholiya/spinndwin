package com.sudhir.dao;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository("CommonDao")
public class CommonDaoImpl implements CommonDao {
	@Autowired
	SessionFactory sessionFactory;

	final SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");



	@Override
	public List createSqlQuery(String query) {
		Query  query2=	 sessionFactory.getCurrentSession().createSQLQuery(query);
		return query2.list();
	}

	@Override
	public List createQuery(String query) {
	Query  query2=	 sessionFactory.getCurrentSession().createQuery(query);	
	return query2.list();
	}

	@Override
	public int createupdateQuery(String query) {
		Query  query2=	 sessionFactory.getCurrentSession().createQuery(query);	
		return query2.executeUpdate();
	}

	@Override
	public Object saveObject(Object obj) {
		 try{
			 sessionFactory.getCurrentSession().save(obj);
			 return obj;
		 }catch(Exception e) {
			return obj;
		}
	}
	
	
	@Override
	public int createupdateSqlQuery(String query) {
		Query query2=	 sessionFactory.getCurrentSession().createSQLQuery(query);	
		return query2.executeUpdate();
	}

	@Override
	public int updateObject(Object obj) {
		 try{
			 sessionFactory.getCurrentSession().update(obj);
			 return 1;
		 }catch(Exception e) {
			return 0;
		}
	}

	
	
	@Override
	public BigInteger getcount(String query) {
		Query query2 = sessionFactory.getCurrentSession()
				.createSQLQuery(query);
		return (BigInteger) query2.uniqueResult();
	}

	@Override
	public List createSqlQuery(String query, int first, int max) {
		return sessionFactory.getCurrentSession().createSQLQuery(query).setMaxResults(max).setFirstResult(first).list();		
	}

	@Override
	public int updateResponce(int folId, String reponce) {
		// TODO Auto-generated method stub
		String hql = "UPDATE AndroidLog set responce = :res "  + 
	             "WHERE folloId = :folloId";
	Query query = sessionFactory.getCurrentSession().createQuery(hql);
	query.setParameter("folloId", folId);
	query.setParameter("res", reponce);
	int result = query.executeUpdate();
	System.out.println("Rows affected: " + result);
		return result;
	}

	@Override
	public void saveORUpdate(Object obj) {
		System.out.println("In saveORUpdate");
		sessionFactory.getCurrentSession().saveOrUpdate(obj);
	}

	@Override
	public Object getSingleObject(String query) {
		Query query2 = sessionFactory.getCurrentSession().createQuery(query);
		return query2.uniqueResult();
	}

	@Override
	public int getUserEnableCount(long uid) {
		List myVidCount=null;
		myVidCount = sessionFactory.getCurrentSession().createSQLQuery("SELECT COUNT(*) FROM appuser WHERE uid="+uid+" AND enabled=TRUE").list();
		return Integer.parseInt(myVidCount.get(0).toString());
	}

	@Override
	public int getUserEnableCountByEmail(String email) {
		List myVidCount=null;
		myVidCount = sessionFactory.getCurrentSession().createSQLQuery("SELECT COUNT(*) FROM appuser WHERE email='"+email+"' AND enabled=TRUE").list();
		return Integer.parseInt(myVidCount.get(0).toString());
	}
}
