package com.sudhir.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sudhir.model.AppUser;
import com.sudhir.model.Contact;
import com.sudhir.model.LoginLog;
import com.sudhir.model.SpinWallet;
import com.sudhir.model.User;
import com.sudhir.model.UserMoneyTrans;
import com.sudhir.model.UserRole;
import com.sudhir.model.WinWallet;
import com.sudhir.model.WithdrawReq;

@Repository("UserDao")
public class UserDaoImpl implements UserDao {

	@Autowired
	SessionFactory sessionFactory;

	public void saveLoginLog(LoginLog lg) {

		sessionFactory.getCurrentSession().save(lg);

	}

	@Override
	public void addUserRole(UserRole role) {
		sessionFactory.getCurrentSession().save(role);

	}


	@SuppressWarnings("unchecked")
	public User findByUserName(String username) {

		List<User> users = new ArrayList<User>();

		users = sessionFactory.getCurrentSession().createQuery("from User where username=?").setParameter(0, username)
				.list();

		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}

	}

	@Override
	public Long saveRegUser(User user) {

		return (Long) sessionFactory.getCurrentSession().save(user);
	}

	@Override
	public void saveAppUsers(AppUser au) {
		sessionFactory.getCurrentSession().save(au);
	}

	@Override
	public List<Object[]> getUserDetails(String email) {
		return sessionFactory.getCurrentSession().createSQLQuery(" SELECT * FROM appuser WHERE email='"+email+"' ").list();
	}

	@Override
	public List<Object[]> getUserDetailsById(long userId) {
		return sessionFactory.getCurrentSession().createSQLQuery(" SELECT * FROM appuser WHERE uid="+userId+" ").list();
	}

	@Override
	public int updateWalletaBalance(long uid, double winAmt) {
		String hql2 = " UPDATE appuser SET walletamount=walletamount+"+winAmt+" WHERE uid="+uid+" ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Object[]> getUserDetailsByRefCode(String refCode) {
		return sessionFactory.getCurrentSession().createSQLQuery(" SELECT * FROM appuser WHERE myReflCode='"+refCode+"' ").list();
	}

	@Override
	public int updateClientRefl(String reflCode) {
		String hql2 = " UPDATE appuser SET myReflCodeLimit=myReflCodeLimit+1 WHERE myReflCode='"+reflCode+"' ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public void saveSpinWallet(SpinWallet sw) {
		sessionFactory.getCurrentSession().save(sw);
	}

	@Override
	public void saveWinWallet(WinWallet ww) {
		sessionFactory.getCurrentSession().save(ww);
	}

	@Override
	public int updateWalletaBalAfterSpin(long uid, double winAmt) {
		String hql2 = " UPDATE appuser SET winamount="+winAmt+" WHERE uid="+uid+" ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public double getWinAmountByUser(long uid) {
		List myVidCount=null;
		myVidCount = sessionFactory.getCurrentSession().createSQLQuery("SELECT COALESCE(SUM(winAmt),0) AS total FROM winwallet WHERE userId="+uid+" AND trType='Cr' ").list();
		return Double.parseDouble(myVidCount.get(0).toString());
	}

	@Override
	public int updateWalletaBalByUser(long uid, double walAmt) {
		String hql2 = " UPDATE appuser SET walletamount="+walAmt+" WHERE uid="+uid+" ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public int getSpinCountByUser(long uid) {
		List myVidCount=null;
		myVidCount = sessionFactory.getCurrentSession().createSQLQuery("SELECT COUNT(*) FROM spinwallet WHERE userId="+uid+" AND trType='Spin'").list();
		return Integer.parseInt(myVidCount.get(0).toString());
	}

	@Override
	public void saveWithdrawReq(WithdrawReq wr) {
		sessionFactory.getCurrentSession().save(wr);
	}

	@Override
	public void saveContactReq(Contact ct) {
		sessionFactory.getCurrentSession().save(ct);
	}

	@Override
	public List<Object[]> getUserSpinTransById(long userId) {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM spinwallet WHERE userId="+userId+" AND trType='spin' ORDER BY trDate DESC").list();
	}

	@Override
	public List<Object[]> getUserRefTransById(long userId) {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM spinwallet WHERE userId="+userId+" AND trType='EU Referal' ORDER BY trDate DESC").list();
	}

	@Override
	public List<Object[]> getUserAdminTransById(long userId) {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM spinwallet WHERE userId="+userId+" AND trType='By Admin' ORDER BY trDate DESC").list();
	}

	@Override
	public List<Object[]> getUserSignUpTransById(long userId) {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM spinwallet WHERE userId="+userId+" AND trType='SignUp Bonus' ORDER BY trDate DESC").list();
	}

	@Override
	public List<Object[]> getUserWinTransById(long userId) {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM winwallet WHERE userId="+userId+" ORDER BY trDate DESC").list();
	}
	
	@Override
	public List<Object[]> getUserPaytmTransById(long userId) {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM usermoneytrans WHERE userId="+userId+" ORDER BY addedOn DESC").list();
	}

	@Override
	public void saveUserMoneyLogs(UserMoneyTrans umt) {
		sessionFactory.getCurrentSession().save(umt);
	}

	@Override
	public int updateResetTocketByUser(String tocken, String email) {
		String hql2 = " UPDATE appuser SET resetTocken='"+tocken+"' WHERE email='"+email+"' ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Object[]> getAppVersionInfo() {
		return sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM appversioninfo ORDER BY srno DESC LIMIT 1").list();
	}

	@Override
	public int updateUserCutting(long uid,int cutt) {
		String hql2 = " UPDATE users SET cutting="+cutt+" WHERE uid="+uid+" ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public int userGSNumberCount(double amt) {
		List myVidCount=null;
		myVidCount = sessionFactory.getCurrentSession().createSQLQuery("SELECT COUNT(*) FROM spinwallet WHERE userGSAmt="+amt+" ").list();
		return Integer.parseInt(myVidCount.get(0).toString());
	}

	@Override
	public int updateWinWalletaBalByUser(long uid, double amt) {
		String hql2 = " UPDATE appuser SET winamount="+amt+" WHERE uid="+uid+" ";
		int result = 0;
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hql2);
			result = query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
