package com.sudhir.dao;

import java.math.BigInteger;
import java.util.List;



public interface CommonDao {

	List createSqlQuery(String query);

	List createQuery(String query);

	int createupdateQuery(String query);

	Object saveObject(Object obj);

	int createupdateSqlQuery(String query);

	int updateObject(Object obj);
	
	public BigInteger getcount(String query);
	
	List createSqlQuery(String query,int first,int max);
	
	public int updateResponce(int folId,String reponce);
	
	public void saveORUpdate(Object obj);
	
	Object getSingleObject(String query);
	
	int getUserEnableCount(long uid);
	
	int getUserEnableCountByEmail(String email);
	
}
