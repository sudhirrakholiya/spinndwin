package com.sudhir.service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.itextpdf.text.Phrase;

public class SmsExcelBuilder extends AbstractExcelView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
/*
		// try {
		// get data model which is passed by the Spring container
		List listSmsHistoryList = (List) model.get("smsHistoryList");

		String title = new String("SMS_History");
		// create a new Excel sheet
		HSSFSheet sheet = workbook.createSheet(title);
		sheet.setDefaultColumnWidth(20);

		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.BLUE.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);

		// create header row
		HSSFRow header = sheet.createRow(0);

		header.createCell(0).setCellValue("SrNo");
		header.getCell(0).setCellStyle(style);

		header.createCell(1).setCellValue("Mobile no");
		header.getCell(1).setCellStyle(style);

		header.createCell(2).setCellValue("Message");
		header.getCell(2).setCellStyle(style);

		header.createCell(3).setCellValue("Sent On");
		header.getCell(3).setCellStyle(style);

		header.createCell(4).setCellValue("Status");
		header.getCell(4).setCellStyle(style);

		header.createCell(5).setCellValue("Service");
		header.getCell(5).setCellStyle(style);

		header.createCell(6).setCellValue("SenderName");
		header.getCell(6).setCellStyle(style);

		// create data rows
		int rowCount = 1;
		for (int i = 0; i < listSmsHistoryList.size(); i++) {

			Object[] smsHistory = (Object[]) listSmsHistoryList.get(i);
			HSSFRow aRow = sheet.createRow(rowCount++);
			aRow.createCell(0).setCellValue(i + "");
			aRow.createCell(1).setCellValue(smsHistory[3].toString());
			aRow.createCell(2).setCellValue(smsHistory[2].toString());
			aRow.createCell(3).setCellValue(smsHistory[5].toString());
			aRow.createCell(4).setCellValue(smsHistory[7].toString());
			aRow.createCell(5).setCellValue(smsHistory[8].toString());
			aRow.createCell(6).setCellValue(smsHistory[9].toString());
		}*/
		
		
		


		try {
	
		//    FileOutputStream fileOut = new FileOutputStream("D:\\workbook.xls");
		//    Workbook workbook = new HSSFWorkbook();
		    Sheet sheet = workbook.createSheet("new sheet");
			
			  Row row = sheet.createRow((short)0);
			  row.createCell(0).setCellValue("Id");
		      row.createCell(1).setCellValue("Category");
		      row.createCell(2).setCellValue("Company");
              row.createCell(3).setCellValue("Created Date");
		      row.createCell(4).setCellValue("Email");
		      row.createCell(5).setCellValue("First Name");
		      row.createCell(6).setCellValue("Last Name");
		      row.createCell(7).setCellValue("Process State");
		      row.createCell(8).setCellValue("Lead State");
		      row.createCell(9).setCellValue("Mobile No");
		    /*  row.createCell(10).setCellValue("Telecaller");*/
		     /* row.createCell(11).setCellValue("Other Data");*/
		      row.createCell(10).setCellValue("CompanyName");
		      row.createCell(11).setCellValue("Website");
		      row.createCell(12).setCellValue("Country");
		      row.createCell(13).setCellValue("State");
		      row.createCell(14).setCellValue("City");
		      
		  
		      List listq = (List) model.get("excelList");
		      
		      int i=0;
				if (listq.size() != 0) {
					 Object[] result = null;
					for (i = 0; i < listq.size(); i++) {
						result = (Object[]) listq.get(i);
					 Row rowadd = sheet.createRow((short)i+1);

					 rowadd.createCell(0).setCellValue(result[0].toString()== null ? "N/A" : result[0].toString());
			            
						rowadd.createCell(1).setCellValue(result[3].toString()== null ? "N/A" : result[3].toString());

			            rowadd.createCell(2).setCellValue(result[10].toString()== null ? "N/A" : result[10].toString());

			            rowadd.createCell(3).setCellValue(result[8].toString()== null ? "N/A" : result[8].toString());

			            rowadd.createCell(4).setCellValue(result[4].toString()== null ? "N/A" :  result[4].toString());

			            rowadd.createCell(5).setCellValue(result[1].toString()== null ? "N/A" : result[1].toString());

			            rowadd.createCell(6).setCellValue(result[2].toString()== null ? "N/A" : result[2].toString());

			            rowadd.createCell(7).setCellValue(result[7].toString()== null ? "N/A" : result[7].toString());

			            rowadd.createCell(8).setCellValue(result[6].toString()== null ? "N/A" : result[6].toString());

			            rowadd.createCell(9).setCellValue(result[5].toString()== null ? "N/A" : result[5].toString());

		                rowadd.createCell(10).setCellValue(result[17].toString()== null ? "N/A" : result[17].toString());

			          //  rowadd.createCell(11).setCellValue(result[16].toString()== null ? "N/A" : result[16].toString());

			            rowadd.createCell(10).setCellValue(result[14].toString()== null ? "N/A" : result[14].toString());

			           // rowadd.createCell(13).setCellValue(result[15].toString()== null ? "N/A" : result[15].toString());

			            rowadd.createCell(11).setCellValue(result[13].toString()== null ? "N/A" : result[13].toString());

			            rowadd.createCell(12).setCellValue(result[12].toString()== null ? "N/A" : result[12].toString());

			            rowadd.createCell(13).setCellValue(result[11].toString()== null ? "N/A" : result[11].toString());


					}
				}
			/*for(i=1;i<=10;i++)
			{
			}*/
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		
		
		
		
		

	}
}