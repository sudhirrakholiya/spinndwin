package com.sudhir.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sudhir.model.AppVersionInfo;
import com.sudhir.model.DisplaySetting;
import com.sudhir.model.EmailLogs;
import com.sudhir.model.ReferSetting;

@Service
public interface AdminService {

	Object saveObject(Object obj);

	void executeQuery(String query);

	String ConvertDataToWebJSON(String queryCount, String query, int page, int listSize);

	DisplaySetting getDisplaySettingByURL(String url);
	
	public void saveReferSetting(ReferSetting rs);
	
	public void deleteRefSetting(long srnum); 
	
	public List<Object[]> getRerferalSetting();
	
	public void updateRefSetting(ReferSetting rs);
	
	public List<Object[]> getAppUsers();
	
	public int updateUserStatus(long uid , boolean stat);
	
	public void deleteAppUserByAdmin(long uid);
	
	public int changeWinnerFlag(long uid, boolean winFlag);
	
	public int approveWithdrawReq(long uid, boolean status,double newWalletAmt,long rqid);
	
	public List<Object[]> getUsersByReferalUsed(String refCode);
	
	public int enableRefSett(long setId, boolean status);
	
	int updateClientReflLimit(String reflCode);
	
	public List<Object[]> getWidAmountByUser(long uid,long rqid);
	
	public void saveEmailLogs(EmailLogs logs);
	
	public int updatePasswordByEmail(String emailId,String Password);
	
	public void saveAppDetails(AppVersionInfo ad);
	
	public int changeWinnerFlagByApp(long uid, boolean winFlag);
	
	public List<Object[]> getMerchantKey();
	
	public int getReferalCodeCount(String reflCode);
	
	public int adminSetWinAll();
	
	public int adminSetLoseAll();
	
}
