package com.sudhir.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sudhir.dao.UserDao;
import com.sudhir.model.MediUser;
import com.sudhir.model.UserRole;


@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserDao userDao;

	@Transactional(readOnly = true)
	@Override
	public UserDetails loadUserByUsername(final String username) {

		com.sudhir.model.User user = userDao.findByUserName(username);
		//AegeCustDetail agentmaster = userDao.findAgentByemailId(username);
		// System.out.println("username:
		// ----------------->"+user.getName()+"*********"+user.getPassword());
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		if (user == null) {

			/*if (agentmaster == null)
				throw new UsernameNotFoundException(username);

			setAuths.add(new SimpleGrantedAuthority("AGENT"));
			return new MediUser(agentmaster.getNAME(), agentmaster.getPASSWORD(), true, agentmaster.getIsActive(), true,
					true, setAuths, 1, agentmaster.getId(), agentmaster.getCompanyId(), 0L, 0, 0);*/

		}

		List<GrantedAuthority> authorities = buildUserAuthority(user.getUserRole());
		return buildUserForAuthentication(user, authorities);

	}

	// Converts com.mkyong.users.model.User user to
	// org.springframework.security.core.userdetails.User
	private User buildUserForAuthentication(com.sudhir.model.User user, List<GrantedAuthority> authorities) {
		return new MediUser(user.getUsername(), user.getPassword(), user.isEnabled(), user.isActive(), true, true,
				authorities, user.getPriority(), user.getUid(), user.getParentId(), user.getDefaultSenderId(),
				user.getMinNumber(), user.getCutting());
	}

	private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities
		for (UserRole userRole : userRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
		}

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

		return Result;
	}

}