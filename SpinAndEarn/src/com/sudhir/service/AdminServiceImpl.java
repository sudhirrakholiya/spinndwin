package com.sudhir.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sudhir.dao.AdminDao;
import com.sudhir.dto.Lead;
import com.sudhir.dto.My_Lead;
import com.sudhir.model.Agents;
import com.sudhir.model.AppVersionInfo;
import com.sudhir.model.Campain;
import com.sudhir.model.DisplaySetting;
import com.sudhir.model.EmailLogs;
import com.sudhir.model.My_Leads;
import com.sudhir.model.Priority;
import com.sudhir.model.ReferSetting;
import com.sudhir.model.Sentsmslog;
import com.sudhir.model.Smssettings;
import com.sudhir.model.Smstemplate;
import com.sudhir.model.Stage;
import com.sudhir.model.User;


@Service("AdminService")
@Transactional
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminDao adminDao;


	@Override
	public Object saveObject(Object obj) {
		return adminDao.saveObject(obj);
	}

	@Override
	public void executeQuery(String query) {
		adminDao.executeQuery(query);
	}

	@Override
	public String ConvertDataToWebJSON(String queryCount, String query, int page, int listSize) {
		return adminDao.ConvertDataToWebJSON(queryCount, query, page, listSize);
	}

	@Override
	public DisplaySetting getDisplaySettingByURL(String url) {
		return adminDao.getDisplaySettingByURL(url);
	}

	@Override
	public void saveReferSetting(ReferSetting rs) {
		adminDao.saveReferSetting(rs);
	}

	@Override
	public void deleteRefSetting(long srnum) {
		adminDao.deleteRefSetting(srnum);
	}

	@Override
	public List<Object[]> getRerferalSetting() {
		return adminDao.getRerferalSetting();
	}

	@Override
	public void updateRefSetting(ReferSetting rs) {
		adminDao.updateRefSetting(rs);
	}

	@Override
	public List<Object[]> getAppUsers() {
		return adminDao.getAppUsers();
	}

	@Override
	public int updateUserStatus(long uid, boolean stat) {
		return adminDao.updateUserStatus(uid, stat);
	}

	@Override
	public void deleteAppUserByAdmin(long uid) {
		adminDao.deleteAppUserByAdmin(uid);
	}

	@Override
	public int changeWinnerFlag(long uid, boolean winFlag) {
		return adminDao.changeWinnerFlag(uid, winFlag);
	}
	
	@Override
	public int approveWithdrawReq(long uid, boolean status,double newWalletAmt,long rqid) {
		return adminDao.approveWithdrawReq(uid, status, newWalletAmt,rqid);
	}

	@Override
	public List<Object[]> getUsersByReferalUsed(String refCode) {
		return adminDao.getUsersByReferalUsed(refCode);
	}

	@Override
	public int enableRefSett(long setId, boolean status) {
		return adminDao.enableRefSett(setId, status);
	}

	@Override
	public int updateClientReflLimit(String reflCode) {
		return adminDao.updateClientReflLimit(reflCode);
	}


	@Override
	public List<Object[]> getWidAmountByUser(long uid, long rqid) {
		return adminDao.getWidAmountByUser(uid, rqid);
	}

	@Override
	public void saveEmailLogs(EmailLogs logs) {
		adminDao.saveEmailLogs(logs);
	}

	@Override
	public int updatePasswordByEmail(String emailId, String Password) {
		return adminDao.updatePasswordByEmail(emailId, Password);
	}

	@Override
	public void saveAppDetails(AppVersionInfo ad) {
		adminDao.saveAppDetails(ad);
	}

	@Override
	public int changeWinnerFlagByApp(long uid, boolean winFlag) {
		return adminDao.changeWinnerFlagByApp(uid, winFlag);
	}

	@Override
	public List<Object[]> getMerchantKey() {
		return adminDao.getMerchantKey();
	}

	@Override
	public int getReferalCodeCount(String reflCode) {
		return adminDao.getReferalCodeCount(reflCode);
	}

	@Override
	public int adminSetWinAll() {
		return adminDao.adminSetWinAll();
	}

	@Override
	public int adminSetLoseAll() {
		return adminDao.adminSetLoseAll();
	}

}
