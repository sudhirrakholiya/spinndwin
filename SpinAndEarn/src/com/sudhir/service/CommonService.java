package com.sudhir.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public interface CommonService {

	public List createSqlQuery(String query);

	public List createQuery(String query);

	public int createupdateQuery(String query);

	Object saveObject(Object obj);

	int createupdateSqlQuery(String query);

	public int updateObject(Object obj);

	public BigInteger getcount(String query);

	public List createSqlQuery(String query, int first, int max);

	public int updateResponce(int folId, String reponce);

	public void saveORUpdate(Object obj);

	Object getSingleObject(String query);
	
	int getUserEnableCount(long uid);
	int getUserEnableCountByEmail(String email);
}
