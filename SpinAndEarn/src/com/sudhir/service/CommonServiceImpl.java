package com.sudhir.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sudhir.dao.AdminDao;
import com.sudhir.dao.CommonDao;


@Service("CommonService")
@Transactional
public class CommonServiceImpl implements CommonService {

	@Autowired
	private CommonDao dao;
	



	@Override
	public List createSqlQuery(String query) {
		return dao.createSqlQuery(query);
	}

	@Override
	public List createQuery(String query) {
		return dao.createQuery(query);
	}

	@Override
	public int createupdateQuery(String query) {
		return dao.createupdateQuery(query);
	}

	@Override
	public Object saveObject(Object obj) {
		return dao.saveObject(obj);
	}

	@Override
	public int createupdateSqlQuery(String query) {
		return dao.createupdateSqlQuery(query);
	}

	@Override
	public int updateObject(Object obj) {
		return dao.updateObject(obj);
	}

	@Override
	public BigInteger getcount(String query) {
		return dao.getcount(query);
	}

	@Override
	public List createSqlQuery(String query, int first, int max) {
	    return dao.createSqlQuery(query, first, max);
	}

	@Override
	public int updateResponce(int folId, String reponce) {
		// TODO Auto-generated method stub
		return dao.updateResponce(folId, reponce);
	}

	@Override
	public void saveORUpdate(Object obj) {
		dao.saveORUpdate(obj);
	}

	@Override
	public Object getSingleObject(String query) {
		return dao.getSingleObject(query);
	}

	@Override
	public int getUserEnableCount(long uid) {
		return dao.getUserEnableCount(uid);
	}

	@Override
	public int getUserEnableCountByEmail(String email) {
		return dao.getUserEnableCountByEmail(email);
	}
}
