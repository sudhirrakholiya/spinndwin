package com.sudhir.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sudhir.dao.UserDao;
import com.sudhir.model.AppUser;
import com.sudhir.model.Contact;
import com.sudhir.model.LoginLog;
import com.sudhir.model.SpinWallet;
import com.sudhir.model.User;
import com.sudhir.model.UserMoneyTrans;
import com.sudhir.model.UserRole;
import com.sudhir.model.WinWallet;
import com.sudhir.model.WithdrawReq;

@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	public Long saveRegUser(User user) {

		return userDao.saveRegUser(user);

	}

	public void saveLoginLog(LoginLog lg) {
		userDao.saveLoginLog(lg);

	}

	@Override
	public void addUserRole(UserRole role) {
		// TODO Auto-generated method stub
		userDao.addUserRole(role);

	}
	
	
//   ======================================================================================

	@Override
	public void saveAppUsers(AppUser au) {
		userDao.saveAppUsers(au);
	}

	@Override
	public List<Object[]> getUserDetails(String email) {
		return userDao.getUserDetails(email);
	}

	@Override
	public List<Object[]> getUserDetailsById(long userId) {
		return userDao.getUserDetailsById(userId);
	}

	@Override
	public int updateWalletaBalance(long uid, double winAmt) {
		return userDao.updateWalletaBalance(uid, winAmt);
	}

	@Override
	public List<Object[]> getUserDetailsByRefCode(String refCode) {
		return userDao.getUserDetailsByRefCode(refCode);
	}

	@Override
	public int updateClientRefl(String reflCode) {
		return userDao.updateClientRefl(reflCode);
	}

	@Override
	public void saveSpinWallet(SpinWallet sw) {
		userDao.saveSpinWallet(sw);
	}

	@Override
	public void saveWinWallet(WinWallet ww) {
		userDao.saveWinWallet(ww);
	}

	@Override
	public int updateWalletaBalAfterSpin(long uid,double winAmt) {
		return userDao.updateWalletaBalAfterSpin(uid, winAmt);
	}

	@Override
	public double getWinAmountByUser(long uid) {
		return userDao.getWinAmountByUser(uid);
	}

	@Override
	public int updateWalletaBalByUser(long uid, double walAmt) {
		return userDao.updateWalletaBalByUser(uid, walAmt);
	}

	@Override
	public int getSpinCountByUser(long uid) {
		return userDao.getSpinCountByUser(uid);
	}

	@Override
	public void saveWithdrawReq(WithdrawReq wr) {
		userDao.saveWithdrawReq(wr);
	}

	@Override
	public void saveContactReq(Contact ct) {
		userDao.saveContactReq(ct);
	}

	@Override
	public List<Object[]> getUserSpinTransById(long userId) {
		return userDao.getUserSpinTransById(userId);
	}

	@Override
	public List<Object[]> getUserRefTransById(long userId) {
		return userDao.getUserRefTransById(userId);
	}

	@Override
	public List<Object[]> getUserAdminTransById(long userId) {
		return userDao.getUserAdminTransById(userId);
	}

	@Override
	public List<Object[]> getUserSignUpTransById(long userId) {
		return userDao.getUserSignUpTransById(userId);
	}

	@Override
	public List<Object[]> getUserWinTransById(long userId) {
		return userDao.getUserWinTransById(userId);
	}

	@Override
	public void saveUserMoneyLogs(UserMoneyTrans umt) {
		userDao.saveUserMoneyLogs(umt);
	}

	@Override
	public int updateResetTocketByUser(String tocken, String email) {
		return userDao.updateResetTocketByUser(tocken, email);
	}

	@Override
	public List<Object[]> getUserPaytmTransById(long userId) {
		return userDao.getUserPaytmTransById(userId);
	}

	@Override
	public List<Object[]> getAppVersionInfo() {
		return userDao.getAppVersionInfo();
	}

	@Override
	public int updateUserCutting(long uid,int cutt) {
		return userDao.updateUserCutting(uid, cutt);
	}

	@Override
	public int userGSNumberCount(double amt) {
		return userDao.userGSNumberCount(amt);
	}

	@Override
	public int updateWinWalletaBalByUser(long uid, double amt) {
		return userDao.updateWinWalletaBalByUser(uid, amt);
	}

}
